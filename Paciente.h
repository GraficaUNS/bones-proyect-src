#pragma once

#define Pac
#include <string>
#include "vtkDICOMValue.h"
class Paciente {
private:

public:

	std::string StudyInstanceUID;
	std::string PatientName;
	std::string PatientAge;
	std::string PatientID;
	std::string PatientBirthDate;	std::string PatientBirthName;	std::string PatientBirthTime;	std::string PatientAddress;	std::string PatientSex;	std::string PatientComments;	std::string PatientSize;	std::string PatientWeight;	std::string StudyID;	std::string StudyDescription;	std::string StudyDate;	std::string StudyTime;	std::string AccessionNumber;	std::string ReferringPhysician;	std::string PerformingPhysicianName;	std::string Modality;	std::string AdditionalPatientHistory;	std::string Allergies;	std::string CurrentPatientLocation;	std::string ModalitiesInStudy;	std::string InstitutionName;	std::string PersonName;	std::string PersonTelephoneNumbers;

	Paciente();
	//No son est�ticos los m�todos ya que no se crean ni se destruyen cuando inicia y termina el programa, sino en medio
	//de la ejecuci�n

	void setPatient(vtkDICOMValue StudyUID, vtkDICOMValue name, vtkDICOMValue age, vtkDICOMValue id, vtkDICOMValue birthdate, vtkDICOMValue birthname,
		vtkDICOMValue birthtime, vtkDICOMValue address, vtkDICOMValue sexo, vtkDICOMValue comments,
		vtkDICOMValue size, vtkDICOMValue weight, vtkDICOMValue studyID, vtkDICOMValue description, vtkDICOMValue studydate, vtkDICOMValue studytime,
		vtkDICOMValue accessionnumber, vtkDICOMValue referringphysician, vtkDICOMValue performingphysicianname, vtkDICOMValue modality,
		vtkDICOMValue additionalpatienthistory, vtkDICOMValue allergies, vtkDICOMValue currentpatientlocation,
		vtkDICOMValue modalitiesinstudy, vtkDICOMValue institutionname, vtkDICOMValue personname,
		vtkDICOMValue persontelephonenumbers);

	void setStudyInstanceUID();
	void setPatientName();
	void setPatientAge();
	void setPatientID();
	void setPatientBirthDate();
	void setPatientBirthName();
	void setPatientBirthTime();
	void setPatientAddress();
	void setPatientSex();
	void setPatientComments();
	void setPatientSize();
	void setPatientWeight();
	void setStudyID();
	void setStudyDescription();
	void setStudyDate();
	void setStudyTime();
	void setAccessionNumber();
	void setReferringPhysician();
	void setPerformingPhysicianName();
	void setModality();
	void setAdditionalPatientHistory();
	void setAllergies();
	void setCurrentPatientLocation();
	void setModalitiesInStudy();
	void setInstitutionName();
	void setPersonName();
	void setPersonTelephoneNumbers();

	std::string getStudyInstanceUID();
	std::string getPatientName();
	std::string getPatientAge();
	std::string getPatientID();
	std::string getPatientBirthDate();
	std::string getPatientBirthName();
	std::string getPatientBirthTime();
	std::string getPatientAddress();
	std::string getPatientSex();
	std::string getPatientComments();
	std::string getPatientSize();
	std::string getPatientWeight();
	std::string getStudyID();
	std::string getStudyDescription();
	std::string getStudyDate();
	std::string getStudyTime();
	std::string getAccessionNumber();
	std::string getReferringPhysician();
	std::string getPerformingPhysicianName();
	std::string getModality();
	std::string getAdditionalPatientHistory();
	std::string getAllergies();
	std::string getCurrentPatientLocation();
	std::string getModalitiesInStudy();
	std::string getInstitutionName();
	std::string getPersonName();
	std::string getPersonTelephoneNumbers();

};