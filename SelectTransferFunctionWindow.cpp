#include <QVTKOpenGLWindow.h>
#include "SelectTransferFunctionWindow.h"
#include "SelectTransferFunctionDialog.h"
#include <vtkGenericOpenGLRenderWindow.h>
#include <vtkRendererCollection.h>
#include <vtkRenderer.h>
#include <qvector.h>

#include <vtkCamera.h>
#include <vtkSmartVolumeMapper.h>

#include <vtkWindowToImageFilter.h>
#include <vtkPNGWriter.h>

//Ventana que tiene el volumen, deber�a guardar una imagen aqu� en vez de un volumen
SelectTransferFunctionWindow::~SelectTransferFunctionWindow()
{
	this->renderer1->Delete();
}

void SelectTransferFunctionWindow::SetRenderWindow(vtkGenericOpenGLRenderWindow * renwin)
{
	vol = new myVolume();

	renwin->AddRenderer(renderer1);
	Superclass::SetRenderWindow(renwin);
	renderer1->ResetCamera();
	renderer1->GetActiveCamera()->SetPosition(CameraPos);
	renderer1->GetActiveCamera()->SetViewUp(CameraUp);

	//Superclass:GetRenderWindow();

}

//vtkGenericOpenGLRenderWindow * 

void SelectTransferFunctionWindow::SetParentDialog(SelectTransferFunctionDialog * d)
{
	this->parentDialog = d;
}

void SelectTransferFunctionWindow::SetIndex(int i)
{
	this->index = i;
}

void SelectTransferFunctionWindow::mousePressEvent(QMouseEvent * event)
{
	this->parentDialog->accept(index);
}

void SelectTransferFunctionWindow::mouseMoveEvent(QMouseEvent * event)
{
	this->parentDialog->select(index);

}

void SelectTransferFunctionWindow::mouseReleaseEvent(QMouseEvent * event)
{
}

void SelectTransferFunctionWindow::wheelEvent(QWheelEvent * e)
{
}

void SelectTransferFunctionWindow::setData(vtkImageData* data)
{
	vtkSmartVolumeMapper* mapper = (vtkSmartVolumeMapper*)this->vol->GetMapper();

	mapper->RemoveAllInputs();

	mapper->SetInputData(data);

	data->Modified();

	mapper->Modified();
	mapper->Update();

	vol->Modified();
	vol->Update();

	vol->centrar();

	//aqu� elimino el volumen y seteo imagen
}

void SelectTransferFunctionWindow::removeVolumeFromRender()
{
	if (GetRenderWindow()->GetRenderers()->GetFirstRenderer()->GetVolumes()->GetNumberOfItems() > 0) {
		GetRenderWindow()->GetRenderers()->GetFirstRenderer()->RemoveVolume(vol);
		GetRenderWindow()->Render();
	}
}

void SelectTransferFunctionWindow::addVolumeToRender()
{
	{
		//esta operaci�n agrega el volumen de esta ventana al renderer, y lo hace correr
		//no voy a usar mas esta operaci�n
		GetRenderWindow()->GetRenderers()->GetFirstRenderer()->AddVolume(vol);
		GetRenderWindow()->Render();

	}
}
double * SelectTransferFunctionWindow::obtenerViewport() {
	return GetRenderWindow()->GetRenderers()->GetFirstRenderer()->GetViewport();
}

void SelectTransferFunctionWindow::setTransferFunction(double* vector, int cant)
{

	for (int i = 0; i < cant; i++) {
		vol->addOpacityPoint(i, vector[i]);
	}

	//aqu� puedo  tomar una imagen del volumen


}

QVector<double>* SelectTransferFunctionWindow::getTransferFunction()
{
	return vol->GetOpacityFunction_Y();
}

SelectTransferFunctionWindow::SelectTransferFunctionWindow()
	: SelectTransferFunctionWindow(nullptr, QOpenGLContext::currentContext(), NoPartialUpdate, Q_NULLPTR)
{



}

//-----------------------------------------------------------------------------
SelectTransferFunctionWindow::SelectTransferFunctionWindow(QOpenGLContext *shareContext,
	UpdateBehavior updateBehavior, QWindow *parent)
	: QVTKOpenGLWindow(nullptr, shareContext, updateBehavior, parent)
{


}

//-----------------------------------------------------------------------------
SelectTransferFunctionWindow::SelectTransferFunctionWindow(vtkGenericOpenGLRenderWindow* w,
	QOpenGLContext *shareContext, UpdateBehavior updateBehavior, QWindow *parent)
	: QVTKOpenGLWindow(w, shareContext, updateBehavior, parent)

{
	this->renderer1 = vtkRenderer::New();

}

