#pragma once
#include <vtkActor.h>
#include <QKeyEvent>
#include <vtkRenderWindow.h>
#include "CameraHandler.h"
#include "textonota.h"
#include "MasterHandler.h"
#include <qmap.h>
#include <QString>

#include <vtkSphereWidget.h>
#include <vtkTextSource.h>

#define MIN_RADIUS 5
#define MAX_RADIUS 15


class NoteInteractions: public QObject
{

	Q_OBJECT

public:
	NoteInteractions();
	~NoteInteractions();

	vtkSmartPointer<vtkRenderWindow> GetRenderWindow();
	void SetRenderWindow(vtkSmartPointer<vtkRenderWindow> ren);
	void setSelectedNoteToNull();
	vtkSmartPointer<vtkActor> getSelectedNote();
	void checkPickedNote(CameraHandler* camera, QMouseEvent *event, double height, double width);
	void keyPress();

	void crearNota();
	void removeSelectedNote();

	void mouseMove(CameraHandler* camera, QMouseEvent *event);

	void rotate();

	void AddNote(double* pos, QString text);

	QMap<vtkSmartPointer<vtkSphereWidget>,QString>* GetNotes();

public slots:
	void recibirDatosPopUp(QStringList);


};

