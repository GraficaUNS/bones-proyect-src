
#pragma once

#include <vtkVolume.h>
#include <qvector.h>
#include "Paciente.h"
#include <vtkColorTransferFunction.h>
#include <vtkVolumeProperty.h>
#include <vtkDICOMImageReader.h>
#include <vtkImageData.h>
#include <vtkImageActor.h>
#include <vtkOpenGLGPUVolumeRayCastMapper.h>
#include <sliceVolume.h>
#include <QPixmap>

#define VALUERANGE 3000

#define NUMERODEPUNTOS_SCALAR 301
#define NUMERODEPUNTOS_GRADIENTE 301
#define INIT_TRANSFER_FUNCTION_OPACITY_VALUE 1000
#define INIT_TRANSFER_FUNCTION_GRADIENT_VALUE 1000
#define STEP_TRANSFER_FUNCTION (VALUERANGE / (NUMERODEPUNTOS_SCALAR - 1))


#define CANT_COLOR 250
#define STEP_COLOR (VALUERANGE / CANT_COLOR)

#define CANT_COMPONENTS_COLOR 3
#define INIT_VALUE_COLOR 1000
#define CANT_COLOR_INDEX_ACTIVE_DEFAULT 9

#define SELECT_CELL 5000
#define RADIO_DENSITY_ZERO -1500

#define Default_Ambient 0.1
#define Default_Diffuse 0.9
#define Default_Specular 0.5
#define Default_SpecularPower 100.0

#define NUMBEROFTRANSFERFUNCTIONS 8

class myVolume: public vtkVolume
{
public:
	
	typedef vtkVolume Superclass;
	
	myVolume();
	myVolume(vtkSmartPointer<vtkImageData> img);
	myVolume(std::string path);
	~myVolume();
	
	//vtkSmartPointer<vtkVolumeProperty * devolverPropiedad();
	void restoreValues();

	QVector<double>* GetOpacityFunction_X();
	QVector<double>* GetOpacityFunction_Y();
	QVector<double>* GetGradientFunction_X();
	QVector<double>* GetGradientFunction_Y(); 
	QVector<double>* GetColorFunction();

	void addOpacityPoint(int index, double value);
	void addGradientPoint(int index, double value);
	void addColorPoint(int index, int R, int G, int B);
	void removeColorPoint(int index);

	Paciente* myVolume::getPaciente();
	void myVolume::setPaciente(Paciente* p);


	void setPicked(bool v);

	vtkSmartPointer<vtkImageData> getImageData(); 
	vtkSmartPointer<vtkImageActor> getImageActorXY();
	vtkSmartPointer<vtkImageActor> getImageActorYZ();
	vtkSmartPointer<vtkImageActor> getImageActorZX();

	void setImageActorXY(vtkSmartPointer<vtkImageActor> act);
	void setImageActorYZ(vtkSmartPointer<vtkImageActor> act);
	void setImageActorZX(vtkSmartPointer<vtkImageActor> act);

	vtkSmartPointer<sliceVolume> getSliceVolumeXY();
	vtkSmartPointer<sliceVolume> getSliceVolumeYZ();
	vtkSmartPointer<sliceVolume> getSliceVolumeZX();

	void setSliceVolumeXY(vtkSmartPointer<sliceVolume> act);
	void setSliceVolumeYZ(vtkSmartPointer<sliceVolume> act);
	void setSliceVolumeZX(vtkSmartPointer<sliceVolume> act);

	void SetCustomPort(int p);

	int GetCustomPort();

	bool* getActiveArrayColor();

	void SetBounds(double bnds[6]);

	double* getCustomBounds();

	void deletePoint2D(int x, int y);
	void selectPoint(int x, int y, int z);

	void removeSelectedPoints();

	void SetAmbient(double a);
	void SetDiffuse(double d);
	void SetSpecular(double s);
	void SetSpecularPower(double sp);

	double GetAmbient();
	double GetDiffuse();
	double GetSpecular();
	double GetSpecularPower();
	
	void centrar();

	QString getPath();
	void setPath(QString p);
	void addClippingPlane(vtkSmartPointer<vtkPlane> plane);
	vtkSmartPointer<vtkPlaneCollection> getClippingPlanes();

	void addImage(QPixmap * pix, int i);

	QPixmap * getImage(int i);

	vtkSmartPointer<vtkColorTransferFunction> GetColorTransferFunction();


	void setIsSplitted();
	bool getIsSplitted();

private:

	bool splitted = false;

	vtkSmartPointer<vtkVolumeProperty> initVolumeProperty();

	QString path = "";

	Paciente * paciente;

	vtkSmartPointer<vtkPlaneCollection> planeCollection = nullptr;

	//vtkSmartPointer<vtkVolumeProperty * propiedad;

	QVector<double> *vec_opacity_x, *vec_opacity_y;

	QVector<double> *vec_gradient_x, *vec_gradient_y;

	QVector<double> *vec_color;

	vtkSmartPointer<vtkColorTransferFunction> color;
	vtkSmartPointer<vtkColorTransferFunction> colorSelected;

	std::string directory_path;

	vtkSmartPointer<vtkImageData> imageData = nullptr;

	QPixmap * pixArray[NUMBEROFTRANSFERFUNCTIONS];

	vtkSmartPointer<vtkImageActor> imageActorXY = nullptr;
	vtkSmartPointer<vtkImageActor> imageActorYZ = nullptr;
	vtkSmartPointer<vtkImageActor> imageActorZX = nullptr;


	vtkSmartPointer<sliceVolume> volumenXY = nullptr;
	vtkSmartPointer<sliceVolume> volumenYZ = nullptr;
	vtkSmartPointer<sliceVolume> volumenZX = nullptr;


	int Aport;

	vtkSmartPointer<vtkOpenGLGPUVolumeRayCastMapper> volumeMapper;

	double* bounds = nullptr;
	
	QList<double> puntosSeleccionados;

	bool colorArrayActive[CANT_COLOR] = {
		true,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		true,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		true,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		true,
		false,
		true,
		false,
		false,
		false,
		false,
		true,
		false,
		true,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		true,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		true

	};
};