/********************************************************************************
** Form generated from reading UI file 'manualPDF.ui'
**
** Created by: Qt User Interface Compiler version 5.11.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MANUALPDF_H
#define UI_MANUALPDF_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <qmainwindow.h>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QVBoxLayout>
#include <QWebEngineView>
#include <QUrl>

#include <qwebengineview.h>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <qtwebengineglobal.h>

QT_BEGIN_NAMESPACE

class Ui_manualPDF
{
public:
	QWidget * centralWidget;

	QWebEngineView *webEngine;
	
	
	QVBoxLayout *verticalLayout;

	void setupUi(QMainWindow *MainWindow)
	{

		QtWebEngine::initialize();

		if (MainWindow->objectName().isEmpty())
			MainWindow->setObjectName(QStringLiteral("MainWindow"));
		MainWindow->resize(700, 600);
		MainWindow->setUnifiedTitleAndToolBarOnMac(true);
		centralWidget = new QWidget(MainWindow);
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		verticalLayout = new QVBoxLayout(centralWidget);
		verticalLayout->setSpacing(0);
		verticalLayout->setContentsMargins(11, 11, 11, 11);
		verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
		verticalLayout->setContentsMargins(0, 0, 0, 0);
		webEngine = new QWebEngineView(centralWidget);
		webEngine->setObjectName(QStringLiteral("widget"));

		//webEngine->setOpenExternalLinks(true);
		

		verticalLayout->addWidget(webEngine);

		MainWindow->setCentralWidget(centralWidget);

		retranslateUi(MainWindow);

		QMetaObject::connectSlotsByName(MainWindow);
	} // setupUi

	void retranslateUi(QMainWindow *MainWindow)
	{
		MainWindow->setWindowTitle(QApplication::translate("MainWindow", "PDF Viewer", nullptr));
	} // retranslateUi

};



namespace Ui {
    class manualPDF : public Ui_manualPDF {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MANUALPDF_H
