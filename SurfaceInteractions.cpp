#include "SurfaceInteractions.h"
#include <vtkActor.h>
#include <vtkNew.h>
#include <vtkCollection.h>
#include <vtkSmartPointer.h>
#include <vtkMatrix4x4.h>
#include <QKeyEvent>
#include <vtkRenderWindow.h>
#include <vtkPropPicker.h>
#include <vtkRendererCollection.h>
#include <vtkRenderer.h>
#include <vtkProperty.h>
#include <vtkMapper.h>
#include <vtkPolyDataMapper.h>
#include <vtkAppendPolyData.h>
#include <vtkTransformFilter.h>
#include <vtkTransform.h>
#include <vtkPolyDataConnectivityFilter.h>
#include <vtkPlane.h>
#include <vtkPlaneCollection.h>


#include "CameraHandler.h"
#include "Consola.h"
#include "CallbacksClass.h"
#include "Dialog.h"
#include "MasterHandler.h"
#include "PlaneHandler.h"
#include <vtkPlane.h>
#include <vtkDataObject.h>

#include "MeshOperations.h"
#include "ThreadHandler.h"
#include "InputManager.h"
#include <qmap.h>
//selected actor and color

QMap<vtkSmartPointer<vtkActor>,double*>* ActorToColor;

double selectColor[3] = { 0,128.0 / 255.0 ,128.0 / 255.0 };

SurfaceInteractions::SurfaceInteractions()
{
	meshoperations = new MeshOperations();
	actorMeshMatrix = vtkSmartPointer<vtkMatrix4x4>::New();
	WordMatrix = vtkSmartPointer<vtkMatrix4x4>::New();
	WordNormalMatrix = vtkSmartPointer<vtkMatrix4x4>::New();
	WordNormalMatrix2 = vtkSmartPointer<vtkMatrix4x4>::New();
	controladorSuperficie = new ControladorObjeto();

	ActorToColor = new QMap<vtkSmartPointer<vtkActor>, double*>();
	//ui = new Ui_ControladorObjeto;

	//ui->setupUi(splitter);

	//customPlot_Opacity->setIlluminationSpins(ui->doubleSpinBox_Ambiente, ui->doubleSpinBox_Difuso, ui->doubleSpinBox_Especular, ui->doubleSpinBox_PotencaEspecular);
}


SurfaceInteractions::~SurfaceInteractions()
{
}

void SurfaceInteractions::keyPress() {
	if (InputManager::IsKeyPressed(Qt::Key::Key_Control)) {
		controlKeyPress = true;
	}

	if (InputManager::IsKeyPressed(Qt::Key_Delete)) {
		//simpleview->slotEliminar();
		if (lastPickedActor != nullptr) {
			GetRenderWindow()->GetRenderers()->GetFirstRenderer()->RemoveActor(lastPickedActor);
			lastPickedActor = nullptr;
			int longitud = actoresSeleccionados->GetNumberOfItems();
			for (int i = 0; i < longitud; i++) {
				vtkSmartPointer<vtkActor> aux = (vtkActor*)actoresSeleccionados->GetItemAsObject(i);
				GetRenderWindow()->GetRenderers()->GetFirstRenderer()->RemoveActor(aux);
			}
			actoresSeleccionados->RemoveAllItems();
			GetRenderWindow()->Render();
		}
	}

	//interactuarConObjeto(event);
}

void SurfaceInteractions::keyRelease() {
	if (!InputManager::IsKeyPressed(Qt::Key::Key_Control)) {
		controlKeyPress = false;
	}
}

vtkSmartPointer<vtkRenderWindow> SurfaceInteractions::GetRenderWindow() {
	return renderWindow_s;
}


void  SurfaceInteractions::SetRenderWindow(vtkSmartPointer<vtkRenderWindow> ren) {
	renderWindow_s = ren;
}



void SurfaceInteractions::setSelectedActorToNull()
{
	lastPickedActor = nullptr;
	actoresSeleccionados->RemoveAllItems();
}

vtkSmartPointer<vtkActor> SurfaceInteractions::getSelectedActor()
{
	return lastPickedActor;
}



void setPicked(vtkSmartPointer<vtkActor> actor, bool b) {
	if (b) {
		if (!ActorToColor->contains(actor)) {
			double* color = new double(3);
			color[0] = actor->GetProperty()->GetColor()[0];
			color[1] = actor->GetProperty()->GetColor()[1];
			color[2] = actor->GetProperty()->GetColor()[2];
			ActorToColor->insert(actor, color);
		}

		actor->GetProperty()->SetColor(selectColor);


	}
	else {
		if (ActorToColor->contains(actor)) {
			double* color = ActorToColor->value(actor);
			actor->GetProperty()->SetColor(color);
		}
		else {
			actor->GetProperty()->SetColor(1,1,1);
		}
	}
}

void SurfaceInteractions::checkPickedActor(CameraHandler* camera, QMouseEvent *event)
{

	int* point = camera->GetRenderWindowInteractor()->GetEventPosition();

	// Pick from this location.
	vtkSmartPointer<vtkPropPicker>  picker =
		vtkSmartPointer<vtkPropPicker>::New();
	picker->Pick(point[0], point[1], 0, GetRenderWindow()->GetRenderers()->GetFirstRenderer());

	vtkSmartPointer<vtkActor> actor = picker->GetActor();
	/*ESTO SE MANEJA EN EL MASTERHANDLER/**/
	if (actor != nullptr) {

		if(controlKeyPress)//se selecciona mas de un actor o se deselecciona un actor de la selaccion
			if (actoresSeleccionados->IsItemPresent(actor)) {//se deselecciona
				actoresSeleccionados->RemoveItem(actor);
				setPicked(actor,false);
				lastPickedActor = nullptr;
			}
			else {//se selecciona
				actoresSeleccionados->AddItem(actor);
				setPicked(actor,true);
				lastPickedActor = actor;
			}
		else {//si no esta el control presionado es un seleccionar individual
			if (lastPickedActor != nullptr) {//si habia algo seleccionado se deselecciona
				for (int i = 0; i < actoresSeleccionados->GetNumberOfItems(); i++) {
					vtkSmartPointer<vtkActor> aux = (vtkActor*)actoresSeleccionados->GetItemAsObject(i);
					setPicked(aux,false);
				}	
				actoresSeleccionados->RemoveAllItems();
				controladorSuperficie->close();
				//cerrar controlador Objeto
			}

			if (actor != lastPickedActor) {//se selecciona
				actoresSeleccionados->AddItem(actor);
				setPicked(actor,true);
				lastPickedActor = actor;
				controladorSuperficie->move(QPoint(0, 0));
				double* pos = lastPickedActor->GetPosition();
				double* orient = lastPickedActor->GetOrientation();
				double* scale = lastPickedActor->GetScale();


				int canFaces = lastPickedActor->GetMapper()->GetInput()->GetNumberOfCells();

				controladorSuperficie->setValores(pos[0], pos[1], pos[2], orient[0], orient[1], orient[2], scale[0], scale[1], scale[2], canFaces);
				
				
				controladorSuperficie->show();
				//abrir controlador objeto
			}
			
			
		}

	}
	else //no se pickeo nada
		if (!controlKeyPress) {// se deseleccion a todo
			if (lastPickedActor != nullptr) {
				setPicked(lastPickedActor,false);
			}
			for (int i = 0; i < actoresSeleccionados->GetNumberOfItems(); i++) {
				vtkSmartPointer<vtkActor> aux = (vtkActor*)actoresSeleccionados->GetItemAsObject(i);
				setPicked(aux,false);
			}
			actoresSeleccionados->RemoveAllItems();
			lastPickedActor = nullptr;
			controladorSuperficie->close();

		}
	
	


	GetRenderWindow()->Render();
}


void SurfaceInteractions::girarActor(QKeyEvent *event)
{
	if (press_Z) {
		if (event->key() == Qt::Key::Key_Left) {
			double* orient = lastPickedActor->GetOrientation();
			if (orient[2] > -88) {
				orient[2]--;
			}

			lastPickedActor->SetOrientation(orient);
		}
		if (event->key() == Qt::Key::Key_Right) {
			double* orient = lastPickedActor->GetOrientation();
			if (orient[2] < 88) {
				orient[2]++;
			}
			lastPickedActor->SetOrientation(orient);
		}

		GetRenderWindow()->Render();

	}
	if (press_Y) {
		if (event->key() == Qt::Key::Key_Left) {
			double* orient = lastPickedActor->GetOrientation();

			if (orient[1] > -88) {
				orient[1]--;
			}
			lastPickedActor->SetOrientation(orient);

		}
		if (event->key() == Qt::Key::Key_Right) {
			double* orient = lastPickedActor->GetOrientation();

			if (orient[1] < 88) {
				orient[1]++;
			}
			lastPickedActor->SetOrientation(orient);
		}
		GetRenderWindow()->Render();

	}
	if (press_X) {
		if (event->key() == Qt::Key::Key_Left) {
			double* orient = lastPickedActor->GetOrientation();

			if (orient[0] > -88) {
				orient[0]--;
			}
			lastPickedActor->SetOrientation(orient);
		}
		if (event->key() == Qt::Key::Key_Right) {
			double* orient = lastPickedActor->GetOrientation();

			if (orient[0] < 88) {
				orient[0]++;
				//printf("Orientacion: %lf\n", lastPickedActor->GetOrientation()[0]);

			}
			lastPickedActor->SetOrientation(orient);
		}

		GetRenderWindow()->Render();

	}
}

void SurfaceInteractions::moverActor(QKeyEvent *event)
{
	if (event->key() == Qt::Key::Key_Up) {

		double translation[4] = { -1,0,0,0 };
		lastPickedActor->GetMatrix(WordMatrix);

		double* translationConvert = WordMatrix->MultiplyDoublePoint(translation);

		double* position = lastPickedActor->GetPosition();

		translationConvert[0] += position[0];
		translationConvert[1] += position[1];
		translationConvert[2] += position[2];

		lastPickedActor->SetPosition(translationConvert);
		//printf("%lf - %lf - %lf\n",position);
	}
	if (event->key() == Qt::Key::Key_Down) {

		double translation[4] = { 1,0,0,0 };
		lastPickedActor->GetMatrix(WordMatrix);

		double* translationConvert = WordMatrix->MultiplyDoublePoint(translation);

		double* position = lastPickedActor->GetPosition();

		translationConvert[0] += position[0];
		translationConvert[1] += position[1];
		translationConvert[2] += position[2];

		lastPickedActor->SetPosition(translationConvert);
	}
}

void SurfaceInteractions::clearTransforms()
{
	if (lastPickedActor != nullptr) {
		lastPickedActor->SetPosition(0, 0, 0);
		lastPickedActor->SetOrientation(0, 0, 0);
	}
	GetRenderWindow()->Render();
}

void SurfaceInteractions::interactuarConObjeto(QKeyEvent * event)
{
	if (lastPickedActor != nullptr) {
		if (event->key() == Qt::Key_X) {
			if (controlKeyPress) {
				//la normal se alinea con el eje X
				clearTransforms();

			}
			else {
				press_X = true;
				press_Y = false;
				press_Z = false;
				lastPickedActor->GetProperty()->SetColor(1, 0, 0);

			}
		}
		else if (event->key() == Qt::Key_Y) {

			if (controlKeyPress) {
				//se alinea con el eje Y
				lastPickedActor->SetPosition(0, 0, 0);
				lastPickedActor->SetOrientation(0, 0, 0);
				lastPickedActor->SetOrientation(0, 0, 90);

			}
			else {
				press_X = false;
				press_Y = true;
				press_Z = false;
				lastPickedActor->GetProperty()->SetColor(0, 1, 0);

			}

		}
		else if (event->key() == Qt::Key_Z) {
			if (controlKeyPress) {
				//se alinea con el eje Z
				lastPickedActor->SetPosition(0, 0, 0);
				lastPickedActor->SetOrientation(0, 0, 0);
				lastPickedActor->SetOrientation(0, -90, 0);

			}
			else {
				press_X = false;
				press_Y = false;
				press_Z = true;
				lastPickedActor->GetProperty()->SetColor(0, 0, 1);
			}
		}
		else {
			girarActor(event);
			moverActor(event);
		}
		GetRenderWindow()->Render();
	}
}

void SurfaceInteractions::separarObjeto(vtkSmartPointer<vtkActor> actor, double ratio)
{
	{
		if (actor != nullptr && actor != MasterHandler::GetActorPlane()) 
		{
			
			vtkSmartPointer<vtkPolyDataConnectivityFilter> connectivityFilter = vtkSmartPointer<vtkPolyDataConnectivityFilter>::New();

			connectivityFilter->SetInputData(actor->GetMapper()->GetInput());
			connectivityFilter->SetExtractionModeToAllRegions();
			Consola::appendln("Separando objeto seleccionado");
			
			vtkSmartPointer<vtkCallbackCommand> m_vtkCallback = CallbacksClass::getProgressCallBack();

			if (m_vtkCallback.Get() != NULL)
			{
				CallbacksClass::setEncabezado("Buscando Objetos: ");
				connectivityFilter->AddObserver(vtkCommand::ProgressEvent, m_vtkCallback);
			}
			connectivityFilter->Update();

			
			int anterior;
			bool primera = true;

			long maxSize = 0;
			for (int i = 0; i < connectivityFilter->GetNumberOfExtractedRegions(); i++) {
				if (connectivityFilter->GetRegionSizes()->GetValue(i) > maxSize) {
					maxSize = connectivityFilter->GetRegionSizes()->GetValue(i);
				}

			}

			long minVertices = maxSize * ratio;

			int cant = 0;
			for (int i = 0; i < connectivityFilter->GetNumberOfExtractedRegions(); i++) {
				if (connectivityFilter->GetRegionSizes()->GetValue(i) > minVertices) {
					cant++;
				}

			}

			int j = 0;
			if (connectivityFilter->GetNumberOfExtractedRegions() > 1) {
				for (int i = 0; i < connectivityFilter->GetNumberOfExtractedRegions(); i++) {
					if (connectivityFilter->GetRegionSizes()->GetValue(i) > minVertices) {
						j++;

						connectivityFilter->SetExtractionModeToSpecifiedRegions();

						Consola::append("...Extrayendo sub-objeto ");
						Consola::append(j);
						Consola::append("/");
						Consola::append(cant);
						Consola::append("... ");
						if (!primera)
							connectivityFilter->DeleteSpecifiedRegion(anterior);
						connectivityFilter->AddSpecifiedRegion(i);


						CallbacksClass::setEncabezado(QString(
							QString("Extrayendo sub-objeto: ")
							+QString::number(j)
							+QString("/")
							+QString::number(cant)
							+QString(": ")
						));

						connectivityFilter->Update();
						
						vtkNew<vtkPolyData> region;

						region->DeepCopy(connectivityFilter->GetOutput());

						vtkNew<vtkPolyDataMapper> regionMap;
						regionMap->SetInputData(region);
						regionMap->ScalarVisibilityOff();

						vtkSmartPointer<vtkActor> regionActor = vtkSmartPointer<vtkActor>::New();

						regionActor->SetMapper(regionMap);

						regionActor->GetProperty()->SetColor(1, 1, 1);

						GetRenderWindow()->GetRenderers()->GetFirstRenderer()->AddActor(regionActor);

						emit MasterHandler::GetSimpleView()->signal_renderizar();

						anterior = i;

						if (primera)
							primera = !primera;

						Consola::appendln("OK");

					}

				}
				MasterHandler::GetThread()->lock_Actor();
				emit MasterHandler::GetSimpleView()->signal_RemoveActor(actor);
				MasterHandler::GetThread()->lock_Actor();
				MasterHandler::GetThread()->unlock_Actor();
			}
			else {
				Consola::appendln("El objeto es unico");
			}

			
		}
		Consola::append("Extraccion finalizada\n");

	}
}

void SurfaceInteractions::unirObjetosSeleccionados()
{
	if (actoresSeleccionados->GetNumberOfItems() > 1) {
		vtkNew<vtkAppendPolyData> append;

		Consola::append("Uniendo objetos seleccionados\n");
		vtkSmartPointer<vtkCallbackCommand> m_vtkCallback = CallbacksClass::getProgressCallBack();

		if (m_vtkCallback.Get() != NULL)
		{
			append->AddObserver(vtkCommand::ProgressEvent, m_vtkCallback);
		}
		for (int i = 0; i < actoresSeleccionados->GetNumberOfItems(); i++) {
			vtkSmartPointer<vtkActor> actor = (vtkActor*)actoresSeleccionados->GetItemAsObject(i);

			vtkNew<vtkTransformFilter> Transformfilter;
			Transformfilter->SetInputData(actor->GetMapper()->GetInput());

			vtkNew<vtkTransform> Transform;

			double mat[16];

			actor->GetMatrix(mat);

			Transform->SetMatrix(mat);

			Transformfilter->SetTransform(Transform);

			Transformfilter->Update();

			append->AddInputConnection(Transformfilter->GetOutputPort());
			MasterHandler::GetThread()->lock_Actor();
			//GetRenderWindow()->GetRenderers()->GetFirstRenderer()->RemoveActor(actor);
			emit MasterHandler::GetSimpleView()->signal_RemoveActor(actor);
			MasterHandler::GetThread()->lock_Actor();
			MasterHandler::GetThread()->unlock_Actor();
		}


		append->Update();
		lastPickedActor = nullptr;

		//remuevo los actores seleccionados de la lista
		actoresSeleccionados->RemoveAllItems();

		//mapper
		vtkNew<vtkPolyDataMapper> mapper;
		mapper->SetInputConnection(append->GetOutputPort());
		mapper->ScalarVisibilityOff();

		//actor
		vtkSmartPointer<vtkActor> nuevoActor = vtkSmartPointer<vtkActor>::New();

		nuevoActor->SetMapper(mapper);

		nuevoActor->GetProperty()->SetColor(1, 1, 1);
		//agrego actor al Renderer
		GetRenderWindow()->GetRenderers()->GetFirstRenderer()->AddActor(nuevoActor);

		emit MasterHandler::GetSimpleView()->signal_renderizar();

		Consola::append("Hecho\n");

		//GetRenderWindow()->Render();
	}
	else emit MasterHandler::GetSimpleView()->signal_Warning(WARN_NO_ITEMS_SELECCIONADOS);
}

void SurfaceInteractions::removeSelectedActor() {
	if (lastPickedActor != nullptr) {
		GetRenderWindow()->GetRenderers()->GetFirstRenderer()->RemoveActor(lastPickedActor);
		//coleccionDeActores->RemoveItem(actor);
		GetRenderWindow()->Render();
	}
	//TODO aca tengo que hhacer un get de distance widget
	
}

void SurfaceInteractions::cortarPorPlano(PlaneHandler* handler, vtkSmartPointer<vtkActor> actor) {
	vtkSmartPointer<vtkActor> actorPlane = handler->GetActor();
	if (actorPlane!=nullptr) 
	{
		vtkSmartPointer<vtkRegularPolygonSource> plane = handler->getPlane();
		//Orientacion y centro a partir del actor y del hueso
		//actor actual y su matriz de transformacion
		//vtkSmartPointer<vtkActor> actor = getSelectedActor();
		if (actor != nullptr && actor != actorPlane)
		{

			vtkSmartPointer<vtkPlane> vtkPlaneObj1 =
				vtkSmartPointer<vtkPlane>::New();
			vtkSmartPointer<vtkPlaneCollection> planes1 =
				vtkSmartPointer<vtkPlaneCollection>::New();

			vtkSmartPointer<vtkPlane> vtkPlaneObj2 =
				vtkSmartPointer<vtkPlane>::New();
			vtkSmartPointer<vtkPlaneCollection> planes2 =
				vtkSmartPointer<vtkPlaneCollection>::New();

			vtkSmartPointer<vtkClipClosedSurface> clipper1 =
				vtkSmartPointer<vtkClipClosedSurface>::New();
			vtkSmartPointer<vtkClipClosedSurface> clipper2 =
				vtkSmartPointer<vtkClipClosedSurface>::New();
			//trabajo con matrices punto central y normales de los planos
			{
				//quito el plano
				//planeOn_h = false;

				actor->GetMatrix(actorMeshMatrix);

				actorMeshMatrix->Invert();

				//matriz de transformacion del plano

				//se pide la matriz al plano
				actorPlane->GetMatrix(WordMatrix);

				double res[16];

				//se translada al espacio de clipper para realizar el corte
				WordMatrix->Multiply4x4(actorMeshMatrix->GetData(), WordMatrix->GetData(), res);

				WordMatrix->DeepCopy(res);

				double* origin = plane->GetCenter();

				origin = WordMatrix->MultiplyDoublePoint(origin);

				//Matriz para transformar la normal
				//debe ser la inversa traspuesta

				WordNormalMatrix->DeepCopy(WordMatrix);

				WordNormalMatrix->Invert();
				WordNormalMatrix->Transpose();

				WordNormalMatrix2->DeepCopy(WordMatrix);

				WordNormalMatrix2->Invert();
				WordNormalMatrix2->Transpose();

				double* normal = plane->GetNormal();
				double normal2[4] = { -normal[0],normal[1],normal[2], normal[3] };



				//multiplico por la matriz normal
				normal = WordNormalMatrix->MultiplyDoublePoint(normal);
				double* normal2Pointer = WordNormalMatrix2->MultiplyDoublePoint(normal2);

				vtkPlaneObj1->SetOrigin(origin);

				vtkPlaneObj1->SetNormal(normal);

				vtkPlaneObj2->SetOrigin(origin);

				vtkPlaneObj2->SetNormal(normal2Pointer);

				planes1->AddItem(vtkPlaneObj1);
				planes2->AddItem(vtkPlaneObj2);
			}

			//seteo el clipper anterior
			vtkSmartPointer<vtkPolyData> poly1 = vtkSmartPointer<vtkPolyData>::New();
			poly1->DeepCopy(actor->GetMapper()->GetInput());
			vtkSmartPointer<vtkPolyData> poly2 = vtkSmartPointer<vtkPolyData>::New();
			poly2->DeepCopy(actor->GetMapper()->GetInput());

			clipper1->SetInputData(poly1);
			clipper2->SetInputData(poly2);

			//seteo el plano
			clipper1->SetClippingPlanes(planes1);
			clipper2->SetClippingPlanes(planes2);


			//muestro el progreso
			Consola::append("Haciendo corte transversal\n");

			vtkSmartPointer<vtkCallbackCommand> m_vtkCallback = CallbacksClass::getProgressCallBack();

			if (m_vtkCallback.Get() != NULL)
			{
				clipper1->AddObserver(vtkCommand::ProgressEvent, m_vtkCallback);
			}
			clipper1->SetActivePlaneId(2);

			clipper1->Update();

			if (m_vtkCallback.Get() != NULL)
			{
				clipper2->AddObserver(vtkCommand::ProgressEvent, m_vtkCallback);
			}

			clipper2->SetActivePlaneId(2);

			clipper2->Update();

			std::cout << " " << std::endl;

			//elimino actor anterior
			Consola::append("Hecho\n");
			//GetRenderWindow()->GetRenderers()->GetFirstRenderer()->RemoveActor(seleccionado);
			MasterHandler::GetThread()->lock_Actor();
			emit MasterHandler::GetSimpleView()->signal_RemoveActor(actor);
			MasterHandler::GetThread()->lock_Actor();
			MasterHandler::GetThread()->unlock_Actor();
			//creo nuevo mapper y nuevo actor
			//MESH MAPPER 
			vtkSmartPointer<vtkPolyDataMapper> mapperMesh2 = vtkSmartPointer<vtkPolyDataMapper>::New();
			vtkSmartPointer<vtkPolyDataMapper> mapperMesh1 = vtkSmartPointer<vtkPolyDataMapper>::New();

			mapperMesh1->SetInputConnection(clipper1->GetOutputPort());
			mapperMesh2->SetInputConnection(clipper2->GetOutputPort());


			//MESH ACTOR
			//actorMesh = vtkSmartPointer<vtkActor>::New();

			vtkSmartPointer<vtkActor> actorMesh2 = vtkSmartPointer<vtkActor>::New();
			vtkSmartPointer<vtkActor> actorMesh1 = vtkSmartPointer<vtkActor>::New();

			actorMesh1->SetMapper(mapperMesh1);
			actorMesh2->SetMapper(mapperMesh2);


			actorMesh1->GetProperty()->SetColor(1, 1, 1);
			actorMesh2->GetProperty()->SetColor(1, 1, 1);


			GetRenderWindow()->GetRenderers()->GetFirstRenderer()->AddActor(actorMesh1);
			GetRenderWindow()->GetRenderers()->GetFirstRenderer()->AddActor(actorMesh2);

			vtkNew<vtkMatrix4x4> MatActor1;
			vtkNew<vtkMatrix4x4> MatActor2;

			actor->GetMatrix(actorMeshMatrix);

			MatActor1->DeepCopy(actorMeshMatrix);
			MatActor2->DeepCopy(actorMeshMatrix);

			actorMesh1->SetUserMatrix(MatActor1);
			actorMesh2->SetUserMatrix(MatActor2);

			
			handler->quitarPlano();
			
			emit MasterHandler::GetSimpleView()->signal_renderizar();

			lastPickedActor = nullptr;
			actoresSeleccionados->RemoveAllItems();

		}
		else {
			//Consola::append("Por favor seleccione un objeto para cortar\n");
		}
	}

	else{
		emit MasterHandler::GetSimpleView()->signal_Warning("No se ha insertado ningun plano");
	}
}

void SurfaceInteractions::removeActor(vtkSmartPointer<vtkActor > actor)
{
	GetRenderWindow()->GetRenderers()->GetFirstRenderer()->RemoveActor(actor);
}

MeshOperations * SurfaceInteractions::GetMeshOperations()
{
	return meshoperations;
}

void SurfaceInteractions::AddActor(vtkSmartPointer<vtkActor > actor)
{
	GetRenderWindow()->GetRenderers()->GetFirstRenderer()->AddActor(actor);
}

//implemento aqu� el trasladar, rotar, etc

void SurfaceInteractions::move_pos_x(double d)
{
	if (lastPickedActor != nullptr && lastPickedActor.Get() != nullptr) {
		double translation[4] = { 0,0,0,0 };
		lastPickedActor->GetMatrix(WordMatrix);

		double* translationConvert = WordMatrix->MultiplyDoublePoint(translation);

		double* position = lastPickedActor->GetPosition();

		translationConvert[0] += d;
		translationConvert[1] += position[1];
		translationConvert[2] += position[2];

		lastPickedActor->SetPosition(translationConvert);
	}
}

void SurfaceInteractions::move_pos_y(double d)
{
	if (lastPickedActor != nullptr && lastPickedActor.Get() != nullptr) {
		double translation[4] = { 0,0,0,0 };
		lastPickedActor->GetMatrix(WordMatrix);

		double* translationConvert = WordMatrix->MultiplyDoublePoint(translation);

		double* position = lastPickedActor->GetPosition();

		translationConvert[0] += position[0];
		translationConvert[1] += d;
		translationConvert[2] += position[2];

		lastPickedActor->SetPosition(translationConvert);
	}
}

void SurfaceInteractions::move_pos_z(double d)
{
	if (lastPickedActor != nullptr && lastPickedActor.Get() != nullptr) {
		double translation[4] = { 0,0,0,0 };
		lastPickedActor->GetMatrix(WordMatrix);

		double* translationConvert = WordMatrix->MultiplyDoublePoint(translation);

		double* position = lastPickedActor->GetPosition();

		translationConvert[0] += position[0];
		translationConvert[1] += position[1];
		translationConvert[2] += d;

		lastPickedActor->SetPosition(translationConvert);
	}
}

void SurfaceInteractions::rot_x(double d)
{
	if (lastPickedActor != nullptr && lastPickedActor.Get() != nullptr) {
		double* orient = lastPickedActor->GetOrientation();
		orient[0] = d;
		lastPickedActor->SetOrientation(orient);
		
	}
}

void SurfaceInteractions::rot_y(double d)
{
	if (lastPickedActor != nullptr && lastPickedActor.Get() != nullptr) {
		double* orient = lastPickedActor->GetOrientation();
		orient[1] = d;
		lastPickedActor->SetOrientation(orient);
	}
}

void SurfaceInteractions::rot_z(double d)
{
	if (lastPickedActor != nullptr && lastPickedActor.Get() != nullptr) {
		double* orient = lastPickedActor->GetOrientation();
		orient[2] = d;
		lastPickedActor->SetOrientation(orient);
	}
}

void SurfaceInteractions::scale_x(double d)
{
	if (lastPickedActor != nullptr && lastPickedActor.Get() != nullptr) {
		double* scale = lastPickedActor->GetScale();
		scale[0] = d;
		lastPickedActor->SetScale(scale);
		lastPickedActor->Modified();
	}
}

void SurfaceInteractions::scale_y(double d)
{
	if (lastPickedActor != nullptr && lastPickedActor.Get() != nullptr) {
		double* scale = lastPickedActor->GetScale();
		scale[1] = d;
		lastPickedActor->SetScale(scale);
		lastPickedActor->Modified();
	}
}

void SurfaceInteractions::scale_z(double d)
{
	if (lastPickedActor != nullptr && lastPickedActor.Get() != nullptr) {
		double* scale = lastPickedActor->GetScale();
		scale[2] = d;
		lastPickedActor->SetScale(scale);
		lastPickedActor->Modified();
	}
}
