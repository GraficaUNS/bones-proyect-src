#include "RectangleHandler.h"
#include <vtkSmartPointer.h>
#include <vtkActor.h>
#include <vtkPlane.h>
#include <vtkPlaneWidget.h>
#include <vtkCollection.h>
#include <vtkRenderer.h>
#include <vtkPolyData.h>
#include <vtkProperty.h>
#include <vtkCallbackCommand.h>
#include <vtkWidgetEvent.h>
#include <vtkSphereSource.h>
#include <vtkTriangle.h>
#include <vtkPolyDataMapper.h>

#include "MasterHandler.h"
#include "VolumeInteractions.h"

vtkSmartPointer<vtkPlaneWidget> planeWidgetXY;
vtkSmartPointer<vtkPlaneWidget> planeWidgetYZ;
vtkSmartPointer<vtkPlaneWidget> planeWidgetZX;

vtkSmartPointer<vtkRenderer> renXY;
vtkSmartPointer<vtkRenderer> renYZ;
vtkSmartPointer<vtkRenderer> renZX;

vtkSmartPointer<vtkPolyData> dataXY = vtkSmartPointer<vtkPolyData>::New();
vtkSmartPointer<vtkPolyData> dataYZ = vtkSmartPointer<vtkPolyData>::New();
vtkSmartPointer<vtkPolyData> dataZX = vtkSmartPointer<vtkPolyData>::New();

vtkSmartPointer<vtkCallbackCommand> rectXYCallback = vtkSmartPointer<vtkCallbackCommand>::New();
vtkSmartPointer<vtkCallbackCommand> rectYZCallback = vtkSmartPointer<vtkCallbackCommand>::New();
vtkSmartPointer<vtkCallbackCommand> rectZXCallback = vtkSmartPointer<vtkCallbackCommand>::New();

double* boundsXY;
double* boundsYZ;
double* boundsZX;


void functionCallbackRectangleXY(vtkObject* caller, long unsigned int eventId, void* /*clientData*/, void* /*callData*/) {

	if (dataXY!=nullptr && dataYZ != nullptr && dataZX != nullptr) {
		planeWidgetXY->GetPolyData(dataXY);
		planeWidgetYZ->GetPolyData(dataYZ);
		planeWidgetZX->GetPolyData(dataZX);


		double *bounds1 = dataXY->GetBounds();

		double *bounds2 = dataYZ->GetBounds();

		double *bounds3 = dataZX->GetBounds();

		double newBounds2[6] = {
			bounds1[2],
			bounds1[3],
			bounds2[2],
			bounds2[3],
			bounds2[4],
			bounds2[5]
		};

		double newBounds3[6] = {
			bounds1[0],
			bounds1[1],
			bounds3[2],
			bounds3[3],
			bounds3[4],
			bounds3[5]
		};

		planeWidgetYZ->PlaceWidget(newBounds2);
		planeWidgetZX->PlaceWidget(newBounds3);

		double boundsVolume[6] = {
			bounds1[0] - (boundsXY[1] - boundsXY[0]) / 2,
			bounds1[1] - (boundsXY[1] - boundsXY[0]) / 2,
			bounds1[2] - (boundsXY[3] - boundsXY[2]) / 2,
			bounds1[3] - (boundsXY[3] - boundsXY[2]) / 2,
			-(bounds2[3] - (boundsYZ[3] - boundsYZ[2]) / 2),
			-(bounds2[2] - (boundsYZ[3] - boundsYZ[2]) / 2)

		};

		MasterHandler::GetVolumeInteractor()->setBounds(boundsVolume);
	}
}

void functionCallbackRectangleYZ(vtkObject* caller, long unsigned int eventId, void* /*clientData*/, void* /*callData*/) {
	if (dataXY != nullptr && dataYZ != nullptr && dataZX != nullptr) {
		planeWidgetXY->GetPolyData(dataXY);
		planeWidgetYZ->GetPolyData(dataYZ);
		planeWidgetZX->GetPolyData(dataZX);


		double *bounds1 = dataXY->GetBounds();

		double *bounds2 = dataYZ->GetBounds();

		double *bounds3 = dataZX->GetBounds();

		double newBounds1[6] = {
			bounds1[0],
			bounds1[1],
			bounds2[0],
			bounds2[1],
			bounds1[4],
			bounds1[5]
		};

		double newBounds3[6] = {
			bounds3[0],
			bounds3[1],
			bounds2[2],
			bounds2[3],
			bounds3[4],
			bounds3[5]
		};

		planeWidgetXY->PlaceWidget(newBounds1);
		planeWidgetZX->PlaceWidget(newBounds3);

		double boundsVolume[6] = {
			bounds1[0] - (boundsXY[1] - boundsXY[0]) / 2,
			bounds1[1] - (boundsXY[1] - boundsXY[0]) / 2,
			bounds2[0] - (boundsYZ[1] - boundsYZ[0]) / 2,
			bounds2[1] - (boundsYZ[1] - boundsYZ[0]) / 2,
			-(bounds2[3] - (boundsYZ[3] - boundsYZ[2]) / 2),
			-(bounds2[2] - (boundsYZ[3] - boundsYZ[2]) / 2)
		};

		MasterHandler::GetVolumeInteractor()->setBounds(boundsVolume);
	}
}

void functionCallbackRectangleZX(vtkObject* caller, long unsigned int eventId, void* /*clientData*/, void* /*callData*/) {
	if (dataXY != nullptr && dataYZ != nullptr && dataZX != nullptr) {
		planeWidgetXY->GetPolyData(dataXY);
		planeWidgetYZ->GetPolyData(dataYZ);
		planeWidgetZX->GetPolyData(dataZX);


		double *bounds1 = dataXY->GetBounds();

		double *bounds2 = dataYZ->GetBounds();

		double *bounds3 = dataZX->GetBounds();


		double newBounds1[6] = {
			bounds3[0],
			bounds3[1],
			bounds1[2],
			bounds1[3],
			bounds1[4],
			bounds1[5]
		};

		double newBounds2[6] = {
			bounds2[0],
			bounds2[1],
			bounds3[2],
			bounds3[3],
			bounds2[4],
			bounds2[5]
		};


		planeWidgetXY->PlaceWidget(newBounds1);
		planeWidgetYZ->PlaceWidget(newBounds2);

		double boundsVolume[6] = {
			bounds3[0] - (boundsZX[1] - boundsZX[0]) / 2,
			bounds3[1] - (boundsZX[1] - boundsZX[0]) / 2,
			bounds2[0] - (boundsYZ[1] - boundsYZ[0]) / 2,
			bounds2[1] - (boundsYZ[1] - boundsYZ[0]) / 2,
			-(bounds3[3] - (boundsZX[3] - boundsZX[2]) / 2),
			-(bounds3[2] - (boundsZX[3] - boundsZX[2]) / 2)
		};

		MasterHandler::GetVolumeInteractor()->setBounds(boundsVolume);
	}
}



RectangleHandler::RectangleHandler(vtkSmartPointer<vtkRenderer> ren1, vtkSmartPointer<vtkRenderer> ren2, vtkSmartPointer<vtkRenderer> ren3)
{
	
	planeWidgetXY = vtkSmartPointer<vtkPlaneWidget>::New();
	planeWidgetXY->SetCenter(0, 0, 100);
	//planeWidgetXY->SetOrigin(0, 0, 100);
	planeWidgetXY->SetNormalToZAxis(true);
	planeWidgetXY->SetResolution(2);
	renXY = ren1;
	planeWidgetXY->GetPlaneProperty()->SetColor(0.005, 0.98, 0.98);
	
	rectXYCallback->SetCallback(functionCallbackRectangleXY);
	planeWidgetXY->AddObserver(vtkCommand::StartInteractionEvent, rectXYCallback);
	planeWidgetXY->AddObserver(vtkCommand::InteractionEvent, rectXYCallback);
	planeWidgetXY->AddObserver(vtkWidgetEvent::Move, rectXYCallback);
	planeWidgetXY->AddObserver(vtkWidgetEvent::EndSelect, rectXYCallback);
	planeWidgetXY->AddObserver(vtkCommand::EndInteractionEvent, rectXYCallback);

	//YZ

	planeWidgetYZ = vtkSmartPointer<vtkPlaneWidget>::New();
	planeWidgetYZ->SetCenter(0, 0, 100);
	planeWidgetYZ->SetNormalToZAxis(true);
	planeWidgetYZ->SetResolution(2);
	renYZ = ren2;
	planeWidgetYZ->GetPlaneProperty()->SetColor(0.005, 0.98, 0.98);

	rectYZCallback->SetCallback(functionCallbackRectangleYZ);
	planeWidgetYZ->AddObserver(vtkCommand::StartInteractionEvent, rectYZCallback);
	planeWidgetYZ->AddObserver(vtkCommand::InteractionEvent, rectYZCallback);
	planeWidgetYZ->AddObserver(vtkWidgetEvent::Move, rectYZCallback);
	planeWidgetYZ->AddObserver(vtkWidgetEvent::EndSelect, rectYZCallback);
	planeWidgetYZ->AddObserver(vtkCommand::EndInteractionEvent, rectYZCallback);


	//ZX

	planeWidgetZX = vtkSmartPointer<vtkPlaneWidget>::New();
	planeWidgetZX->SetCenter(0, 0, 100);
	planeWidgetZX->SetNormalToZAxis(true);
	planeWidgetZX->SetResolution(2);
	renZX = ren3;
	planeWidgetZX->GetPlaneProperty()->SetColor(0.005, 0.98, 0.98);

	rectZXCallback->SetCallback(functionCallbackRectangleZX);
	planeWidgetZX->AddObserver(vtkCommand::StartInteractionEvent, rectZXCallback);
	planeWidgetZX->AddObserver(vtkCommand::InteractionEvent, rectZXCallback);
	planeWidgetZX->AddObserver(vtkWidgetEvent::Move, rectZXCallback);
	planeWidgetZX->AddObserver(vtkWidgetEvent::EndSelect, rectZXCallback);
	planeWidgetZX->AddObserver(vtkCommand::EndInteractionEvent, rectZXCallback);


}


RectangleHandler::~RectangleHandler()
{

}

void RectangleHandler::setInteractor(vtkSmartPointer<vtkRenderWindowInteractor >interactor)
{
	if (firstFourCamera) {

		planeWidgetXY->SetCurrentRenderer(renXY);
		planeWidgetXY->SetDefaultRenderer(renXY);

		planeWidgetYZ->SetCurrentRenderer(renYZ);
		planeWidgetYZ->SetDefaultRenderer(renYZ);

		planeWidgetZX->SetCurrentRenderer(renZX);
		planeWidgetZX->SetDefaultRenderer(renZX);
		

	}
	
	planeWidgetXY->SetInteractor(interactor);
	planeWidgetYZ->SetInteractor(interactor);
	planeWidgetZX->SetInteractor(interactor);

	if (firstFourCamera) {
		firstFourCamera = false;
		
		setRectanglesOn(50);
		setRectanglesOff();
	}
	
}

void RectangleHandler::clearMove()
{
	planeWidgetXY->SetNormalToZAxis(true);
	planeWidgetXY->SetNormal(0, 0, 1);

	planeWidgetYZ->SetNormalToZAxis(true);
	planeWidgetYZ->SetNormal(0, 0, 1);

	planeWidgetZX->SetNormalToZAxis(true);
	planeWidgetZX->SetNormal(0, 0, 1);

	
	//functionCallbackRectangleXY();

}

void RectangleHandler::setActors(vtkSmartPointer<vtkProp3D > actor1, vtkSmartPointer<vtkProp3D > actor2, vtkSmartPointer<vtkProp3D> actor3, double* bounds)
{
	
	if (actor1 != nullptr)
	{
		planeWidgetXY->SetProp3D(actor1);

		boundsXY = actor1->GetBounds();

		if (bounds == nullptr) {
			double* bounds1 = actor1->GetBounds();

			planeWidgetXY->SetPlaceFactor(1);

			planeWidgetXY->PlaceWidget(bounds1);
		}
		else {
			double bounds1[6] = {
				bounds[0] + (boundsXY[1] - boundsXY[0]) / 2,
				bounds[1] + (boundsXY[1] - boundsXY[0]) / 2,
				bounds[2] + (boundsXY[3] - boundsXY[2]) / 2,
				bounds[3] + (boundsXY[3] - boundsXY[2]) / 2,
				actor1->GetBounds()[4],
				actor1->GetBounds()[5]
			};
			/*/
				bounds1[0] - (boundsXY[1] - boundsXY[0]) / 2,
				bounds1[1] - (boundsXY[1] - boundsXY[0]) / 2,
				bounds1[2] - (boundsXY[3] - boundsXY[2]) / 2,
				bounds1[3] - (boundsXY[3] - boundsXY[2]) / 2,
				-(bounds2[3] - (boundsYZ[3] - boundsYZ[2]) / 2),
				-(bounds2[2] - (boundsYZ[3] - boundsYZ[2]) / 2)
			/**/
			planeWidgetXY->SetPlaceFactor(1);

			planeWidgetXY->PlaceWidget(bounds1);
		}
			

	}
	
	//YZ
	if (actor2 != nullptr)
	{
		planeWidgetYZ->SetProp3D(actor2);
		//double* bounds2 = actor2->GetBounds();

		boundsYZ = actor2->GetBounds();
		if (bounds == nullptr) {
			double* bounds2 = actor2->GetBounds();

			planeWidgetYZ->SetPlaceFactor(1);

			planeWidgetYZ->PlaceWidget(bounds2);
		}
		else {
			double bounds2[6] = {
				bounds[2] + (boundsYZ[1] - boundsYZ[0]) / 2,
				bounds[3] + (boundsYZ[1] - boundsYZ[0]) / 2,
				-(bounds[5] - (boundsYZ[3] - boundsYZ[2]) / 2),
				-(bounds[4] - (boundsYZ[3] - boundsYZ[2]) / 2),
				actor2->GetBounds()[4],
				actor2->GetBounds()[5]
			};
			planeWidgetYZ->SetPlaceFactor(1);

			planeWidgetYZ->PlaceWidget(bounds2);
		}

		
	}
	//ZX
	if (actor3 != nullptr)
	{
		planeWidgetZX->SetProp3D(actor3);
		//double* bounds3 = actor3->GetBounds();

		boundsZX = actor3->GetBounds();
		if (bounds == nullptr) {
			double* bounds3 = actor3->GetBounds();

			planeWidgetZX->SetPlaceFactor(1);

			planeWidgetZX->PlaceWidget(bounds3);
		}
		else {
			double bounds3[6] = {
				bounds[0] + (boundsZX[1] - boundsZX[0]) / 2,
				bounds[1] + (boundsZX[1] - boundsZX[0]) / 2,
				-(bounds[5] - (boundsZX[3] - boundsZX[2]) / 2),
				-(bounds[4] - (boundsZX[3] - boundsZX[2]) / 2),
				actor3->GetBounds()[4],
				actor3->GetBounds()[5]
			};
			planeWidgetZX->SetPlaceFactor(1);

			planeWidgetZX->PlaceWidget(bounds3);
		}

		
	}
	
}

void RectangleHandler::setRectanglesOn(double pos)
{
	
	planeWidgetXY->On(); // Turn on the interactor observer
	planeWidgetYZ->On(); // Turn on the interactor observer
	planeWidgetZX->On(); // Turn on the interactor observer

	
	vtkSmartPointer<vtkActor> last1 = renXY->GetActors()->GetLastActor();
	renXY->RemoveActor(renXY->GetActors()->GetLastActor());
	
	while (renXY->GetActors()->GetNumberOfItems() > 0) 
	{
		renXY->RemoveActor(renXY->GetActors()->GetLastActor());
	}

	renXY->AddActor(last1);

	
	//renXY->AddActor(last1);
	last1->SetPosition(0, 0, pos);
	
	vtkSmartPointer<vtkActor> last2 = renYZ->GetActors()->GetLastActor();

	while (renYZ->GetActors()->GetNumberOfItems() > 0) {

		renYZ->RemoveActor(renYZ->GetActors()->GetLastActor());
	}

	renYZ->AddActor(last2);
	last2->SetPosition(0, 0, pos);

	vtkSmartPointer<vtkActor> last3 = renZX->GetActors()->GetLastActor();

	while (renZX->GetActors()->GetNumberOfItems() > 0) {
		renZX->RemoveActor(renZX->GetActors()->GetLastActor());
	}

	
	renZX->AddActor(last3);
	last3->SetPosition(0, 0, pos);

	enabled = true;
}

void RectangleHandler::setRectanglesOff()
{
	planeWidgetXY->Off(); // Turn off the interactor observer
	planeWidgetYZ->Off(); // Turn off the interactor observer
	planeWidgetZX->Off(); // Turn off the interactor observer
	enabled = false;
}

bool RectangleHandler::isOn()
{
	return enabled;
}




