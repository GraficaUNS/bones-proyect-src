#pragma once
#ifndef ACERCADE_H
#define ACERCADE_H

#include <QDialog>

namespace Ui {
	class AcercaDe;
}

class acercaDe : public QDialog
{
	Q_OBJECT

public:
	explicit acercaDe(QWidget *parent = 0);
	~acercaDe();

	private slots:
	void on_botonCerrar_clicked();


private:
	Ui::AcercaDe *ui;
};

#endif // ACERCADE_H
