#include <vtkVolume.h>
#include <vtkVolumeProperty.h>
#include <vtkPiecewiseFunction.h>
#include <vtkSmartPointer.h>
#include <vtkColorTransferFunction.h>
#include <vtkDICOMImageReader.h>
#include "CallbacksClass.h"
#include <vtkCallbackCommand.h>
#include "Consola.h"
#include <vtkImageData.h>
#include <vtkTransform.h>
//#include <vtkOpenGLGPUVolumeRayCastMapper.h>
#include <vtkOpenGLGPUVolumeRayCastMapper.h>
#include <vtkDICOMReader.h>
#include <vtkDICOMMetaData.h>
#include <vtkImageActor.h> 
#include <vtkImageSliceMapper.h>
#include <vtkPlaneCollection.h>

#include <qdir.h>
#include <qdiriterator.h>

#include "myVolume.h"
#include <qvector.h>

#include "Paciente.h"
#include "MasterHandler.h"

double Opacity_Y_Default[NUMERODEPUNTOS_SCALAR] = {
	0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
	0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
	0,    0,    0,    0,    0,    1,    1,    1,    1,    1,
	1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
	1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
	1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
	1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
	0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
	0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
	0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
	0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
	0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
	0,    0,    0,    0,    0,    1,    1,    1,    1,    1,
	1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
	1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
	1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
	1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
	1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
	1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
	1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
	1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
	1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
	1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
	1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
	1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
	1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
	1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
	1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
	1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
	1,    1,    1,    1,    1,    1,    1,    1,    1,    1,

	1
};

//HAY QUE RECALCULAR LOS VALORES DE LA FUNCION TRANSFERENCIA

double colorDefault[CANT_COLOR*CANT_COMPONENTS_COLOR] = {
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,//10
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,//20
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,//30
		0,0,0,
		0,0,0,
		194.0f / 255.0f, 105.0f / 255.0f, 82.0f / 255.0f,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,//40
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,	
		194.0f / 255.0f, 105.0f / 255.0f, 82.0f / 255.0f,//50
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,//60	
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,//70
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		194.0f / 255.0f, 166.0f / 255.0f, 115.0f / 255.0f,
		0,0,0,
		0,0,0,
		194.0f / 255.0f, 166.0f / 255.0f, 115.0f / 255.0f,
		0,0,0,
		0,0,0,//80
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		102.0f / 255.0f, 0, 0,
		0,0,0,
		0,0,0,
		0,0,0,
		153.0f / 255.0f, 0, 0,//90
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,//100
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0.80, 0.80, 0.80,
		0,0,0,
		0,0,0,//110
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,//120
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0, 0, 0,
		0,0,0,//130
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0,0,0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
			0.80, 0.80, 0.80,
			0.80, 0.80, 0.80,
			0.80, 0.80, 0.80,
			0.80, 0.80, 0.80,
			0.80, 0.80, 0.80,
			0.80, 0.80, 0.80,

			0.80, 0.80, 0.80,
			0.80, 0.80, 0.80,
};

int colorDefaultIndexActive[CANT_COLOR_INDEX_ACTIVE_DEFAULT] = {
	//0,5,13,16,20,26,49
	((-1000) + 1000) / STEP_COLOR,
	((-600) + 1000) / STEP_COLOR,
	((-400) + 1000) / STEP_COLOR,
	((-100) + 1000) / STEP_COLOR,
	((-60) + 1000) / STEP_COLOR,
	((40) + 1000) / STEP_COLOR,
	((80) + 1000) / STEP_COLOR,
	((300) + 1000) / STEP_COLOR,
	((1999) + 1000) / STEP_COLOR,
};


vtkSmartPointer<vtkVolumeProperty> myVolume::initVolumeProperty() {

	vec_opacity_x = new QVector<double>(NUMERODEPUNTOS_SCALAR);
	vec_opacity_y = new QVector<double>(NUMERODEPUNTOS_SCALAR);

	vec_gradient_x = new QVector<double>(NUMERODEPUNTOS_GRADIENTE);
	vec_gradient_y = new QVector<double>(NUMERODEPUNTOS_GRADIENTE);

	vec_color = new QVector<double>(CANT_COLOR*CANT_COMPONENTS_COLOR);
	
	vtkSmartPointer<vtkVolumeProperty> volumeProperty = vtkSmartPointer<vtkVolumeProperty>::New();

	volumeProperty->ShadeOn();
	volumeProperty->SetInterpolationTypeToLinear();

	volumeProperty->SetAmbient(Default_Ambient);
	volumeProperty->SetDiffuse(Default_Diffuse);
	volumeProperty->SetSpecular(Default_Specular);
	volumeProperty->SetSpecularPower(Default_SpecularPower);

	vtkSmartPointer<vtkPiecewiseFunction> scalarOpacity = vtkSmartPointer<vtkPiecewiseFunction>::New();
	vtkSmartPointer<vtkPiecewiseFunction> gradientOpacity = vtkSmartPointer<vtkPiecewiseFunction>::New();
	color = vtkSmartPointer<vtkColorTransferFunction>::New();
	colorSelected = vtkSmartPointer<vtkColorTransferFunction>::New();

	for (int i = 0; i < NUMERODEPUNTOS_SCALAR; i++)
	{
		int value_x = -INIT_TRANSFER_FUNCTION_OPACITY_VALUE + i * STEP_TRANSFER_FUNCTION;
		int value_y = Opacity_Y_Default[i];
		(*vec_opacity_x)[i] = value_x;//i / (double)(x1.size() - 1) * 10;
		(*vec_opacity_y)[i] = value_y;//qCos(x1[i] * 0.8 + qSin(x1[i] * 0.16 + 1.0))*qSin(x1[i] * 0.54) + 1.4;
		scalarOpacity->AddPoint(value_x, value_y);
	}

	scalarOpacity->AddPoint(SELECT_CELL, 1);

	int j = 0;
	for (int i = 0; i<NUMERODEPUNTOS_GRADIENTE; i++)
	{

		int value_x = -INIT_TRANSFER_FUNCTION_GRADIENT_VALUE + i * STEP_TRANSFER_FUNCTION;
		double value_y = 1;

		if (value_x >= 0 && value_x <= 1500) {
			value_y = (1.0f / 1500.0f)*(j);
			j += STEP_TRANSFER_FUNCTION;
		}
		else if(value_x < 0)
			value_y = 0;

		(*vec_gradient_x)[i] = value_x;//i / (double)(x1.size() - 1) * 10;
		(*vec_gradient_y)[i] = value_y;//qCos(x1[i] * 0.8 + qSin(x1[i] * 0.16 + 1.0))*qSin(x1[i] * 0.54) + 1.4;
		gradientOpacity->AddPoint(value_x, value_y);
	}

	gradientOpacity->AddPoint(SELECT_CELL, 1);

	color->RemoveAllPoints();

	for (int i = 0; i < CANT_COLOR; i++) {
		//(*vec_color)[i * 3 + 0] = colorDefault[i * 3 + 0];
		//(*vec_color)[i * 3 + 1] = colorDefault[i * 3 + 1];
		//(*vec_color)[i * 3 + 2] = colorDefault[i * 3 + 2];
		if (colorArrayActive[i]) {
			addColorPoint(i, colorDefault[i * 3 + 0] * 255, colorDefault[i * 3 + 1] * 255, colorDefault[i * 3 + 2] * 255);
			//int valueX = i * STEP_COLOR - INIT_VALUE_COLOR;
			//color->AddRGBPoint(valueX, colorDefault[i * 3 + 0], colorDefault[i * 3 + 1], colorDefault[i * 3 + 2]);
		}
	}
	color->AddRGBPoint(2000, 0.80, 0.80, 0.80);
	color->AddRGBPoint(SELECT_CELL, 0.005, 0.98, 0.98);
	color->AddRGBPoint(SELECT_CELL*2, 0.005, 0.98, 0.98);

	/*
	//color->AddRGBPoint(-1500.0, 0, 0, 0);
	color->AddRGBPoint(-1000.0, 0, 0.28, 0.5);
	color->AddRGBPoint(-750.0, 0.08, 0.05, 0.03);
	color->AddRGBPoint(-350.0, 0.39, 0.25, 0.16);
	color->AddRGBPoint(-200.0, 0.30, 0.1, 0.1);
	color->AddRGBPoint(0.0, 0.30, 0.10, 0.10);
	color->AddRGBPoint(300.0, 0.70, 0.70, 0.70);
	//color->AddRGBPoint(0, 0.80, 0.80, 0.80);
	color->AddRGBPoint(2750.0, 0.70, 0.70, 0.70);
	color->AddRGBPoint(3000.0, 0.35, 0.35, 0.35);
	/**/
	colorSelected->AddRGBPoint(-750.0, 0.08, 0.95, 0.95);
	colorSelected->AddRGBPoint(-350.0, 0.08, 0.95, 0.95);
	colorSelected->AddRGBPoint(-200.0, 0.08, 0.80, 0.80);
	colorSelected->AddRGBPoint(2750.0, 0.08, 0.70, 0.70);
	colorSelected->AddRGBPoint(3000.0, 0.08, 0.35, 0.35);



	volumeProperty->SetScalarOpacity(scalarOpacity);
	volumeProperty->SetGradientOpacity(gradientOpacity);
	volumeProperty->SetColor(color);

	for (int i = 0; i < NUMBEROFTRANSFERFUNCTIONS; i++) {
		pixArray[i] = nullptr;
	}

	return volumeProperty;
}


//este constructor asume que el volumen esta inicializado externamente (datos) 
//siempre se inicializa con la misma funcion transferencia

myVolume::myVolume() : vtkVolume()
{

	imageActorXY = nullptr;
	imageActorYZ = nullptr;
	imageActorZX = nullptr;

	vtkSmartPointer<vtkVolumeProperty> volumeProperty = initVolumeProperty();

	SetProperty(volumeProperty);

	paciente = nullptr;

	volumeMapper = vtkSmartPointer<vtkOpenGLGPUVolumeRayCastMapper>::New();

	SetMapper(volumeMapper);

	//planeCollection = vtkSmartPointer<vtkPlaneCollection>::New();
}

myVolume::myVolume(vtkSmartPointer<vtkImageData> img): vtkVolume()
{

	imageActorXY = nullptr;
	imageActorYZ = nullptr;
	imageActorZX = nullptr;

	vtkSmartPointer<vtkVolumeProperty> volumeProperty = initVolumeProperty();

	SetProperty(volumeProperty);

	imageData = img;

	volumeMapper = vtkSmartPointer<vtkOpenGLGPUVolumeRayCastMapper>::New();

	volumeMapper->SetInputData(imageData);

	double* objectCenter = volumeMapper->GetCenter();

	vtkSmartPointer<vtkTransform> translation = vtkSmartPointer<vtkTransform>::New();
	translation->Translate(-objectCenter[0], -objectCenter[1], -objectCenter[2]);

	SetMapper(volumeMapper);
	SetUserTransform(translation);

	paciente = nullptr;

	//planeCollection = vtkSmartPointer<vtkPlaneCollection>::New();
}

#include <vtkDICOMDirectory.h>

myVolume::myVolume(std::string path): vtkVolume()
{
	directory_path = path;

	vtkSmartPointer<vtkVolumeProperty> volumeProperty = initVolumeProperty();

	SetProperty(volumeProperty);

	const char * dir = path.data();

	imageData = vtkSmartPointer<vtkImageData>::New();

	imageActorXY = nullptr;
	imageActorYZ = nullptr;
	imageActorZX = nullptr;

	vtkSmartPointer<vtkDICOMReader> reader = vtkSmartPointer<vtkDICOMReader>::New();
	/*vtkSmartPointer<vtkOpenGLGPUVolumeRayCastMapper>/**/ volumeMapper = vtkSmartPointer<vtkOpenGLGPUVolumeRayCastMapper>::New();

	//volumeMapper->SetBlendMode(vtkSmartPointer<vtkVolumeMapper::COMPOSITE_BLEND);


	vtkNew<vtkDICOMDirectory> dicomdir;
	dicomdir->SetDirectoryName(path.data());
	dicomdir->RequirePixelDataOn();
	dicomdir->Update();
	int n = dicomdir->GetNumberOfSeries();
	//vtkNew<vtkDICOMReader> reader;
	//reader->SetDirectoryName(dir);
	vtkSmartPointer<vtkCallbackCommand> m_vtkCallback = CallbacksClass::getProgressCallBack();
	if (m_vtkCallback.Get() != NULL)
	{
		CallbacksClass::setEncabezado("Cargando imagenes DICOM: ");
		reader->AddObserver(vtkCommand::ProgressEvent, m_vtkCallback);
	}

	if (n > 0)
	{
		// read the first series found
		reader->SetFileNames(dicomdir->GetFileNamesForSeries(0));
		reader->Update();
		imageData->DeepCopy(reader->GetOutput());
	}
	else
	{
		std::cerr << "No DICOM images in directory!" << std::endl;
	}
	
	//reader->Update();

	

	int* dims = imageData->GetDimensions();
	if (dims[0] < 1 || dims[1] < 1 || dims[2] < 1) {
		imageData = nullptr;
		return;
	}

	//volumeMapper->SetBlendModeToIsoSurface();
	//volumeMapper->SetRequestedRenderModeToGPU();
	volumeMapper->SetInputData(imageData);


	double* objectCenter = volumeMapper->GetCenter();

	vtkSmartPointer<vtkTransform> translation = vtkSmartPointer<vtkTransform>::New();
	translation->Translate(-objectCenter[0], -objectCenter[1], -objectCenter[2]);

	SetMapper(volumeMapper);
	SetUserTransform(translation);
	/**
	QDir directory(path.c_str());
	QDirIterator iterator(directory);
	//primero .
	iterator.next();
	//segundo ..
	iterator.next();
	//tercero DIRFILE
	iterator.next();
	//cuarto primera imagen DICOM


	vtkSmartPointer<vtkDICOMReader> reader2 =
		vtkSmartPointer<vtkDICOMReader>::New();

	//reader->GetFileName();
	
	//reader2->set
	reader2->SetFileName(iterator.next().toStdString().c_str());
	reader2->Update();

	/**/

	vtkSmartPointer<vtkDICOMMetaData > meta = reader->GetMetaData();

	vtkDICOMValue photometric = meta->Get(DC::PhotometricInterpretation);
	if (photometric.Matches("MONOCHROME1"))
	{
		// display with a lookup table that goes from white to black
		printf("MONOCHROME1\n");
	}
	else if (photometric.Matches("MONOCHROME2"))
	{
		// display with a lookup table that goes from black to white,
		// or display with a suitable pseudocolor lookup table
		printf("MONOCHROME2\n");

	}
	else if (photometric.Matches("PALETTE*"))
	{
		// display with palette lookup table (see vtkDICOMLookupTable),
		// or convert to RGB with vtkDICOMApplyPalette
		printf("PALETTE\n");

	}
	else if (photometric.Matches("RGB*") || photometric.Matches("YBR*"))
	{
		// display RGB data directly
		printf("RGB\n");

	}


	vtkDICOMValue name = (meta->GetAttributeValue(DC::PatientName));
	vtkDICOMValue StudyUID = (meta->GetAttributeValue(DC::StudyInstanceUID));
	vtkDICOMValue age = (meta->GetAttributeValue(DC::PatientAge));
	vtkDICOMValue id = (meta->GetAttributeValue(DC::PatientID));
	vtkDICOMValue birthdate = (meta->GetAttributeValue(DC::PatientBirthDate));
	vtkDICOMValue birthname = (meta->GetAttributeValue(DC::PatientBirthName));
	vtkDICOMValue birthtime = (meta->GetAttributeValue(DC::PatientBirthTime));
	vtkDICOMValue address = (meta->GetAttributeValue(DC::PatientAddress));
	vtkDICOMValue sexo = (meta->GetAttributeValue(DC::PatientSex));
	vtkDICOMValue comments = (meta->GetAttributeValue(DC::PatientComments));
	vtkDICOMValue size = (meta->GetAttributeValue(DC::PatientSize));
	vtkDICOMValue weight = (meta->GetAttributeValue(DC::PatientWeight));
	vtkDICOMValue studyID = (meta->GetAttributeValue(DC::StudyID));
	vtkDICOMValue description = (meta->GetAttributeValue(DC::StudyDescription));
	vtkDICOMValue studydate = (meta->GetAttributeValue(DC::StudyDate));
	vtkDICOMValue studytime = (meta->GetAttributeValue(DC::StudyTime));
	vtkDICOMValue accessionnumber = (meta->GetAttributeValue(DC::AccessionNumber));
	vtkDICOMValue referringphysicianname = (meta->GetAttributeValue(DC::ReferringPhysicianName));
	vtkDICOMValue performingphysicianname = (meta->GetAttributeValue(DC::PerformingPhysicianName));
	vtkDICOMValue modality = (meta->GetAttributeValue(DC::Modality));
	vtkDICOMValue additionalpatienthistory = (meta->GetAttributeValue(DC::AdditionalPatientHistory));
	vtkDICOMValue allergies = (meta->GetAttributeValue(DC::Allergies));
	vtkDICOMValue currentpatientlocation = (meta->GetAttributeValue(DC::CurrentPatientLocation));
	vtkDICOMValue modalitiesinstudy = (meta->GetAttributeValue(DC::ModalitiesInStudy));
	vtkDICOMValue institutionname = (meta->GetAttributeValue(DC::InstitutionName));
	vtkDICOMValue personname = (meta->GetAttributeValue(DC::PersonName));
	vtkDICOMValue persontelephonenumbers = (meta->GetAttributeValue(DC::PersonTelephoneNumbers));

	paciente = new Paciente();

	paciente->setPatient(StudyUID, name, age, id, birthdate, birthname,
		birthtime, address, sexo, comments,
		size, weight, studyID, description, studydate, studytime,
		accessionnumber, referringphysicianname, performingphysicianname, modality,
		additionalpatienthistory, allergies, currentpatientlocation,
		modalitiesinstudy, institutionname, personname,
		persontelephonenumbers);

	
	/**/
	//imageData->DeepCopy(reader2->GetOutput());

}

myVolume::~myVolume()
{
	
	imageActorXY = nullptr;
	imageActorYZ = nullptr;
	imageActorZX = nullptr;

	volumenXY = nullptr;
	volumenYZ = nullptr;
	volumenZX = nullptr;
	/**/
	if(vec_opacity_x)
		delete vec_opacity_x;
	if(vec_opacity_y)
		delete vec_opacity_y;
	if(vec_gradient_x)
		delete vec_gradient_x;
	if(vec_gradient_y)
		delete vec_gradient_y;
	if (vec_color)
		delete vec_color;

	for (int i = 0; i < NUMBEROFTRANSFERFUNCTIONS; i++) {
		if(pixArray[i])
			delete pixArray[i];
	}

	if(planeCollection)
		planeCollection->RemoveAllItems();

	volumeMapper = nullptr;

}

void myVolume::SetBounds(double bnds[6])
{
	if (bounds == nullptr)
		bounds = new double[6];
	bounds[0] = bnds[0];
	bounds[1] = bnds[1];
	bounds[2] = bnds[2];
	bounds[3] = bnds[3];
	bounds[4] = bnds[4];
	bounds[5] = bnds[5];
}

double * myVolume::getCustomBounds()
{
	return bounds;
}

void myVolume::deletePoint2D(int x, int y)
{
	int z1 = getImageActorXY()->GetZSlice();

	double d = getImageData()->GetScalarComponentAsDouble(x, y, z1, 0);

	if (d > RADIO_DENSITY_ZERO) {//si fue 0 es porque toque un punto que no debia tocar

		puntosSeleccionados.append(x);
		puntosSeleccionados.append(y);
		puntosSeleccionados.append(z1);
		puntosSeleccionados.append(d);
		getImageData()->SetScalarComponentFromDouble(x, y, z1, 0, RADIO_DENSITY_ZERO);

		getImageData()->Modified();

		//imageActorXY->SetZSlice(z + 1);
		imageActorYZ->SetZSlice(x);
		imageActorZX->SetZSlice(y);


	}
}

void myVolume::selectPoint(int x, int y, int z)
{

	double d = getImageData()->GetScalarComponentAsDouble(x, y, z + 1, 0);

	{//si fue 0 es porque toque un punto que no debia tocar

		puntosSeleccionados.append(x);
		puntosSeleccionados.append(y);
		puntosSeleccionados.append(z + 1);
		puntosSeleccionados.append(d);
		getImageData()->SetScalarComponentFromDouble(x, y, z + 1, 0, SELECT_CELL * 3);

		getImageData()->Modified();

		//imageActorXY->SetZSlice(z + 1);
		//imageActorYZ->SetZSlice(x);
		//imageActorZX->SetZSlice(y);


	}

}

void myVolume::removeSelectedPoints()
{
#define COMP_X i*4
#define COMP_Y i*4 + 1
#define COMP_Z i*4 + 2

	int cant = puntosSeleccionados.size() / 4;
	int x, y, z;
	for (int i = 0; i < cant; i++) {
		x = puntosSeleccionados.value(COMP_X);
		y = puntosSeleccionados.value(COMP_Y);
		z = puntosSeleccionados.value(COMP_Z);
		getImageData()->SetScalarComponentFromDouble(x, y, z, 0, RADIO_DENSITY_ZERO);
	}
	getImageData()->Modified();

}

void myVolume::SetAmbient(double a)
{
	if (a >= 0) {
		this->GetProperty()->SetAmbient(a);
	}
}

void myVolume::SetDiffuse(double d)
{
	if (d >= 0)
		this->GetProperty()->SetDiffuse(d);
}

void myVolume::SetSpecular(double s)
{
	if (s >= 0)
		this->GetProperty()->SetSpecular(s);
}

void myVolume::SetSpecularPower(double sp)
{
	if (sp >= 0)
		this->GetProperty()->SetSpecularPower(sp);
}

double myVolume::GetAmbient()
{
	return this->GetProperty()->GetAmbient();
}

double myVolume::GetDiffuse()
{
	return this->GetProperty()->GetDiffuse();
}

double myVolume::GetSpecular()
{
	return this->GetProperty()->GetSpecular();
}

double myVolume::GetSpecularPower()
{
	return this->GetProperty()->GetSpecularPower();
}

void myVolume::restoreValues() {

	for (int i = 0; i < NUMERODEPUNTOS_SCALAR; i++)
	{
		int value_x = -INIT_TRANSFER_FUNCTION_OPACITY_VALUE + i * STEP_TRANSFER_FUNCTION;
		int value_y = Opacity_Y_Default[i];
		(*vec_opacity_x)[i] = value_x;//i / (double)(x1.size() - 1) * 10;
		(*vec_opacity_y)[i] = value_y;//qCos(x1[i] * 0.8 + qSin(x1[i] * 0.16 + 1.0))*qSin(x1[i] * 0.54) + 1.4;
		GetProperty()->GetScalarOpacity()->AddPoint(value_x, value_y);
	}

	int j = 0;
	for (int i = 0; i<NUMERODEPUNTOS_GRADIENTE; i++)
	{

		int value_x = -INIT_TRANSFER_FUNCTION_GRADIENT_VALUE + i * STEP_TRANSFER_FUNCTION;
		double value_y = 1;

		if (value_x >= 0 && value_x <= 1500) {
			value_y = (1.0f / 1500.0f)*(j);
			j += STEP_TRANSFER_FUNCTION;
		}
		else if(value_x < 0) 
			value_y = 0;

		(*vec_gradient_x)[i] = value_x;//i / (double)(x1.size() - 1) * 10;
		(*vec_gradient_y)[i] = value_y;//qCos(x1[i] * 0.8 + qSin(x1[i] * 0.16 + 1.0))*qSin(x1[i] * 0.54) + 1.4;
		GetProperty()->GetGradientOpacity()->AddPoint(value_x, value_y);
	}

	color->RemoveAllPoints();
	j = 0;
	for (int i = 0; i < CANT_COLOR; i++) {
		if (j< CANT_COLOR_INDEX_ACTIVE_DEFAULT && i == colorDefaultIndexActive[j]) {
			
			j++;
			colorArrayActive[i] = true;
		}
		else {
			colorArrayActive[i] = false;
			
		}
	}

	for (int i = 0; i < CANT_COLOR; i++) {
		

		//(*vec_color)[i * 3 + 0] = colorDefault[i * 3 + 0];
		//(*vec_color)[i * 3 + 1] = colorDefault[i * 3 + 1];
		//(*vec_color)[i * 3 + 2] = colorDefault[i * 3 + 2];
		if (colorArrayActive[i]) {
			addColorPoint(i, colorDefault[i * 3 + 0] * 255, colorDefault[i * 3 + 1] * 255, colorDefault[i * 3 + 2] * 255);
		//	int valueX = i * STEP_COLOR - INIT_VALUE_COLOR;
			//color->AddRGBPoint(valueX, colorDefault[i * 3 + 0], colorDefault[i * 3 + 1], colorDefault[i * 3 + 2]);
		}
	}

	color->AddRGBPoint(SELECT_CELL, 0.005, 0.98, 0.98);
	color->AddRGBPoint(SELECT_CELL * 2, 0.005, 0.98, 0.98);

	vtkSmartPointer<vtkVolumeProperty> volumeProperty = this->GetProperty();

	volumeProperty->SetAmbient(Default_Ambient);
	volumeProperty->SetDiffuse(Default_Diffuse);
	volumeProperty->SetSpecular(Default_Specular);
	volumeProperty->SetSpecularPower(Default_SpecularPower);

	/*
	color->AddRGBPoint(-1500.0, 0, 0, 0);
	color->AddRGBPoint(-1000.0, 0, 0.28, 0.5);
	color->AddRGBPoint(-750.0, 0.08, 0.05, 0.03);
	color->AddRGBPoint(-350.0, 0.39, 0.25, 0.16);
	color->AddRGBPoint(-200.0, 0.30, 0.1, 0.1);
	color->AddRGBPoint(0.0, 0.30, 0.10, 0.10);
	color->AddRGBPoint(300.0, 0.70, 0.70, 0.70);
	//color->AddRGBPoint(0, 0.80, 0.80, 0.80);
	color->AddRGBPoint(2750.0, 0.70, 0.70, 0.70);
	color->AddRGBPoint(3000.0, 0.35, 0.35, 0.35);
	/**/
}

QVector<double>* myVolume::GetOpacityFunction_X()
{
	return vec_opacity_x;
}

QVector<double>* myVolume::GetOpacityFunction_Y()
{
	return vec_opacity_y;
}

QVector<double>* myVolume::GetGradientFunction_X()
{
	return vec_gradient_x;
}

QVector<double>* myVolume::GetGradientFunction_Y()
{
	return vec_gradient_y;
}

QVector<double>* myVolume::GetColorFunction()
{
	return vec_color;
}

void myVolume::addOpacityPoint(int index_op, double value)
{

	if (index_op >= NUMERODEPUNTOS_SCALAR)
		index_op = NUMERODEPUNTOS_SCALAR - 1;
	if (index_op < 0)
		index_op = 0;

	if (value > 1)
		value = 1;

	if (value <= 0.0f)
		value = 0;

	GetProperty()->GetScalarOpacity()->AddPoint((*vec_opacity_x)[index_op], value);//AddPoint(PointX[index], newValue);

	Update();

	(*vec_opacity_y)[index_op] = value;
}

void myVolume::addGradientPoint(int index_gra, double value)
{
	if (index_gra >= NUMERODEPUNTOS_GRADIENTE)
		index_gra = NUMERODEPUNTOS_GRADIENTE - 1;
	if (index_gra < 0)
		index_gra = 0;

	if (value > 1)
		value = 1;


	if (value <= 0.0f)
		value = 0;

	GetProperty()->GetScalarOpacity()->AddPoint((*vec_gradient_x)[index_gra], value);//AddPoint(PointX[index], newValue);

	Update();

	(*vec_gradient_y)[index_gra] = value;

}

void myVolume::addColorPoint(int index, int R, int G, int B)
{

	color->AddRGBPoint(index * STEP_COLOR - INIT_VALUE_COLOR, R / 255.0f, G / 255.0f, B / 255.0f );//AddPoint(PointX[index], newValue);

	colorArrayActive[index] = true;

	int prevIndex, nextIndex;

	for (prevIndex = index - 1; prevIndex > 0 && !colorArrayActive[prevIndex]; prevIndex--);
	for (nextIndex = index +1 ; nextIndex < CANT_COLOR - 1 && !colorArrayActive[nextIndex]; nextIndex++);

	Update();
	//init + (fin-init)/(indexfin-indexinit)
	(*vec_color)[index*CANT_COMPONENTS_COLOR + 0] = R / 255.0f;
	(*vec_color)[index*CANT_COMPONENTS_COLOR + 1] = G / 255.0f;
	(*vec_color)[index*CANT_COMPONENTS_COLOR + 2] = B / 255.0f;

	double Rc, Gc, Bc;

	for (int i = prevIndex + 1; i < index; i++) {
		
		Rc = (*vec_color)[prevIndex*CANT_COMPONENTS_COLOR + 0] +
			(i - prevIndex) * ((*vec_color)[index*CANT_COMPONENTS_COLOR + 0] - (*vec_color)[prevIndex*CANT_COMPONENTS_COLOR + 0])/(index - prevIndex);
		(*vec_color)[i*CANT_COMPONENTS_COLOR + 0] = Rc;

		Gc = (*vec_color)[prevIndex*CANT_COMPONENTS_COLOR + 1] +
			(i - prevIndex) * ((*vec_color)[index*CANT_COMPONENTS_COLOR + 1] - (*vec_color)[prevIndex*CANT_COMPONENTS_COLOR + 1]) / (index - prevIndex);
		(*vec_color)[i*CANT_COMPONENTS_COLOR + 1] = Gc;

		Bc = (*vec_color)[prevIndex*CANT_COMPONENTS_COLOR + 2] +
			(i-prevIndex) * ((*vec_color)[index*CANT_COMPONENTS_COLOR + 2] - (*vec_color)[prevIndex*CANT_COMPONENTS_COLOR + 2]) / (index - prevIndex);
		(*vec_color)[i*CANT_COMPONENTS_COLOR + 2] = Bc;
	}

	for (int i = index + 1; i < nextIndex; i++) {

		Rc = (*vec_color)[index*CANT_COMPONENTS_COLOR + 0] +
			(i - index) * ((*vec_color)[nextIndex*CANT_COMPONENTS_COLOR + 0] - (*vec_color)[index*CANT_COMPONENTS_COLOR + 0]) / (nextIndex - index);
		(*vec_color)[i*CANT_COMPONENTS_COLOR + 0] = Rc;

		Gc = (*vec_color)[index*CANT_COMPONENTS_COLOR + 1] +
			(i - index) * ((*vec_color)[nextIndex*CANT_COMPONENTS_COLOR + 1] - (*vec_color)[index*CANT_COMPONENTS_COLOR + 1]) / (nextIndex - index);
		(*vec_color)[i*CANT_COMPONENTS_COLOR + 1] = Gc;

		Bc = (*vec_color)[index*CANT_COMPONENTS_COLOR + 2] +
			(i - index) * ((*vec_color)[nextIndex*CANT_COMPONENTS_COLOR + 2] - (*vec_color)[index*CANT_COMPONENTS_COLOR + 2]) / (nextIndex - index);
		(*vec_color)[i*CANT_COMPONENTS_COLOR + 2] = Bc;
	}

}

void myVolume::removeColorPoint(int index)
{
	colorArrayActive[index] = false;

	color->RemoveAllPoints();

	for (int i = 0; i < CANT_COLOR; i++) {

		if (colorArrayActive[i]) {
			int valueX = i * STEP_COLOR - INIT_VALUE_COLOR;
			color->AddRGBPoint(valueX, (*vec_color)[i * 3 + 0], (*vec_color)[i * 3 + 1], (*vec_color)[i * 3 + 2]);
		}
	}

	int prevIndex, nextIndex;

	for (prevIndex = index - 1; prevIndex > 0 && !colorArrayActive[prevIndex]; prevIndex--);
	for (nextIndex = index + 1; nextIndex < CANT_COLOR - 1 && !colorArrayActive[nextIndex]; nextIndex++);

	//hago interpolacion entre el prev y el next
	double Rc, Gc, Bc;
	for (int i = prevIndex + 1; i < nextIndex; i++) {

		Rc = (*vec_color)[prevIndex*CANT_COMPONENTS_COLOR + 0] +
			(i - prevIndex) * ((*vec_color)[nextIndex*CANT_COMPONENTS_COLOR + 0] - (*vec_color)[prevIndex*CANT_COMPONENTS_COLOR + 0]) / (nextIndex - prevIndex);
		(*vec_color)[i*CANT_COMPONENTS_COLOR + 0] = Rc;

		Gc = (*vec_color)[prevIndex*CANT_COMPONENTS_COLOR + 1] +
			(i - prevIndex) * ((*vec_color)[nextIndex*CANT_COMPONENTS_COLOR + 1] - (*vec_color)[prevIndex*CANT_COMPONENTS_COLOR + 1]) / (nextIndex - prevIndex);
		(*vec_color)[i*CANT_COMPONENTS_COLOR + 1] = Gc;

		Bc = (*vec_color)[prevIndex*CANT_COMPONENTS_COLOR + 2] +
			(i - prevIndex) * ((*vec_color)[nextIndex*CANT_COMPONENTS_COLOR + 2] - (*vec_color)[prevIndex*CANT_COMPONENTS_COLOR + 2]) / (nextIndex - prevIndex);
		(*vec_color)[i*CANT_COMPONENTS_COLOR + 2] = Bc;
	}
}

Paciente* myVolume::getPaciente(){
	return paciente;
}

void myVolume::setPaciente(Paciente * p)
{
	paciente = p;
}

void myVolume::setPicked(bool v)
{
	if (v) {
		GetProperty()->SetColor(colorSelected);
	}
	else {
		GetProperty()->SetColor(color);

	}
}

vtkSmartPointer<vtkImageData> myVolume::getImageData()
{
	return imageData;
}

vtkSmartPointer<vtkImageActor> myVolume::getImageActorXY()
{
	return imageActorXY;
}

vtkSmartPointer<vtkImageActor> myVolume::getImageActorYZ()
{
	return imageActorYZ;
}

vtkSmartPointer<vtkImageActor> myVolume::getImageActorZX()
{
	return imageActorZX;
}

void myVolume::setImageActorXY(vtkSmartPointer<vtkImageActor > act)
{
	imageActorXY = act;
}

void myVolume::setImageActorYZ(vtkSmartPointer<vtkImageActor > act)
{
	imageActorYZ = act;
}

void myVolume::setImageActorZX(vtkSmartPointer<vtkImageActor > act)
{
	imageActorZX = act;
}

vtkSmartPointer<sliceVolume> myVolume::getSliceVolumeXY()
{
	return volumenXY;
}

vtkSmartPointer<sliceVolume> myVolume::getSliceVolumeYZ()
{
	return volumenYZ;
}

vtkSmartPointer<sliceVolume> myVolume::getSliceVolumeZX()
{
	return volumenZX;
}

void myVolume::setSliceVolumeXY(vtkSmartPointer<sliceVolume> vol)
{
	volumenXY = vol;
}

void myVolume::setSliceVolumeYZ(vtkSmartPointer<sliceVolume> vol)
{
	volumenYZ = vol;

}

void myVolume::setSliceVolumeZX(vtkSmartPointer<sliceVolume> vol)
{
	volumenZX = vol;

}

void myVolume::SetCustomPort(int p)
{
	Aport = p;
}

int myVolume::GetCustomPort()
{
	return Aport;
}


bool * myVolume::getActiveArrayColor()
{
	return this->colorArrayActive;
}


void myVolume::centrar() {
	double* objectCenter = volumeMapper->GetCenter();
	vtkSmartPointer<vtkTransform> translation = vtkSmartPointer<vtkTransform>::New();
	translation->Translate(-objectCenter[0], -objectCenter[1], -objectCenter[2]);
	SetUserTransform(translation);
}

void myVolume::setPath(QString p) {
	path = p;
}

void myVolume::addClippingPlane(vtkSmartPointer<vtkPlane > plane)
{
	if (planeCollection == nullptr)
		planeCollection = vtkSmartPointer<vtkPlaneCollection>::New();
	planeCollection->AddItem(plane);
	GetMapper()->GetClippingPlanes()->AddItem(plane);
}

vtkSmartPointer<vtkPlaneCollection > myVolume::getClippingPlanes()
{
	return planeCollection;
}

QString myVolume::getPath() {
	return path;
}

void myVolume::addImage(QPixmap * pix, int i) {
	pixArray[i] = pix;
}

QPixmap * myVolume::getImage(int i) {
	return pixArray[i];
}

vtkSmartPointer<vtkColorTransferFunction> myVolume::GetColorTransferFunction()
{
	return color;
}

void myVolume::setIsSplitted()
{
	splitted = true;
}

bool myVolume::getIsSplitted()
{
	return splitted;
}
