#pragma once
#include <vtkActor.h>
#include "myVolume.h"
#include <vtkRenderWindow.h>

#define THRESHOLD 10

class VolumeToSurface 
{

public:
	VolumeToSurface();
	~VolumeToSurface();
	
	static void SetRenderWindow(vtkSmartPointer<vtkRenderWindow> r);

	static void ConvertVolume(vtkSmartPointer<myVolume> vol, double ratio);

	static bool extraerRango(int &min, int &max, vtkSmartPointer<myVolume> vol);

	static void convertToGCode(vtkSmartPointer<myVolume> vol, double presicion);

private:

	static void aplicarRango(vtkSmartPointer<vtkImageData> input, int min, int max, vtkSmartPointer<vtkImageData> output);

	static void extraerColor(double &R, double &G, double &B, int index, vtkSmartPointer<myVolume> vol);


};

