#ifndef CONTROLADOROBJETO_H
#define CONTROLADOROBJETO_H
#include <QDoubleSpinBox>
#include <QWidget>
#include <QDoubleSpinBox>
namespace Ui {
class ControladorObjeto;
}

class ControladorObjeto : public QWidget
{
    Q_OBJECT

public:
    explicit ControladorObjeto(QWidget *parent = 0);
    ~ControladorObjeto();
	void setValores(double pos_x, double pos_y, double pos_z, double rot_x, double rot_y, double rot_z, double scale_x, double scale_y, double scale_z, int cantFaces);

public slots:
	virtual void slot_pos_x(double d);
	virtual void slot_pos_y(double d);
	virtual void slot_pos_z(double d);
	virtual void slot_rot_x(double d);
	virtual void slot_rot_y(double d);
	virtual void slot_rot_z(double d);
	virtual void slot_scale_x(double d);
	virtual void slot_scale_y(double d);
	virtual void slot_scale_z(double d);

private:
    Ui::ControladorObjeto *ui;
};

#endif // CONTROLADOROBJETO_H
