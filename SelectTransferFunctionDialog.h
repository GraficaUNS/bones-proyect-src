#pragma once

#include <QDialog>
#include "ui_SelectTransferFunctionWindow.h"
#include <vtkRenderWindow.h>
#include <QGroupbox.h>
#include "myVolume.h"
#include "SelectTransferFunctionLabel.h"

class SelectTransferFunctionWindow;
//class SelectTransferFunctionLabel;

#define NUM_TRANSFER_FUNCTION 8

namespace Ui {
class SelectTransferFunctionDialog;
}

class SelectTransferFunctionDialog : public QDialog
{
    Q_OBJECT
		
public:
    explicit SelectTransferFunctionDialog(QWidget *parent = 0);
    ~SelectTransferFunctionDialog();

	void accept(int index);
	void select(int index);

	void mostrar();
	void Init();

	void setVolume(vtkSmartPointer<myVolume> vol);

private:
	Ui_SelectTransferFunctionWindow *ui;
	
	int indexSeleccionado = 0;

	QGroupBox* listaDeWidgets[NUM_TRANSFER_FUNCTION];
	
	SelectTransferFunctionLabel* listaDeLabels[NUM_TRANSFER_FUNCTION];

	vtkSmartPointer<myVolume> volumen;

	vtkSmartPointer<vtkImageData> imageData;

	bool first = true;
};

