#include "VolumeToSurface.h"
#include "SimpleView.h"
#include "myVolume.h"
#include "Consola.h"
#include "CallbacksClass.h"
#include "MasterHandler.h"
#include "ThreadHandler.h"

#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRendererCollection.h>
#include <vtkMarchingCubes.h>
#include <vtkCallbackCommand.h>

#include <vtkPolyData.h>
#include <vtkActor.h>
#include <vtkProperty.h>
#include <vtkPolyDataMapper.h>
#include <vtkClipClosedSurface.h>
#include <vtkPlane.h>
#include <vtkPlaneCollection.h>
#include <vtkVolumeMapper.h>
#include <vtkQuadricDecimation.h>
#include <vtkTriangleFilter.h>
#include "MeshOperations.h"
#include "SurfaceInteractions.h"
#include <vtkCleanPolyData.h>
#include <vtkClipPolyData.h>
#include <vtkBox.h>
#include <qmath.h>
#include <vtkPolyDataConnectivityFilter.h>


#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))


void convertToPoints(vtkSmartPointer<myVolume> vol, double presicion, double min, double max);


vtkSmartPointer<vtkRenderWindow> renwinVTS;

class MasterHandler;

vtkNew<vtkMarchingCubes> surfaceExtractor;
vtkNew<vtkImageData> imageData;

VolumeToSurface::VolumeToSurface()
{
}

VolumeToSurface::~VolumeToSurface()
{
}

void VolumeToSurface::SetRenderWindow(vtkSmartPointer<vtkRenderWindow > r)
{
	renwinVTS = r;
}

void VolumeToSurface::ConvertVolume(vtkSmartPointer<myVolume> vol, double r)
{
	if (vol == nullptr)
		return;
	
	vol->setPicked(false);

	double ratio = 1.0 - r;

	bool finish = false;
	
	vtkSmartPointer<vtkPlaneCollection> planes = vol->GetMapper()->GetClippingPlanes();

	int min = -1000;
	int max = -1000;

	double R, G, B;


	//convertToPoints(vol, 0.15, 200, 1800);

	while(!finish){
		
		finish = extraerRango(min, max, vol);

		if (min < 10000) {
			Consola::append("Creando superficie con rango ");
			Consola::append(min);
			Consola::append(" - ");
			if(max < 10000)
				Consola::append(max);
			else
				Consola::append("Max");

			Consola::append("\n");


			imageData->DeepCopy(vol->getImageData());



			surfaceExtractor->ComputeNormalsOn();
			
			Consola::append("Etapa 1 de 7: Filtrando valores...");
			if (abs(min) > abs(max)) {
				
				surfaceExtractor->SetValue(max, min);
				Consola::appendln("Salteada");
			}
			else {

				aplicarRango(vol->getImageData(), min, max, imageData);
				Consola::appendln("OK");
				surfaceExtractor->SetValue(0, 128);
			}

			//marchingcubes

			surfaceExtractor->SetInputData(imageData);

			vtkSmartPointer<vtkCallbackCommand> callback = CallbacksClass::getProgressCallBack();
			if (callback != nullptr)
			{
				CallbacksClass::setEncabezado("Generando superficie: ");
				surfaceExtractor->AddObserver(vtkCommand::ProgressEvent, callback);
			}

			Consola::append("Etapa 2 de 7: Generando superficie...");
			surfaceExtractor->Update();
			Consola::appendln("OK");
			
			//marchingcubes

			vtkNew<vtkPolyData> mesh;
			mesh->DeepCopy(surfaceExtractor->GetOutput());

			//triangulate
			vtkSmartPointer<vtkTriangleFilter> triangleFilter =
				vtkSmartPointer<vtkTriangleFilter>::New();
			triangleFilter->SetInputData(mesh);

			if (callback != nullptr)
			{
				CallbacksClass::setEncabezado("Triangulando caras: ");
				triangleFilter->AddObserver(vtkCommand::ProgressEvent, callback);
			}
			Consola::append("Etapa 3 de 7: Triangulando caras...");
			triangleFilter->Update();
			mesh->DeepCopy(triangleFilter->GetOutput());
			Consola::appendln("OK");
			//triangulate

			//decimator
			Consola::append("Etapa 4 de 7: Reduciendo caras...");

			if (ratio > 0.01) {
				vtkSmartPointer<vtkQuadricDecimation> decimator = vtkSmartPointer<vtkQuadricDecimation>::New();
				decimator->SetInputData(mesh);
				decimator->SetTargetReduction(ratio);

				if (callback != nullptr)
				{
					CallbacksClass::setEncabezado("Reduciendo caras: ");
					decimator->AddObserver(vtkCommand::ProgressEvent, callback);
				}

				decimator->Update();

				mesh->DeepCopy(decimator->GetOutput());
				Consola::appendln("OK");
			}
			else{

				Consola::appendln("Salteado");
			}

			//decimator


			

			//clean mesh

			vtkSmartPointer<vtkCleanPolyData> cleanPolyData =
				vtkSmartPointer<vtkCleanPolyData>::New();
			cleanPolyData->SetInputData(mesh);

			cleanPolyData->SetTolerance(0.001);

			if (callback != nullptr)
			{
				CallbacksClass::setEncabezado("Limpiando puntos: ");
				cleanPolyData->AddObserver(vtkCommand::ProgressEvent, callback);
			}
			Consola::append("Etapa 5 de 7: Limpiando puntos...");

			cleanPolyData->Update();
			Consola::appendln("OK");
			mesh->DeepCopy(cleanPolyData->GetOutput());

			
			//clean mesh


			//making slices

			vtkSmartPointer<vtkBox> implicitCube =
				vtkSmartPointer<vtkBox>::New();


			double* bnds = vol->getCustomBounds();
			double* origBounds = vol->getImageData()->GetBounds();
			//printf("BOUNDS %f\n", bnds[0]);

			double bounds[6];

			bounds[0] = bnds[0] + origBounds[1] / 2.0;
			bounds[1] = bnds[1] + origBounds[1] / 2.0;
			bounds[2] = bnds[2] + origBounds[3] / 2.0;
			bounds[3] = bnds[3] + origBounds[3] / 2.0;
			bounds[4] = bnds[4] + origBounds[5] / 2.0;
			bounds[5] = bnds[5] + origBounds[5] / 2.0;

			implicitCube->SetBounds(bounds);

			vtkSmartPointer<vtkClipPolyData> clipper =
				vtkSmartPointer<vtkClipPolyData>::New();
			clipper->SetClipFunction(implicitCube);

			clipper->SetInputData(mesh);

			clipper->InsideOutOn();
			Consola::append("Etapa 6 de 7: Realizando cortes...");
			if (callback != nullptr)
			{
				CallbacksClass::setEncabezado("Realizando cortes: ");
				clipper->AddObserver(vtkCommand::ProgressEvent, callback);
			}
			clipper->Update();

			
			mesh->DeepCopy(clipper->GetOutput());
			//MasterHandler::GetSurfaceInteractor()->GetMeshOperations()->moveMeshToCOSCenter(mesh);
		

			//making slices

			//remove small objects

			Consola::appendln("Etapa 7 de 7: Removiendo objetos sueltos");

			vtkSmartPointer<vtkPolyDataConnectivityFilter> connectivityFilter = vtkSmartPointer<vtkPolyDataConnectivityFilter>::New();
			connectivityFilter->SetInputData(mesh);
			connectivityFilter->SetExtractionModeToAllRegions();
			vtkSmartPointer<vtkCallbackCommand> m_progressCallback = CallbacksClass::getProgressCallBack();

			CallbacksClass::setEncabezado("Analizando objeto: ");
			connectivityFilter->AddObserver(vtkCommand::ProgressEvent, m_progressCallback);
			Consola::appendln("...Etapa 1 de 2: Analizando objeto");
			connectivityFilter->Update();

			// remove objects consisting of less than ratio vertexes of the biggest object
			vtkIdTypeArray* regionSizes = connectivityFilter->GetRegionSizes();

			// find object with most vertices
			long maxSize = 0;
			for (int regions = 0; regions < connectivityFilter->GetNumberOfExtractedRegions(); regions++)
				if (regionSizes->GetValue(regions) > maxSize)
					maxSize = regionSizes->GetValue(regions);


			// append regions of sizes over the threshold
			connectivityFilter->SetExtractionModeToSpecifiedRegions();
			for (int regions = 0; regions < connectivityFilter->GetNumberOfExtractedRegions(); regions++)
				if (regionSizes->GetValue(regions) > maxSize * 0.02)
					connectivityFilter->AddSpecifiedRegion(regions);

			Consola::appendln("...Etapa 2 de 2: Removiendo objetos");

			CallbacksClass::setEncabezado("Removiendo objetos: ");
			connectivityFilter->Update();

			/**/
			//remove small objects
			
			vtkNew<vtkPolyDataMapper> mapper;
			mapper->SetInputData(mesh);
			mapper->ScalarVisibilityOff();
			vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
			actor->SetMapper(mapper);

			
			if(max<10000)
				extraerColor(R, G, B, (max - min) / 2, vol);
			else
				extraerColor(R, G, B, (1500 - min) / 2, vol);
			actor->GetProperty()->SetColor(R, G, B);

			renwinVTS->GetRenderers()->GetFirstRenderer()->AddActor(actor);
			emit MasterHandler::GetSimpleView()->signal_renderizar();
			Consola::append("Finalizado\n");
		}
		
	}

}

bool VolumeToSurface::extraerRango(int & min, int & max, vtkSmartPointer<myVolume> vol)
{
	QVector<double> X_function = *vol->GetOpacityFunction_X();
	QVector<double> Y_function = *vol->GetOpacityFunction_Y();

	int cant = NUMERODEPUNTOS_SCALAR;

	int i = 0;

	while (i < cant && X_function[i] < max) {
		i++;
	}

	while (i < cant && Y_function[i] < 0.9) {
		i++;
	}

	if (i < cant)
		min = X_function[i];
	else {
		min = 50000;
		max = 50000;
		return true;
	}

	while (i < cant && Y_function[i] >= 0.9) {
		i++;
	}

	if (i < cant) {
		max = X_function[i];
		return false;
	}
	else {
		max = 50000;
		return true;
	}
}

bool neighbourInRange(vtkSmartPointer<vtkImageData> imageData, int i, int j, int k, int* dims, int min, int max);

void VolumeToSurface::aplicarRango(vtkSmartPointer<vtkImageData> input, int min, int max, vtkSmartPointer<vtkImageData> output)
{

	emit  MasterHandler::GetSimpleView()->visibleProgressBar(true);
	int* dims = output->GetDimensions();
	int recorridas = 0;
	int numCmp = dims[0] * dims[1] * dims[2];

	for (int i = 0; i < dims[0]; i++) {
		for (int j = 0; j < dims[1]; j++) {
			for (int k = 0; k < dims[2]; k++) {
				double val = input->GetScalarComponentAsDouble(i, j, k, 0);


				if (val < min || val > max) {
					
					if (neighbourInRange(input, i, j, k, dims, min, max)) {
						val = 255;
					}
					else
						val = 0;
					
				}
				else {
					val = 255;
				}
				output->SetScalarComponentFromDouble(i, j, k, 0, val);
				recorridas++;
			}
			emit MasterHandler::GetSimpleView()->signal_WriteLabel(QString(
				QString("Filtrando valores: ") 
				+ QString::number((int)(recorridas*100.0f / (double)numCmp)) 
				+ QString("%")
			));

			emit MasterHandler::GetSimpleView()->progressChanged(recorridas*100.0f / (double)numCmp);
		}
	}
	emit  MasterHandler::GetSimpleView()->visibleProgressBar(false);
}

void VolumeToSurface::extraerColor(double & R, double & G, double & B, int index, vtkSmartPointer<myVolume> vol)
{

	QVector<double> Color_function = *vol->GetColorFunction();

	

	int i = (index + 1000) / STEP_COLOR;

	R = Color_function[i * 3 + 0];
	G = Color_function[i * 3 + 1];
	B = Color_function[i * 3 + 2];
}

void VolumeToSurface::convertToGCode(vtkSmartPointer<myVolume> vol, double presicion)
{
	bool finish = false;
	int min, max;
	while (!finish) {

		finish = extraerRango(min, max, vol);
		convertToPoints(vol, presicion, min, max);
	}
}

bool neighbourInRange(vtkSmartPointer<vtkImageData> imageData, int i, int j, int k, int* dims, int min, int max) {

	int dim_x = dims[0];
	int dim_y = dims[1];
	int dim_z = dims[2];


	int minI = MAX(i - 1, 0);
	int minJ = MAX(j - 1, 0);
	int minK = MAX(k - 1, 0);

	int maxI = MIN(i + 1, dim_x - 1);
	int maxJ = MIN(j + 1, dim_y - 1);
	int maxK = MIN(k + 1, dim_z - 1);




	return false;

}

#include <vtkVertexGlyphFilter.h>
#include <vtkUnsignedCharArray.h>
#include <vtkPointData.h>
void convertToPoints(vtkSmartPointer<myVolume> vol, double presicion, double min, double max)
{

	vtkSmartPointer<vtkPoints> pts = vtkSmartPointer<vtkPoints>::New();


	//for (int i = 0; i < ptrToData->GetNumberOfTuples(); i++){
	//	pts->InsertNextPoint(ptrToData->GetComponent(i, 0), ptrToData->GetComponent(i, 1), ptrToData->GetComponent(i, 2));

	int* dims = vol->getImageData()->GetDimensions();
	double* spacing = vol->getImageData()->GetSpacing();

	vtkSmartPointer<vtkImageData> image = vol->getImageData();


	int dim_x = dims[0];
	int dim_y = dims[1];
	int dim_z = dims[2];

	vtkPoints* last_pts = vtkPoints::New();
	int k = 0;
	bool firstIter = true;
	double offset = 0;

	emit  MasterHandler::GetSimpleView()->visibleProgressBar(true);
	int recorridas = 0;
	int numCmp = dims[0] * dims[1] * dims[2];
	
	double* bnds = vol->getCustomBounds();
	double* origBounds = vol->getImageData()->GetBounds();
	//printf("BOUNDS %f\n", bnds[0]);

	double bounds[6];

	bounds[0] = bnds[0] + origBounds[1] / 2.0;
	bounds[1] = bnds[1] + origBounds[1] / 2.0;
	bounds[2] = bnds[2] + origBounds[3] / 2.0;
	bounds[3] = bnds[3] + origBounds[3] / 2.0;
	bounds[4] = bnds[4] + origBounds[5] / 2.0;
	bounds[5] = bnds[5] + origBounds[5] / 2.0;

	//printf("CONVERTED %f\n", bounds[0]);


	unsigned char red[3] = { 255, 0, 0 };
	unsigned char green[3] = { 0, 255, 0 };
	unsigned char blue[3] = { 0, 0, 255 };
	unsigned char yellow[3] = { 255, 255, 0 };
	unsigned char cian[3] = { 0, 255, 255 };
	unsigned char magenta[3] = { 255, 0, 255 };

	int index_color = 0;
	int cant_color = 6;

	unsigned char* colorArray[6] = {red,green,blue,cian,magenta,yellow};

	
	vtkSmartPointer<vtkUnsignedCharArray> colors =
		vtkSmartPointer<vtkUnsignedCharArray>::New();
	colors->SetNumberOfComponents(3);
	colors->SetName("Colors");

	for (int k = 0; k < dim_z; k++) {

		for (int i = 0; i < dim_x; i++) {
			for (int j = 0; j < dim_y; j++) {
				double value = image->GetScalarComponentAsDouble(i, j, k, 0);
				if (value >= min && value <= max) {
					
					last_pts->InsertNextPoint(i*spacing[0], j*spacing[1], k*spacing[2]);
					double x = i * spacing[0];
					double y = j*spacing[1];
					double z = k*spacing[2];
					if (
						x>= bounds[0] && x <= bounds[1] &&
						y >= bounds[2] && y <= bounds[3] &&
						z >= bounds[4] && z <= bounds[5]
						) 
					{

						pts->InsertNextPoint(x, y, z);
						colors->InsertNextTuple3(colorArray[index_color][0], colorArray[index_color][1], colorArray[index_color][2]);
					}
				}
				recorridas++;
			}
			emit MasterHandler::GetSimpleView()->signal_WriteLabel(QString(
				QString("Generando nube de puntos: ")
				+ QString::number((int)(recorridas*100.0f / (double)numCmp))
				+ QString("%")
			));

			emit MasterHandler::GetSimpleView()->progressChanged(recorridas*100.0f / (double)numCmp);

		}

		index_color = (index_color + 1) % cant_color;

		for (offset = presicion; offset <= spacing[2]; offset += presicion) {
			for (int i = 0; i < last_pts->GetNumberOfPoints(); i++) {
				double* point = last_pts->GetPoint(i);

				double x = point[0];
				double y = point[1];
				double z = point[2] + offset;
				if (
					x >= bounds[0] && x <= bounds[1] &&
					y >= bounds[2] && y <= bounds[3] &&
					z >= bounds[4] && z <= bounds[5]
					)
				{

					pts->InsertNextPoint(x, y, z);
					colors->InsertNextTuple3(colorArray[index_color][0], colorArray[index_color][1], colorArray[index_color][2]);
				}

				//pts->InsertNextPoint(point[0], point[1], point[2] + offset);
				//colors->InsertNextTuple3(colorArray[index_color][0], colorArray[index_color][1], colorArray[index_color][2]);

			}

			index_color = (index_color + 1) % cant_color;
		}


		last_pts->Delete();
		last_pts = vtkPoints::New();
	}

	emit  MasterHandler::GetSimpleView()->visibleProgressBar(false);

	//poly data for points
	vtkSmartPointer<vtkPolyData> pointsPolydata =
		vtkSmartPointer<vtkPolyData>::New();

	pointsPolydata->SetPoints(pts);

	//filter
	vtkSmartPointer<vtkVertexGlyphFilter> vertexFilter =
		vtkSmartPointer<vtkVertexGlyphFilter>::New();

	vertexFilter->SetInputData(pointsPolydata);

	vertexFilter->Update();

	//final poly data
	vtkSmartPointer<vtkPolyData> polydata =
		vtkSmartPointer<vtkPolyData>::New();
	polydata->ShallowCopy(vertexFilter->GetOutput());
	polydata->GetPointData()->SetScalars(colors);	
	
	//mapper
	vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	mapper->SetInputData(polydata);

	//actor
	vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
	actor->SetMapper(mapper);
	actor->GetProperty()->SetPointSize(presicion);
	actor->GetProperty()->SetColor(1, 0, 0);

	renwinVTS->GetRenderers()->GetFirstRenderer()->AddActor(actor);
	emit MasterHandler::GetSimpleView()->signal_renderizar();
}