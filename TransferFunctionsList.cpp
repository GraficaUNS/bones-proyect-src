#include "TransferFunctionsList.h"

double* transferFunctionArray[8];
TransferFunctionsList* a;

double ** TransferFunctionsList::GetTransferFunctionArray()
{
	return transferFunctionArray;
}

void TransferFunctionsList::Init()
{

	a = new TransferFunctionsList();
	transferFunctionArray[0] = a->Opacity_Y_1;
	transferFunctionArray[1] = a->Opacity_Y_2;
	transferFunctionArray[2] = a->Opacity_Y_3;
	transferFunctionArray[3] = a->Opacity_Y_4;
	transferFunctionArray[4] = a->Opacity_Y_5;
	transferFunctionArray[5] = a->Opacity_Y_6;
	transferFunctionArray[6] = a->Opacity_Y_7;
	transferFunctionArray[7] = a->Opacity_Y_8;

}

TransferFunctionsList::TransferFunctionsList()
{
}


TransferFunctionsList::~TransferFunctionsList()
{
}
