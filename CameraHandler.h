#pragma once

#include <vtkRenderWindow.h>
#include <vtkSmartPointer.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkInteractorStyleTrackballActor.h>
#include <vtkParallelCoordinatesInteractorStyle.h>
#include <vtkInteractorStyleImage.h>


#include <vtkImageActor.h> 
#include <vtkAxesActor.h> 
#include <vtkLeaderActor2D.h> 

#include "VolumeInteractions.h"
#include "MasterHandler.h"

#include "sliceVolume.h"


class MyQVTKOpenGLWindow;

class CameraHandler
{
public:
	CameraHandler();
	~CameraHandler();

	void SetRenderWindow(vtkSmartPointer<vtkRenderWindow> ren);

	vtkSmartPointer<vtkRenderWindowInteractor> GetRenderWindowInteractor();

	void setOpenGLWindow(MyQVTKOpenGLWindow* win);

	void changeInteractor();

	void zoomIn();
	void zoomOut();

	void setFourCameras();
	void setImageActors(vtkSmartPointer<vtkImageActor> img1, 
		vtkSmartPointer<vtkImageActor> img2, vtkSmartPointer<vtkImageActor> img3, double* bounds);
	void setSliceVolumes(vtkSmartPointer<sliceVolume> img1, vtkSmartPointer<sliceVolume> img2, vtkSmartPointer<sliceVolume> img3, double* bounds);
	void removeImageActors(); 
	void removeSliceVolumes();
	bool isFourCamera();
	bool isCompareCamera();

	void wheelEvent(QWheelEvent* e);

	void mouseMoveEvent(QMouseEvent *event);
	
	void setStylePolygon();

	void clearPlaneMove();

	void setAxesMode();

	void setButtonActionFourCameras(QAction* btn);

	void setButtonActionCompareMode(QAction* btn);
	
	void setButtonCorte(QAction* btn);

	void setCompareMode();

	void ResetCamera();

	vtkSmartPointer<vtkRenderer> GetImageRenderer();

	bool canMouseMove();

	void resetCameras();

private:

	void procesarWheelEvent(int delta, int width, int height, QPoint point);
	void procesarWheelEventVol(int delta, int width, int height, QPoint point);

	void initAxes();

	vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor;

	vtkSmartPointer<vtkInteractorStyleTrackballActor> styleActor;

	vtkSmartPointer<vtkInteractorStyleTrackballCamera> styleCamera;

	vtkSmartPointer<vtkInteractorStyleImage> styleImage;

	vtkSmartPointer<vtkInteractorStyle > actualStyle;
	vtkSmartPointer<vtkInteractorStyle > lastStyle;

	vtkSmartPointer<vtkRenderWindow > cameraRenderWindow;
	MyQVTKOpenGLWindow* OpenGLWindow;

	vtkSmartPointer<vtkAxesActor> axes;
	QAction* fourCamerasAction;
	QAction* compareCamerasAction;
	QAction* corteAction;

	bool cameraFlag = true;

	bool fourCameras = false;
	bool twoCameras = false;
	bool drawPolygon = false;

	bool axesActive = true;
	vtkSmartPointer<vtkRenderWindow> GetRenderWindow();

	vtkSmartPointer<vtkRenderer> ren0;
	vtkSmartPointer<vtkRenderer> ren1;
	vtkSmartPointer<vtkRenderer> ren2;
	vtkSmartPointer<vtkRenderer> ren3;

	vtkSmartPointer<vtkRenderer> compare_ren;

	double xmins_four[4] = { 0.0, 0.5, 0.0, 0.5 };
	double xmaxs_four[4] = { 0.5, 1.0, 0.5, 1.0 };
	double ymins_four[4] = { 0.5, 0.5, 0.0, 0.0 };
	double ymaxs_four[4] = { 1.0, 1.0, 0.5, 0.5 };

	double xmins_comp[2] = { 0.0, 0.5 };
	double xmaxs_comp[2] = { 0.5, 1.0 };
	double ymins_comp[2] = { 0.0, 0.0 };
	double ymaxs_comp[2] = { 1.0, 1.0 };


	vtkSmartPointer<vtkImageActor > actor1;
	vtkSmartPointer<vtkImageActor > actor2;
	vtkSmartPointer<vtkImageActor > actor3;
	
	vtkSmartPointer<sliceVolume> volumen1;
	vtkSmartPointer<sliceVolume> volumen2;
	vtkSmartPointer<sliceVolume> volumen3;

	vtkSmartPointer<vtkLeaderActor2D> actorXY_x;
	vtkSmartPointer<vtkLeaderActor2D> actorXY_y;
	vtkSmartPointer<vtkLeaderActor2D> actorYZ_x;
	vtkSmartPointer<vtkLeaderActor2D> actorYZ_y;
	vtkSmartPointer<vtkLeaderActor2D> actorZX_x;
	vtkSmartPointer<vtkLeaderActor2D> actorZX_y;

	double defaultCameraPos[3] = { 179.193, 208.335, 1670.44 };

	double defaultCameraUp[3] = { 0, 1, 0 };


};

