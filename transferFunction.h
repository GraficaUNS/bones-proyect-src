/********************************************************************************
** Form generated from reading UI file 'transferFunction.ui'
**
** Created by: Qt User Interface Compiler version 5.11.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TRANSFERFUNCTION_H
#define UI_TRANSFERFUNCTION_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include <QtWidgets/QDoubleSpinBox>

QT_BEGIN_NAMESPACE

class Ui_transferFunction : QWidget
{
	Q_OBJECT
public:
	bool hide = false;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_2;
    QPushButton *pushButton;
    QGroupBox *groupBox_2;
    QHBoxLayout *horizontalLayout;
    QGroupBox *FuncionTransferencia_Box;
    QGridLayout *gridLayout_2;
    QCheckBox *check1;
    QCheckBox *check2;
    QCheckBox *check3;
    QLabel *label3;
    QLabel *label1;
    QLabel *label2;
	QGroupBox *Iluminacion_Box;
	QGridLayout *gridLayout_3;
	QDoubleSpinBox *doubleSpinBox_Especular;
	QDoubleSpinBox *doubleSpinBox_Ambiente;
	QLabel *label;
	QDoubleSpinBox *doubleSpinBox_Difuso;
	QDoubleSpinBox *doubleSpinBox_PotencaEspecular;
	QLabel *label_2;
	QLabel *label_3;
	QLabel *label_4;
	QGroupBox *groupBox_4;
	QGridLayout *gridLayout_4;
    QVBoxLayout *verticalLayout;
    QPushButton *restoreButton;
    QPushButton *selectButton;
	QSpacerItem *horizontalSpacer;

	QGroupBox *MapeoBox;
	QVBoxLayout *verticalLayoutMapeo;
	QComboBox *MapeoComboBox;

	QSplitter *splitter;

    void setupUi(QSplitter *transferFunction)
    {

		splitter = transferFunction;
        if (transferFunction->objectName().isEmpty())
            transferFunction->setObjectName(QStringLiteral("transferFunction"));
       // transferFunction->resize(962, 535);
        groupBox = new QGroupBox(transferFunction);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(10, 90, 941, 431));
		QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
		sizePolicy.setHorizontalStretch(0);
		sizePolicy.setVerticalStretch(0);
		sizePolicy.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
		groupBox->setSizePolicy(sizePolicy);
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
		gridLayout->setContentsMargins(2, 2, 2, 2);

		centralWidget = new QWidget(groupBox);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        sizePolicy.setHeightForWidth(centralWidget->sizePolicy().hasHeightForWidth());
		centralWidget->setSizePolicy(sizePolicy);
		centralWidget->setMinimumSize(QSize(0, 210));
        
		verticalLayout_2 = new QVBoxLayout(centralWidget);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
		verticalLayout_2->setContentsMargins(2, 2, 2, 2);

        gridLayout->addWidget(centralWidget, 1, 0, 1, 1);

        pushButton = new QPushButton(groupBox);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        gridLayout->addWidget(pushButton, 0, 0, 1, 1);

        groupBox_2 = new QGroupBox(groupBox);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        
		QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(groupBox_2->sizePolicy().hasHeightForWidth());
       
		groupBox_2->setSizePolicy(sizePolicy1);
        
		horizontalLayout = new QHBoxLayout(groupBox_2);
		horizontalLayout->setSpacing(0);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
		horizontalLayout->setContentsMargins(0, 0, 0, 0);

		FuncionTransferencia_Box = new QGroupBox(groupBox_2);
		FuncionTransferencia_Box->setObjectName(QStringLiteral("groupBox_3"));
        
		QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(FuncionTransferencia_Box->sizePolicy().hasHeightForWidth());
        
		FuncionTransferencia_Box->setSizePolicy(sizePolicy2);
		FuncionTransferencia_Box->setAlignment(Qt::AlignLeading | Qt::AlignLeft | Qt::AlignVCenter);

		gridLayout_2 = new QGridLayout(FuncionTransferencia_Box);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        
		/*FUNCION TRANFERENCIA CHECKBOX*/
		check1 = new QCheckBox(FuncionTransferencia_Box);
        check1->setObjectName(QStringLiteral("check1"));
        
		QSizePolicy sizePolicy3(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(check1->sizePolicy().hasHeightForWidth());
        
		check1->setSizePolicy(sizePolicy3);

        gridLayout_2->addWidget(check1, 0, 0, 1, 1);

        check2 = new QCheckBox(FuncionTransferencia_Box);
        check2->setObjectName(QStringLiteral("check2"));

        gridLayout_2->addWidget(check2, 1, 0, 1, 1);

        check3 = new QCheckBox(FuncionTransferencia_Box);
        check3->setObjectName(QStringLiteral("check3"));

        gridLayout_2->addWidget(check3, 2, 0, 1, 1);

		/*FUNCION TRANSFERENCIA LABEL CHECKBOX*/
        label3 = new QLabel(FuncionTransferencia_Box);
        label3->setObjectName(QStringLiteral("label3"));
        QSizePolicy sizePolicy4(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(label3->sizePolicy().hasHeightForWidth());
        label3->setSizePolicy(sizePolicy4);
		/**
		QPalette palette3 = label3->palette();
		palette3.setColor(label3->backgroundRole(), Qt::green);
		palette3.setColor(label3->foregroundRole(), Qt::green);
		label3->setPalette(palette3);
		/**/

        gridLayout_2->addWidget(label3, 2, 1, 1, 3);

        label1 = new QLabel(FuncionTransferencia_Box);
        label1->setObjectName(QStringLiteral("label1"));
        sizePolicy4.setHeightForWidth(label1->sizePolicy().hasHeightForWidth());
        label1->setSizePolicy(sizePolicy4);
		
		QPalette palette1 = label1->palette();
		palette1.setColor(label1->backgroundRole(), Qt::black);
		palette1.setColor(label1->foregroundRole(), Qt::black);
		label1->setPalette(palette1);
		label1->setStyleSheet("QLabel { background-color : black; color : black; }");
        gridLayout_2->addWidget(label1, 0, 1, 1, 1);

        label2 = new QLabel(FuncionTransferencia_Box);
        label2->setObjectName(QStringLiteral("label2"));
        sizePolicy4.setHeightForWidth(label2->sizePolicy().hasHeightForWidth());
        label2->setSizePolicy(sizePolicy4);
		
		QPalette palette2 = label2->palette();
		palette2.setColor(label2->backgroundRole(), Qt::red);
		palette2.setColor(label2->foregroundRole(), Qt::red);
		label2->setPalette(palette2);
		label2->setStyleSheet("QLabel { background-color : red; color : red; }");
		
        gridLayout_2->addWidget(label2, 1, 1, 1, 3);

		/*FUNCION TRANSFERENCIA LAYOUT*/
        horizontalLayout->addWidget(FuncionTransferencia_Box);

/***/
		/*ILLUMINATION*/
		Iluminacion_Box = new QGroupBox(groupBox_2);
		Iluminacion_Box->setObjectName(QStringLiteral("Iluminacion_Box"));
		sizePolicy2.setHeightForWidth(Iluminacion_Box->sizePolicy().hasHeightForWidth());
		Iluminacion_Box->setSizePolicy(sizePolicy2);
		Iluminacion_Box->setLayoutDirection(Qt::LeftToRight);
		Iluminacion_Box->setAlignment(Qt::AlignLeading | Qt::AlignLeft | Qt::AlignVCenter);
		gridLayout_3 = new QGridLayout(Iluminacion_Box);
		gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));

		/*ILLUMINATION SPINBOX*/
		doubleSpinBox_Especular = new QDoubleSpinBox(Iluminacion_Box);
		doubleSpinBox_Especular->setObjectName(QStringLiteral("doubleSpinBox_Especular"));
		doubleSpinBox_Especular->setSingleStep(0.02);
		sizePolicy3.setHeightForWidth(doubleSpinBox_Especular->sizePolicy().hasHeightForWidth());
		doubleSpinBox_Especular->setSizePolicy(sizePolicy3);

		gridLayout_3->addWidget(doubleSpinBox_Especular, 2, 0, 1, 1);

		doubleSpinBox_Ambiente = new QDoubleSpinBox(Iluminacion_Box);
		doubleSpinBox_Ambiente->setObjectName(QStringLiteral("doubleSpinBox_Ambiente"));
		doubleSpinBox_Ambiente->setSingleStep(0.02);
		sizePolicy3.setHeightForWidth(doubleSpinBox_Ambiente->sizePolicy().hasHeightForWidth());
		doubleSpinBox_Ambiente->setSizePolicy(sizePolicy3);

		gridLayout_3->addWidget(doubleSpinBox_Ambiente, 0, 0, 1, 1);

		doubleSpinBox_Difuso = new QDoubleSpinBox(Iluminacion_Box);
		doubleSpinBox_Difuso->setObjectName(QStringLiteral("doubleSpinBox_Difuso"));
		doubleSpinBox_Difuso->setSingleStep(0.02);
		sizePolicy3.setHeightForWidth(doubleSpinBox_Difuso->sizePolicy().hasHeightForWidth());
		doubleSpinBox_Difuso->setSizePolicy(sizePolicy3);

		gridLayout_3->addWidget(doubleSpinBox_Difuso, 1, 0, 1, 1);

		doubleSpinBox_PotencaEspecular = new QDoubleSpinBox(Iluminacion_Box);
		doubleSpinBox_PotencaEspecular->setObjectName(QStringLiteral("doubleSpinBox_PotencaEspecular"));
		doubleSpinBox_PotencaEspecular->setSingleStep(0.02);
		sizePolicy3.setHeightForWidth(doubleSpinBox_PotencaEspecular->sizePolicy().hasHeightForWidth());
		doubleSpinBox_PotencaEspecular->setSizePolicy(sizePolicy3);
		doubleSpinBox_PotencaEspecular->setMaximum(1000);
		gridLayout_3->addWidget(doubleSpinBox_PotencaEspecular, 3, 0, 1, 1);

		/*ILLUMINATION SPINBOX LABEL*/
		label = new QLabel(Iluminacion_Box);
		label->setObjectName(QStringLiteral("label"));

		gridLayout_3->addWidget(label, 0, 1, 1, 1);

		label_2 = new QLabel(Iluminacion_Box);
		label_2->setObjectName(QStringLiteral("label_2"));

		gridLayout_3->addWidget(label_2, 1, 1, 1, 1);

		label_3 = new QLabel(Iluminacion_Box);
		label_3->setObjectName(QStringLiteral("label_3"));

		gridLayout_3->addWidget(label_3, 2, 1, 1, 1);

		label_4 = new QLabel(Iluminacion_Box);
		label_4->setObjectName(QStringLiteral("label_4"));

		gridLayout_3->addWidget(label_4, 3, 1, 1, 1);

		/*ILLUMINATION LAYOUT*/
		horizontalLayout->addWidget(Iluminacion_Box);

		horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);
		horizontalLayout->addItem(horizontalSpacer);

		/*PANEL MAPEO*/
		/**
		MapeoBox = new QGroupBox(groupBox_2);
		MapeoBox->setObjectName(QStringLiteral("MapeoBox"));
		sizePolicy2.setHeightForWidth(MapeoBox->sizePolicy().hasHeightForWidth());
		MapeoBox->setSizePolicy(sizePolicy2);
		MapeoBox->setAlignment(Qt::AlignLeading | Qt::AlignLeft | Qt::AlignTop);
		verticalLayoutMapeo = new QVBoxLayout(MapeoBox);
		verticalLayoutMapeo->setObjectName(QStringLiteral("verticalLayoutMapeo"));
		MapeoComboBox = new QComboBox(MapeoBox);
		MapeoComboBox->setObjectName(QStringLiteral("MapeoComboBox"));
		QSizePolicy sizePolicy5_mapeo(QSizePolicy::Maximum, QSizePolicy::Fixed);
		sizePolicy5_mapeo.setHorizontalStretch(0);
		sizePolicy5_mapeo.setVerticalStretch(0);
		sizePolicy5_mapeo.setHeightForWidth(MapeoComboBox->sizePolicy().hasHeightForWidth());
		MapeoComboBox->setSizePolicy(sizePolicy5_mapeo);

		verticalLayoutMapeo->addWidget(MapeoComboBox);

		//  COMPOSITE_BLEND, MAXIMUM_INTENSITY_BLEND, MINIMUM_INTENSITY_BLEND, AVERAGE_INTENSITY_BLEND, 
		//	ADDITIVE_BLEND, ISOSURFACE_BLEND

		MapeoComboBox->addItem(QIcon(), "COMPOSITE_BLEND");
		MapeoComboBox->addItem(QIcon(), "MAXIMUM_INTENSITY_BLEND");
		MapeoComboBox->addItem(QIcon(), "MINIMUM_INTENSITY_BLEND");
		MapeoComboBox->addItem(QIcon(), "AVERAGE_INTENSITY_BLEND");
		MapeoComboBox->addItem(QIcon(), "ADDITIVE_BLEND");
		MapeoComboBox->addItem(QIcon(), "ISOSURFACE_BLEND");

		horizontalLayout->addWidget(MapeoBox);
		/**/
/***/
		/*PANEL BOTONES*/
        groupBox_4 = new QGroupBox(groupBox_2);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
		sizePolicy4.setHeightForWidth(groupBox_4->sizePolicy().hasHeightForWidth());
		groupBox_4->setSizePolicy(sizePolicy4);

		/*PANEL BOTONES LAYOUT*/
		gridLayout_4 = new QGridLayout(groupBox_4);
		gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
		
		/*BOTON RESTORE*/
		restoreButton = new QPushButton(groupBox_4);
		restoreButton->setObjectName(QStringLiteral("pushButton_2"));
		
		QSizePolicy sizePolicy5(QSizePolicy::Expanding, QSizePolicy::Fixed);
		sizePolicy5.setHorizontalStretch(0);
		sizePolicy5.setVerticalStretch(0);
		sizePolicy5.setHeightForWidth(restoreButton->sizePolicy().hasHeightForWidth());
		restoreButton->setSizePolicy(sizePolicy5);
		
		gridLayout_4->addWidget(restoreButton, 0, 0, 1, 1);

		/*BOTON SELECT*/
		selectButton = new QPushButton(groupBox_4);
		selectButton->setObjectName(QStringLiteral("pushButton_3"));

		gridLayout_4->addWidget(selectButton, 1, 0, 1, 1);

		/*PANEL BOTONES LAYOUT*/
        horizontalLayout->addWidget(groupBox_4);

        gridLayout->addWidget(groupBox_2, 2, 0, 1, 1);


        retranslateUi(transferFunction);

        QMetaObject::connectSlotsByName(transferFunction);


		transferFunction->addWidget(groupBox);
		transferFunction->setSizes(QList<int>({ 1 ,  1 }));

		connect(pushButton, SIGNAL(clicked(bool)), this, SLOT(slotHideTransferFunction()));

    } // setupUi

    void retranslateUi(QSplitter *transferFunction)
    {
        //transferFunction->setWindowTitle(QApplication::translate("transferFunction", "Dialog", nullptr));
		groupBox->setTitle(QString());
		pushButton->setText(QApplication::translate("transferFunction", "\342\226\274", nullptr));
		groupBox_2->setTitle(QString());
		FuncionTransferencia_Box->setTitle(QApplication::translate("transferFunction", "Funci\303\263n Transferencia", nullptr));
		check1->setText(QApplication::translate("transferFunction", "Opacidad", nullptr));
		check2->setText(QApplication::translate("transferFunction", "Gradiente", nullptr));
		check3->setText(QApplication::translate("transferFunction", "Color", nullptr));
		label3->setText(QString("         "));
        label1->setText(QString("         "));
        label2->setText(QString("         "));
		Iluminacion_Box->setTitle(QApplication::translate("transferFunction", "Iluminacion", nullptr));
		label->setText(QApplication::translate("transferFunction", "Ambiente", nullptr));
		label_2->setText(QApplication::translate("transferFunction", "Difuso", nullptr));
		label_3->setText(QApplication::translate("transferFunction", "Especular", nullptr));
		label_4->setText(QApplication::translate("transferFunction", "Pot. Especular", nullptr));
		//MapeoBox->setTitle(QApplication::translate("transferFunction", "Tipo de mapa", nullptr));
		groupBox_4->setTitle(" ");
		restoreButton->setText(QApplication::translate("transferFunction", "Restaurar", nullptr));
		selectButton->setText(QApplication::translate("transferFunction", "Func. Transferencia", nullptr));
    } // retranslateUi



public slots:
	void slotHideTransferFunction() {
		if (hide) {
			hide = false;
			splitter->setSizes(QList<int>({ 2 ,  1 }));
			pushButton->setText(QApplication::translate("transferFunction", "\342\226\274", nullptr));
			
			splitter->replaceWidget(1,groupBox);
			gridLayout->addWidget(pushButton, 0, 0, 1, 1);

			splitter->setSizes(QList<int>({ 1 ,  1 }));
		}
		else {
			hide = true;
			splitter->replaceWidget(1,pushButton);
			pushButton->setText(QApplication::translate("transferFunction", "\342\226\262", nullptr));

			splitter->setSizes(QList<int>({ 20 ,  1 }));
		}
	}
};

namespace Ui {
    class transferFunction: public Ui_transferFunction {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TRANSFERFUNCTION_H
