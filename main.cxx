/*
 * Copyright 2007 Sandia Corporation.
 * Under the terms of Contract DE-AC04-94AL85000, there is a non-exclusive
 * license for use of this work by or on behalf of the
 * U.S. Government. Redistribution and use in source and binary forms, with
 * or without modification, are permitted provided that this Notice and any
 * statement of authorship are reproduced on all copies.
 */
 // QT includes
 //*
#include <QApplication>
#include <QWindow>
#include <QSurfaceFormat>
#include <vtkFileOutputWindow.h>

#include "QVTKOpenGLWidget.h"
#include "SimpleView.h"

extern int qInitResources_icons();


void vtkConsoleOff() {
	vtkFileOutputWindow *outwin = vtkFileOutputWindow::New();

	outwin->SetFileName(".\vtklog.txt");

	outwin->SetInstance(outwin);
}

int main(int argc, char** argv)
{

	vtkConsoleOff();
	// needed to ensure appropriate OpenGL context is created for VTK rendering.
	QSurfaceFormat::setDefaultFormat(QVTKOpenGLWidget::defaultFormat());

	int nargs = argc + 1;
	char** args = new char*[nargs];
	for (int i = 0; i < argc; i++) {
		args[i] = argv[i];
	}
	args[argc] = (char*)"--disable-web-security";

	// QT Stuff
	QApplication app(argc, args);

	//QApplication::setStyle("fusion");

	qInitResources_icons();

	SimpleView mySimpleView;

	mySimpleView.showMaximized();

	return app.exec();
}
/**/
/*
#include <vtkSphereSource.h>
#include <vtkProperty.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkCursor3D.h>

char* path = "P:/CRUDO/DICOM/S01890/S20"

int main(int, char *[])
{


// Create a renderer, render window, and interactor
vtkSmartPointer<vtkRenderer> renderer =
vtkSmartPointer<vtkRenderer>::New();
vtkSmartPointer<vtkRenderWindow> renderWindow =
vtkSmartPointer<vtkRenderWindow>::New();
renderWindow->AddRenderer(renderer);
vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor =
vtkSmartPointer<vtkRenderWindowInteractor>::New();
renderWindowInteractor->SetRenderWindow(renderWindow);

// Add the actor to the scene
renderer->AddVolume(volume);
renderer->SetBackground(1,1,1); // Background color white

// Render and interact
renderWindow->Render();
renderWindowInteractor->Start();

return EXIT_SUCCESS;
}
/**/
