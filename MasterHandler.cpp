#include "MasterHandler.h"

#include "MeasureHandler.h"
#include "PlaneHandler.h"

#include <vtkRenderWindow.h>

#include <vtkRenderer.h>
#include <vtkRendererCollection.h>

#include "VolumeInteractions.h"
#include "NoteInteractions.h"

#include "tablaPaciente.h"
#include "ventanaObjetos.h"
#include "Consola.h"
#include "SurfaceInteractions.h"


#include "ui_SimpleView.h"
#include <qsplitter.h>
#include "MyQVTKOpenGLWidget.h"
#include <QFileDialog>
#include <QString>
#include "ThreadHandler.h"

#include "SimpleView.h"
#include "ListInteractions.h"
#include "VolumeToSurface.h"
#include "IOSurface.h"

#include <vtkWindowToImageFilter.h>
#include <vtkPNGWriter.h>

tablaPaciente* tablapaciente;
ventanaObjetos* ventanaobjetos;

class SurfaceInteractions;
class VolumeInteractions;

SimpleView* simpleview_m;

ThreadHandler* thread;

PlaneHandler* plane;
MeasureHandler* medidas;
SurfaceInteractions* surface;
VolumeInteractions* volume;
ListInteractions* listW;
CameraHandler* camera;
IOSurface* iosurface;

NoteInteractions* note;

vtkSmartPointer<vtkRenderWindow> renWin;

QProgressBar * progress_bar;


MasterHandler::MasterHandler(){}

MasterHandler::~MasterHandler(){}



void MasterHandler::initHandlers(vtkSmartPointer<vtkRenderWindow > ren, CameraHandler* cam) {

	camera = cam;

	plane = new PlaneHandler();
	medidas = new MeasureHandler();
	volume = new VolumeInteractions();

	surface = new SurfaceInteractions();

	note = new NoteInteractions();

	listW = new ListInteractions();

	iosurface = new IOSurface();

	medidas->SetRenderWindow(ren);
	plane->SetRenderWindow(ren);
	surface->SetRenderWindow(ren);
	volume->SetRenderWindow(ren);
	note->SetRenderWindow(ren);

	VolumeToSurface::SetRenderWindow(ren);

	renWin = ren;

	thread = new ThreadHandler();
	
	thread->init();


}

void  MasterHandler::InsertarMedida() {
	medidas->insertarMedida();
}


vtkSmartPointer<vtkActor> MasterHandler::GetActorPlane() {
	return plane->GetActor();
}

void MasterHandler::cortarPorPlano(vtkSmartPointer<vtkActor> actor) {
	surface->cortarPorPlano(plane, actor);
	emit simpleview_m->signal_renderizar();
	//surface->setSelectedActorToNull();
}

void MasterHandler::cortarVolumenPorPlano(vtkSmartPointer<myVolume> vol)
{
	volume->cortarPorPlano(plane, vol);
	emit simpleview_m->signal_renderizar();
}

void MasterHandler::mostrarPlano() {
	plane->mostrarPlano();
}

void MasterHandler::mouseMoveEvent(QMouseEvent * event, double height, double width)
{
	
	volume->mouseMoveEvent(camera, event);
	
	note->mouseMove(camera, event);

}

void MasterHandler::mousePressEvent(QMouseEvent * event, double height, double width)
{
	note->checkPickedNote(camera, event, height, width);
	if (!thread->isWorking()) {
		surface->checkPickedActor(camera, event);
		volume->mousePressEvent(camera, event);
	}
	
}

void MasterHandler::mouseReleaseEvent(QMouseEvent * event, double height, double width)
{
	volume->mouseReleaseEvent(camera, event);
}

void MasterHandler::masterKeyPress(QKeyEvent *event) {
	

	if (!thread->isWorking()) {
		surface->keyPress();
		//volume->keyPress(event);
	}
	else {
		emit GetSimpleView()->signal_Warning(HILO_TRABAJANDO);
	}
	medidas->keyPress();
	note->keyPress();
}


void MasterHandler::masterKeyRelease(QKeyEvent *event) {

	surface->keyRelease();
	medidas->restaurarColor();
}


SurfaceInteractions* MasterHandler::GetSurfaceInteractor() {
	return surface;
}


void MasterHandler::Renderizar() {
	renWin->Render();
}

VolumeInteractions * MasterHandler::GetVolumeInteractor()
{
	return volume;
}


ThreadHandler * MasterHandler::GetThread()
{
	return thread;
}

PlaneHandler * MasterHandler::getPlaneHandler()
{
	return plane;
}

NoteInteractions* MasterHandler::GetNoteInteractor() 
{
	return note;

}

void MasterHandler::setTablaPaciente(tablaPaciente * tabla)
{
	tablapaciente = tabla;
}

void MasterHandler::setVentanaObjetos(ventanaObjetos* ventana)
{
	ventanaobjetos = ventana;
}

tablaPaciente * MasterHandler::getTablaPaciente()
{
	return tablapaciente;
}


ventanaObjetos * MasterHandler::getVentanaObjetos()
{
	return ventanaobjetos;
}


void MasterHandler::setProgressBar(QProgressBar * prog_bar)
{
	progress_bar = prog_bar;
}

QProgressBar * MasterHandler::getProgressBar()
{
	return progress_bar;
}

void MasterHandler::SetSimpleView(SimpleView * s)
{
	simpleview_m = s;
}

SimpleView* MasterHandler::GetSimpleView()
{
	return simpleview_m;
}

vtkSmartPointer<myVolume> MasterHandler::get_last_volume()
{
	return nullptr;
}

void MasterHandler::setListWidget(ListInteractions * l)
{
	listW = l;
}

ListInteractions * MasterHandler::getListWidget()
{
	return listW;
}

CameraHandler * MasterHandler::GetCameraHandler()
{
	return camera;
}

IOSurface * MasterHandler::GetIOSurface()
{
	return iosurface;
}

void MasterHandler::run_openVolume(std::string dir_path)
{
	if (dir_path.length() > 3) {
		Consola::append("Leyendo directorio DICOM\n\n");
		vtkSmartPointer<myVolume> last_volume = new myVolume(dir_path);
		
		last_volume->SetReferenceCount(1);
		
		if (last_volume->getImageData() == nullptr) {
			emit GetSimpleView()->signal_Error("No se pudo leer informacion del directorio seleccionado");
			last_volume->SetReferenceCount(0);
			
			return;
		}
		
		QStringList list = QString(dir_path.data()).split("/");



		GetVolumeInteractor()->registrarVolumen(last_volume, QString(list[list.size() - 1]));
		

		/*
		esto de aqui no se ejecuta con el hilo principal, take snapshots se ejecuta con el
		hilo principal, entonces tengo que forzar a este hilo no principal que espere por el principal.
		*/
		//GetThread()->lock_Render();

		emit MasterHandler::GetSimpleView()->signal_WriteLabel("Renderizando");
		emit simpleview_m->signal_take_snapshots(GetVolumeInteractor()->GetRenderWindow2(),last_volume);
		
		//GetThread()->lock_Render();
		//MasterHandler::GetThread()->unlock_Render();

		
		
		/*
		termina aqu�
		*/

		if (last_volume->getPaciente() != nullptr)
			MasterHandler::getTablaPaciente()->insertarPaciente(last_volume->getPaciente());
		else
			emit GetSimpleView()->signal_Error("Error al leer los datos del paciente");
		
		last_volume = nullptr;
	}
}


