 

#include "ui_SimpleView.h"
#include "SimpleView.h"
#include "MyQVTKOpenGLWidget.h"
#include "MyQVTKOpenGLWindow.h"

#include <vtkDataObjectToTable.h>
#include <vtkGenericOpenGLRenderWindow.h>
#include <vtkRendererCollection.h>
#include <vtkNew.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkSmartPointer.h>

#include <vtkWindowToImageFilter.h>
#include <vtkPNGWriter.h>
#include <iostream>

//progress bar
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QTextBrowser>

//Consola
#include "Consola.h"
#include "InputManager.h"

//Ventana de error
#include "Dialog.h"

#include <vtkActor.h>

#include "CallbacksClass.h"

#include "SurfaceInteractions.h"
#include "VolumeInteractions.h"
#include "tablaPaciente.h"


#include "NoteInteractions.h"
#include "ventanaObjetos.h"

#include "ThreadHandler.h"


#include <TransferFunctionsList.h>
#include <vtkImageExport.h>
#include <vtkPointData.h>

class MasterHandler;

MyQVTKOpenGLWindow* OpenGLwindow;

QProgressBar * barraProgreso;


//directorio abierto
std::string path = "";
QString ArchivoActual;

//flags
bool barraFlag = false;
bool cameraOn = true;

SimpleView* thisSimpleView;

void ProgressCallbackToWidget(vtkObject* caller, long unsigned int /*eventId*/, void* /*clientData*/, void* /*callData*/)
{
	// display progress in terminal
	vtkAlgorithm* filter = static_cast<vtkAlgorithm*>(caller);
	
	
	if (!barraFlag) {
		barraFlag = true;
		//barraProgreso->setVisible(true);
		emit thisSimpleView->visibleProgressBar(true);
		
		
	}

	if (filter->GetProgress() > 0.999) {
		
		//barraProgreso->setVisible(false);
		emit thisSimpleView->visibleProgressBar(false);
		barraFlag = false;
		emit thisSimpleView->signal_WriteLabel("Finalizado");
		CallbacksClass::setEncabezado("");
	}
	else {
		//barraProgreso->setValue(filter->GetProgress() * 100);
		emit thisSimpleView->progressChanged(filter->GetProgress() * 100);
		emit thisSimpleView->signal_WriteLabel(QString(CallbacksClass::getEncabezado() + QString::number((int)(filter->GetProgress() * 100)) + QString("%")));
			
		
	}
	
}

SimpleView::SimpleView()
{
	thisSimpleView = this;
	this->ui = new Ui_SimpleView;
	this->ui->setupUi(this);

	this->ui->progressBar->setVisible(false);
	barraProgreso = this->ui->progressBar;

	Consola::setConsola(this->ui->textBrowser);
	Consola::setProgLabel(this->ui->progressLabel);

	QMessageBox* error = new QMessageBox();
	Dialog::setErrorMessage(error);

	CallbacksClass::setProgressCallBack(ProgressCallbackToWidget);

	//valor por defecto del splitter
	this->ui->splitter->setSizes(QList<int>({ this->ui->groupBox->minimumWidth() ,  this->ui->groupBox_3->maximumWidth() }));

	//ventanas
	tablapaciente = new tablaPaciente(this);

	ventanaobjetos = new ventanaObjetos(this);

	acercade = new acercaDe(this);
	
	manual = new ManualPDF();

	qualityWidget = new QualityBarWidget(this);
	//progress callback
	m_vtkCallback = vtkSmartPointer<vtkCallbackCommand>::New();
	m_vtkCallback->SetCallback(ProgressCallbackToWidget);
	
	//renderer
	vtkNew<vtkRenderer> ren;
	ren->LightFollowCameraOn();

	//renderWindow
	vtkSmartPointer<vtkGenericOpenGLRenderWindow> renderWindow = vtkSmartPointer<vtkGenericOpenGLRenderWindow>::New();
	renderWindow->AddRenderer(ren);
	this->ui->qvtkWidget->SetRenderWindow(renderWindow);

	OpenGLWidget = (MyQVTKOpenGLWidget*)this->ui->qvtkWidget;
	OpenGLwindow = OpenGLWidget->getWindowInternal();


	OpenGLwindow->GetCameraHandler()->setButtonActionFourCameras(this->ui->actionCorte_Tridimensional);
	OpenGLwindow->GetCameraHandler()->setButtonActionCompareMode(this->ui->actionIniciar_Comparacion);
	OpenGLwindow->GetCameraHandler()->setButtonCorte(this->ui->actionDibujar_Poligono);

	//connections
	connect(this->ui->actionOpenFile, SIGNAL(triggered()), this, SLOT(slotOpenFile()));
	connect(this->ui->actionExit, SIGNAL(triggered()), this, SLOT(slotExit()));
	connect(this->ui->actionSaveFile, SIGNAL(triggered()), this, SLOT(slotSaveFile()));
	connect(this->ui->actionImport_File, SIGNAL(triggered()), this, SLOT(slotImportFile()));
	connect(this->ui->actionSaveFileAs, SIGNAL(triggered()), this, SLOT(slotSaveFileAs()));
	connect(this->ui->actionCut, SIGNAL(triggered()), this, SLOT(slotCut()));
	connect(this->ui->actionUndo, SIGNAL(triggered()), this, SLOT(slotUndo()));
	connect(this->ui->actionCamara, SIGNAL(triggered()), this, SLOT(slotCamera()));
	connect(this->ui->actionAddPlane, SIGNAL(triggered()), this, SLOT(slotAddPlane()));
	connect(this->ui->actionMeasure, SIGNAL(triggered()), this, SLOT(slotMeasure()));
	connect(this->ui->actionAxes, SIGNAL(triggered()), this, SLOT(slotAxes()));
	connect(this->ui->actionEliminar, SIGNAL(triggered()), this, SLOT(slotEliminar()));
	connect(this->ui->actionSeparar, SIGNAL(triggered()), this, SLOT(slotSeparar()));
	connect(this->ui->actionUnir, SIGNAL(triggered()), this, SLOT(slotUnir()));
	connect(this->ui->actionResetObjeto, SIGNAL(triggered()), this, SLOT(slotResetObjeto()));
	connect(this->ui->actionZoomIn, SIGNAL(triggered()), this, SLOT(slotZoomIn()));
	connect(this->ui->actionZoomOut, SIGNAL(triggered()), this, SLOT(slotZoomOut()));
	connect(this->ui->actionOpen_Volume, SIGNAL(triggered()), this, SLOT(slotOpenVolume()));
	connect(this->ui->actionInfoPaciente, SIGNAL(triggered()), this, SLOT(slotInfoPaciente()));
	connect(this->ui->actionNota, SIGNAL(triggered()), this, SLOT(slotNota()));

	connect(this->ui->actionTomar_Captura, SIGNAL(triggered()), this, SLOT(slotExportCaptura()));

	//connect(this->ui->actionAgregarObjeto, SIGNAL(triggered()), this, SLOT(slotAgregarObjeto()));

	connect(this->ui->actionCorte_Tridimensional, SIGNAL(triggered()), this, SLOT(slotFourCameras()));
	connect(this->ui->actionDibujar_Poligono, SIGNAL(triggered()), this, SLOT(slotDrawPolygon()));
	connect(this->ui->actionCorte_Preciso, SIGNAL(triggered()), this, SLOT(slotCortePreciso()));

	connect(this->ui->actionConvertir_en_Objeto, SIGNAL(triggered()), this, SLOT(slotConvertirEnObjeto()));
	connect(this->ui->actionConvertToGCode, SIGNAL(triggered()), this, SLOT(slotConvertToGCode()));
	connect(this->ui->actionExportar_Objeto, SIGNAL(triggered()), this, SLOT(slotExportarObjeto3D()));
	connect(this->ui->actionRemoveSmallObjects, SIGNAL(triggered()), this, SLOT(slotRemoveSmallObjects()));
	connect(this->ui->actionSuavizar_Objeto, SIGNAL(triggered()), this, SLOT(slotSuavizarObjeto()));
	connect(this->ui->actionMeshReduction, SIGNAL(triggered()), this, SLOT(slotMeshReduction()));


	connect(this->ui->actionIniciar_Comparacion, SIGNAL(triggered()), this, SLOT(slotVistaComparacion()));
	connect(this->ui->actionHelp, SIGNAL(triggered()), this, SLOT(slotAcercaDe()));
	connect(this->ui->actionVer_Manual, SIGNAL(triggered()), this, SLOT(slotManual()));

	connect(this->ui->actionResetCamera, SIGNAL(triggered()), this, SLOT(slotResetCamera()));
	connect(this->ui->actionClear_Console, SIGNAL(triggered()), this, SLOT(slotClearConsole()));

	
	MasterHandler::SetSimpleView(this);
	MasterHandler::initHandlers(renderWindow, OpenGLwindow->GetCameraHandler());
	MasterHandler::setTablaPaciente(tablapaciente);

	MasterHandler::setVentanaObjetos(ventanaobjetos);

	MasterHandler::setProgressBar(barraProgreso);
	
	MasterHandler::setListWidget(this->ui->listWidget);

	//conectando se�ales internas entre el hilo main y el worker
	connect(this, SIGNAL(progressChanged(int)), SLOT(onProgressChanged(int)));
	connect(this, SIGNAL(visibleProgressBar(bool)), SLOT(onVisibleProgressBar(bool)));


	connect(this, SIGNAL(signal_renderizar()), SLOT(slot_renderizar()));
	connect(this, SIGNAL(signal_take_snapshots(vtkRenderWindow*, myVolume*)), SLOT(slot_take_snapshots(vtkRenderWindow*, myVolume*)));
	connect(this, SIGNAL(signal_RemoveActor(vtkActor*)), SLOT(slot_RemoveActor(vtkActor*)));
	
	connect(this, SIGNAL(signal_ResetCameras()), SLOT(slot_ResetCameras()));
	connect(this, SIGNAL(signal_WriteLabel(QString)), SLOT(slot_WriteLabel(QString)));
	connect(this, SIGNAL(signal_Error(QString)), SLOT(slot_Error(QString)));
	connect(this, SIGNAL(signal_Warning(QString)), SLOT(slot_Warning(QString)));
	connect(this, SIGNAL(signal_Info(QString)), SLOT(slot_Info(QString)));
	connect(this, SIGNAL(signal_SaveCapture(QString)), SLOT(slot_SaveCapture(QString)));
	connect(this, SIGNAL(signal_Show_QualityWidget()), SLOT(slot_Show_QualityWidget()));

	

	//setMultiVolumes(ren);

	this->ui->qvtkWidget->GetRenderWindow()->Render();

	//this->ui->ta
	this->slotResetCamera();


	//se inicializa la gui de funcion transferencia
	MasterHandler::GetVolumeInteractor()->initGUI(this->ui->splitter_3);
	//this->ui->splitter_3->setSizes({ this->ui->qvtkWidget->maximumHeight(),300 });
	
};

SimpleView::~SimpleView()
{
	// The smart pointers should clean up for up

}

QualityBarWidget * SimpleView::getQualityWidget()
{
	return qualityWidget;
}

void SimpleView::keyPressEvent(QKeyEvent * event)
{
	InputManager::keyPressEvent(event);
	OpenGLwindow->keyPress(event);
}

void SimpleView::keyReleaseEvent(QKeyEvent * event)
{
	InputManager::keyReleaseEvent(event);
}


void SimpleView::slotOpenVolume()
{
	QString dir = QFileDialog::getExistingDirectory(this, SimpleView::tr("Abrir directorio DICOM"),
		QString::fromStdString(path),
		QFileDialog::ShowDirsOnly
		| QFileDialog::DontResolveSymlinks);

	std::string path= dir.toStdString();

	if (dir.length() > 3) {

		MasterHandler::GetThread()->set_openVolume(path);
	}
}


void SimpleView::slotSaveFile() {   //estaba ya agregada, pero es para el save, lo mismo que la onSave_action triggered() del video
									//QString fileName = QFileDialog
	vtkSmartPointer<myVolume> vol = MasterHandler::GetVolumeInteractor()->GetActualVolume();
	if (vol != nullptr) {
		if (vol->getPath() != "") {
			MasterHandler::GetThread()->set_exportVolume((vol->getPath()).toStdString(), vol);
		}
		else {
			//Consola::append((vol->getPath()).toStdString());
			slotSaveFileAs();
		}

	} else {
		emit MasterHandler::GetSimpleView()->signal_Warning(WARN_NO_VOLUMEN_LISTA);
	}

}

void SimpleView::slotSaveFileAs() {   //es lo mismo que la onSave_action triggered() del video

	QString direction = QFileDialog::getExistingDirectory(this, SimpleView::tr("Exportar volumen"),
		QString::fromStdString(""),
		QFileDialog::ShowDirsOnly
		| QFileDialog::DontResolveSymlinks);

	vtkSmartPointer<myVolume> vol = MasterHandler::GetVolumeInteractor()->GetActualVolume();
	if (!direction.isEmpty() && !direction.isNull()) {
		if (vol != nullptr) {
			MasterHandler::GetThread()->set_exportVolume(direction.toStdString(), vol);
		} else {
			emit MasterHandler::GetSimpleView()->signal_Warning(WARN_NO_VOLUMEN_LISTA);
		}
	}
	

}


void SimpleView::slotImportFile()
{
	
	QString direction = QFileDialog::getOpenFileName(this, tr("Abrir Archivo"),
		QString::fromStdString(path),
		"Archivo Bones (*.bones)");

	direction = direction.split(".bones")[0];

	if (!direction.isEmpty() && !direction.isNull()) {
		MasterHandler::GetThread()->set_importVolume(direction.toStdString());

		this->ui->qvtkWidget->GetRenderWindow()->Render();
	}
}

void SimpleView::slotCamera() {   //es lo mismo que la onSave_action triggered() del video
	OpenGLwindow->GetCameraHandler()->changeInteractor();

	QIcon icon2;
	if (cameraOn) {
		icon2.addFile(QStringLiteral(":/Icons/nocamera.png"), QSize(), QIcon::Normal, QIcon::Off);
		this->ui->actionCamara->setText("Modo Objeto");
	}
	else {
		icon2.addFile(QStringLiteral(":/Icons/camera.png"), QSize(), QIcon::Normal, QIcon::Off);
		this->ui->actionCamara->setText("Modo Camara");
	}

	cameraOn = !cameraOn;
	this->ui->actionCamara->setIcon(icon2);

	this->ui->qvtkWidget->GetRenderWindow()->Render();
}


void SimpleView::slotCut() {   //es lo mismo que la onSave_action triggered() del video
	if (MasterHandler::GetSurfaceInteractor()->getSelectedActor())
		MasterHandler::GetThread()->set_cortarSuperficie(MasterHandler::GetSurfaceInteractor()->getSelectedActor());
	else if (MasterHandler::GetVolumeInteractor()->getPickedVolume())
		MasterHandler::GetThread()->set_cortarVolumen(MasterHandler::GetVolumeInteractor()->getPickedVolume());
	else
		emit MasterHandler::GetSimpleView()->signal_Warning(WARN_NO_OBJETO_SELECCIONADO);
}

void SimpleView::slotUndo() {   //es lo mismo que la onSave_action triggered() del vide


}

void SimpleView::slotAddPlane() {   //TODO aca hay que ver de agregar mas planos como actores							// Add Actor to renderer
	MasterHandler::mostrarPlano();
}

void SimpleView::slotMeasure()
{
	MasterHandler::InsertarMedida();
}

void SimpleView::slotAxes() {
	
	MasterHandler::GetCameraHandler()->setAxesMode();
	this->ui->qvtkWidget->GetRenderWindow()->Render();

}

void SimpleView::slotEliminar()
{
	if (MasterHandler::GetSurfaceInteractor()->getSelectedActor() != nullptr)
		MasterHandler::GetSurfaceInteractor()->removeSelectedActor();
	else if (MasterHandler::GetVolumeInteractor()->getPickedVolume() != nullptr)
		MasterHandler::GetVolumeInteractor()->RemoveSelectedVolume();
	else
		emit MasterHandler::GetSimpleView()->signal_Warning(WARN_NO_OBJETO_SELECCIONADO);

}

void SimpleView::slotSeparar() {
	if (MasterHandler::GetSurfaceInteractor()->getSelectedActor() != nullptr)
		MasterHandler::GetThread()->set_separarSuperficie(MasterHandler::GetSurfaceInteractor()->getSelectedActor(), 0.02);
	else if (MasterHandler::GetVolumeInteractor()->getPickedVolume() != nullptr)
		MasterHandler::GetThread()->set_separarVolumen(MasterHandler::GetVolumeInteractor()->getPickedVolume(),0.0002);
	else
		emit signal_Warning(WARN_NO_OBJETO_SELECCIONADO);

}

//VER LUEGO
void SimpleView::slotUnir() {
	
	if (MasterHandler::GetSurfaceInteractor()->getSelectedActor() != nullptr)
		MasterHandler::GetThread()->set_unirSuperficie();
	else if (MasterHandler::getListWidget()->GetSelectedPorts()->size()>1)
		MasterHandler::GetThread()->set_unirVolumen();
	else
		emit signal_Warning(WARN_NO_ITEMS_SELECCIONADOS);
}

void SimpleView::slotResetObjeto() {
	if(MasterHandler::GetSurfaceInteractor()->getSelectedActor() != nullptr)
		MasterHandler::GetSurfaceInteractor()->clearTransforms();
	else if(MasterHandler::GetVolumeInteractor()->GetActualVolume() != nullptr)
		MasterHandler::GetVolumeInteractor()->clearTransforms();
	else
		emit signal_Warning(WARN_NO_OBJETO_SELECCIONADO);


}

void SimpleView::slotZoomIn() {
	OpenGLwindow->GetCameraHandler()->zoomIn();
}

void SimpleView::slotZoomOut() {
	OpenGLwindow->GetCameraHandler()->zoomOut();
}

void SimpleView::slotAgregarObjeto() {
	ventanaobjetos->show();
}

void SimpleView::slotExit() {
	this->ui->qvtkWidget->GetRenderWindow()->Finalize();
	qApp->exit();
	exit(0);
}

void SimpleView::slotInfoPaciente() {

	//probar en vtkSmartPointer<vtkMeshRoutines directamente setear los datos

	tablapaciente->show();
}

void SimpleView::slot_ResetCameras()
{
	MasterHandler::GetCameraHandler()->resetCameras();
}

void SimpleView::slotNota() {

	MasterHandler::GetNoteInteractor()->crearNota();

}

void SimpleView::slotFourCameras()
{

	OpenGLwindow->GetCameraHandler()->setFourCameras();

	if (OpenGLwindow->GetCameraHandler()->isFourCamera()) {
		MasterHandler::GetCameraHandler()->removeImageActors();
		MasterHandler::GetCameraHandler()->removeSliceVolumes();

		MasterHandler::Renderizar();

		vtkSmartPointer<myVolume> vol = MasterHandler::GetVolumeInteractor()->GetActualVolume();
		if (vol != nullptr) {
			if (!(vol->getImageActorXY() != nullptr && vol->getImageActorYZ() != nullptr && vol->getImageActorZX() != nullptr)) {
				MasterHandler::GetThread()->set_corteTridimensional(MasterHandler::GetVolumeInteractor()->GetActualVolume());
			}
			//si son nulos no los inserta
			MasterHandler::GetCameraHandler()->setImageActors(vol->getImageActorXY(), vol->getImageActorYZ(), vol->getImageActorZX(), vol->getCustomBounds());

		}
		else
			emit signal_Warning(WARN_NO_VOLUMEN_LISTA);

	}
}

void SimpleView::slotDrawPolygon()
{
	MasterHandler::GetCameraHandler()->setStylePolygon();
}

void SimpleView::slotConvertirEnObjeto()
{
	vtkSmartPointer<myVolume> vol = MasterHandler::GetVolumeInteractor()->GetActualVolume();
	if (vol != nullptr) {
		//pedir calidad double entre 0 y 1
		

		//if (qualityWidget->isAccepted()) {

		qualityWidget->lock();
		qualityWidget->setText("Elija la calidad del objeto de salida", 
			"Cuidado: Puede afectar el rendimiento del sistema", 
			"Calidad normal",
			"Cuidado: Puede provocar distorsiones en los objetos",
			60,30, 75
		);
		qualityWidget->mostrar();

		MasterHandler::GetThread()->set_convertVolume(vol, 0);
		
	}
	else
		emit signal_Warning(WARN_NO_VOLUMEN_LISTA);


}

void SimpleView::slotSuavizarObjeto()
{
	vtkSmartPointer<vtkActor> actor = MasterHandler::GetSurfaceInteractor()->getSelectedActor();
	if (actor != nullptr) {
		//pedir itercaiones entero mayor a 1
		qualityWidget->lock();
		qualityWidget->setText("Elija el numero de iteraciones",
			"Cuidado: Puede provocar distorsiones en los objetos ",
			"Recomendado",
			"",
			1, 20, 70
		);
		qualityWidget->mostrar();

		MasterHandler::GetThread()->set_smoothMesh(actor, 10);
	}
	else
		emit signal_Warning(WARN_NO_SUPERFICIE_SELECCIONADA);
}

void SimpleView::slotRemoveSmallObjects()
{
	vtkSmartPointer<vtkActor> actor = MasterHandler::GetSurfaceInteractor()->getSelectedActor();
	if (actor != nullptr) {
		//pedir calidad  double entre 0 y 1
		qualityWidget->lock();
		qualityWidget->setText("Elija el tama�o promedio de los objetos que desea mantener",
			"Cuidado: Podrian no mostrarse algunos objetos deseables",
			"Recomendado",
			"Podrian eliminarse menos objetos de lo esperado",
			30, 99, 95
		);
		qualityWidget->mostrar();

		double calidad = 0.02;

		MasterHandler::GetThread()->set_removeSmallObjects(actor, 0.02);

	}
	else
		emit MasterHandler::GetSimpleView()->signal_Warning(WARN_NO_SUPERFICIE_SELECCIONADA);
}

void SimpleView::slotMeshReduction()
{
	vtkSmartPointer<vtkActor> actor = MasterHandler::GetSurfaceInteractor()->getSelectedActor();
	if (actor != nullptr && !MasterHandler::GetThread()->isWorking()) {
		//pedir calidad  double entre 0 y 1
		qualityWidget->lock();
		qualityWidget->setText("Elija la calidad del objeto de salida",
			"Poca diferencia de calidad",
			"Diferencia de calidad significativa",
			"Cuidado: Puede provocar distorsiones en los objetos",
			60, 30, 75
		);
		qualityWidget->mostrar();

		double calidad = 0.75;

		MasterHandler::GetThread()->set_meshReduction(actor, 1.0 - calidad);
	}
	else
		emit signal_Warning(WARN_NO_SUPERFICIE_SELECCIONADA);
}

void SimpleView::slotExportarObjeto3D()
{
	vtkSmartPointer<vtkActor> actor = MasterHandler::GetSurfaceInteractor()->getSelectedActor();

	if (actor != nullptr) {
		QString fileName = QFileDialog::getSaveFileName(this, "SimpleView - Guardar como", "", "Obj Files (*.obj) ;; Stl Files (*.stl) ;; Ply Files (*.ply)");
		std::string direccion = fileName.toStdString();
	
		MasterHandler::GetThread()->set_exportObject(actor, direccion);
	}
	else
		emit signal_Warning(WARN_NO_SUPERFICIE_SELECCIONADA);
}

//VER LUEGO
void SimpleView::slotOpenFile()
{
	QString dir = QFileDialog::getOpenFileName(this, tr("Abrir Archivo"), QString::fromStdString(path), 
		"Formatos Soportados (*.obj *.stl *.ply);;Archivo Obj (*.obj) ;; Archivo Stl (*.stl) ;; Archivo Ply (*.ply)");
	//QString fileName = QFileDialog::getOpenFileUrl()

	path = dir.toStdString();
	if (dir.length() > 3) {

		MasterHandler::GetThread()->set_importObject(path);
	}
}

void SimpleView::slotVistaComparacion()
{
	if(MasterHandler::GetVolumeInteractor()->GetActualVolume() != nullptr)
		MasterHandler::GetCameraHandler()->setCompareMode();
	else {
		emit signal_Warning(WARN_NO_VOLUMEN_LISTA);
		this->ui->actionIniciar_Comparacion->setChecked(false);
	}
}

void SimpleView::slotResetCamera()
{
	MasterHandler::GetCameraHandler()->ResetCamera();
}

void SimpleView::slotAcercaDe()
{
	
	//Consola::appendln("deberia tener 7");

	acercade->show();
}

void SimpleView::slotManual()
{
	//"libs/pdf.js/web/manual.pdf"
	manual->OpenPDF("libs/PDFObject/viewer.html");

}

void SimpleView::slotCortePreciso()
{
	MasterHandler::GetVolumeInteractor()->setCortePreciso();
}

void SimpleView::slotClearConsole()
{
	Consola::clear();
}

void SimpleView::onProgressChanged(int p)
{
	emit visibleProgressBar(true);
	barraProgreso->setValue(p);
}

void SimpleView::onVisibleProgressBar(bool b)
{
	CallbacksClass::lockProgressBar();

	barraProgreso->setVisible(b);

	CallbacksClass::unlockProgressBar();
}

void SimpleView::slot_Show_QualityWidget()
{
	qualityWidget->show();
}

void SimpleView::slotExportCaptura()
{

	QString fileName = QFileDialog::getSaveFileName(this, "SimpleView - Guardar como", "",
		"Archivo PNG (*.png) ;; Archivo JPG (*.jpg)");
	
	slot_SaveCapture(fileName);
	
}

void SimpleView::slotConvertToGCode()
{
	vtkSmartPointer<myVolume> vol = MasterHandler::GetVolumeInteractor()->GetActualVolume();

	if (vol != nullptr) {
		MasterHandler::GetThread()->set_convertToGCode(vol, 0.15);
	}
	else {
		emit signal_Warning(WARN_NO_VOLUMEN_LISTA);
	}
}

void SimpleView::slot_SaveCapture(QString path)
{

	vtkSmartPointer<vtkWindowToImageFilter> windowToImageFilter =
		vtkSmartPointer<vtkWindowToImageFilter>::New();
	windowToImageFilter->SetInput(MasterHandler::GetVolumeInteractor()->GetRenderWindow());
	//windowToImageFilter->Set(3); //set the resolution of the output image (3 times the current resolution of vtkSmartPointer<vtk render window)
	windowToImageFilter->SetInputBufferTypeToRGB(); //also record the alpha (transparency) channel
	windowToImageFilter->ReadFrontBufferOff(); // read from the back bufferwindowToImageFilter
	windowToImageFilter->SetViewport(MasterHandler::GetVolumeInteractor()->GetRenderWindow()->GetRenderers()->GetFirstRenderer()->GetViewport());
	windowToImageFilter->Update();

	vtkSmartPointer<vtkPNGWriter> writer =
		vtkSmartPointer<vtkPNGWriter>::New();
	writer->SetFileName(QString(path).toStdString().data());
	writer->SetInputConnection(windowToImageFilter->GetOutputPort());
	writer->Write();
}

void SimpleView::slot_renderizar() {
	MasterHandler::Renderizar();
}




void SimpleView::slot_WriteLabel(QString str) {
	Consola::writeLabel(str);
}

void SimpleView::slot_Error(QString str)
{
	Dialog::mensajeError(str);
}

void SimpleView::slot_Warning(QString str)
{
	Dialog::mensajeWarning(str);
}


void SimpleView::slot_Info(QString str) {
	Dialog::mensajeInfo(str);
}

#include <TransferFunctionsList.h>
#include <vtkImageExport.h>
#include <vtkPointData.h>

void SimpleView::slot_take_snapshots(vtkRenderWindow* rw, myVolume* volumen)
{
	slot_WriteLabel(QString("Creando Funciones Transferencia"));

	int num = 8;

	rw->GetRenderers()->GetFirstRenderer()->AddVolume(volumen);
	

	for (int j = 0; j < num; j++) {
		if (volumen != nullptr) {

			volumen->restoreValues();
			//recupero los valores aqu� de la imagen
			double* vec = (TransferFunctionsList::GetTransferFunctionArray()[j]);


			for (int i = 0; i < NUMERODEPUNTOS_SCALAR; i++) {

				volumen->addOpacityPoint(i, vec[i]);
			}

			
		}
		rw->Render();

		vtkSmartPointer<vtkWindowToImageFilter> windowToImageFilter =
			vtkSmartPointer<vtkWindowToImageFilter>::New();
		windowToImageFilter->SetInput(rw);
		//windowToImageFilter->Set(3); //set the resolution of the output image (3 times the current resolution of vtkSmartPointer<vtk render window)
		windowToImageFilter->SetInputBufferTypeToRGB(); //also record the alpha (transparency) channel
		windowToImageFilter->ReadFrontBufferOn(); // read from the back bufferwindowToImageFilter
		windowToImageFilter->SetViewport(rw->GetRenderers()->GetFirstRenderer()->GetViewport());
		windowToImageFilter->Update();

		vtkSmartPointer<vtkPNGWriter> writer =
			vtkSmartPointer<vtkPNGWriter>::New();

		vtkSmartPointer<vtkImageData> image = vtkSmartPointer<vtkImageData>::New();

		image->DeepCopy(windowToImageFilter->GetOutput());


		QString path = QString(QString("temp") + QString::number(j) + QString(".png"));

		writer->SetFileName(path.toStdString().data());
		//Q:\CRUDO\DICOM\S01890\S20
		writer->SetInputData(image);
		writer->Write();

		int* dims = image->GetDimensions();
		int c = image->GetNumberOfScalarComponents();
		unsigned char *cImage = new unsigned char[dims[0] * dims[1] * dims[2]* image->GetNumberOfScalarComponents()];

		for (int k = 0; k < dims[0]; k++) {
			for (int l = 0; l < dims[1]; l++) {
				for (int m = 0; m < c; m++) {
					cImage[(k*dims[1] + l)*c + m] = image->GetScalarComponentAsDouble(k, l, 0, m);
				}
			}
		}

		QImageReader imgreader(path);


		QImage qimgOriginal = imgreader.read();

		//GUARDAR LOS PIXMAP EN MYVOLUME
		QPixmap * pix1 = new QPixmap(QPixmap::fromImage(qimgOriginal));
		volumen->addImage(pix1, j);

	}

	volumen->restoreValues();
	//recupero los valores aqu� de la imagen
	double* vec = (TransferFunctionsList::GetTransferFunctionArray()[3]);


	for (int i = 0; i < NUMERODEPUNTOS_SCALAR; i++) {

		volumen->addOpacityPoint(i, vec[i]);
	}



	rw->GetRenderers()->GetFirstRenderer()->RemoveVolume(volumen);
	
	rw->Finalize();
	
	slot_WriteLabel("Finalizado");
	//MasterHandler::GetThread()->unlock_Render();
}

void SimpleView::slot_RemoveActor(vtkActor* actor) {
	vtkSmartPointer<vtkActor> a = actor;
	MasterHandler::GetSurfaceInteractor()->removeActor(a);
	MasterHandler::GetThread()->unlock_Actor();
	//MasterHandler::GetVolumeInteractor()->setVolume(MasterHandler::get_last_volume());
}
