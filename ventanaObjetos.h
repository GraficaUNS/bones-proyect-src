#ifndef VENTANAOBJETOS_H
#define VENTANAOBJETOS_H

#include <QDialog>

namespace Ui {
class ventanaObjetos;
}

class ventanaObjetos : public QDialog
{
    Q_OBJECT

public:
    explicit ventanaObjetos(QWidget *parent = 0);
    ~ventanaObjetos();

private slots:
	void on_botonCerrar_clicked();
	void on_botonAgregar_clicked();

private:
    Ui::ventanaObjetos *ui;
};

#endif // VENTANAOBJETOS_H
