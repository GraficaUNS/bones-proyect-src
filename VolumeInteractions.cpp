#include "VolumeInteractions.h"
#include "Consola.h"
#include "ListInteractions.h"
#include "VolumeToSurface.h"

#include <qmath.h>
#include "MyCustomPlot.h"
#include <QSplitter>
#include <vtkPiecewiseFunction.h>
#include <myVolume.h>
#include <vtkVolumeProperty.h>
#include <vtkVolumePicker.h>
#include <vtkSmartPointer.h>
#include <vtkRenderWindow.h>
#include <vtkRendererCollection.h>
#include <vtkVolumeMapper.h>
#include <vtkDataSet.h>
#include <vtkDICOMImageReader.h>
#include <vtkImageData.h>
#include <vtkPropPicker.h>
#include <vtkPlane.h>
#include <vtkPlaneCollection.h>
#include <vtkImageActor.h> 
#include <vtkImageSliceMapper.h>
#include <vtkDICOMWriter.h>
#include <vtkDICOMMRGenerator.h>
#include <vtkDICOMMetaData.h>
#include <vtkWindowToImageFilter.h>
#include <vtkPNGWriter.h>


#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

#include "tablaPaciente.h"

#include <vtkMatrix4x4.h>

#include "Dialog.h"
#include "transferFunction.h"
#include "ThreadHandler.h"
#include <qlist.h>
#include "sliceVolume.h"

class CameraHandler;
IOVolume * claseIO = nullptr;

VolumeInteractions::VolumeInteractions()
{
	coleccion = vtkSmartPointer<vtkVolumeCollection>::New();
}

VolumeInteractions::~VolumeInteractions()
{
}

vtkSmartPointer<vtkRenderWindow > VolumeInteractions::GetRenderWindow()
{
	return renwin;
}

vtkSmartPointer<vtkRenderWindow> VolumeInteractions::GetRenderWindow2()
{
	return renWinSecondPlane;
}
#include <vtkRenderer.h>
#include <vtkCamera.h>
#include <TransferFunctionsList.h>

void VolumeInteractions::SetRenderWindow(vtkSmartPointer<vtkRenderWindow > ren)
{
	renwin = ren;
	renwin->SetMultiSamples(0);
	renWinSecondPlane = vtkSmartPointer<vtkRenderWindow>::New();

	vtkSmartPointer<vtkRenderer> rn = vtkSmartPointer<vtkRenderer>::New();
	renWinSecondPlane->AddRenderer(rn);

	double CameraPos[3] = {
		393.697
		,
		361.101
		,
		-379.154
	};
	double CameraUp[3] = {
		-0.369397
		,
		-0.404254
		,
		-0.836735
	};

	TransferFunctionsList::Init();

	rn->ResetCamera();
	rn->GetActiveCamera()->SetPosition(CameraPos);
	rn->GetActiveCamera()->SetViewUp(CameraUp);

}

void VolumeInteractions::setVolume(int port)
{

	if (port == 0) {
		if (lastVolume != nullptr) {
			renwin->GetRenderers()->GetFirstRenderer()->RemoveVolume(lastVolume);
			lastVolume = nullptr;
			getCustomPlot()->setVolume(nullptr);
		}
	}
	else {
		emit MasterHandler::GetSimpleView()->signal_WriteLabel("Cargando volumen");

		bool encontre = false;

		int cant = coleccion->GetNumberOfItems();

		int i = 0;
		vtkSmartPointer<myVolume> volumen = nullptr;
		while (i < cant && !encontre) {
			volumen = (myVolume*)coleccion->GetItemAsObject(i);
			i++;
			if (volumen->GetCustomPort() == port) {
				encontre = true;
			}
		}

		MasterHandler::GetSimpleView()->progressChanged(20);

		if (!encontre)
			volumen = nullptr;
		else{
			if (volumen != lastVolume) {
				if (lastVolume != nullptr) {
					renwin->GetRenderers()->GetFirstRenderer()->RemoveVolume(lastVolume);
					if(lastPickedVolume != nullptr)
						lastPickedVolume->setPicked(false);
				}

				lastVolume = volumen;

				emit MasterHandler::GetSimpleView()->signal_WriteLabel("Renderizando volumen");

				renwin->GetRenderers()->GetFirstRenderer()->AddVolume(volumen);

				MasterHandler::GetSimpleView()->progressChanged(30);
				if (volumen->getPaciente() != nullptr)
					MasterHandler::getTablaPaciente()->insertarPaciente(volumen->getPaciente());

				if (MasterHandler::GetCameraHandler()->isFourCamera()) {
					
					MasterHandler::GetCameraHandler()->setFourCameras();
				}
			}
			emit MasterHandler::GetSimpleView()->signal_WriteLabel("Cargando datos de Funcion Transferencia");
			getCustomPlot()->setVolume(volumen);
			
			
			MasterHandler::GetSimpleView()->progressChanged(90);
		}
	}

	emit MasterHandler::GetSimpleView()->signal_WriteLabel("Renderizando");
	renwin->Render();


	MasterHandler::GetSimpleView()->progressChanged(100);
	emit MasterHandler::GetSimpleView()->signal_WriteLabel("Finalizado");
	MasterHandler::GetSimpleView()->visibleProgressBar(false);
}

void VolumeInteractions::registrarVolumen(vtkSmartPointer<myVolume> vol, QString name)
{
	if (vol->getImageData() != nullptr) {
		vol->SetCustomPort(portReg);

		coleccion->AddItem(vol);

		
		/*
		parte para sacar las fotos a las diferentes funciones de transferencia
		Se rompe todo aca, no deja ni seleccionar el hueso
		
		
		vtkSmartPointer<vtkWindowToImageFilter> windowToImageFilter =
			vtkSmartPointer<vtkWindowToImageFilter>::New();
		windowToImageFilter->SetInput(MasterHandler::GetVolumeInteractor()->GetRenderWindow());
		//windowToImageFilter->Set(3); //set the resolution of the output image (3 times the current resolution of vtkSmartPointer<vtk render window)
		windowToImageFilter->SetInputBufferTypeToRGB(); //also record the alpha (transparency) channel
		windowToImageFilter->ReadFrontBufferOff(); // read from the back bufferwindowToImageFilter
		windowToImageFilter->SetViewport(MasterHandler::GetVolumeInteractor()->GetRenderWindow()->GetRenderers()->GetFirstRenderer()->GetViewport());
		windowToImageFilter->Update();

		vtkSmartPointer<vtkPNGWriter> writer =
			vtkSmartPointer<vtkPNGWriter>::New();
		writer->SetFileName(QString("prueba.png").toStdString().data());
		//Q:\CRUDO\DICOM\S01890\S20
		writer->SetInputConnection(windowToImageFilter->GetOutputPort());
		writer->Write();

		/*
		termina aqu�
		*/
		/**
		if (vol->getImageData()->GetScalarRange()[0] > -50) {
			emit MasterHandler::GetSimpleView()->signal_Warning(QString("Los datos cargados no tienen un formato estandar\nEsto provoca que no se puedan visualizar correctamente\nConvirtiendo a formato standard (puede demorar unos segundos)"));
			
			bool b = true;

			double Range = vol->getImageData()->GetScalarRange()[1] - vol->getImageData()->GetScalarRange()[0];

			if (b) {

				vtkSmartPointer<vtkImageData> imageData = vol->getImageData();

				int *dims = imageData->GetDimensions();

				int recorridas = 1;
				int numCmp = dims[0] * dims[1] * dims[2];

				int dim_x = dims[0];
				int dim_y = dims[1];
				int dim_z = dims[2];
				emit MasterHandler::GetSimpleView()->visibleProgressBar(true);
				
				emit MasterHandler::GetSimpleView()->signal_WriteLabel("Procesando volumen: 0%");

				double max = vol->getImageData()->GetScalarRange()[1];
				double min = vol->getImageData()->GetScalarRange()[0];

				//creo una imagen vacia
				for (int i = 0; i < dim_x; i++) {
					for (int j = 0; j < dim_y; j++) {
						for (int k = 0; k < dim_z; k++) {
							//progress->setValue((int)(recorridas*100.0f / (double)numCmp));

							recorridas++;
							double d = imageData->GetScalarComponentAsDouble(i, j, k, 0);
							double newd = 0;
							//			  (inverse  ) (convertion      )
							if(d!=0)
								newd = ((Range - d) - vol->getImageData()->GetScalarRange()[0])*(1500.0/Range) - INIT_TRANSFER_FUNCTION_OPACITY_VALUE;

							imageData->SetScalarComponentFromDouble(i, j, k, 0, newd);

						} 
						emit MasterHandler::GetSimpleView()->progressChanged(recorridas*100.0f / (double)numCmp);
						emit MasterHandler::GetSimpleView()->signal_WriteLabel(QString(QString("Procesando volumen: ") + QString::number((int)(recorridas*100.0f / (double)numCmp)) + QString("%")));

					}
				}
				emit MasterHandler::GetSimpleView()->visibleProgressBar(false);
			}

		}
		/**/
		if (name == nullptr)
			MasterHandler::getListWidget()->addNewItem(portReg, QString::number(portReg) + QString(": Volumen"));
		else
			MasterHandler::getListWidget()->addNewItem(portReg, QString::number(portReg) + QString(": ") + name);


		double* bounds = vol->GetBounds();//TODO debo recuperar los bounds

		this->setBounds(bounds, vol);

		portReg++;


	}
	else {
		emit MasterHandler::GetSimpleView()->signal_Error("No se pudo leer el directorio");
	}
}

void VolumeInteractions::initGUI(QSplitter * splitter)
{

	ui = new Ui_transferFunction;

	ui->setupUi(splitter);

	QWidget* centralwidget = ui->centralWidget;

	customPlot_Opacity = new MyCustomPlot(centralwidget);
	ui->verticalLayout_2->addWidget(customPlot_Opacity);

	customPlot_Opacity->addCkeckBox_opacity(ui->check1);
	customPlot_Opacity->addCkeckBox_gradient(ui->check2);
	customPlot_Opacity->addCkeckBox_color(ui->check3);

	customPlot_Opacity->addActionRestore(ui->restoreButton);
	customPlot_Opacity->addActionSelectTransferFunction(ui->selectButton);
	
	customPlot_Opacity->setIlluminationSpins(ui->doubleSpinBox_Ambiente, ui->doubleSpinBox_Difuso, ui->doubleSpinBox_Especular, ui->doubleSpinBox_PotencaEspecular);

	customPlot_Opacity->addComboBoxBlendModes(ui->MapeoComboBox);
}


MyCustomPlot * VolumeInteractions::getCustomPlot()
{
	return customPlot_Opacity;
}

double pointSelected[4] = {-1,-1,-1};

void VolumeInteractions::mousePressEvent(CameraHandler* camera, QMouseEvent *event)
{
	
		int* point = camera->GetRenderWindowInteractor()->GetEventPosition();
		
		vtkSmartPointer<vtkVolumePicker> pickerVolumen = vtkSmartPointer<vtkVolumePicker>::New();
		//TODO esto cambia cuando hay mas viewports
		pickerVolumen->Pick(point[0], point[1], 0, renwin->GetRenderers()->GetFirstRenderer());
		vtkSmartPointer<myVolume> volumen = (myVolume*)pickerVolumen->GetVolume();

		
		if (volumen != nullptr) {

			if (volumen != lastPickedVolume) {

				if (MasterHandler::GetCameraHandler()->isCompareCamera())
					return;


				//getCustomPlot()->setVolume(volumen);
				if(!cortePreciso)
					volumen->setPicked(true);

				lastPickedVolume = volumen;

				

				if(volumen->getPaciente()!=nullptr)
					MasterHandler::getTablaPaciente()->insertarPaciente(volumen->getPaciente());
			}
			
		}
		else {// se deseleccion a todo
			if (lastPickedVolume != nullptr) {
				lastPickedVolume->setPicked(false);
			}
			
			lastPickedVolume = nullptr;
		}

		/**/
		GetRenderWindow()->Render();
		
}

void VolumeInteractions::mouseMoveEvent(CameraHandler * camera, QMouseEvent *event)
{
	
}

void VolumeInteractions::mouseReleaseEvent(CameraHandler * camera, QMouseEvent *event)
{
}


void VolumeInteractions::RemoveSelectedVolume() {
	if (lastPickedVolume != nullptr) {

		emit MasterHandler::GetSimpleView()->signal_WriteLabel("Removiendo volumen");

		setVolume(0);

		MasterHandler::getListWidget()->deleteItemFromPort(lastPickedVolume->GetCustomPort());
		
		coleccion->RemoveItem(lastPickedVolume);

		if (MasterHandler::GetCameraHandler()->isFourCamera()) {
			MasterHandler::GetCameraHandler()->setFourCameras();
			MasterHandler::GetCameraHandler()->removeSliceVolumes();
		}

		GetRenderWindow()->Render();
		
		lastPickedVolume = nullptr;
		
		emit MasterHandler::GetSimpleView()->signal_WriteLabel("Finalizado");
	}

}



vtkSmartPointer<myVolume> VolumeInteractions::getPickedVolume()
{
	return lastPickedVolume;
}

void VolumeInteractions::clearTransforms()
{
	if (lastVolume != nullptr) {
		lastVolume->SetPosition(0, 0, 0);
		lastVolume->SetOrientation(0, 0, 0);
		lastVolume->centrar();
	}
	
	GetRenderWindow()->Render();
}

void AddNeighbours11x11(vtkSmartPointer<vtkImageData> input, 
	int i, int j, int k, 
	int dim_x, int dim_y, int dim_z, vtkSmartPointer<vtkImageData> output) {

	
	int minI = MAX(i, 0);
	int minJ = MAX(j, 0);
	int minK = MAX(k, 0);

	int maxI = MIN(i + 1, dim_x);
	int maxJ = MIN(j + 1, dim_y);
	int maxK = MIN(k + 1, dim_z);



	for (int I = minI; I < maxI; I++) {
		for (int J = minJ; J < maxJ; J++) {
			for (int K = minK; K < maxK; K++) {
				
				if (output->GetScalarComponentAsDouble(I, J, K, 0) == RADIO_DENSITY_ZERO) {
					double v = input->GetScalarComponentAsDouble(I, J, K, 0);
					output->SetScalarComponentFromDouble(I, J, K, 0, v);
				}
			}
		}
	}

}



int VolumeInteractions::separarObjeto_rec(vtkSmartPointer<vtkImageData> input, int* contorno, int cant_contorno, int min, int max, vtkSmartPointer<vtkImageData> output, vtkSmartPointer<vtkImageData> acumulado, int* contornoExterno, int minCeldas)
{
	int n = 1;
	int mod = 10000;

	int cant = cant_contorno;

	int cant_contornoExterno = 0;

	int sum = 1;
	double aux;
	//vtkSmartPointer<vtkImageData> aux_img = vtkSmartPointer<vtkImageData>::New();
	//aux_img->DeepCopy(output);
	int x = contorno[0];
	int y = contorno[1];
	int z = contorno[2];

	double d = input->GetScalarComponentAsDouble(x, y, z, 0);
	output->SetScalarComponentFromDouble(x, y, z, 0, d);
	acumulado->SetScalarComponentFromDouble(x, y, z, 0, d);

	int *dims = input->GetDimensions();

	//AddNeighbours11x11(input, x, y, z, dims, output);

	int dim_x = dims[0];
	int dim_y = dims[1];
	int dim_z = dims[2];


	while (cant > 0) {
		cant--;
		//extraigo esa componente
		int x = contorno[cant * 3 + 0];
		int y = contorno[cant * 3 + 1];
		int z = contorno[cant * 3 + 2];

		//AddNeighbours11x11(input, x, y, z, dims, output);

		if(n==0)
			emit MasterHandler::GetSimpleView()->signal_WriteLabel(QString(QString("Analizando, restan ") + QString::number(cant) + QString(" celdas")));
		n = (n + 1) % mod;

		//extraigo los 26-vecinos de las coordenadas x, y, z y los agrego al contorno si no estaban en la salida
		//x + 1
		if (x + 1 < dim_x) {
			if (output->GetScalarComponentAsDouble(x + 1, y, z, 0) == RADIO_DENSITY_ZERO) {
				aux = input->GetScalarComponentAsDouble(x + 1, y, z, 0);
				// x = 1, y = 0, z = 0
				if (aux > min && aux < max) {
					sum++;
					contorno[cant * 3 + 0] = x + 1;
					contorno[cant * 3 + 1] = y;
					contorno[cant * 3 + 2] = z;
					output->SetScalarComponentFromDouble(x + 1, y, z, 0, aux);
					
					acumulado->SetScalarComponentFromDouble(x + 1, y, z, 0, aux);
					cant++;
				}
				else {
					contornoExterno[cant_contornoExterno * 3 + 0] = x + 1;
					contornoExterno[cant_contornoExterno * 3 + 1] = y;
					contornoExterno[cant_contornoExterno * 3 + 2] = z;
					cant_contornoExterno++;
				}
			}
			//y + 1
			if (y + 1 < dim_y && output->GetScalarComponentAsDouble(x + 1, y + 1, z, 0) == RADIO_DENSITY_ZERO)
			{
				aux = input->GetScalarComponentAsDouble(x + 1, y + 1, z, 0);
				//x = 1, y = 1, z = 0
				if (aux > min && aux < max) {
					sum++;
					contorno[cant * 3 + 0] = x + 1;
					contorno[cant * 3 + 1] = y + 1;
					contorno[cant * 3 + 2] = z;
					output->SetScalarComponentFromDouble(x + 1, y + 1, z, 0, aux);
					
					acumulado->SetScalarComponentFromDouble(x + 1, y + 1, z, 0, aux);
					cant++;
				}
				else {
					contornoExterno[cant_contornoExterno * 3 + 0] = x + 1;
					contornoExterno[cant_contornoExterno * 3 + 1] = y + 1;
					contornoExterno[cant_contornoExterno * 3 + 2] = z;
					cant_contornoExterno++;
				}

				/*
				//z + 1
				if (z + 1 < dim_z && output->GetScalarComponentAsDouble(x + 1, y + 1, z + 1, 0) == RADIO_DENSITY_ZERO) {
				aux = input->GetScalarComponentAsDouble(x + 1, y + 1, z + 1, 0);
				if (aux > umbral) {
				sum++;
				contorno[cant * 3 + 0] = x + 1;
				contorno[cant * 3 + 1] = y + 1;
				contorno[cant * 3 + 2] = z + 1;
				output->SetScalarComponentFromDouble(x + 1, y + 1, z + 1, 0, aux);
				acumulado->SetScalarComponentFromDouble(x + 1, y + 1, z + 1, 0, aux);
				cant++;
				}
				}
				/*
				//z - 1
				if (z - 1 >= 0 && output->GetScalarComponentAsDouble(x + 1, y + 1, z - 1, 0) == RADIO_DENSITY_ZERO) {
				aux = input->GetScalarComponentAsDouble(x + 1, y + 1, z - 1, 0);
				if (aux > umbral) {
				sum++;
				contorno[cant * 3 + 0] = x + 1;
				contorno[cant * 3 + 1] = y + 1;
				contorno[cant * 3 + 2] = z - 1;
				output->SetScalarComponentFromDouble(x + 1, y + 1, z - 1, 0, aux);
				acumulado->SetScalarComponentFromDouble(x + 1, y + 1, z - 1, 0, aux);
				cant++;
				}
				}
				/**/

			}
			//y - 1
			if (y - 1 >= 0 && output->GetScalarComponentAsDouble(x + 1, y - 1, z, 0) == RADIO_DENSITY_ZERO)
			{
				aux = input->GetScalarComponentAsDouble(x + 1, y - 1, z, 0);
				//x = 1, y = -1, z = 0
				if (aux > min && aux < max) {
					sum++;
					contorno[cant * 3 + 0] = x + 1;
					contorno[cant * 3 + 1] = y - 1;
					contorno[cant * 3 + 2] = z;
					output->SetScalarComponentFromDouble(x + 1, y - 1, z, 0, aux);
					acumulado->SetScalarComponentFromDouble(x + 1, y - 1, z, 0, aux);
					cant++;
				}
				else {
					contornoExterno[cant_contornoExterno * 3 + 0] = x + 1;
					contornoExterno[cant_contornoExterno * 3 + 1] = y - 1;
					contornoExterno[cant_contornoExterno * 3 + 2] = z;
					cant_contornoExterno++;
				}
				//z + 1
				/*
				if (z + 1 < dim_z && output->GetScalarComponentAsDouble(x + 1, y - 1, z + 1, 0) == RADIO_DENSITY_ZERO) {
				aux = input->GetScalarComponentAsDouble(x + 1, y - 1, z + 1, 0);
				if (aux > umbral) {
				sum++;
				contorno[cant * 3 + 0] = x + 1;
				contorno[cant * 3 + 1] = y - 1;
				contorno[cant * 3 + 2] = z + 1;
				output->SetScalarComponentFromDouble(x + 1, y - 1, z + 1, 0, aux);
				acumulado->SetScalarComponentFromDouble(x + 1, y - 1, z + 1, 0, aux);
				cant++;
				}
				}
				/*
				//z - 1
				if (z - 1 >= 0 && output->GetScalarComponentAsDouble(x + 1, y - 1, z - 1, 0) == RADIO_DENSITY_ZERO) {
				aux = input->GetScalarComponentAsDouble(x + 1, y - 1, z - 1, 0);
				if (aux > umbral ) {
				sum++;
				contorno[cant * 3 + 0] = x + 1;
				contorno[cant * 3 + 1] = y - 1;
				contorno[cant * 3 + 2] = z - 1;
				output->SetScalarComponentFromDouble(x + 1, y - 1, z - 1, 0, aux);
				acumulado->SetScalarComponentFromDouble(x + 1, y - 1, z - 1, 0, aux);
				cant++;
				}
				}
				/**/
			}

			//x = 1, y = 0, z = 1 
			if (z + 1 < dim_z && output->GetScalarComponentAsDouble(x + 1, y, z + 1, 0) == RADIO_DENSITY_ZERO)
			{
				aux = input->GetScalarComponentAsDouble(x + 1, y, z + 1, 0);
				if (aux > min && aux < max) {
					sum++;
					contorno[cant * 3 + 0] = x + 1;
					contorno[cant * 3 + 1] = y;
					contorno[cant * 3 + 2] = z + 1;
					output->SetScalarComponentFromDouble(x + 1, y, z + 1, 0, aux);
					acumulado->SetScalarComponentFromDouble(x + 1, y, z + 1, 0, aux);
					cant++;
				}
				else {
					contornoExterno[cant_contornoExterno * 3 + 0] = x + 1;
					contornoExterno[cant_contornoExterno * 3 + 1] = y;
					contornoExterno[cant_contornoExterno * 3 + 2] = z + 1;
					cant_contornoExterno++;
				}
			}
			//x = 1, y = 0, z = - 1
			if (z - 1 >= 0 && output->GetScalarComponentAsDouble(x + 1, y, z - 1, 0) == RADIO_DENSITY_ZERO)
			{
				aux = input->GetScalarComponentAsDouble(x + 1, y, z - 1, 0);
				if (aux > min && aux < max) {
					sum++;
					contorno[cant * 3 + 0] = x + 1;
					contorno[cant * 3 + 1] = y;
					contorno[cant * 3 + 2] = z - 1;
					output->SetScalarComponentFromDouble(x + 1, y, z - 1, 0, aux);
					acumulado->SetScalarComponentFromDouble(x + 1, y, z - 1, 0, aux);
					cant++;
				}
				else {
					contornoExterno[cant_contornoExterno * 3 + 0] = x + 1;
					contornoExterno[cant_contornoExterno * 3 + 1] = y;
					contornoExterno[cant_contornoExterno * 3 + 2] = z - 1;
					cant_contornoExterno++;
				}
			}

		}
		//check 1
		//x - 1;
		if (x - 1 >= 0)
		{
			if (output->GetScalarComponentAsDouble(x - 1, y, z, 0) == RADIO_DENSITY_ZERO)
			{
				aux = input->GetScalarComponentAsDouble(x - 1, y, z, 0);
				//y = 0 z = 0
				if (aux > min && aux < max) {
					sum++;
					contorno[cant * 3 + 0] = x - 1;
					contorno[cant * 3 + 1] = y;
					contorno[cant * 3 + 2] = z;
					output->SetScalarComponentFromDouble(x - 1, y, z, 0, aux);
					acumulado->SetScalarComponentFromDouble(x - 1, y, z, 0, aux);
					cant++;
				}
				else {
					contornoExterno[cant_contornoExterno * 3 + 0] = x - 1;
					contornoExterno[cant_contornoExterno * 3 + 1] = y;
					contornoExterno[cant_contornoExterno * 3 + 2] = z;
					cant_contornoExterno++;
				}
			}
			//y+1 
			if (y + 1 < dim_y && output->GetScalarComponentAsDouble(x - 1, y + 1, z, 0) == RADIO_DENSITY_ZERO)
			{
				aux = input->GetScalarComponentAsDouble(x - 1, y + 1, z, 0);
				//z = 0
				if (aux > min && aux < max) {
					sum++;
					contorno[cant * 3 + 0] = x - 1;
					contorno[cant * 3 + 1] = y + 1;
					contorno[cant * 3 + 2] = z;
					output->SetScalarComponentFromDouble(x - 1, y + 1, z, 0, aux);
					acumulado->SetScalarComponentFromDouble(x - 1, y + 1, z, 0, aux);
					cant++;
				}
				else {
					contornoExterno[cant_contornoExterno * 3 + 0] = x - 1;
					contornoExterno[cant_contornoExterno * 3 + 1] = y + 1;
					contornoExterno[cant_contornoExterno * 3 + 2] = z;
					cant_contornoExterno++;
				}
				/*
				//z + 1
				if (z + 1 < dim_z && output->GetScalarComponentAsDouble(x - 1, y + 1, z + 1, 0) == RADIO_DENSITY_ZERO) {
				aux = input->GetScalarComponentAsDouble(x - 1, y + 1, z + 1, 0);
				if (aux > umbral) {
				sum++;
				contorno[cant * 3 + 0] = x - 1;
				contorno[cant * 3 + 1] = y + 1;
				contorno[cant * 3 + 2] = z + 1;
				output->SetScalarComponentFromDouble(x - 1, y + 1, z + 1, 0, aux);
				acumulado->SetScalarComponentFromDouble(x - 1, y + 1, z + 1, 0, aux);
				cant++;
				}
				}
				//z - 1
				/*
				if (z - 1 >= 0 && output->GetScalarComponentAsDouble(x - 1, y + 1, z - 1, 0) == RADIO_DENSITY_ZERO) {
				aux = input->GetScalarComponentAsDouble(x - 1, y + 1, z - 1, 0);
				if (aux > umbral) {
				sum++;
				contorno[cant * 3 + 0] = x - 1;
				contorno[cant * 3 + 1] = y + 1;
				contorno[cant * 3 + 2] = z - 1;
				output->SetScalarComponentFromDouble(x - 1, y + 1, z - 1, 0, aux);
				acumulado->SetScalarComponentFromDouble(x - 1, y + 1, z - 1, 0, aux);
				cant++;
				}
				}
				/**/
			}
			//y - 1
			if (y - 1 >= 0 && output->GetScalarComponentAsDouble(x - 1, y - 1, z, 0) == RADIO_DENSITY_ZERO)
			{
				aux = input->GetScalarComponentAsDouble(x - 1, y - 1, z, 0);
				//z = 0
				if (aux > min && aux < max) {
					sum++;
					contorno[cant * 3 + 0] = x - 1;
					contorno[cant * 3 + 1] = y - 1;
					contorno[cant * 3 + 2] = z;
					output->SetScalarComponentFromDouble(x - 1, y - 1, z, 0, aux);
					acumulado->SetScalarComponentFromDouble(x - 1, y - 1, z, 0, aux);
					cant++;
				}
				else {
					contornoExterno[cant_contornoExterno * 3 + 0] = x - 1;
					contornoExterno[cant_contornoExterno * 3 + 1] = y - 1;
					contornoExterno[cant_contornoExterno * 3 + 2] = z;
					cant_contornoExterno++;
				}
				//z + 1
				/*
				if (z + 1 < dim_z && output->GetScalarComponentAsDouble(x - 1, y - 1, z + 1, 0) == RADIO_DENSITY_ZERO) {
				aux = input->GetScalarComponentAsDouble(x - 1, y - 1, z + 1, 0);
				if (aux > umbral) {
				sum++;
				contorno[cant * 3 + 0] = x - 1;
				contorno[cant * 3 + 1] = y - 1;
				contorno[cant * 3 + 2] = z + 1;
				output->SetScalarComponentFromDouble(x - 1, y - 1, z + 1, 0, aux);
				acumulado->SetScalarComponentFromDouble(x - 1, y - 1, z + 1, 0, aux);
				cant++;
				}
				}
				//z - 1
				/*
				if (z - 1 >= 0 && output->GetScalarComponentAsDouble(x - 1, y - 1, z - 1, 0) == RADIO_DENSITY_ZERO) {
				aux = input->GetScalarComponentAsDouble(x - 1, y - 1, z - 1, 0);
				if (aux > umbral) {
				sum++;
				contorno[cant * 3 + 0] = x - 1;
				contorno[cant * 3 + 1] = y - 1;
				contorno[cant * 3 + 2] = z - 1;
				output->SetScalarComponentFromDouble(x - 1, y - 1, z - 1, 0, aux);
				acumulado->SetScalarComponentFromDouble(x - 1, y - 1, z - 1, 0, aux);
				cant++;
				}
				}
				/**/
			}
			//z + 1 y = 0
			if (z + 1 < dim_z && output->GetScalarComponentAsDouble(x - 1, y, z + 1, 0) == RADIO_DENSITY_ZERO)
			{
				aux = input->GetScalarComponentAsDouble(x - 1, y, z + 1, 0);
				if (aux > min && aux < max) {
					sum++;
					contorno[cant * 3 + 0] = x - 1;
					contorno[cant * 3 + 1] = y;
					contorno[cant * 3 + 2] = z + 1;
					output->SetScalarComponentFromDouble(x - 1, y, z + 1, 0, aux);
					acumulado->SetScalarComponentFromDouble(x - 1, y, z + 1, 0, aux);
					cant++;
				}
				else {
					contornoExterno[cant_contornoExterno * 3 + 0] = x - 1;
					contornoExterno[cant_contornoExterno * 3 + 1] = y;
					contornoExterno[cant_contornoExterno * 3 + 2] = z + 1;
					cant_contornoExterno++; 
				}
			}
			//z - 1 y = 0
			if (z - 1 >= 0 && output->GetScalarComponentAsDouble(x - 1, y, z - 1, 0) == RADIO_DENSITY_ZERO)
			{
				aux = input->GetScalarComponentAsDouble(x - 1, y, z - 1, 0);
				if (aux > min && aux < max) {
					sum++;
					contorno[cant * 3 + 0] = x - 1;
					contorno[cant * 3 + 1] = y;
					contorno[cant * 3 + 2] = z - 1;
					output->SetScalarComponentFromDouble(x - 1, y, z - 1, 0, aux);
					acumulado->SetScalarComponentFromDouble(x - 1, y, z - 1, 0, aux);
					cant++;
				}
				else {
					contornoExterno[cant_contornoExterno * 3 + 0] = x - 1;
					contornoExterno[cant_contornoExterno * 3 + 1] = y;
					contornoExterno[cant_contornoExterno * 3 + 2] = z - 1;
					cant_contornoExterno++;
				}
			}
		}

		//x = 0
		//y + 1;
		if (y + 1 < dim_y) {
			if (output->GetScalarComponentAsDouble(x, y + 1, z, 0) == RADIO_DENSITY_ZERO)
			{
				aux = input->GetScalarComponentAsDouble(x, y + 1, z, 0);
				//plano
				if (aux > min && aux < max) {
					sum++;
					contorno[cant * 3 + 0] = x;
					contorno[cant * 3 + 1] = y + 1;
					contorno[cant * 3 + 2] = z;
					output->SetScalarComponentFromDouble(x, y + 1, z, 0, aux);
					acumulado->SetScalarComponentFromDouble(x, y + 1, z, 0, aux);
					cant++;
				}
				else {
					contornoExterno[cant_contornoExterno * 3 + 0] = x;
					contornoExterno[cant_contornoExterno * 3 + 1] = y + 1 ;
					contornoExterno[cant_contornoExterno * 3 + 2] = z;
					cant_contornoExterno++;
				}
			}
			//z + 1
			if (z + 1 < dim_z && output->GetScalarComponentAsDouble(x, y + 1, z + 1, 0) == RADIO_DENSITY_ZERO) {
				aux = input->GetScalarComponentAsDouble(x, y + 1, z + 1, 0);
				if (aux > min && aux < max) {
					sum++;
					contorno[cant * 3 + 0] = x;
					contorno[cant * 3 + 1] = y + 1;
					contorno[cant * 3 + 2] = z + 1;
					output->SetScalarComponentFromDouble(x, y + 1, z + 1, 0, aux);
					acumulado->SetScalarComponentFromDouble(x, y + 1, z + 1, 0, aux);
					cant++;
				}
				else {
					contornoExterno[cant_contornoExterno * 3 + 0] = x;
					contornoExterno[cant_contornoExterno * 3 + 1] = y + 1;
					contornoExterno[cant_contornoExterno * 3 + 2] = z + 1;
					cant_contornoExterno++;
				}
			}
			//z - 1
			if (z - 1 >= 0 && output->GetScalarComponentAsDouble(x, y + 1, z - 1, 0) == RADIO_DENSITY_ZERO) {
				aux = input->GetScalarComponentAsDouble(x, y + 1, z - 1, 0);
				if (aux > min && aux < max) {
					//sum++;
					contorno[cant * 3 + 0] = x;
					contorno[cant * 3 + 1] = y + 1;
					contorno[cant * 3 + 2] = z - 1;
					output->SetScalarComponentFromDouble(x, y + 1, z - 1, 0, aux);
					acumulado->SetScalarComponentFromDouble(x, y + 1, z - 1, 0, aux);
					cant++;
				}
				else {
					contornoExterno[cant_contornoExterno * 3 + 0] = x;
					contornoExterno[cant_contornoExterno * 3 + 1] = y + 1;
					contornoExterno[cant_contornoExterno * 3 + 2] = z - 1;
					cant_contornoExterno++;
				}
			}
		}
		//x = 0
		//y - 1
		if (y - 1 >= 0) {
			if (output->GetScalarComponentAsDouble(x, y - 1, z, 0) == RADIO_DENSITY_ZERO)
			{
				aux = input->GetScalarComponentAsDouble(x, y - 1, z, 0);
				if (aux > min && aux < max) {
					sum++;
					contorno[cant * 3 + 0] = x;
					contorno[cant * 3 + 1] = y - 1;
					contorno[cant * 3 + 2] = z;
					output->SetScalarComponentFromDouble(x, y - 1, z, 0, aux);
					acumulado->SetScalarComponentFromDouble(x, y - 1, z, 0, aux);
					cant++;
				}
				else {
					contornoExterno[cant_contornoExterno * 3 + 0] = x;
					contornoExterno[cant_contornoExterno * 3 + 1] = y - 1;
					contornoExterno[cant_contornoExterno * 3 + 2] = z;
					cant_contornoExterno++;
				}
			}
			//z + 1
			if (z + 1 < dim_z && output->GetScalarComponentAsDouble(x, y - 1, z + 1, 0) == RADIO_DENSITY_ZERO)
			{
				aux = input->GetScalarComponentAsDouble(x, y - 1, z + 1, 0);
				if (aux > min && aux < max) {
					sum++;
					contorno[cant * 3 + 0] = x;
					contorno[cant * 3 + 1] = y - 1;
					contorno[cant * 3 + 2] = z + 1;
					output->SetScalarComponentFromDouble(x, y - 1, z + 1, 0, aux);
					acumulado->SetScalarComponentFromDouble(x, y - 1, z + 1, 0, aux);
					cant++;
				}
				else {
					contornoExterno[cant_contornoExterno * 3 + 0] = x;
					contornoExterno[cant_contornoExterno * 3 + 1] = y - 1;
					contornoExterno[cant_contornoExterno * 3 + 2] = z + 1;
					cant_contornoExterno++;
				}
			}
			//z - 1
			if (z - 1 >= 0 && output->GetScalarComponentAsDouble(x, y - 1, z - 1, 0) == RADIO_DENSITY_ZERO)
			{
				aux = input->GetScalarComponentAsDouble(x, y - 1, z - 1, 0);
				if (aux > min && aux < max) {
					sum++;
					contorno[cant * 3 + 0] = x;
					contorno[cant * 3 + 1] = y - 1;
					contorno[cant * 3 + 2] = z - 1;
					output->SetScalarComponentFromDouble(x, y - 1, z - 1, 0, aux);
					acumulado->SetScalarComponentFromDouble(x, y - 1, z - 1, 0, aux);
					cant++;
				}
				else {
					contornoExterno[cant_contornoExterno * 3 + 0] = x;
					contornoExterno[cant_contornoExterno * 3 + 1] = y - 1;
					contornoExterno[cant_contornoExterno * 3 + 2] = z - 1;
					cant_contornoExterno++;
				}
			}
		}

		//x = 0 y = 0
		if (z + 1 < dim_z && output->GetScalarComponentAsDouble(x, y, z + 1, 0) == RADIO_DENSITY_ZERO)
		{
			aux = input->GetScalarComponentAsDouble(x, y, z + 1, 0);
			if (aux > min && aux < max) {
				sum++;
				contorno[cant * 3 + 0] = x;
				contorno[cant * 3 + 1] = y;
				contorno[cant * 3 + 2] = z + 1;
				output->SetScalarComponentFromDouble(x, y, z + 1, 0, aux);
				acumulado->SetScalarComponentFromDouble(x, y, z + 1, 0, aux);
				cant++;
			}
			else {
				contornoExterno[cant_contornoExterno * 3 + 0] = x;
				contornoExterno[cant_contornoExterno * 3 + 1] = y;
				contornoExterno[cant_contornoExterno * 3 + 2] = z + 1;
				cant_contornoExterno++;
			}
		}
		//x = 0 y = 0
		if (z - 1 >= 0 && output->GetScalarComponentAsDouble(x, y, z - 1, 0) == RADIO_DENSITY_ZERO)
		{
			aux = input->GetScalarComponentAsDouble(x, y, z - 1, 0);
			if (aux > min && aux < max) {
				sum++;
				contorno[cant * 3 + 0] = x;
				contorno[cant * 3 + 1] = y;
				contorno[cant * 3 + 2] = z - 1;
				output->SetScalarComponentFromDouble(x, y, z - 1, 0, aux);
				acumulado->SetScalarComponentFromDouble(x, y, z - 1, 0, aux);
				cant++;
			}
			else {
				contornoExterno[cant_contornoExterno * 3 + 0] = x;
				contornoExterno[cant_contornoExterno * 3 + 1] = y;
				contornoExterno[cant_contornoExterno * 3 + 2] = z - 1;
				cant_contornoExterno++;
			}
		}

	}

	

	if (sum > minCeldas) {
		mod = 500;
		n = 0;
		for (int i = 0; i < cant_contornoExterno; i++) {
			x = contornoExterno[i * 3];
			y = contornoExterno[i * 3 + 1];
			z = contornoExterno[i * 3 + 2];


			//AddNeighbours11x11(input, x, y, z, dim_x, dim_y, dim_z, output);
			double v = input->GetScalarComponentAsDouble(x, y, z, 0);
			output->SetScalarComponentFromDouble(x, y, z, 0, v);
			if (n == 0)
				emit MasterHandler::GetSimpleView()->signal_WriteLabel(QString(QString("Analizando, restan ") + QString::number(cant_contornoExterno - i) + QString(" celdas")));
			n = (n + 1) % mod;

		}
	}

	return sum;
}

void VolumeInteractions::limpiar(vtkSmartPointer<vtkImageData> input, int* contorno, int cant_contorno, double umbral)
{
	//caso base es un contorno vacio
	int m = 0;

	int cant = cant_contorno;
	int sum = 1;

	//vtkSmartPointer<vtkImageData> aux_img = vtkSmartPointer<vtkImageData>::New();
	//aux_img->DeepCopy(output);
	int x = contorno[0];
	int y = contorno[1];
	int z = contorno[2];

	input->SetScalarComponentFromDouble(x, y, z, 0, RADIO_DENSITY_ZERO);

	int *dims = input->GetDimensions();

	int dim_x = dims[0];
	int dim_y = dims[1];
	int dim_z = dims[2];


	int n = 1;
	int mod = 10000;

	while (cant > 0) {
		cant--;
		//extraigo esa componente
		int x = contorno[cant * 3 + 0];
		int y = contorno[cant * 3 + 1];
		int z = contorno[cant * 3 + 2];

		if(n==0)
			emit MasterHandler::GetSimpleView()->signal_WriteLabel(QString(QString("Limpiando, restan ") + QString::number(cant) + QString(" celdas")));
		n = (n + 1) % mod;
		//extraigo los 26-vecinos de las coordenadas x, y, z y los agrego al contorno si no estaban en la salida
		//x + 1
		if (x + 1 < dim_x) {
			if (input->GetScalarComponentAsDouble(x + 1, y, z, 0) > umbral)
			{
				input->SetScalarComponentFromDouble(x + 1, y, z, 0, RADIO_DENSITY_ZERO);
				contorno[cant * 3 + 0] = x + 1;
				contorno[cant * 3 + 1] = y;
				contorno[cant * 3 + 2] = z;
				cant++;
			}

			//y + 1
			if (y + 1 < dim_y && input->GetScalarComponentAsDouble(x + 1, y + 1, z, 0) > umbral)
			{
				input->SetScalarComponentFromDouble(x + 1, y + 1, z, 0, RADIO_DENSITY_ZERO);
				contorno[cant * 3 + 0] = x + 1;
				contorno[cant * 3 + 1] = y + 1;
				contorno[cant * 3 + 2] = z;
				cant++;
			}
			//y - 1
			if (y - 1 >= 0 && input->GetScalarComponentAsDouble(x + 1, y - 1, z, 0) > umbral)
			{
				input->SetScalarComponentFromDouble(x + 1, y - 1, z, 0, RADIO_DENSITY_ZERO);
				//z = 0
				contorno[cant * 3 + 0] = x + 1;
				contorno[cant * 3 + 1] = y - 1;
				contorno[cant * 3 + 2] = z;
				cant++;
			}

			//z + 1 y = 0
			if (z + 1 < dim_z && input->GetScalarComponentAsDouble(x + 1, y, z + 1, 0) > umbral)
			{
				input->SetScalarComponentFromDouble(x + 1, y, z + 1, 0, RADIO_DENSITY_ZERO);
				contorno[cant * 3 + 0] = x + 1;
				contorno[cant * 3 + 1] = y;
				contorno[cant * 3 + 2] = z + 1;
				cant++;
			}
			//z - 1 y =0
			if (z - 1 >= 0 && input->GetScalarComponentAsDouble(x + 1, y, z - 1, 0) > umbral)
			{
				input->SetScalarComponentFromDouble(x + 1, y, z - 1, 0, RADIO_DENSITY_ZERO);
				contorno[cant * 3 + 0] = x + 1;
				contorno[cant * 3 + 1] = y;
				contorno[cant * 3 + 2] = z - 1;
				cant++;
			}

		}
		//check 1
		//x - 1;
		if (x - 1 >= 0)
		{
			if (input->GetScalarComponentAsDouble(x - 1, y, z, 0) > umbral)
			{
				input->SetScalarComponentFromDouble(x - 1, y, z, 0, RADIO_DENSITY_ZERO);
				//y = 0 z = 0
				contorno[cant * 3 + 0] = x - 1;
				contorno[cant * 3 + 1] = y;
				contorno[cant * 3 + 2] = z;
				cant++;
			}

			//y+1  z = 0
			if (y + 1 < dim_y && input->GetScalarComponentAsDouble(x - 1, y + 1, z, 0) > umbral) {
				input->SetScalarComponentFromDouble(x - 1, y + 1, z, 0, RADIO_DENSITY_ZERO);
				contorno[cant * 3 + 0] = x - 1;
				contorno[cant * 3 + 1] = y + 1;
				contorno[cant * 3 + 2] = z;
				cant++;

			}
			//y - 1 z = 0
			if (y - 1 >= 0 && input->GetScalarComponentAsDouble(x - 1, y - 1, z, 0) > umbral) {
				input->SetScalarComponentFromDouble(x - 1, y - 1, z, 0, RADIO_DENSITY_ZERO);

				contorno[cant * 3 + 0] = x - 1;
				contorno[cant * 3 + 1] = y - 1;
				contorno[cant * 3 + 2] = z;
				cant++;

			}
			//z + 1 y = 0
			if (z + 1 < dim_z && input->GetScalarComponentAsDouble(x - 1, y, z + 1, 0) > umbral) {
				input->SetScalarComponentFromDouble(x - 1, y, z + 1, 0, RADIO_DENSITY_ZERO);
				contorno[cant * 3 + 0] = x - 1;
				contorno[cant * 3 + 1] = y;
				contorno[cant * 3 + 2] = z + 1;
				cant++;
			}
			//z - 1 y = 0
			if (z - 1 >= 0 && input->GetScalarComponentAsDouble(x - 1, y, z - 1, 0) > umbral)
			{
				input->SetScalarComponentFromDouble(x - 1, y, z - 1, 0, RADIO_DENSITY_ZERO);
				contorno[cant * 3 + 0] = x - 1;
				contorno[cant * 3 + 1] = y;
				contorno[cant * 3 + 2] = z - 1;
				cant++;
			}
		}

		//x = 0
		//y + 1;
		if (y + 1 < dim_y)
		{
			if (input->GetScalarComponentAsDouble(x, y + 1, z, 0) > umbral)
			{
				input->SetScalarComponentFromDouble(x, y + 1, z, 0, RADIO_DENSITY_ZERO);
				//plano
				contorno[cant * 3 + 0] = x;
				contorno[cant * 3 + 1] = y + 1;
				contorno[cant * 3 + 2] = z;
				cant++;
			}


			//z + 1
			if (z + 1 < dim_z && input->GetScalarComponentAsDouble(x, y + 1, z + 1, 0) > umbral)
			{
				input->SetScalarComponentFromDouble(x, y + 1, z + 1, 0, RADIO_DENSITY_ZERO);
				contorno[cant * 3 + 0] = x;
				contorno[cant * 3 + 1] = y + 1;
				contorno[cant * 3 + 2] = z + 1;
				cant++;
			}
			//z - 1
			if (z - 1 >= 0 && input->GetScalarComponentAsDouble(x, y + 1, z - 1, 0) > umbral)
			{
				input->SetScalarComponentFromDouble(x, y + 1, z - 1, 0, RADIO_DENSITY_ZERO);
				contorno[cant * 3 + 0] = x;
				contorno[cant * 3 + 1] = y + 1;
				contorno[cant * 3 + 2] = z - 1;
				cant++;
			}
		}
		//x = 0
		//y - 1
		if (y - 1 >= 0)
		{
			if (input->GetScalarComponentAsDouble(x, y - 1, z, 0) > umbral) {
				input->SetScalarComponentFromDouble(x, y - 1, z, 0, RADIO_DENSITY_ZERO);
				contorno[cant * 3 + 0] = x;
				contorno[cant * 3 + 1] = y - 1;
				contorno[cant * 3 + 2] = z;
				cant++;
			}

			//z + 1
			if (z + 1 < dim_z && input->GetScalarComponentAsDouble(x, y - 1, z + 1, 0) > umbral) {
				input->SetScalarComponentFromDouble(x, y - 1, z + 1, 0, RADIO_DENSITY_ZERO);
				contorno[cant * 3 + 0] = x;
				contorno[cant * 3 + 1] = y - 1;
				contorno[cant * 3 + 2] = z + 1;
				cant++;
			}
			//z - 1
			if (z - 1 >= 0 && input->GetScalarComponentAsDouble(x, y - 1, z - 1, 0) > umbral) {
				input->SetScalarComponentFromDouble(x, y - 1, z - 1, 0, RADIO_DENSITY_ZERO);
				contorno[cant * 3 + 0] = x;
				contorno[cant * 3 + 1] = y - 1;
				contorno[cant * 3 + 2] = z - 1;
				cant++;
			}
		}

		//x = 0 y = 0
		if (z + 1 < dim_z && input->GetScalarComponentAsDouble(x, y, z + 1, 0) > umbral) {
			input->SetScalarComponentFromDouble(x, y, z + 1, 0, RADIO_DENSITY_ZERO);
			contorno[cant * 3 + 0] = x;
			contorno[cant * 3 + 1] = y;
			contorno[cant * 3 + 2] = z + 1;
			cant++;
		}
		//x = 0 y = 0
		if (z - 1 >= 0 && input->GetScalarComponentAsDouble(x, y, z - 1, 0) > umbral) {
			input->SetScalarComponentFromDouble(x, y, z - 1, 0, RADIO_DENSITY_ZERO);
			contorno[cant * 3 + 0] = x;
			contorno[cant * 3 + 1] = y;
			contorno[cant * 3 + 2] = z - 1;
			cant++;
		}

	}

}

void VolumeInteractions::separarVolumen(vtkSmartPointer<myVolume> vol, double ratio)
{
	if (vol == nullptr) {
		return;
	}

	vol->setPicked(false);

	vtkSmartPointer<vtkImageData> imageData = vol->getImageData();

	int *dims = imageData->GetDimensions();

	QProgressBar* progress = MasterHandler::getProgressBar();
	
	vtkSmartPointer<vtkCollection> col = vtkSmartPointer<vtkCollection>::New();

	int recorridas = 1;
	int numCmp = dims[0] * dims[1] * dims[2];

	int minimoCeldas = numCmp * ratio;//numCmp/5000

	int x = 0, y = 0, z = 0;

	int* contorno = new int[numCmp * 3];
	int* contornoExterno = new int[numCmp * 3];

	int dim_x = dims[0];
	int dim_y = dims[1];
	int dim_z = dims[2];
	
	vtkSmartPointer<vtkImageData> imgVacia = vtkSmartPointer<vtkImageData>::New();
	imgVacia->DeepCopy(imageData);
	//progress->setVisible(true);
	emit MasterHandler::GetSimpleView()->visibleProgressBar(true);
	Consola::appendln("Separar volumen:");
	Consola::appendln("...Procesando volumen");
	emit MasterHandler::GetSimpleView()->signal_WriteLabel("Procesando volumen: 0%");

	//creo una imagen vacia
	for (int i = 0; i < dim_x; i++) {
		for (int j = 0; j < dim_y; j++) {
			for (int k = 0; k < dim_z; k++) {
				//progress->setValue((int)(recorridas*100.0f / (double)numCmp));
				
				recorridas++;
				imgVacia->SetScalarComponentFromDouble(i, j, k, 0, RADIO_DENSITY_ZERO);
				
			}
			emit MasterHandler::GetSimpleView()->progressChanged(recorridas*100.0f / (double)numCmp);
			emit MasterHandler::GetSimpleView()->signal_WriteLabel(QString(QString("Procesando volumen: ") + QString::number((int)(recorridas*100.0f / (double)numCmp)) + QString("%")));

		}
	}

	vtkSmartPointer<vtkImageData> acumulado = vtkSmartPointer<vtkImageData>::New();
	

	emit MasterHandler::GetSimpleView()->visibleProgressBar(false);
	
	vtkSmartPointer<vtkImageData> img = vtkSmartPointer<vtkImageData>::New();

	int cant = 0;

	int min = -1500 ,max = -1500;
	bool finish = false;

	while (!finish) 
	{

		recorridas = 1;

		finish = VolumeToSurface::extraerRango(min, max, vol);

		if (min < 10000) {
			Consola::append("...Extrayendo volumen con rango ");
			Consola::append(min);
			Consola::append(" - ");
			Consola::append(max);
			Consola::append("\n");



			emit MasterHandler::GetSimpleView()->progressChanged(recorridas*100.0f / (double)numCmp);
			emit MasterHandler::GetSimpleView()->visibleProgressBar(true);

			emit MasterHandler::GetSimpleView()->signal_WriteLabel(QString(QString("Extrayendo volumenes: ") + QString::number((int)(recorridas*100.0f / (double)numCmp)) + QString("%")));


			img->DeepCopy(imgVacia);
			acumulado->DeepCopy(imgVacia);
			{

				for (x = 0; x < dim_x; x++) {
					for (y = 0; y < dim_y; y++) {
						//printf("%f\n", (double)((x*dim_y + y)*100.0f / (double)(dim_x * dim_y)));
						for (z = 0; z < dim_z; z++) {
							recorridas++;
							if (acumulado->GetScalarComponentAsDouble(x, y, z, 0) == RADIO_DENSITY_ZERO) {
								double d = imageData->GetScalarComponentAsDouble(x, y, z, 0);

								if (d > min && d < max) {

									contorno[0] = x;
									contorno[1] = y;
									contorno[2] = z;

									int cant_contornoExterno = 0;

									int cant_ret = separarObjeto_rec(imageData, contorno, 1, min, max, img, acumulado, contornoExterno, minimoCeldas);

									if (cant_ret > minimoCeldas) {
										
										emit MasterHandler::GetSimpleView()->signal_WriteLabel("Nuevo volumen encontrado! Renderizando");
										col->AddItem(img);
										//esto va abajo

										{//agrego y renderizo

											vtkSmartPointer<myVolume> newvol = new myVolume(img);

											newvol->SetReferenceCount(1);

											newvol->setPaciente(vol->getPaciente());
											//setVolume(vol);

											registrarVolumen(newvol, nullptr);

											//GetRenderWindow()->GetRenderers()->GetFirstRenderer()->AddVolume(vol);
											newvol->setIsSplitted();

											newvol->restoreValues();
											//recupero los valores aqu� de la imagen
											double* vec = (TransferFunctionsList::GetTransferFunctionArray()[3]);


											for (int i = 0; i < NUMERODEPUNTOS_SCALAR; i++) {

												newvol->addOpacityPoint(i, vec[i]);
											}
											//emit MasterHandler::GetSimpleView()->signal_take_snapshots(this->GetRenderWindow2(), newvol);

											emit MasterHandler::GetSimpleView()->signal_renderizar();


										}
										/**/
										img = vtkSmartPointer<vtkImageData>::New();
										img->DeepCopy(imgVacia);
										cant++;

									}
									else {
										contorno[0] = x;
										contorno[1] = y;
										contorno[2] = z;
										limpiar(img, contorno, 1, -1200);
									}
								}
							}
						}
					}
					//progress->setValue((int)(x*100.0f / (double)dim_x));
					emit MasterHandler::GetSimpleView()->progressChanged((int)(x*100.0f / (double)dim_x));
					emit MasterHandler::GetSimpleView()->signal_WriteLabel(QString(QString("Extrayendo volumenes: ") + QString::number((int)(recorridas*100.0f / (double)numCmp)) + QString("%")));

				}
				Consola::appendln("...Hecho");
			}
			//progress->setVisible(false);
			

			emit MasterHandler::GetSimpleView()->visibleProgressBar(false);
		}
	}


	Consola::appendln("Separar volumen: Finalizado");
	Consola::appendln("");
	//delete coord;
	delete[] contorno;
	delete[] contornoExterno;
	//GetRenderWindow()->Render();
	emit MasterHandler::GetSimpleView()->signal_renderizar();

	emit MasterHandler::GetSimpleView()->signal_WriteLabel("Finalizado");
	//reader->getda

}

void VolumeInteractions::unirVolumenesSeleccionados()
{
	QList<int>* lista = MasterHandler::getListWidget()->GetSelectedPorts();

	if (lista->size() <= 1) 
		return;
	

	vtkSmartPointer<vtkVolumeCollection> selectedVolumes =
		vtkSmartPointer<vtkVolumeCollection>::New();

	//encuentro todos los volumenes
	vtkSmartPointer<myVolume> volumen = nullptr;
	for (int i = 0; i < lista->size(); i++) 
	{
		int port = (*lista)[i];
		bool encontre = false;

		int cant = coleccion->GetNumberOfItems();

		int j = 0;
		
		while (j < cant && !encontre) {
			volumen = (myVolume*)coleccion->GetItemAsObject(j);
			j++;
			if (volumen->GetCustomPort() == port) {
				encontre = true;
			}
		}
		if(encontre)
			selectedVolumes->AddItem(volumen);

	}

	vtkSmartPointer<vtkImageData> newImage = vtkSmartPointer<vtkImageData>::New();

	newImage->DeepCopy(volumen->getImageData());

	int*dims = newImage->GetDimensions();
	int recorridas = 1;
	int numCmp = dims[0] * dims[1] * dims[2] * (selectedVolumes->GetNumberOfItems()-1);

	int newDim_x = dims[0];
	int newDim_y = dims[1];
	int newDim_z = dims[2];

	int dim_x = 0, dim_y = 0, dim_z = 0;

	emit MasterHandler::GetSimpleView()->visibleProgressBar(true);
	

	for (int cant = 0; cant < selectedVolumes->GetNumberOfItems()-1; cant++) {

		vtkSmartPointer<myVolume> actualVolume = (myVolume*)selectedVolumes->GetItemAsObject(cant);
		vtkSmartPointer<vtkImageData> actualImage = actualVolume->getImageData();

		dims = actualImage->GetDimensions();

		dim_x = dims[0];
		dim_y = dims[1];
		dim_z = dims[2];



		for (int i = 0; i < dim_x && i < newDim_x; i++) {
			for (int j = 0; j < dim_y && j < newDim_y; j++) {
				for (int k = 0; k < dim_z && k < newDim_z; k++) {
					//progress->setValue((int)(recorridas*100.0f / (double)numCmp));

					recorridas++;

					double d = actualImage->GetScalarComponentAsDouble(i, j, k, 0);

					if(d > newImage->GetScalarComponentAsDouble(i, j, k, 0))
						newImage->SetScalarComponentFromDouble(i, j, k, 0, d);

				}
				emit MasterHandler::GetSimpleView()->progressChanged(recorridas*100.0f / (double)numCmp);
				emit MasterHandler::GetSimpleView()->signal_WriteLabel(QString(QString("Procesando volumenes: ") + QString::number((int)(recorridas*100.0f / (double)numCmp)) + QString("%")));

			}
		}
	}

	emit MasterHandler::GetSimpleView()->visibleProgressBar(false);

	vtkSmartPointer<myVolume> newVolume = new myVolume(newImage);

	registrarVolumen(newVolume, nullptr);

	newVolume->SetReferenceCount(1);

	emit MasterHandler::GetSimpleView()->signal_renderizar();

	emit MasterHandler::GetSimpleView()->signal_WriteLabel("Finalizado");
}

void VolumeInteractions::setCorteTridimensional(CameraHandler* camera, vtkSmartPointer<myVolume> vol)
{
	if (vol != nullptr){

		vol->setPicked(false);

		vtkSmartPointer<vtkImageData> imageData = vol->getImageData();
		black_white = false;
		if (imageData != nullptr) {
			int* dims = imageData->GetDimensions();
			if (!black_white) {
				//if(vol->GetSliceVolumeXY == nullptr)
				if (vol->getSliceVolumeXY() == nullptr) {
					vtkSmartPointer<sliceVolume> volXY = new sliceVolume(imageData, SLICE_XY_PLANE, vol->GetColorTransferFunction());

					
					volXY->SetReferenceCount(1);

					//volXY->SetReferenceCount(1);
					vol->setSliceVolumeXY(volXY);
				}
				//if()
				if (vol->getSliceVolumeYZ() == nullptr) {
					vtkSmartPointer<sliceVolume> volYZ = new sliceVolume(imageData, SLICE_YZ_PLANE, vol->GetColorTransferFunction());

					
					volYZ->SetReferenceCount(1);

					//volYZ->SetReferenceCount(1);
					vol->setSliceVolumeYZ(volYZ);

				}
				//if()
				if (vol->getSliceVolumeZX() == nullptr) {
					vtkSmartPointer<sliceVolume> volZX = new sliceVolume(imageData, SLICE_ZX_PLANE, vol->GetColorTransferFunction());

					
					volZX->SetReferenceCount(1);

					//volZX->SetReferenceCount(1);
					vol->setSliceVolumeZX(volZX);
				}

				camera->setSliceVolumes(vol->getSliceVolumeXY(), vol->getSliceVolumeYZ(), vol->getSliceVolumeZX(), vol->getCustomBounds());
				emit MasterHandler::GetSimpleView()->signal_ResetCameras();

				emit MasterHandler::GetSimpleView()->signal_renderizar();
			}
			else {
				int recorridas = 0;
				int numCmp = dims[0] * dims[1] * dims[2] * 2;



				Consola::appendln("Cortes tridimensionales");

				if (vol->getImageActorXY() == nullptr) {

					Consola::appendln("Cortes tridimensionales: creando cortes sobre Z");

					emit MasterHandler::GetSimpleView()->visibleProgressBar(true);
					emit MasterHandler::GetSimpleView()->progressChanged(0);

					vtkSmartPointer<vtkImageData> imageDataXY = imageData;

					recorridas = 1;
					emit MasterHandler::GetSimpleView()->progressChanged(recorridas*100.0f / (double)numCmp);

					vtkSmartPointer<vtkImageActor> imageActorXY = vtkSmartPointer<vtkImageActor>::New();
					imageActorXY->GetMapper()->SetInputData(imageDataXY);
					int actorDisplayExtent[6];
					imageDataXY->GetExtent(actorDisplayExtent);
					imageActorXY->SetDisplayExtent(actorDisplayExtent[0], actorDisplayExtent[1], actorDisplayExtent[2], actorDisplayExtent[3], dims[2] / 2, dims[2] / 2);
					imageActorXY->SetZSlice(dims[2] / 2);
					imageActorXY->SetPosition(0, 0, -imageDataXY->GetSpacing()[2] * dims[2] / 2);

					vol->setImageActorXY(imageActorXY);

					camera->setImageActors(vol->getImageActorXY(), nullptr, nullptr, vol->getCustomBounds());

					emit MasterHandler::GetSimpleView()->signal_ResetCameras();
					emit MasterHandler::GetSimpleView()->signal_renderizar();

					Consola::appendln("Cortes tridimensionales: Hecho");
				}

				if (vol->getImageActorYZ() == nullptr) {

					Consola::appendln("Cortes tridimensionales: creando cortes sobre X");

					vtkSmartPointer<vtkImageData> imageDataYZ = vtkSmartPointer<vtkImageData>::New();
					//TODO
					imageDataYZ->SetDimensions(dims[1], dims[2], dims[0]);

					double* spacing = imageData->GetSpacing();
					imageDataYZ->SetSpacing(spacing[1], spacing[2], spacing[0]);

					imageDataYZ->AllocateScalars(VTK_DOUBLE, 1);

					for (int i = 0; i < dims[0]; i++) {
						for (int j = 0; j < dims[1]; j++) {
							for (int k = 0; k < dims[2]; k++) {
								double d = imageData->GetScalarComponentAsDouble(i, j, k, 0);
								imageDataYZ->SetScalarComponentFromDouble(j, dims[2] - 1 - k, i, 0, d);
								recorridas++;
							}

						}
						emit MasterHandler::GetSimpleView()->progressChanged(recorridas*100.0f / (double)numCmp);
						emit MasterHandler::GetSimpleView()->signal_WriteLabel(QString(QString("Creando cortes sobre X ") + QString::number((int)(recorridas*100.0f / (double)numCmp) * 2) + QString("%")));

					}

					vtkSmartPointer<vtkImageActor> imageActorYZ = vtkSmartPointer<vtkImageActor>::New();
					imageActorYZ->GetMapper()->SetInputData(imageDataYZ);
					int actorDisplayExtent[6];
					imageDataYZ->GetExtent(actorDisplayExtent);
					imageActorYZ->SetDisplayExtent(actorDisplayExtent[0], actorDisplayExtent[1], actorDisplayExtent[2], actorDisplayExtent[3], dims[0] / 2, dims[0] / 2);
					imageActorYZ->SetZSlice(dims[0] / 2);
					imageActorYZ->SetPosition(0, 0, -spacing[0] * dims[0] / 2);

					vol->setImageActorYZ(imageActorYZ);

					camera->setImageActors(nullptr, vol->getImageActorYZ(), nullptr, vol->getCustomBounds());
					emit MasterHandler::GetSimpleView()->signal_ResetCameras();
					emit MasterHandler::GetSimpleView()->signal_renderizar();

					Consola::appendln("Cortes tridimensionales: Hecho");

				}

				if (vol->getImageActorZX() == nullptr) {

					Consola::appendln("Cortes tridimensionales: creando cortes sobre Y");

					vtkSmartPointer<vtkImageData> imageDataZX = vtkSmartPointer<vtkImageData>::New();

					imageDataZX->SetDimensions(dims[0], dims[2], dims[1]);
					double* spacing = imageData->GetSpacing();
					imageDataZX->SetSpacing(spacing[0], spacing[2], spacing[1]);
					imageDataZX->AllocateScalars(VTK_DOUBLE, 1);

					for (int i = 0; i < dims[0]; i++) {
						for (int j = 0; j < dims[1]; j++) {
							for (int k = 0; k < dims[2]; k++) {
								double d = imageData->GetScalarComponentAsDouble(i, j, k, 0);
								imageDataZX->SetScalarComponentFromDouble(i, dims[2] - 1 - k, j, 0, d);
								recorridas++;
							}

						}
						emit MasterHandler::GetSimpleView()->progressChanged(recorridas*100.0f / (double)numCmp);
						emit MasterHandler::GetSimpleView()->signal_WriteLabel(QString(QString("Creando cortes sobre Y ") + QString::number((int)((recorridas*100.0f / (double)numCmp) - 50) * 2) + QString("%")));
					}

					vtkSmartPointer<vtkImageActor> imageActorZX = vtkSmartPointer<vtkImageActor>::New();
					imageActorZX->GetMapper()->SetInputData(imageDataZX);
					int actorDisplayExtent[6];
					imageDataZX->GetExtent(actorDisplayExtent);
					imageActorZX->SetDisplayExtent(actorDisplayExtent[0], actorDisplayExtent[1], actorDisplayExtent[2], actorDisplayExtent[3], dims[1] / 2, dims[1] / 2);
					imageActorZX->SetZSlice(dims[1] / 2);
					imageActorZX->SetPosition(0, 0, -spacing[1] * dims[1] / 2);

					vol->setImageActorZX(imageActorZX);
					camera->setImageActors(nullptr, nullptr, imageActorZX, vol->getCustomBounds());

					emit MasterHandler::GetSimpleView()->visibleProgressBar(false);
					emit MasterHandler::GetSimpleView()->signal_ResetCameras();
					emit MasterHandler::GetSimpleView()->signal_renderizar();

					Consola::appendln("Cortes tridimensionales: Hecho");

				}
			}
			/**/
			emit MasterHandler::GetSimpleView()->signal_WriteLabel("Finalizado");
		}

	}
	else {
		emit MasterHandler::GetSimpleView()->signal_Warning("No se ha seleccionado un volumen de la lista.\nNo se hace nada.");
	}
}

void VolumeInteractions::removeSelectedCells()
{
	if (lastVolume != nullptr) {

		lastVolume->removeSelectedPoints();

		/**
		vtkSmartPointer<vtkImageData* image = lastVolume->getImageData();
		int *dims = image->GetDimensions();

		int numCmp = dims[0] * dims[1] * dims[2];
		int recorridas = 0;

		emit MasterHandler::GetSimpleView()->progressChanged(recorridas*100.0f / (double)numCmp);
		emit MasterHandler::GetSimpleView()->visibleProgressBar(true);

		for (int i = 0; i < dims[0]; i++) {
			for (int j = 0; j < dims[1]; j++) {
				for (int k = 0; k < dims[2]; k++) {
					double valor = image->GetScalarComponentAsDouble(i, j, k, 0);

					if(valor > SELECT_CELL*2)
						image->SetScalarComponentFromDouble(i, j, k, 0, RADIO_DENSITY_ZERO);

					recorridas++;
				}
				emit MasterHandler::GetSimpleView()->progressChanged(recorridas*100.0f / (double)numCmp);
			}
		}

		emit MasterHandler::GetSimpleView()->visibleProgressBar(false);

		image->Modified();
		/**/
	}
}

void VolumeInteractions::setCortePreciso()
{
	if (cortePreciso) {
		cortePreciso = false;
	}
	else {
		cortePreciso = true;
	}
}

vtkSmartPointer<vtkVolumeCollection > VolumeInteractions::getCollection()
{
	return coleccion;
}

void VolumeInteractions::setBounds(double bounds[6])
{

	if (lastVolume != nullptr) {
		vtkSmartPointer<vtkPlaneCollection> colectionPlanes = lastVolume->GetMapper()->GetClippingPlanes();

		if (colectionPlanes == nullptr)
			colectionPlanes = vtkSmartPointer<vtkPlaneCollection>::New();

		if (colectionPlanes->GetNumberOfItems() < 6)
		{
			vtkNew<vtkPlane> planoXY_min;
			vtkNew<vtkPlane> planoXY_max;
			vtkNew<vtkPlane> planoYZ_min;
			vtkNew<vtkPlane> planoYZ_max;
			vtkNew<vtkPlane> planoZX_min;
			vtkNew<vtkPlane> planoZX_max;

			colectionPlanes->AddItem(planoXY_min);
			colectionPlanes->AddItem(planoXY_max);
			colectionPlanes->AddItem(planoYZ_min);
			colectionPlanes->AddItem(planoYZ_max);
			colectionPlanes->AddItem(planoZX_min);
			colectionPlanes->AddItem(planoZX_max);
		}
		
		vtkSmartPointer<vtkPlane> planoXY_min = (vtkSmartPointer<vtkPlane>)colectionPlanes->GetItem(0);
		vtkSmartPointer<vtkPlane> planoXY_max = (vtkSmartPointer<vtkPlane>)colectionPlanes->GetItem(1);
		vtkSmartPointer<vtkPlane> planoYZ_min = (vtkSmartPointer<vtkPlane>)colectionPlanes->GetItem(2);
		vtkSmartPointer<vtkPlane> planoYZ_max = (vtkSmartPointer<vtkPlane>)colectionPlanes->GetItem(3);
		vtkSmartPointer<vtkPlane> planoZX_min = (vtkSmartPointer<vtkPlane>)colectionPlanes->GetItem(4);
		vtkSmartPointer<vtkPlane> planoZX_max = (vtkSmartPointer<vtkPlane>)colectionPlanes->GetItem(5);

		planoXY_min->SetNormal(0, 0, 1);
		planoXY_min->SetOrigin(bounds[0], bounds[2], bounds[4]);

		planoXY_max->SetNormal(0, 0, -1);
		planoXY_max->SetOrigin(bounds[1], bounds[3], bounds[5]);

		planoYZ_min->SetNormal(1, 0, 0);
		planoYZ_min->SetOrigin(bounds[0], bounds[2], bounds[4]);

		planoYZ_max->SetNormal(-1, 0, 0);
		planoYZ_max->SetOrigin(bounds[1], bounds[3], bounds[5]);

		planoZX_min->SetNormal(0, 1, 0);
		planoZX_min->SetOrigin(bounds[0], bounds[2], bounds[4]);

		planoZX_max->SetNormal(0, -1, 0);
		planoZX_max->SetOrigin(bounds[1], bounds[3], bounds[5]);

		lastVolume->GetMapper()->SetClippingPlanes(colectionPlanes);
		lastVolume->SetBounds(bounds);
	}
}

vtkSmartPointer<myVolume> VolumeInteractions::GetActualVolume()
{
	return lastVolume;
}

void VolumeInteractions::cortarPorPlano(PlaneHandler * handler, vtkSmartPointer<myVolume> vol)
{
	vtkSmartPointer<vtkActor> actorPlane = handler->GetActor();
	if (actorPlane != nullptr)
	{
		vtkSmartPointer<vtkRegularPolygonSource> plane = handler->getPlane();
		//Orientacion y centro a partir del actor y del hueso
		//actor actual y su matriz de transformacion
		//vtkSmartPointer<vtkActor> actor = getSelectedActor();
		if (vol != nullptr)
		{

			vol->setPicked(false);

			vtkSmartPointer<vtkPlane> vtkPlaneObj1 =
				vtkSmartPointer<vtkPlane>::New();
			
			vtkSmartPointer<vtkPlane> vtkPlaneObj2 =
				vtkSmartPointer<vtkPlane>::New();
			
			//trabajo con matrices punto central y normales de los planos
			{

				//seleccionado->GetMatrix(volumeMatrix);

				//volumeMatrix->Invert();

				//matriz de transformacion del plano

				///se pide la matriz al plano
				actorPlane->GetMatrix(WordMatrix);

				//double res[16];

				//se translada al espacio de clipper para realizar el corte
				//WordMatrix->Multiply4x4(volumeMatrix->GetData(), WordMatrix->GetData(), res);

				//WordMatrix->DeepCopy(res);

				double* origin = plane->GetCenter();

				origin = WordMatrix->MultiplyDoublePoint(origin);

				//Matriz para transformar la normal
				//debe ser la inversa traspuesta

				WordNormalMatrix->DeepCopy(WordMatrix);

				WordNormalMatrix->Invert();
				WordNormalMatrix->Transpose();

				WordNormalMatrix2->DeepCopy(WordMatrix);

				WordNormalMatrix2->Invert();
				WordNormalMatrix2->Transpose();

				double* normal = plane->GetNormal();
				double normal2[3] = { -normal[0],normal[1],normal[2] };

				//multiplico por la matriz normal
				normal = WordNormalMatrix->MultiplyDoublePoint(normal);
				double* normal2aux = WordNormalMatrix2->MultiplyDoublePoint(normal2);

				vtkPlaneObj1->SetOrigin(origin);

				vtkPlaneObj1->SetNormal(normal);

				vtkPlaneObj2->SetOrigin(origin);

				vtkPlaneObj2->SetNormal(normal2aux);

			}



			Consola::appendln("Haciendo corte transversal");

			vtkSmartPointer<vtkImageData> image = vtkSmartPointer<vtkImageData>::New();

			image->DeepCopy(vol->getImageData());
			//MESH ACTOR
			//actorMesh = vtkSmartPointer<vtkActor>::New();

			vtkSmartPointer<myVolume> volumen2 = new myVolume(image);
			
			volumen2->SetReferenceCount(1);

			registrarVolumen(volumen2, nullptr);



			vtkSmartPointer<vtkPlaneCollection> colectionPlanes = vol->GetMapper()->GetClippingPlanes();
			vtkSmartPointer<vtkPlaneCollection> colectionPlanes2 = volumen2->GetMapper()->GetClippingPlanes();

			setBounds(vol->GetBounds(), volumen2);

			vol->addClippingPlane(vtkPlaneObj1);
			volumen2->addClippingPlane(vtkPlaneObj1);

			handler->quitarPlano();

			Consola::appendln("");

		}
		else {
			emit MasterHandler::GetSimpleView()->signal_Warning("Por favor seleccione un objeto para cortar");
		}
	}
	else {
		emit MasterHandler::GetSimpleView()->signal_Warning("No se ha insertado ningun plano");
	}
}

void VolumeInteractions::setBounds(double bounds[6], vtkSmartPointer<myVolume> vol)
{
	if (vol != nullptr) {
		vtkSmartPointer<vtkPlaneCollection> colectionPlanes = vol->GetMapper()->GetClippingPlanes();

		if (colectionPlanes == nullptr)
			colectionPlanes = vtkSmartPointer<vtkPlaneCollection>::New();

		if (colectionPlanes->GetNumberOfItems() < 6)
		{
			vtkNew<vtkPlane> planoXY_min;
			vtkNew<vtkPlane> planoXY_max;
			vtkNew<vtkPlane> planoYZ_min;
			vtkNew<vtkPlane> planoYZ_max;
			vtkNew<vtkPlane> planoZX_min;
			vtkNew<vtkPlane> planoZX_max;

			colectionPlanes->AddItem(planoXY_min);
			colectionPlanes->AddItem(planoXY_max);
			colectionPlanes->AddItem(planoYZ_min);
			colectionPlanes->AddItem(planoYZ_max);
			colectionPlanes->AddItem(planoZX_min);
			colectionPlanes->AddItem(planoZX_max);
		}

		vtkSmartPointer<vtkPlane> planoXY_min = (vtkSmartPointer<vtkPlane>)colectionPlanes->GetItem(0);
		vtkSmartPointer<vtkPlane> planoXY_max = (vtkSmartPointer<vtkPlane>)colectionPlanes->GetItem(1);
		vtkSmartPointer<vtkPlane> planoYZ_min = (vtkSmartPointer<vtkPlane>)colectionPlanes->GetItem(2);
		vtkSmartPointer<vtkPlane> planoYZ_max = (vtkSmartPointer<vtkPlane>)colectionPlanes->GetItem(3);
		vtkSmartPointer<vtkPlane> planoZX_min = (vtkSmartPointer<vtkPlane>)colectionPlanes->GetItem(4);
		vtkSmartPointer<vtkPlane> planoZX_max = (vtkSmartPointer<vtkPlane>)colectionPlanes->GetItem(5);

		planoXY_min->SetNormal(0, 0, 1);
		planoXY_min->SetOrigin(bounds[0], bounds[2], bounds[4]);

		planoXY_max->SetNormal(0, 0, -1);
		planoXY_max->SetOrigin(bounds[1], bounds[3], bounds[5]);

		planoYZ_min->SetNormal(1, 0, 0);
		planoYZ_min->SetOrigin(bounds[0], bounds[2], bounds[4]);

		planoYZ_max->SetNormal(-1, 0, 0);
		planoYZ_max->SetOrigin(bounds[1], bounds[3], bounds[5]);

		planoZX_min->SetNormal(0, 1, 0);
		planoZX_min->SetOrigin(bounds[0], bounds[2], bounds[4]);

		planoZX_max->SetNormal(0, -1, 0);
		planoZX_max->SetOrigin(bounds[1], bounds[3], bounds[5]);

		vol->GetMapper()->SetClippingPlanes(colectionPlanes);
		vol->SetBounds(bounds);
	}
}

void VolumeInteractions::exportarVolumen(QString direction, vtkSmartPointer<myVolume> vol) {
	
	if (claseIO == nullptr) {
		claseIO = new IOVolume();
	}
	if (vol != nullptr) {

		vol->setPicked(false);

		QStringList lista = MasterHandler::getListWidget()->GetItemName(vol->GetCustomPort()).split(" ");
		QString direction2 = direction + QString("/") + lista[lista.size()-1];//
		vol->setPath(direction);
		Consola::appendln(QString(QString("Exportando volumen en ") + direction));

		//INIT DICOM FILES
		{
			QString DICOMFilesDirection = direction2 + QString("/Data Files");
			//Consola::append(DICOMFilesDirection.toStdString());
			std::string outputPath = DICOMFilesDirection.toStdString();

			QDir dir;
			if (!dir.exists(DICOMFilesDirection)) {
				dir.mkpath(DICOMFilesDirection);
			}

			vtkNew<vtkDICOMWriter> writer;
			vtkNew<vtkDICOMMRGenerator> generator;
			vtkNew<vtkDICOMMetaData> meta;
			writer->AddInputData(vol->getImageData());

			if (vol->getPaciente() != nullptr) {
				meta->Set(DC::StudyInstanceUID, vol->getPaciente()->getStudyInstanceUID());
				meta->Set(DC::PatientName, vol->getPaciente()->getPatientName());
				meta->Set(DC::PatientAge, vol->getPaciente()->getPatientAge());
				meta->Set(DC::PatientID, vol->getPaciente()->getPatientID());
				meta->Set(DC::PatientBirthDate, vol->getPaciente()->getPatientBirthDate());
				meta->Set(DC::PatientBirthName, vol->getPaciente()->getPatientBirthName());
				meta->Set(DC::PatientBirthTime, vol->getPaciente()->getPatientBirthTime());
				meta->Set(DC::PatientAddress, vol->getPaciente()->getPatientAddress());
				meta->Set(DC::PatientSex, vol->getPaciente()->getPatientSex());
				meta->Set(DC::PatientComments, vol->getPaciente()->getPatientComments());
				meta->Set(DC::PatientSize, vol->getPaciente()->getPatientSize());
				meta->Set(DC::PatientWeight, vol->getPaciente()->getPatientWeight());
				meta->Set(DC::StudyID, vol->getPaciente()->getStudyID());
				meta->Set(DC::StudyDescription, vol->getPaciente()->getStudyDescription());
				meta->Set(DC::StudyDate, vol->getPaciente()->getStudyDate());
				meta->Set(DC::StudyTime, vol->getPaciente()->getStudyTime());
				meta->Set(DC::AccessionNumber, vol->getPaciente()->getAccessionNumber());
				meta->Set(DC::ReferringPhysicianAddress, vol->getPaciente()->getReferringPhysician());
				meta->Set(DC::PerformingPhysicianName, vol->getPaciente()->getPerformingPhysicianName());
				meta->Set(DC::Modality, vol->getPaciente()->getModality());
				meta->Set(DC::AdditionalPatientHistory, vol->getPaciente()->getAdditionalPatientHistory());
				meta->Set(DC::Allergies, vol->getPaciente()->getAllergies());
				meta->Set(DC::CurrentPatientLocation, vol->getPaciente()->getCurrentPatientLocation());
				meta->Set(DC::ModalitiesInStudy, vol->getPaciente()->getModalitiesInStudy());
				meta->Set(DC::InstitutionName, vol->getPaciente()->getInstitutionName());
				meta->Set(DC::PersonName, vol->getPaciente()->getPersonName());
				meta->Set(DC::PersonTelephoneNumbers, vol->getPaciente()->getPersonTelephoneNumbers());
			}

			writer->SetMetaData(meta.GetPointer());
			writer->SetGenerator(generator.GetPointer());

			// Set the output filename format as a printf-style string.

			writer->SetFilePattern("%s/IM-%04.4d.dcm");
			// Set the directory to write the files into.
			writer->SetFilePrefix(outputPath.data());
			// Write the file.

			writer->AddObserver(vtkCommand::ProgressEvent, CallbacksClass::getProgressCallBack());
			CallbacksClass::setEncabezado("Exportando Volumen: ");
			writer->Write();
		}
		claseIO->exportTransferenceFunction(direction2, vol);

		QFile saveFile(direction2 + ".bones");
		if (saveFile.open(QIODevice::WriteOnly)) {
			saveFile.write(" ");
		}

		emit MasterHandler::GetSimpleView()->signal_SaveCapture(direction2);
	}
	else {

	}
}

void VolumeInteractions::importarVolumen(QString direction) {

	QStringList aux = direction.split("/");

	QString name = aux[aux.size() - 1];
	QString ftPath = direction;
	QString data = direction + QString("/Data Files");
	std::string inputPath = data.toStdString();

	vtkSmartPointer<myVolume> vol = new myVolume(inputPath);

	QString dir = direction.split("/Volumen")[0];
	
	vol->SetReferenceCount(1);
	vol->setPath(dir);
	MasterHandler::GetVolumeInteractor()->registrarVolumen(vol, name);
	claseIO->importTransferenceFunction(ftPath, vol);
	
}


