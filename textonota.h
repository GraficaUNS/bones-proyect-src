#ifndef TEXTONOTA_H
#define TEXTONOTA_H

#include <QDialog>

namespace Ui {
class textoNota;
}

class textoNota : public QDialog
{
    Q_OBJECT
		
public:
    explicit textoNota(QWidget *parent = 0);
    ~textoNota();
	void appendText(QString str);

private slots:
	void accept();

signals:
	void enviarDatosPopUp(QStringList);

private:
    Ui::textoNota *ui;
	
	
};

#endif // TEXTONOTA_H
