#include "CallbacksClass.h"
#include <vtkCallbackCommand.h>
#include <vtkSmartPointer.h>
#include <QMutex>

QString encabezadoCallback("");

vtkSmartPointer<vtkCallbackCommand> m_vtkCallback;

QMutex* mutex = new QMutex();

CallbacksClass::CallbacksClass()
{
}


CallbacksClass::~CallbacksClass()
{
}

void CallbacksClass::setProgressCallBack(void(*f)(vtkObject* caller, long unsigned int, void*, void*)) {

	//myvtkSmartPointer<vtkProgressCallback = f;
	m_vtkCallback = vtkSmartPointer<vtkCallbackCommand>::New();
	m_vtkCallback->SetCallback(*f);

}

vtkSmartPointer<vtkCallbackCommand> CallbacksClass::getProgressCallBack() {
	return m_vtkCallback;
}

void CallbacksClass::setEncabezado(QString s)
{
	encabezadoCallback = s;
}

QString CallbacksClass::getEncabezado()
{
	return encabezadoCallback;
}

void CallbacksClass::lockProgressBar() {
	mutex->lock();
}

void CallbacksClass::unlockProgressBar() {
	mutex->unlock();
}