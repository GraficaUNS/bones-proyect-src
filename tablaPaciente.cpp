#include "tablapaciente.h"
#include "ui_tablapaciente.h"
#include "qstringlist.h"
#include "Consola.h"
#include "QHeaderView.h"

tablaPaciente::tablaPaciente(QWidget *parent) :
	QDialog(parent),
	
	ui(new Ui::tablaPaciente)
{
	ui->setupUi(this);
	QStringList titulos,titulos2,titulos3;
	setWindowTitle("Datos del paciente");
	/* SETEO LA PRIMERA TABLA */

	ui->tableWidget->setColumnCount(11);
	ui->tableWidget_2->setColumnCount(9);
	ui->tableWidget_3->setColumnCount(7);

	
	titulos << "Nombre del paciente" << "ID" << "Edad" << "Sexo" << "Fecha nacimiento " << "Nombre nacimiento"
		<< "Hora nacimiento" << "Direcci\�n" << "Altura" << "Peso" << "Comentarios";
		
	titulos2 << "ID del estudio" << "UID del estudio de instancia" << "Descripci\�n" << "Fecha" << "Hora" << "N�mero de acceso" << "M\�dico de referencia"
		<< "M\�dico de desempe�o" << "Modalidad";

	titulos3 << "Localidad actual del paciente" << "Modalidades en estudio" << "Nombre del instituto" 
		<< "Nombre de la persona" << "Historia adicional del paciente" << "Alergias" << "Tel\�fono";

	ui->tableWidget->setHorizontalHeaderLabels(titulos);
	ui->tableWidget_2->setHorizontalHeaderLabels(titulos2);
	ui->tableWidget_3->setHorizontalHeaderLabels(titulos3);

	/*
	QStringList vertical;
	vertical << "";

	ui->tableWidget->setVerticalHeaderLabels(vertical);
	ui->tableWidget_2->setVerticalHeaderLabels(vertical);
	ui->tableWidget_3->setVerticalHeaderLabels(vertical);
	*/

	ui->tableWidget->horizontalHeader()->setDefaultSectionSize(200);
	ui->tableWidget_2->horizontalHeader()->setDefaultSectionSize(200);
	ui->tableWidget_3->horizontalHeader()->setDefaultSectionSize(200);

	ui->tableWidget->insertRow(ui->tableWidget->rowCount());
	ui->tableWidget_2->insertRow(ui->tableWidget_2->rowCount());
	ui->tableWidget_3->insertRow(ui->tableWidget_3->rowCount());

	ui->tableWidget->verticalHeader()->setVisible(false);
	ui->tableWidget_2->verticalHeader()->setVisible(false);
	ui->tableWidget_3->verticalHeader()->setVisible(false);

	ui->tableWidget->setShowGrid(false);
	ui->tableWidget_2->setShowGrid(false);
	ui->tableWidget_3->setShowGrid(false);
	
	//ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	//ui->tableWidget_2->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	//ui->tableWidget_3->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	
}

	

tablaPaciente::~tablaPaciente()
{
    delete ui;
}

void tablaPaciente::on_botonAceptar_clicked()
{
    this->close();
}

void tablaPaciente::insertarPaciente(Paciente * paciente) {
	if (paciente == NULL) {
		//aviso que no se interto porque es nulo
		//Consola::append("es nulo el paciente");
	}
	else {
		
		QTableWidgetItem * item1 = new QTableWidgetItem( );
		item1->setTextAlignment(Qt::AlignHCenter);
		QTableWidgetItem * item2 = new QTableWidgetItem();
		item2->setTextAlignment(Qt::AlignHCenter);
		QTableWidgetItem * item3 = new QTableWidgetItem();
		item3->setTextAlignment(Qt::AlignHCenter);
		QTableWidgetItem * item4 = new QTableWidgetItem();
		item4->setTextAlignment(Qt::AlignHCenter);
		QTableWidgetItem * item5 = new QTableWidgetItem();
		item5->setTextAlignment(Qt::AlignHCenter);
		QTableWidgetItem * item6 = new QTableWidgetItem();
		item6->setTextAlignment(Qt::AlignHCenter);
		QTableWidgetItem * item7 = new QTableWidgetItem();
		item7->setTextAlignment(Qt::AlignHCenter);
		QTableWidgetItem * item8 = new QTableWidgetItem();
		item8->setTextAlignment(Qt::AlignHCenter);
		QTableWidgetItem * item9 = new QTableWidgetItem();
		item9->setTextAlignment(Qt::AlignHCenter);
		QTableWidgetItem * item10 = new QTableWidgetItem();
		item10->setTextAlignment(Qt::AlignHCenter);
		QTableWidgetItem * item11 = new QTableWidgetItem();
		item11->setTextAlignment(Qt::AlignHCenter);

		QTableWidgetItem * item12 = new QTableWidgetItem();
		item12->setTextAlignment(Qt::AlignHCenter);
		QTableWidgetItem * item13 = new QTableWidgetItem();
		item13->setTextAlignment(Qt::AlignHCenter);
		QTableWidgetItem * item14 = new QTableWidgetItem();
		item14->setTextAlignment(Qt::AlignHCenter);
		QTableWidgetItem * item15 = new QTableWidgetItem();
		item15->setTextAlignment(Qt::AlignHCenter);
		QTableWidgetItem * item16 = new QTableWidgetItem();
		item16->setTextAlignment(Qt::AlignHCenter);
		QTableWidgetItem * item17 = new QTableWidgetItem();
		item17->setTextAlignment(Qt::AlignHCenter);
		QTableWidgetItem * item18 = new QTableWidgetItem();
		item18->setTextAlignment(Qt::AlignHCenter);
		QTableWidgetItem * item19 = new QTableWidgetItem();
		item19->setTextAlignment(Qt::AlignHCenter);
		QTableWidgetItem * item20 = new QTableWidgetItem();
		item20->setTextAlignment(Qt::AlignHCenter);
		QTableWidgetItem * item21 = new QTableWidgetItem();
		item21->setTextAlignment(Qt::AlignHCenter);
		QTableWidgetItem * item22 = new QTableWidgetItem();
		item22->setTextAlignment(Qt::AlignHCenter);

		QTableWidgetItem * item23 = new QTableWidgetItem();
		item23->setTextAlignment(Qt::AlignHCenter);
		QTableWidgetItem * item24 = new QTableWidgetItem();
		item24->setTextAlignment(Qt::AlignHCenter);
		QTableWidgetItem * item25 = new QTableWidgetItem();
		item25->setTextAlignment(Qt::AlignHCenter);
		QTableWidgetItem * item26 = new QTableWidgetItem();
		item26->setTextAlignment(Qt::AlignHCenter);
		QTableWidgetItem * item27 = new QTableWidgetItem();

		item1->setText(QString::fromStdString(paciente->getPatientName()));
		ui->tableWidget->setItem(0, 0, item1);
		item2->setText(QString::fromStdString(paciente->getPatientID()));
		ui->tableWidget->setItem(0, 1, item2);
		item3->setText(QString::fromStdString(paciente->getPatientAge()));
		ui->tableWidget->setItem(0, 2, item3);
		item4->setText(QString::fromStdString(paciente->getPatientSex()));
		ui->tableWidget->setItem(0, 3, item4);
		item5->setText(QString::fromStdString(paciente->getPatientBirthDate()));
		ui->tableWidget->setItem(0, 4, item5);
		item6->setText(QString::fromStdString(paciente->getPatientBirthName()));
		ui->tableWidget->setItem(0, 5, item6);
		item7->setText(QString::fromStdString(paciente->getPatientBirthTime()));
		ui->tableWidget->setItem(0, 6, item7);
		item8->setText(QString::fromStdString(paciente->getPatientAddress()));
		ui->tableWidget->setItem(0, 7, item8);
		item9->setText(QString::fromStdString(paciente->getPatientSize()));
		ui->tableWidget->setItem(0, 8, item9);
		item10->setText(QString::fromStdString(paciente->getPatientWeight()));
		ui->tableWidget->setItem(0, 9, item10);
		item11->setText(QString::fromStdString(paciente->getPatientComments()));
		ui->tableWidget->setItem(0, 10, item11);
		
		item12->setText(QString::fromStdString(paciente->getStudyID()));
		ui->tableWidget_2->setItem(0, 0, item12);
		item13->setText(QString::fromStdString(paciente->getStudyInstanceUID()));
		ui->tableWidget_2->setItem(0, 1, item13);
		item14->setText(QString::fromStdString(paciente->getStudyDescription()));
		ui->tableWidget_2->setItem(0, 2, item14);
		item15->setText(QString::fromStdString(paciente->getStudyDate()));
		ui->tableWidget_2->setItem(0, 3, item15);
		item16->setText(QString::fromStdString(paciente->getStudyTime()));
		ui->tableWidget_2->setItem(0, 4, item16);
		item17->setText(QString::fromStdString(paciente->getAccessionNumber()));
		ui->tableWidget_2->setItem(0, 5, item17);
		item18->setText(QString::fromStdString(paciente->getReferringPhysician()));
		ui->tableWidget_2->setItem(0, 6, item18);
		item19->setText(QString::fromStdString(paciente->getPerformingPhysicianName()));
		ui->tableWidget_2->setItem(0, 7, item19);
		item20->setText(QString::fromStdString(paciente->getModality()));
		ui->tableWidget_2->setItem(0, 8, item20);
		

		item23->setText(QString::fromStdString(paciente->getCurrentPatientLocation()));
		ui->tableWidget_3->setItem(0, 0, item23);
		item24->setText(QString::fromStdString(paciente->getModalitiesInStudy()));
		ui->tableWidget_3->setItem(0, 1, item24);
		item25->setText(QString::fromStdString(paciente->getInstitutionName()));
		ui->tableWidget_3->setItem(0, 2, item25);
		item26->setText(QString::fromStdString(paciente->getPersonName()));
		ui->tableWidget_3->setItem(0, 3, item26);
		item21->setText(QString::fromStdString(paciente->getAdditionalPatientHistory()));
		ui->tableWidget_3->setItem(0, 4, item21);
		item22->setText(QString::fromStdString(paciente->getAllergies()));
		ui->tableWidget_3->setItem(0, 5, item22);
		item27->setText(QString::fromStdString(paciente->getPersonTelephoneNumbers()));
		ui->tableWidget_3->setItem(0, 6, item27);

	}
}