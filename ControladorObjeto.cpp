#include "ControladorObjeto.h"
#include "ui_ControladorObjeto.h"
#include "MasterHandler.h"
#include "SurfaceInteractions.h"

ControladorObjeto::ControladorObjeto(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ControladorObjeto)
{
    ui->setupUi(this);
	
	connect(ui->pos_x, SIGNAL(valueChanged(double)), this, SLOT(slot_pos_x(double)));
	connect(ui->pos_y, SIGNAL(valueChanged(double)), this, SLOT(slot_pos_y(double)));
	connect(ui->pos_z, SIGNAL(valueChanged(double)), this, SLOT(slot_pos_z(double)));
	connect(ui->rot_x, SIGNAL(valueChanged(double)), this, SLOT(slot_rot_x(double)));
	connect(ui->rot_y, SIGNAL(valueChanged(double)), this, SLOT(slot_rot_y(double)));
	connect(ui->rot_z, SIGNAL(valueChanged(double)), this, SLOT(slot_rot_z(double)));
	connect(ui->scale_x, SIGNAL(valueChanged(double)), this, SLOT(slot_scale_x(double)));
	connect(ui->scale_y, SIGNAL(valueChanged(double)), this, SLOT(slot_scale_y(double)));
	connect(ui->scale_z, SIGNAL(valueChanged(double)), this, SLOT(slot_scale_z(double)));

}

void ControladorObjeto::slot_pos_x(double d) {
	MasterHandler::GetSurfaceInteractor()->move_pos_x(d);
	MasterHandler::Renderizar();
}
void ControladorObjeto::slot_pos_y(double d) {
	MasterHandler::GetSurfaceInteractor()->move_pos_y(d);
	MasterHandler::Renderizar();
}
void ControladorObjeto::slot_pos_z(double d) {
	MasterHandler::GetSurfaceInteractor()->move_pos_z(d);
	MasterHandler::Renderizar();
}
void ControladorObjeto::slot_rot_x(double d) {
	MasterHandler::GetSurfaceInteractor()->rot_x(d);
	MasterHandler::Renderizar();
}
void ControladorObjeto::slot_rot_y(double d) {
	MasterHandler::GetSurfaceInteractor()->rot_y(d);
	MasterHandler::Renderizar();
}
void ControladorObjeto::slot_rot_z(double d) {
	MasterHandler::GetSurfaceInteractor()->rot_z(d);
	MasterHandler::Renderizar();
}
void ControladorObjeto::slot_scale_x(double d) {
	MasterHandler::GetSurfaceInteractor()->scale_x(d);
	MasterHandler::Renderizar();
}
void ControladorObjeto::slot_scale_y(double d) {
	MasterHandler::GetSurfaceInteractor()->scale_y(d);
	MasterHandler::Renderizar();
}
void ControladorObjeto::slot_scale_z(double d) {
	MasterHandler::GetSurfaceInteractor()->scale_z(d);
	MasterHandler::Renderizar();
}

void ControladorObjeto::setValores(double vpos_x, double vpos_y, double vpos_z, double vrot_x, double vrot_y, double vrot_z, double vscale_x, 
	double vscale_y, double vscale_z, int cantFaces)
{
	ui->pos_x->setValue(vpos_x);
	ui->pos_y->setValue(vpos_y);
	ui->pos_z->setValue(vpos_z);
	ui->rot_x->setValue(vrot_x);
	ui->rot_y->setValue(vrot_y);
	ui->rot_z->setValue(vrot_z);
	ui->scale_x->setValue(vscale_x);
	ui->scale_y->setValue(vscale_y);
	ui->scale_z->setValue(vscale_z);

	ui->TotalCarasNumber->setText(QString::number(cantFaces));
}

ControladorObjeto::~ControladorObjeto()
{
    delete ui;
}
