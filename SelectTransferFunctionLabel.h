#pragma once

#include <QMouseEvent>
#include <QLabel>


class CameraHandler;
class SimpleView;
class SelectTransferFunctionDialog;

//aca esta extendiendo a la clase qvtkopen, asi se extiende
class SelectTransferFunctionLabel : public QLabel
{

	Q_OBJECT

public:

	SelectTransferFunctionLabel();
	SelectTransferFunctionLabel(QWidget * parent);
	~SelectTransferFunctionLabel();


	void SetParentDialog(SelectTransferFunctionDialog* d);

	void SetIndex(int i);

	void mousePressEvent(QMouseEvent *event) override;
	void mouseMoveEvent(QMouseEvent *event) override;
	void mouseReleaseEvent(QMouseEvent *event) override;

	void wheelEvent(QWheelEvent* e) override;


private:
	typedef QLabel Superclass;
	SelectTransferFunctionDialog* parentDialog;
	int index;

};

#pragma once
