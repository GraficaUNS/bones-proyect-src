#include "ThreadHandler.h"
#include "MasterHandler.h"
#include "VolumeInteractions.h"
#include "SurfaceInteractions.h"
#include "Consola.h"
#include "MeshOperations.h"
#include "VolumeToSurface.h"
#include "NoteInteractions.h"
#include <vtkActor.h>
#include "QString"



void ThreadHandler::init() {
	qualityWidget = MasterHandler::GetSimpleView()->getQualityWidget();

	timer = new QTimer(this);
	connect(timer, SIGNAL(timeout()), this, SLOT(slot_updateTimer()));
	timer->start(1000);

	if (!isRunning()) {
		start(QThread::Priority::NormalPriority);
	}
}

bool ThreadHandler::isWorking()
{
	return working;
}

void ThreadHandler::lock_TransferFunction()
{
	mutex_TransferFunction->lock();
}

void ThreadHandler::unlock_TransferFunction()
{
	mutex_TransferFunction->unlock();
}

bool ThreadHandler::tryLock_TransferFunction()
{
	return mutex_TransferFunction->tryLock();
}

void ThreadHandler::lock_Actor()
{
	mutex_Actor->lock();
}

void ThreadHandler::unlock_Actor()
{
	mutex_Actor->unlock();
}

bool ThreadHandler::tryLock_Actor()
{
	return mutex_Actor->tryLock();
}

void ThreadHandler::lock_Render()
{
	mutex_Render->lock();
}

void ThreadHandler::unlock_Render()
{
	mutex_Render->unlock();
}

bool ThreadHandler::tryLock_Render()
{
	return mutex_Render->tryLock();
}

void ThreadHandler::run()
{
	if (!is_scale_thread)
	while (!control_thread_stop) {

		if (control_openVolume) {
			working = true;
			
			MasterHandler::run_openVolume(path);
			control_openVolume = false;
		}
		if (control_exportVolume) {
			if (!tryLock_TransferFunction()) {
				emit MasterHandler::GetSimpleView()->signal_Warning(WARN_FT_ABIERTA);

			}
			else {
				working = true;

				if (vol != nullptr)
					MasterHandler::GetVolumeInteractor()->exportarVolumen(QString(path.data()), vol);
				vol = nullptr;
				unlock_TransferFunction();
			}
			control_exportVolume = false;
		}

		if (control_importVolume) {
			working = true;

			MasterHandler::GetVolumeInteractor()->importarVolumen(QString(path.data()));

			control_importVolume = false;//OJO ESTO VA AL FINAL
		}

		if (control_separarVolume) {
			if (!tryLock_TransferFunction()) {
				emit MasterHandler::GetSimpleView()->signal_Warning(WARN_FT_ABIERTA);

			}
			else {
				working = true;
				if (vol != nullptr) {

					//temporizar aqui
					tiempo = 0;
					MasterHandler::GetVolumeInteractor()->separarVolumen(vol, ratio);
					int t2 = tiempo;
					int minutos = t2 / 60;
					int segundos = t2 % 60;
					
					emit MasterHandler::GetSimpleView()->signal_Info("Operación finalizada en: " + (QDateTime::fromTime_t(t2).toUTC().toString("mm:ss")));
					
					//temporizar aqui
				}
				vol = nullptr;
				unlock_TransferFunction();
				
			}
			control_separarVolume = false;
		}

		if (control_separarSuperficie) {
			working = true;
			if (act != nullptr) {
				//temporizar aqui
				int t1 = tiempo;
				MasterHandler::GetSurfaceInteractor()->separarObjeto(act, ratio);
				int t2 = tiempo - t1;
				int minutos = t2 / 60;
				int segundos = t2 % 60;
				emit MasterHandler::GetSimpleView()->signal_Info("Operación finalizada en: " + (QDateTime::fromTime_t(t2).toUTC().toString("mm:ss")));
				//temporizar aqui
			}
			act = nullptr;
			control_separarSuperficie = false;
		}

		if (control_unirSuperficie) {
			working = true;
			//temporizar aqui
			int t1 = tiempo;
			MasterHandler::GetSurfaceInteractor()->unirObjetosSeleccionados();
			int t2 = tiempo - t1;
			int minutos = t2 / 60;
			int segundos = t2 % 60;
			emit MasterHandler::GetSimpleView()->signal_Info("Operación finalizada en: " + (QDateTime::fromTime_t(t2).toUTC().toString("mm:ss")));
			//temporizar aqui
			control_unirSuperficie = false;
		}

		if (control_unirVolumen) {
			working = true;
			//temporizar aqui
			int t1 = tiempo;
			MasterHandler::GetVolumeInteractor()->unirVolumenesSeleccionados();
			int t2 = tiempo - t1;
			int minutos = t2 / 60;
			int segundos = t2 % 60;
			emit MasterHandler::GetSimpleView()->signal_Info("Operación finalizada en: " + (QDateTime::fromTime_t(t2).toUTC().toString("mm:ss")));
			//temporizar aqui
			control_unirVolumen = false;
		}

		if (control_cortarVolumen) {
			working = true;
			if (vol != nullptr) {
				//temporizar aqui
				int t1 = tiempo;
				MasterHandler::cortarVolumenPorPlano(vol);
				int t2 = tiempo - t1;
				int minutos = t2 / 60;
				int segundos = t2 % 60;
				emit MasterHandler::GetSimpleView()->signal_Info("Operación finalizada en: " + (QDateTime::fromTime_t(t2).toUTC().toString("mm:ss")));
				//temporizar aqui
			}
			vol = nullptr;
			control_cortarVolumen = false;
		}

		if (control_cortarSuperficie) {
			working = true;
			if (act != nullptr) {
				//temporizar aqui
				int t1 = tiempo;
				MasterHandler::cortarPorPlano(act);
				int t2 = tiempo - t1;
				int minutos = t2 / 60;
				int segundos = t2 % 60;
				emit MasterHandler::GetSimpleView()->signal_Info("Operación finalizada en: " + (QDateTime::fromTime_t(t2).toUTC().toString("mm:ss")));
				//temporizar aqui
			}
			act = nullptr;
			control_cortarSuperficie = false;
		}

		if (control_meshReduction) {
			working = true;
			if (act != nullptr) {
				qualityWidget->lock();
				qualityWidget->unlock();
				if (qualityWidget->isAccepted()) {
					double ratio = qualityWidget->getValue() / 100.0;
					//temporizar aqui
					int t1 = tiempo;
					MasterHandler::GetSurfaceInteractor()->GetMeshOperations()->meshReduction(act, 1.0 - ratio);
					int t2 = tiempo - t1;
					int minutos = t2 / 60;
					int segundos = t2 % 60;
					emit MasterHandler::GetSimpleView()->signal_Info("Operación finalizada en: " + (QDateTime::fromTime_t(t2).toUTC().toString("mm:ss")));
					//temporizar aqui
					emit MasterHandler::GetSimpleView()->signal_renderizar();
				}
			}
			act = nullptr;
			control_meshReduction = false;
		}

		if (control_smoothMesh) {
			working = true;
			if (act != nullptr) {
				qualityWidget->lock();
				qualityWidget->unlock();
				if (qualityWidget->isAccepted()) {
					//temporizar aqui
					int t1 = tiempo;
					MasterHandler::GetSurfaceInteractor()->GetMeshOperations()->smoothMesh(act, qualityWidget->getValue());
					int t2 = tiempo - t1;
					int minutos = t2 / 60;
					int segundos = t2 % 60;
					emit MasterHandler::GetSimpleView()->signal_Info("Operación finalizada en: " + (QDateTime::fromTime_t(t2).toUTC().toString("mm:ss")));
					//temporizar aqui
					emit MasterHandler::GetSimpleView()->signal_renderizar();
				}
			}
			act = nullptr;
			control_smoothMesh = false;
			working = false;
		}

		if (control_removeSmallObjects) {
			working = true;
			if (act != nullptr) {
				qualityWidget->lock();
				qualityWidget->unlock();
				if (qualityWidget->isAccepted()) {
					double ratio = qualityWidget->getValue() / 100.0;
					//temporizar aqui
					int t1 = tiempo;
					MasterHandler::GetSurfaceInteractor()->GetMeshOperations()->removeSmallObjects(act, 1.0 - ratio);
					int t2 = tiempo - t1;
					int minutos = t2 / 60;
					int segundos = t2 % 60;
					emit MasterHandler::GetSimpleView()->signal_Info("Operación finalizada en: " + (QDateTime::fromTime_t(t2).toUTC().toString("mm:ss")));
					//temporizar aqui
					emit MasterHandler::GetSimpleView()->signal_renderizar();
				}
			}
			act = nullptr;
			control_removeSmallObjects = false;
		}

		if (control_corteTridimensional) {
			working = true;
			if (vol != nullptr) {
				//temporizar aqui
				int t1 = tiempo;
				MasterHandler::GetVolumeInteractor()->setCorteTridimensional(MasterHandler::GetCameraHandler(), vol);
				int t2 = tiempo - t1;
				int minutos = t2 / 60;
				int segundos = t2 % 60;
				emit MasterHandler::GetSimpleView()->signal_Info("Operación finalizada en: " + (QDateTime::fromTime_t(t2).toUTC().toString("mm:ss")));
				//temporizar aqui
			}
			vol = nullptr;
			control_corteTridimensional = false;

		}

		if (control_convertVolumen) {

			if (!tryLock_TransferFunction()) {
				emit MasterHandler::GetSimpleView()->signal_Warning(WARN_FT_ABIERTA);
			}
			else {
				working = true;
				if (vol != nullptr) {

					qualityWidget->lock(); 
					qualityWidget->unlock();
					if (qualityWidget->isAccepted()) {
						double ratio = qualityWidget->getValue() / 100.0;
						//temporizar aqui
						int t1 = tiempo;
						VolumeToSurface::ConvertVolume(vol, ratio);
						int t2 = tiempo - t1;
						int minutos = t2 / 60;
						int segundos = t2 % 60;
						emit MasterHandler::GetSimpleView()->signal_Info("Operación finalizada en: " + (QDateTime::fromTime_t(t2).toUTC().toString("mm:ss")));
						//temporizar aqui
						emit MasterHandler::GetSimpleView()->signal_renderizar();
					}
				}
				vol = nullptr;
				unlock_TransferFunction();
				

			}
			control_convertVolumen = false;
		}

		if (control_exportObject) {
			working = true;
			if (act != nullptr) {
				//temporizar aqui
				tiempo = 0;
				MasterHandler::GetIOSurface()->exportObject(path, act);
				int t2 = tiempo;
				int minutos = t2 / 60;
				int segundos = t2 % 60;
				emit MasterHandler::GetSimpleView()->signal_Info("Operación finalizada en: " + (QDateTime::fromTime_t(t2).toUTC().toString("mm:ss")));
				//temporizar aqui
			}
			act = nullptr;
			control_exportObject = false;
		}

		if (control_importObject) {
			working = true;
			bool load = false;

			vtkSmartPointer<vtkActor> actorMesh = MasterHandler::GetIOSurface()->importObject(load, path);

			if (load) {
				//temporizar aqui
				tiempo = 0;
				MasterHandler::GetSurfaceInteractor()->AddActor(actorMesh);
				int t2 = tiempo;
				int minutos = t2 / 60;
				int segundos = t2 % 60;
				emit MasterHandler::GetSimpleView()->signal_Info(QString("Operación finalizada en: ") + QString::number(minutos) + QString(":") + QString::number(segundos));
				//temporizar aqui
			} else {
				emit MasterHandler::GetSimpleView()->signal_Error("No se pudo importar la superficie");

			}
			emit MasterHandler::GetSimpleView()->signal_renderizar();
			
			control_importObject = false;
		}

		if (control_convertToGCode) {
			working = true;

			if (vol != nullptr) {
				tiempo = 0;
				VolumeToSurface::convertToGCode(vol,ratio);
				int t2 = tiempo;
				int minutos = t2 / 60;
				int segundos = t2 % 60;
				emit MasterHandler::GetSimpleView()->signal_Info(QString("Operación finalizada en: ") + QString::number(minutos) + QString(":") + QString::number(segundos));
				//temporizar aqui
			}

			control_convertToGCode = false;
		}

		working = false;
		Sleep(200);
	}
	
	else {
		while (!control_thread_stop) {
			MasterHandler::GetNoteInteractor()->rotate();
			Sleep(200);
		}
	}
}


ThreadHandler::ThreadHandler()
{
	mutex_TransferFunction = new QMutex();
	mutex_Actor = new QMutex();
	mutex_Render = new QMutex();


}

ThreadHandler::~ThreadHandler()
{
}

void ThreadHandler::set_openVolume(std::string p)
{
	if (!working) {
		path = p;
		control_openVolume = true;
	}
	else
		emit MasterHandler::GetSimpleView()->signal_Warning(HILO_TRABAJANDO);

	//thread->SingleMethodExecute();
}

void ThreadHandler::set_exportVolume(std::string p, vtkSmartPointer<myVolume> v)
{
	if (!working) {
		path = p;
		vol = v;
		control_exportVolume = true;
	}
	else
		emit MasterHandler::GetSimpleView()->signal_Warning(HILO_TRABAJANDO);

	//thread->SingleMethodExecute();
}

void ThreadHandler::set_importVolume(std::string p)
{
	if (!working) {
		path = p;
		control_importVolume = true;
	}
	else
		emit MasterHandler::GetSimpleView()->signal_Warning(HILO_TRABAJANDO);

	//thread->SingleMethodExecute();
}

void ThreadHandler::set_separarVolumen(vtkSmartPointer<myVolume> v, double r)
{
	if (!working) {
		vol = v;
		ratio = r;
		control_separarVolume = true;
	}
	else
		emit MasterHandler::GetSimpleView()->signal_Warning(HILO_TRABAJANDO);

}

void ThreadHandler::set_separarSuperficie(vtkSmartPointer<vtkActor> a, double r)
{
	if (!working) {
		act = a;
		ratio = r;
		control_separarSuperficie = true;
	}
	else
		emit MasterHandler::GetSimpleView()->signal_Warning(HILO_TRABAJANDO);

}

void ThreadHandler::set_unirSuperficie()
{
	if (!working)
		control_unirSuperficie = true;
	else
		emit MasterHandler::GetSimpleView()->signal_Warning(HILO_TRABAJANDO);

}

void ThreadHandler::set_unirVolumen()
{
	if (!working)
		control_unirVolumen = true;
	else
		emit MasterHandler::GetSimpleView()->signal_Warning(HILO_TRABAJANDO);

}

void ThreadHandler::set_cortarSuperficie(vtkSmartPointer<vtkActor> a)
{
	if (!working) {
		act = a;
		control_cortarSuperficie = true;
	}
	else
		emit MasterHandler::GetSimpleView()->signal_Warning(HILO_TRABAJANDO);

}

void ThreadHandler::set_cortarVolumen(vtkSmartPointer<myVolume> v)
{
	if (!working) {
		vol = v;
		control_cortarVolumen = true;
	}
	else
		emit MasterHandler::GetSimpleView()->signal_Warning(HILO_TRABAJANDO);

}

void ThreadHandler::set_meshReduction(vtkSmartPointer<vtkActor > actor, double r)
{
	if (!working) {
		act = actor;
		ratio = r;
		control_meshReduction = true;
	}
	else
		emit MasterHandler::GetSimpleView()->signal_Warning(HILO_TRABAJANDO);

}

void ThreadHandler::set_smoothMesh(vtkSmartPointer<vtkActor > actor, int iterations)
{
	if (!working) {
		act = actor;
		i = iterations;
		control_smoothMesh = true;
	}
	else
		emit MasterHandler::GetSimpleView()->signal_Warning(HILO_TRABAJANDO);

}

void ThreadHandler::set_removeSmallObjects(vtkSmartPointer<vtkActor > actor, double r)
{
	if (!working) {
		act = actor;
		ratio = r;
		control_removeSmallObjects = true;
	}
	else
		emit MasterHandler::GetSimpleView()->signal_Warning(HILO_TRABAJANDO);

}

void ThreadHandler::set_corteTridimensional(vtkSmartPointer<myVolume> v)
{
	if (!working) {
		vol = v;
		control_corteTridimensional = true;
	}
	else
		emit MasterHandler::GetSimpleView()->signal_Warning(HILO_TRABAJANDO);

}

void ThreadHandler::set_convertVolume(vtkSmartPointer<myVolume> v, double r)
{
	if (!working) {
		vol = v;
		ratio = r;
		control_convertVolumen = true;
	}
	else
		emit MasterHandler::GetSimpleView()->signal_Warning(HILO_TRABAJANDO);

}

void ThreadHandler::set_exportObject(vtkSmartPointer<vtkActor > actor, std::string p)
{
	if (!working) {
		act = actor;
		path = p;
		control_exportObject = true;
	}
	else
		emit MasterHandler::GetSimpleView()->signal_Warning(HILO_TRABAJANDO);

}

void ThreadHandler::set_importObject(std::string p)
{
	if (!working) {
		path = p;
		control_importObject = true;
	}
	else
		emit MasterHandler::GetSimpleView()->signal_Warning(HILO_TRABAJANDO);

}

void ThreadHandler::set_convertToGCode(vtkSmartPointer<myVolume> v, double presition)
{
	if (!working) {
		vol = v;
		control_convertToGCode = true;
		ratio = presition;
	}
}

void ThreadHandler::set_Scale_Thread()
{
	is_scale_thread = true;
}

void ThreadHandler::thread_stop()
{
	control_thread_stop = true;
}

void ThreadHandler::slot_updateTimer() {
	tiempo++;
}
