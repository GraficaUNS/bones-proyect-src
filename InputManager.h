#pragma once
#include <QKeyEvent>

class InputManager 
{

public:

	InputManager();
	~InputManager();
	
	static void keyReleaseEvent(QKeyEvent *event); //estoy poniendo override porque esta definida mas arriba y la voy a redefinir mas abajo
	static void keyPressEvent(QKeyEvent *event);


	static bool IsKeyPressed(int keycode);

};

