#include "manualPDF.h"
#include <QFileInfo>
#include <qdir.h>

#include <windows.h>
#include <fstream>
#include "ShellAPI.h"

ManualPDF::ManualPDF()  
{
   
}

ManualPDF::~ManualPDF()
{

}

void ManualPDF::OpenPDF(char* path)
{
	
	QString currentDir = QDir::currentPath();

	QString file = "file:///";

	QString finalPath = file + currentDir + "/" + path;

	ShellExecute(NULL, "open", finalPath.toStdString().data(), NULL, NULL, SW_SHOWNORMAL);

}
