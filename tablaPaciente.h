#ifndef TABLAPACIENTE_H
#define TABLAPACIENTE_H

#include <QDialog>
#include <QWidget>
#include <Paciente.h>
namespace Ui {
class tablaPaciente;
}

class tablaPaciente : public QDialog
{
	Q_OBJECT

public:
    explicit tablaPaciente(QWidget *parent = 0);
    ~tablaPaciente();
	void insertarPaciente(Paciente * paciente);

private slots:
    void on_botonAceptar_clicked();

private:
    Ui::tablaPaciente *ui;
};

#endif // TABLAPACIENTE_H
