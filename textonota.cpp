#include "textonota.h"
#include "ui_textonota.h"

textoNota::textoNota(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::textoNota)
{
    ui->setupUi(this);
}

textoNota::~textoNota()
{
    delete ui;
}

void textoNota::accept() {
	QStringList texto;
	texto.append(ui->textEdit->toPlainText());
	emit enviarDatosPopUp(texto);
	this->close();
}

void textoNota::appendText(QString str) {
	ui->textEdit->clear();
	ui->textEdit->insertPlainText(str);

}
