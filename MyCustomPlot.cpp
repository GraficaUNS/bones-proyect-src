#include "MyCustomPlot.h"
#include "QWidget.h"
#include "Consola.h"
#include <QList>
//#include "qcustomplot.h"
#include <vtkPiecewiseFunction.h>
#include <vtkVolume.h>
#include "MasterHandler.h"
#include <vtkVolumeProperty.h>
#include <vtkGenericOpenGLRenderWindow.h>

#include <vtkPiecewiseFunction.h>
#include <vtkVolume.h>
#include <vtkVolumeProperty.h>
#include <vtkSmartPointer.h>
#include <QToolTip>
#include <qcolordialog.h>
#include "myVolume.h"
#include <qlist.h>
#include <QtWidgets/QDoubleSpinBox>

#include "SelectTransferFunctionDialog.h"
#include "ThreadHandler.h"

#define MAX(a,b) (((a)>(b))?(a):(b))
#define MIN(a,b) (((a)<(b))?(a):(b))
//#define ANCHO_BARRA_COLOR 50
//#define CANT_BARRA_COLOR 50
//#define INIT_VALUE 1000
#define COLOR_MOUSE_EVENT 40
#define COLOR_TRIANGLE_VALUE -0.55f


MyCustomPlot::MyCustomPlot()
{

}

MyCustomPlot::MyCustomPlot(QWidget* parent): QCustomPlot(parent)
{
	
	//Creo la ventana dialog que tendr� los 8 volumenes para elegir
	this->ventanaTransferFunction = new SelectTransferFunctionDialog(MasterHandler::GetSimpleView());
	initOpacityFunction();
	vol = nullptr;
}


MyCustomPlot::~MyCustomPlot()
{

}


void MyCustomPlot::initOpacityFunction()
{
	//vol = volumen;
	//vol->restoreValues();
	//QVector<double> x1(NUMERODEPUNTOS), y1(NUMERODEPUNTOS);
	
	//grafico de opacidad
	opacity_graph = addGraph();
	//opacity_graph->setData(*vol->GetOpacityFunction_X(), *vol->GetOpacityFunction_Y());

	QPen op_pen(Qt::black);
	op_pen.setWidth(3.0f);

	opacity_graph->setPen(op_pen);
	opacity_graph->rescaleKeyAxis();
	

	//grafico de gradiente de opacidad
	gradient_graph = addGraph();
	//gradient_graph->setData(*vol->GetGradientFunction_X(), *vol->GetGradientFunction_Y());

	QPen grad_pen(Qt::red);
	grad_pen.setWidth(3.0f);

	gradient_graph->setPen(grad_pen);
	gradient_graph->rescaleKeyAxis();

	//rango de los ejes
	yAxis->setRange(-0.65, 1.3);
	xAxis->grid()->setZeroLinePen(Qt::NoPen);
	xAxis->setRange(-INIT_TRANSFER_FUNCTION_OPACITY_VALUE, VALUERANGE - INIT_TRANSFER_FUNCTION_OPACITY_VALUE);

	// add the bracket at the top:
	

	// add the text label at the top:
	QCPItemText *wavePacketText = new QCPItemText(this);
	//wavePacketText->position->setParentAnchor(bracket->center);
	wavePacketText->position->setCoords(0, -10); // move 10 pixels to the top from bracket center anchor
	wavePacketText->setPositionAlignment(Qt::AlignBottom | Qt::AlignHCenter);
	wavePacketText->setText("Funcion Transferencia");
	//wavePacketText->setFont(QFont(font().family(), 10));	
	
	//RANGOS DE COLORES
	//tengo que guardar cada color aparte de las barras.. no puedo recuperar el color de la barra
	
	QPen tri_pen(Qt::darkBlue);
	tri_pen.setWidth(1.5f);
	

	for (int i = 0; i < CANT_COLOR; i++) {
		
		QCPBars *bar = new QCPBars(xAxis, yAxis);
		
		bar->setPen(QPen(QColor(0, 0, 0), 1.5));
		//colorGraph->rescaleAxes();
		bar->setWidth(STEP_COLOR);
		bar->setBrush(QColor(255, 255, 255));
		bar->setBaseValue(-0.2f);
		colorBars[i] = bar;

		QVector<double> yVal(1);
		QVector<double> xVal(1);
		xVal[0] = STEP_COLOR *i + STEP_COLOR /2 - INIT_VALUE_COLOR;
		yVal[0] = -0.3f;
		bar->setData(xVal, yVal);

		colorActiveTriangles[i] = addGraph();
		//colorActiveTriangles[i]->rescaleAxes(true);
		colorActiveTriangles[i]->setPen(tri_pen);
		//colorActiveTriangles->setLineStyle(QCPGraph::lsLine);
		colorActiveTriangles[i]->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssTriangle, 10));

	}

	ventanaTransferFunction->Init();

	//colorActiveTriangles->setData(dataTriangles_X, dataTriangles_Y);

	//setInteraction(QCP::iSelectItems);
	//setInteraction(QCP::iSelectPlottables);
}


void MyCustomPlot::slotRestoreDefault()
{

	if (!MasterHandler::GetThread()->tryLock_TransferFunction()) {
		emit MasterHandler::GetSimpleView()->signal_Warning(FT_BLOQUEADA);
		return;
	}
	
	if (vol == nullptr) {
		emit MasterHandler::GetSimpleView()->signal_Warning(WARN_NO_VOLUMEN_LISTA);

		MasterHandler::GetThread()->unlock_TransferFunction();
		return;
	}

	vol->restoreValues();

	opacity_graph->setData(*vol->GetOpacityFunction_X(), *vol->GetOpacityFunction_Y());
	gradient_graph->setData(*vol->GetGradientFunction_X(), *vol->GetGradientFunction_Y());

	QVector<double> * color = vol->GetColorFunction();
	bool* active = vol->getActiveArrayColor();
	for (int i = 0; i < CANT_COLOR; i++) {


		colorBars[i]->setBrush(QColor((*color)[i * 3 + 0] * 255.0f, (*color)[i * 3 + 1] * 255.0f, (*color)[i * 3 + 2] * 255.0f));
		if (!active[i]) {
			QVector<double> yValTri(1);
			yValTri[0] = COLOR_TRIANGLE_VALUE;
			QVector<double> xVal(1);
			xVal[0] = -2000;
			colorActiveTriangles[i]->setData(xVal, yValTri);
			
		}
		else {
			QVector<double> yValTri(1);
			yValTri[0] = COLOR_TRIANGLE_VALUE;
			QVector<double> xVal(1);
			xVal[0] = STEP_COLOR * i + STEP_COLOR / 2 - INIT_VALUE_COLOR;
			colorActiveTriangles[i]->setData(xVal, yValTri);
		}
	}

	replot();
    MasterHandler::Renderizar();

	MasterHandler::GetThread()->unlock_TransferFunction();
}




void MyCustomPlot::mousePressEvent(QMouseEvent * event)
{
	if (!MasterHandler::GetThread()->tryLock_TransferFunction()) {
		emit MasterHandler::GetSimpleView()->signal_Warning(FT_BLOQUEADA);
		return;
	}
		
	//Superclass::mousePressEvent(event);
	if (vol != nullptr) {
		vol->setPicked(false);
	
		//if (!selectedGraphs().isEmpty()) 
		QCPAbstractPlottable* qap = plottable();
		double key, value;
		qap->pixelsToCoords(event->pos(), key, value);
		if (color_check->isChecked()) {
			if (value<-0.2f && value > -0.5f && key > - INIT_VALUE_COLOR && key < VALUERANGE - INIT_VALUE_COLOR) {
				//pickeo un color
				indexColor = (key + INIT_VALUE_COLOR) / STEP_COLOR;


				QVector<double>* color = vol->GetColorFunction();


				if (event->button() == Qt::RightButton && !copyColorFlag) {

					copyColor[0] = (*color)[indexColor * 3 + 0] * 255.0f;
					copyColor[1] = (*color)[indexColor * 3 + 1] * 255.0f;
					copyColor[2] = (*color)[indexColor * 3 + 2] * 255.0f;
					copyColorFlag = true;
					QToolTip::showText(event->globalPos(),
						"Color copiado"
						, this, rect());
					Consola::appendln("Color copiado\n");
				}
				else
				{
					QColor colorSelected;

					if (copyColorFlag) {
						colorSelected = QColor(copyColor[0], copyColor[1], copyColor[2]);
					}
					else {
						colorSelected = QColorDialog::getColor(
							QColor(
							(*color)[indexColor * 3 + 0] * 255.0f,
								(*color)[indexColor * 3 + 1] * 255.0f,
								(*color)[indexColor * 3 + 2] * 255.0f)
							, this);
					}

					copyColorFlag = false;

					if (colorSelected.isValid()) {

						vol->addColorPoint(indexColor, colorSelected.red(), colorSelected.green(), colorSelected.blue());
						color = vol->GetColorFunction();
						for (int i = 0; i < CANT_COLOR; i++) {

							colorBars[i]->setBrush(QColor((*color)[i * 3 + 0] * 255.0f, (*color)[i * 3 + 1] * 255.0f, (*color)[i * 3 + 2] * 255.0f));

						}
						QVector<double> yValTri(1);
						yValTri[0] = COLOR_TRIANGLE_VALUE;
						QVector<double> xVal(1);
						xVal[0] = STEP_COLOR * indexColor + STEP_COLOR / 2 - INIT_VALUE_COLOR;
						colorActiveTriangles[indexColor]->setData(xVal, yValTri);
					}

					colorBars[indexColor]->setBrush(QColor((*color)[indexColor * 3 + 0] * 255.0f, (*color)[indexColor * 3 + 1] * 255.0f, (*color)[indexColor * 3 + 2] * 255.0f));

					QMouseEvent* mEvt = new QMouseEvent(QEvent::MouseButtonRelease, event->pos(), event->button(), event->button(), nullptr);
					Superclass::mouseReleaseEvent(mEvt);

					replot();
				}

				indexColor = -1;
			}
			else if (value < -0.5) {

				if (value > -0.6 && this->selectedPlottables().empty()) {
					int index = (key + INIT_VALUE_COLOR) / STEP_COLOR;
					if (index > 0 && index < CANT_COLOR - 1) {
						QVector<double> yValTri(1);
						yValTri[0] = COLOR_TRIANGLE_VALUE;
						QVector<double> xVal(1);
						xVal[0] = -2000;
						colorActiveTriangles[index]->setData(xVal, yValTri);
						vol->removeColorPoint(index);
						QVector<double> *color = vol->GetColorFunction();
						for (int i = 0; i < CANT_COLOR; i++) {

							colorBars[i]->setBrush(QColor((*color)[i * 3 + 0] * 255.0f, (*color)[i * 3 + 1] * 255.0f, (*color)[i * 3 + 2] * 255.0f));

						}
						QMouseEvent* mEvt = new QMouseEvent(QEvent::MouseButtonRelease, event->pos(), event->button(), event->button(), nullptr);

						Superclass::mouseReleaseEvent(mEvt);

						replot();
					}
					//graph->selected()
				}
			}
		}
		if (value>-0.2f)
		{
			
			if (opac_check->isChecked())
			{

				index_op = ((INIT_TRANSFER_FUNCTION_OPACITY_VALUE + key + STEP_TRANSFER_FUNCTION / 2) / STEP_TRANSFER_FUNCTION);
				if (index_op < 0)
					index_op = 0;

				oldIndex_op = index_op;

				vol->addOpacityPoint(index_op, value);

				opacity_graph->setData(*vol->GetOpacityFunction_X(), *vol->GetOpacityFunction_Y());

				//update();
				replot();
			}
			if (grad_check->isChecked())
			{

				index_gra = ((INIT_TRANSFER_FUNCTION_OPACITY_VALUE + key + STEP_TRANSFER_FUNCTION / 2) / STEP_TRANSFER_FUNCTION);

				if (index_gra < 0)
					index_gra = 0;

				oldIndex_gra = index_gra;


				vol->addGradientPoint(index_gra, value);


				gradient_graph->setData(*vol->GetGradientFunction_X(), *vol->GetGradientFunction_Y());

				//update();
				replot();
			}


		}

		MasterHandler::Renderizar();
	}

	MasterHandler::GetThread()->unlock_TransferFunction();
}

void MyCustomPlot::mouseMoveEvent(QMouseEvent * event)
{
	if (vol != nullptr) {
		QCPAbstractPlottable* qap = plottable();

		double key, value;
		qap->pixelsToCoords(event->pos(), key, value);

		if (opac_check->isChecked() && index_op >= 0) {

			{
				index_op = ((key + INIT_TRANSFER_FUNCTION_OPACITY_VALUE + STEP_TRANSFER_FUNCTION / 2) / STEP_TRANSFER_FUNCTION);
				if (index_op < 0)
					index_op = 0;

				vol->addOpacityPoint(index_op, value);

				for (oldIndex_op; oldIndex_op <= index_op; oldIndex_op++) {

					vol->addOpacityPoint(oldIndex_op, value);

				}

				for (oldIndex_op; oldIndex_op >= index_op; oldIndex_op--) {

					vol->addOpacityPoint(oldIndex_op, value);

				}

				oldIndex_op = index_op;

				opacity_graph->setData(*vol->GetOpacityFunction_X(), *vol->GetOpacityFunction_Y());

			}
		}


		if (grad_check->isChecked() && index_gra >= 0) {

			{
				index_gra = ((key + INIT_TRANSFER_FUNCTION_GRADIENT_VALUE + STEP_TRANSFER_FUNCTION / 2) / STEP_TRANSFER_FUNCTION);
				if (index_gra < 0)
					index_gra = 0;

				vol->addGradientPoint(index_gra, value);

				for (oldIndex_gra; oldIndex_gra <= index_gra; oldIndex_gra++) {

					vol->addGradientPoint(oldIndex_gra, value);


				}

				for (oldIndex_gra; oldIndex_gra >= index_gra; oldIndex_gra--) {
					vol->addGradientPoint(oldIndex_gra, value);

				}

				oldIndex_gra = index_gra;


				gradient_graph->setData(*vol->GetGradientFunction_X(), *vol->GetGradientFunction_Y());

			}
		}

		if (grad_check->isChecked() && index_gra >= 0 || opac_check->isChecked() && index_op >= 0) {
			replot();
			MasterHandler::Renderizar();
		}
		

		if (value<-0.2f && value > -0.5f && key > -INIT_VALUE_COLOR && key < VALUERANGE - INIT_VALUE_COLOR) {
			//pickeo un color

			//QToolTip::hideText();

			if (color_check->isChecked()) {

				if (indexColor != -1) {
					colorBars[indexColor]->setBrush(QColor(oldColor[0], oldColor[1], oldColor[2]));

				}

				indexColor = (key + INIT_VALUE_COLOR) / STEP_COLOR;

				QVector<double>* color = vol->GetColorFunction();

				oldColor[0] = (*color)[indexColor * 3 + 0] * 255.0f;
				oldColor[1] = (*color)[indexColor * 3 + 1] * 255.0f;
				oldColor[2] = (*color)[indexColor * 3 + 2] * 255.0f;

				colorBars[indexColor]->setBrush(QColor(MIN(oldColor[0] + COLOR_MOUSE_EVENT, 255), MIN(oldColor[1] + COLOR_MOUSE_EVENT, 255), MIN(oldColor[2] + COLOR_MOUSE_EVENT, 255)));

				QToolTip::showText(event->globalPos(),
					"R:" +
					QString::number(oldColor[0], 'g', 6) + "\n" +
					"G:" +
					QString::number(oldColor[1], 'g', 6) + "\n" +
					"B:" +
					QString::number(oldColor[2], 'g', 6)
					, this, rect());

				replot();
			}
		}
		else {
			if (indexColor != -1) {
				colorBars[indexColor]->setBrush(QColor(oldColor[0], oldColor[1], oldColor[2]));
				replot();
			}
			indexColor = -1;

			QToolTip::showText(event->globalPos(),
				QString::number(key, 'g', 4) + ", " +
				QString::number(value, 'g', 3)
				, this, rect());
		}
	}
}

void MyCustomPlot::mouseReleaseEvent(QMouseEvent * event)
{

	Superclass::mouseReleaseEvent(event);

	index_op = -1;
	oldIndex_op = -1;
	index_gra = -1;
	oldIndex_gra = -1;
	
}


void MyCustomPlot::addCkeckBox_opacity(QCheckBox * check)
{
	opac_check = check;
}

void MyCustomPlot::addCkeckBox_gradient(QCheckBox * check)
{
	grad_check = check;
}

void MyCustomPlot::addCkeckBox_color(QCheckBox * check)
{
	color_check = check;
}

void MyCustomPlot::addActionRestore(QPushButton * boton)
{
	connect(boton, SIGNAL(clicked(bool)), this, SLOT(slotRestoreDefault()));
}

void MyCustomPlot::addActionSelectTransferFunction(QPushButton * boton)
{
	connect(boton, SIGNAL(clicked(bool)), this, SLOT(slot_ShowSelectTransferFunction()));
}

void MyCustomPlot::addComboBoxBlendModes(QComboBox * boton)
{
	/*/
	connect(boton, SIGNAL(activated(int)), this, SLOT(slot_SetBlendMode(int)));
	BlendComboBox = boton;
	/**/
}

void MyCustomPlot::setVolume(vtkSmartPointer<myVolume> newVolume)
{
	
	if (vol != newVolume) {
		changeVolume = true;
		
	}
	
	vol = newVolume;

	if (vol != nullptr) {

		this->AmbientSpin->setEnabled(true);
		this->DiffuseSpin->setEnabled(true);
		this->SpecularSpin->setEnabled(true);
		this->SpecularPowerSpin->setEnabled(true);

		this->AmbientSpin->setValue(vol->GetAmbient());
		this->DiffuseSpin->setValue(vol->GetDiffuse());
		this->SpecularSpin->setValue(vol->GetSpecular());
		this->SpecularPowerSpin->setValue(vol->GetSpecularPower());

		vtkSmartPointer<vtkOpenGLGPUVolumeRayCastMapper> mapper = (vtkOpenGLGPUVolumeRayCastMapper*)vol->GetMapper();


		//this->BlendComboBox->setCurrentIndex(mapper->GetBlendMode());

		opacity_graph->setData(*vol->GetOpacityFunction_X(), *vol->GetOpacityFunction_Y());

		gradient_graph->setData(*vol->GetGradientFunction_X(), *vol->GetGradientFunction_Y());

		MasterHandler::GetSimpleView()->progressChanged(40);

		QVector<double> * color = vol->GetColorFunction();
		bool* active = vol->getActiveArrayColor();

		for (int i = 0; i < CANT_COLOR; i++) {

			colorBars[i]->setBrush(QColor((*color)[i * 3 + 0] * 255.0f, (*color)[i * 3 + 1] * 255.0f, (*color)[i * 3 + 2] * 255.0f));
			if (active[i]) {
				QVector<double> yValTri(1);
				yValTri[0] = COLOR_TRIANGLE_VALUE;
				QVector<double> xVal(1);
				xVal[0] = STEP_COLOR * i + STEP_COLOR / 2 - INIT_VALUE_COLOR;
				colorActiveTriangles[i]->setData(xVal, yValTri);
			}
			else {
				QVector<double> yValTri(1);
				yValTri[0] = COLOR_TRIANGLE_VALUE;
				QVector<double> xVal(1);
				xVal[0] = -2000;
				colorActiveTriangles[i]->setData(xVal, yValTri);
			}

			MasterHandler::GetSimpleView()->progressChanged(40 + (i + 1.0f)*(30.0f / CANT_COLOR));

		}

		MasterHandler::GetSimpleView()->progressChanged(70);

		index_op = -1;
		oldIndex_op = -1;
		index_gra = -1;
		oldIndex_gra = -1;

		replot();
	}
	else {

		this->AmbientSpin->setEnabled(false);
		this->DiffuseSpin->setEnabled(false);
		this->SpecularSpin->setEnabled(false);
		this->SpecularPowerSpin->setEnabled(false);
		
		//esto no lo voy a necesitar m�s
		ventanaTransferFunction->setVolume(vol);
	}

	ventanaTransferFunction->hide();
	
}

void MyCustomPlot::setIlluminationSpins(QDoubleSpinBox * AmbientSpin, QDoubleSpinBox * DiffuseSpin, QDoubleSpinBox * SpecularSpin, QDoubleSpinBox * SpecularPowerSpin)
{

	this->AmbientSpin = AmbientSpin;
	this->DiffuseSpin = DiffuseSpin;
	this->SpecularSpin = SpecularSpin;
	this->SpecularPowerSpin = SpecularPowerSpin;

	connect(AmbientSpin, SIGNAL(valueChanged(double)), this, SLOT(slot_SetAmbient(double)));
	connect(DiffuseSpin, SIGNAL(valueChanged(double)), this, SLOT(slot_SetDiffuse(double)));
	connect(SpecularSpin, SIGNAL(valueChanged(double)), this, SLOT(slot_SetSpecular(double)));
	connect(SpecularPowerSpin, SIGNAL(valueChanged(double)), this, SLOT(slot_SetSpecularPower(double)));

	this->AmbientSpin->setEnabled(false);
	this->DiffuseSpin->setEnabled(false);
	this->SpecularSpin->setEnabled(false);
	this->SpecularPowerSpin->setEnabled(false);
}


void MyCustomPlot::slot_SetAmbient(double d) {
	if (vol != nullptr)
		vol->SetAmbient(d);
	MasterHandler::Renderizar();
}

void MyCustomPlot::slot_SetDiffuse(double d) {
	if (vol != nullptr)
		vol->SetDiffuse(d);
	MasterHandler::Renderizar();
}

void MyCustomPlot::slot_SetSpecular(double d) {
	if (vol != nullptr)
		vol->SetSpecular(d);
	MasterHandler::Renderizar();
}

void MyCustomPlot::slot_SetSpecularPower(double d) {
	if (vol != nullptr)
		vol->SetSpecularPower(d);
	MasterHandler::Renderizar();
}

void MyCustomPlot::slot_ShowSelectTransferFunction()
{
	if (!MasterHandler::GetThread()->tryLock_TransferFunction()) {
		emit MasterHandler::GetSimpleView()->signal_Warning(FT_BLOQUEADA);
		return;
	}
	MasterHandler::GetThread()->unlock_TransferFunction();
	
	if (vol == nullptr) {
		emit MasterHandler::GetSimpleView()->signal_Warning(WARN_NO_VOLUMEN_LISTA);
		return;
	}

	if (vol->getIsSplitted()) {
		emit MasterHandler::GetSimpleView()->signal_Warning("El volumen esta separado\nEsta opcion no esta disponible para volumenes separados");
		return;
	}
	
	this->ventanaTransferFunction->mostrar();
	if (changeVolume) {
		//esta tampoco la voy a necesitar
		ventanaTransferFunction->setVolume(vol);
	}
	changeVolume = false;
	
}



void MyCustomPlot::slot_SetBlendMode(int index)
{
	if (vol == nullptr) {
		emit MasterHandler::GetSimpleView()->signal_Warning(WARN_NO_VOLUMEN_LISTA);
		return;
	}

	vtkSmartPointer<vtkOpenGLGPUVolumeRayCastMapper> mapper = (vtkOpenGLGPUVolumeRayCastMapper*)vol->GetMapper();

	mapper->SetBlendMode(BlendModes[index]);

	emit MasterHandler::GetSimpleView()->signal_renderizar();
}