#include <vtkVolume.h>
#include <vtkVolumeProperty.h>
#include <vtkPiecewiseFunction.h>
#include <vtkSmartPointer.h>
#include <vtkColorTransferFunction.h>
#include "CallbacksClass.h"
#include <vtkCallbackCommand.h>
#include "Consola.h"
#include <vtkImageData.h>
#include <vtkTransform.h>
#include <vtkFixedPointVolumeRayCastMapper.h>
#include <vtkPlaneCollection.h>

#include "sliceVolume.h"


/*/
double Slice_colorDefault[SLICE_CANT_COLOR*SLICE_CANT_COMPONENTS_COLOR] = {
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	194.0f / 255.0f, 105.0f / 255.0f, 82.0f / 255.0f,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	194.0f / 255.0f, 105.0f / 255.0f, 82.0f / 255.0f,	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	194.0f / 255.0f, 166.0f / 255.0f, 115.0f / 255.0f,
	0,0,0,
	194.0f / 255.0f, 166.0f / 255.0f, 115.0f / 255.0f,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	102.0f / 255.0f, 0, 0,
	0,0,0,
	153.0f / 255.0f, 0, 0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0.80,0.80,0.80,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0.80,0.80,0.80,

};

int Slice_colorDefaultIndexActive[SLICE_CANT_COLOR_INDEX_ACTIVE_DEFAULT] = {
	//0,5,13,16,20,26,49
	((-1000) + 1000) / SLICE_STEP_COLOR,
	((-600) + 1000) / SLICE_STEP_COLOR,
	((-400) + 1000) / SLICE_STEP_COLOR,
	((-100) + 1000) / SLICE_STEP_COLOR,
	((-60) + 1000) / SLICE_STEP_COLOR,
	((40) + 1000) / SLICE_STEP_COLOR,
	((80) + 1000) / SLICE_STEP_COLOR,
	((300) + 1000) / SLICE_STEP_COLOR,
	((1500) + 1000) / SLICE_STEP_COLOR,
};
/**/

vtkSmartPointer<vtkVolumeProperty> sliceVolume::initVolumeProperty() {

	vtkSmartPointer<vtkVolumeProperty> volumeProperty = vtkSmartPointer<vtkVolumeProperty>::New();

	volumeProperty->ShadeOn();
	volumeProperty->SetInterpolationTypeToLinear();

	volumeProperty->SetAmbient(0.3);
	volumeProperty->SetDiffuse(1);
	volumeProperty->SetSpecular(0);
	volumeProperty->SetSpecularPower(100);

	vtkSmartPointer<vtkPiecewiseFunction> scalarOpacity = vtkSmartPointer<vtkPiecewiseFunction>::New();
	//vtkSmartPointer<vtkPiecewiseFunction> gradientOpacity = vtkSmartPointer<vtkPiecewiseFunction>::New();
	//color = vtkSmartPointer<vtkColorTransferFunction>::New();

	for (int i = -1000; i <= 1500; i+=10) {
		scalarOpacity->AddPoint(i, 1);
		//gradientOpacity->AddPoint(i, 1);
	}

	/**
	color->RemoveAllPoints();
	int j = 0;
	for (int i = 0; i < SLICE_CANT_COLOR; i++) {
		//(*vec_color)[i * 3 + 0] = colorDefault[i * 3 + 0];
		//(*vec_color)[i * 3 + 1] = colorDefault[i * 3 + 1];
		//(*vec_color)[i * 3 + 2] = colorDefault[i * 3 + 2];


		if (i == Slice_colorDefaultIndexActive[j]) {
			j++;
			color->AddRGBPoint(i * SLICE_STEP_COLOR - 1000, Slice_colorDefault[i * 3 + 0], Slice_colorDefault[i * 3 + 1], Slice_colorDefault[i * 3 + 2]);
			//int valueX = i * STEP_COLOR - INIT_VALUE_COLOR;
			//color->AddRGBPoint(valueX, colorDefault[i * 3 + 0], colorDefault[i * 3 + 1], colorDefault[i * 3 + 2]);
		}
	}
	/**/
	volumeProperty->SetScalarOpacity(scalarOpacity);
	//volumeProperty->SetGradientOpacity(gradientOpacity);
	//volumeProperty->SetColor(color);

	return volumeProperty;
}



//este constructor asume que el volumen esta inicializado externamente (datos) 
//siempre se inicializa con la misma funcion transferencia

sliceVolume::sliceVolume() : vtkVolume()
{

	vtkSmartPointer<vtkVolumeProperty> volumeProperty = initVolumeProperty();

	SetProperty(volumeProperty);
	

	volumeMapper = vtkSmartPointer<vtkSmartVolumeMapper>::New();

	SetMapper(volumeMapper);
}

sliceVolume::sliceVolume(vtkSmartPointer<vtkImageData> img, int cut, vtkSmartPointer<vtkColorTransferFunction> colorTransferFunction): vtkVolume()
{

	vtkSmartPointer<vtkVolumeProperty> volumeProperty = initVolumeProperty();

	SetProperty(volumeProperty);

	color = colorTransferFunction;

	imageData = img;

	volumeMapper = vtkSmartPointer<vtkSmartVolumeMapper>::New();
	volumeMapper->SetBlendModeToMinimumIntensity();
	volumeMapper->SetInputData(imageData);

	double* objectCenter = volumeMapper->GetCenter();

	SetMapper(volumeMapper);
	

	// aca configuro lo relativo a los planos de corte


	planos[0] = vtkSmartPointer<vtkPlane>::New();
	planos[1] = vtkSmartPointer<vtkPlane>::New();

	planos[0]->SetNormal(0, 0, 1);
	planos[1]->SetNormal(0, 0, -1);

	spacing = img->GetSpacing()[cut];


	int* dims = imageData->GetDimensions();

	maxDim = dims[cut];

	slice = maxDim/2;


	planos[0]->SetOrigin(0, 0, 0);

	planos[1]->SetOrigin(0, 0, imageData->GetSpacing()[2]);

	vtkSmartPointer<vtkPlaneCollection> col = vtkSmartPointer<vtkPlaneCollection>::New();

	col->AddItem(planos[0]);
	col->AddItem(planos[1]);

	volumeMapper->SetClippingPlanes(col);


	vtkSmartPointer<vtkTransform> translation = vtkSmartPointer<vtkTransform>::New();
	
	if (cut == SLICE_XY_PLANE) {
		translation->Translate(0, 0, -objectCenter[2]);
		planos[1]->SetOrigin(0, 0, imageData->GetSpacing()[2]);
	}

	
	if (cut == SLICE_YZ_PLANE) {
		//rotacion
		
		translation->RotateX(45);
		translation->RotateZ(-90);
		translation->RotateY(45);
		translation->Translate(-objectCenter[0], 0, -objectCenter[2]*2);
		
		planos[1]->SetOrigin(0, 0, imageData->GetSpacing()[0]);

		//translation->Translate(0, -objectCenter[1], -objectCenter[2]);
	}
	if (cut == SLICE_ZX_PLANE){
		//rotacion
		
		translation->RotateX(90);
		translation->Translate(0, -objectCenter[1], -objectCenter[2]*2);
		planos[1]->SetOrigin(0, 0, imageData->GetSpacing()[1]);

	}
	
	SetUserTransform(translation);

	volumeProperty->SetColor(colorTransferFunction);

}

sliceVolume::~sliceVolume()
{
	planos[0] = nullptr;
	planos[1] = nullptr;

	color = nullptr;

	imageData = nullptr;

	volumeMapper = nullptr;
}


/**

void myVolume::deletePoint2D(int x, int y)
{
	int z1 = getImageActorXY()->GetZSlice();

	double d = getImageData()->GetScalarComponentAsDouble(x, y, z1, 0);

	if (d > RADIO_DENSITY_ZERO) {//si fue 0 es porque toque un punto que no debia tocar

		puntosSeleccionados.append(x);
		puntosSeleccionados.append(y);
		puntosSeleccionados.append(z1);
		puntosSeleccionados.append(d);
		getImageData()->SetScalarComponentFromDouble(x, y, z1, 0, RADIO_DENSITY_ZERO);

		getImageData()->Modified();

		//imageActorXY->SetZSlice(z + 1);
		imageActorYZ->SetZSlice(x);
		imageActorZX->SetZSlice(y);


	}
}

void myVolume::selectPoint(int x, int y, int z)
{

	double d = getImageData()->GetScalarComponentAsDouble(x, y, z + 1, 0);

	{//si fue 0 es porque toque un punto que no debia tocar

		puntosSeleccionados.append(x);
		puntosSeleccionados.append(y);
		puntosSeleccionados.append(z + 1);
		puntosSeleccionados.append(d);
		getImageData()->SetScalarComponentFromDouble(x, y, z + 1, 0, SELECT_CELL * 3);

		getImageData()->Modified();

		//imageActorXY->SetZSlice(z + 1);
		//imageActorYZ->SetZSlice(x);
		//imageActorZX->SetZSlice(y);


	}

}

void myVolume::removeSelectedPoints()
{
#define COMP_X i*4
#define COMP_Y i*4 + 1
#define COMP_Z i*4 + 2

	int cant = puntosSeleccionados.size() / 4;
	int x, y, z;
	for (int i = 0; i < cant; i++) {
		x = puntosSeleccionados.value(COMP_X);
		y = puntosSeleccionados.value(COMP_Y);
		z = puntosSeleccionados.value(COMP_Z);
		getImageData()->SetScalarComponentFromDouble(x, y, z, 0, RADIO_DENSITY_ZERO);
	}
	getImageData()->Modified();

}
/**/



vtkSmartPointer<vtkImageData> sliceVolume::getImageData()
{
	return imageData;
}


void sliceVolume::SetSlice(int i)
{

	slice = i;

}


int sliceVolume::GetSlice()
{
	return slice;
}

int sliceVolume::GetMin()
{
	return 0;
}

int sliceVolume::GetMax()
{
	return maxDim;
}

void sliceVolume::centrar() {
	double* objectCenter = volumeMapper->GetCenter();
	vtkSmartPointer<vtkTransform> translation = vtkSmartPointer<vtkTransform>::New();
	translation->Translate(-objectCenter[0], -objectCenter[1], -objectCenter[2]);
	SetUserTransform(translation);
}

