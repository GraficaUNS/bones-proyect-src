#pragma once
#include <QMessageBox>
#include <QCoreApplication.h>
#include "Dialog.h"
#include "MasterHandler.h"
#include <qstring.h>
//en java ser�a class implements class, por ser una interfaz, siempre que la interfaz se llame igual
//ahora si quiero extender la interfaz dialog.h, tengo que hacer un .h para la otra interfaz que extienda
// y cuando voy a extender pongo class Dialogpepito::public Dialog tambien hay que hacer el include ahi de Dialog.h
//este dialog pepito va a tener la implementacion de dialog.cpp tambien
QMessageBox* errorDialog;


/*
Dialog::Dialog(QWidget* ap) : QWidget(ap) {

}
/**/
void Dialog::setErrorMessage(QMessageBox* e) {
	errorDialog = e;
	errorDialog->setStandardButtons(QMessageBox::Ok);
	errorDialog->setDefaultButton(QMessageBox::Ok);
}

void Dialog::mensajeError(QString string) {

	errorDialog->critical(MasterHandler::GetSimpleView(), QString("Bones Viewer"),
		string,
		QMessageBox::Ok,
		QMessageBox::Ok);

}

void Dialog::mensajeWarning(QString string) {

	errorDialog->warning(MasterHandler::GetSimpleView(), QString("Bones Viewer"),
		string,
		QMessageBox::Ok,
		QMessageBox::Ok);

}

void Dialog::mensajeInfo(QString string)
{
	errorDialog->information(MasterHandler::GetSimpleView(), QString("Bones Viewer"),
		string,
		QMessageBox::Ok,
		QMessageBox::Ok);
}

