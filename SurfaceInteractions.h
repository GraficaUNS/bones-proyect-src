#pragma once


#include <vtkActor.h>
#include <QKeyEvent>
#include <vtkRenderWindow.h>
#include <vtkMatrix4x4.h>
#include <vtkSmartPointer.h>
#include "CameraHandler.h"
#include "PlaneHandler.h"
#include "MeshOperations.h"
#include "ControladorObjeto.h"


class SurfaceInteractions
{
public:
	SurfaceInteractions();
	~SurfaceInteractions();

	void setSelectedActorToNull();
	vtkSmartPointer<vtkActor> getSelectedActor();
	void checkPickedActor(CameraHandler* camera, QMouseEvent *event);
	void girarActor(QKeyEvent *event);
	void moverActor(QKeyEvent *event);
	void clearTransforms();
	void interactuarConObjeto(QKeyEvent * event);
	vtkSmartPointer<vtkRenderWindow> GetRenderWindow();
	void SetRenderWindow(vtkSmartPointer<vtkRenderWindow> ren);

	void keyPress();
	void keyRelease();

	void unirObjetosSeleccionados();
	void separarObjeto(vtkSmartPointer<vtkActor> actor, double ratio);
	void removeSelectedActor();
	void cortarPorPlano(PlaneHandler* actorPlane, vtkSmartPointer<vtkActor> actor);

	void removeActor(vtkSmartPointer<vtkActor> actor);

	MeshOperations* GetMeshOperations();

	void AddActor(vtkSmartPointer<vtkActor> actor);


	void move_pos_x(double d);
	void move_pos_y(double d);
	void move_pos_z(double d);
	void rot_x(double d);
	void rot_y(double d);
	void rot_z(double d);
	void scale_x(double d);
	void scale_y(double d);
	void scale_z(double d);


private:
	vtkSmartPointer<vtkActor > lastPickedActor = nullptr;

	vtkNew<vtkCollection> actoresSeleccionados;

	//Matrices
	vtkSmartPointer<vtkMatrix4x4> actorMeshMatrix;
	vtkSmartPointer<vtkMatrix4x4> WordMatrix;
	vtkSmartPointer<vtkMatrix4x4> WordNormalMatrix;
	vtkSmartPointer<vtkMatrix4x4> WordNormalMatrix2;

	vtkSmartPointer<vtkRenderWindow> renderWindow_s;

	MeshOperations* meshoperations;

	//flags
	bool controlKeyPress = false;

	bool press_X = false, press_Y = false, press_Z = false;
	ControladorObjeto* controladorSuperficie;
};

