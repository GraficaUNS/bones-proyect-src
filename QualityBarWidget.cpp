#include "QualityBarWidget.h"
#include "ui_QualityBarWidget.h"

QualityBarWidget::QualityBarWidget(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::QualityBarWidget)
{
    ui->setupUi(this);
	connect(ui->horizontalSlider, SIGNAL(sliderMoved(int)), this, SLOT(handleSlider(int)));
	connect(ui->spinBox, SIGNAL(valueChanged(int)), this, SLOT(handleSlider(int)));
	
	lastValue = 75;
	ui->spinBox->setValue(lastValue);
	ui->horizontalSlider->setValue(lastValue);
}

QualityBarWidget::~QualityBarWidget()
{
    delete ui;
}

void QualityBarWidget::setText(QString texto, QString high_text, QString mid_text, QString low_text, int h_value, int l_value, int default_value)
{
	
	accepted = false;
	ui->textLabel->setText(texto);

	handleSlider(default_value);
	handleSpinBox(default_value);

	highQuality = high_text;
	midQuality = mid_text;
	lowQuality = low_text;
	highValue = h_value;
	lowValue = l_value;

}

void QualityBarWidget::lock()
{
	qualityMutex.lock();

}

void QualityBarWidget::unlock()
{
	qualityMutex.unlock();
}

int QualityBarWidget::getValue()
{
	return lastValue;
}

bool QualityBarWidget::isAccepted()
{
	return accepted;
}

void QualityBarWidget::mostrar() {
	checkWarning();
	this->show();
}

void QualityBarWidget::reject()
{
	accepted = false;
	qualityMutex.unlock();
	this->hide();
	this->close();
}

void QualityBarWidget::handleSlider(int value)
{
	lastValue = value;
	ui->spinBox->setValue(value);
	checkWarning();
}

void QualityBarWidget::handleSpinBox(int value)
{
	lastValue = value;
	ui->horizontalSlider->setValue(value);
	ui->horizontalSlider->update();
	checkWarning();
}

void QualityBarWidget::accept() {
	accepted = true;
	qualityMutex.unlock();
	this->hide();
	this->close();
}


void QualityBarWidget::checkWarning() {
	if(lastValue >= highValue)
		ui->warningLabel->setText(highQuality);
	if(lastValue < highValue && lastValue >= lowValue)
		ui->warningLabel->setText(midQuality);
	if(lastValue < lowValue)
		ui->warningLabel->setText(lowQuality);
}