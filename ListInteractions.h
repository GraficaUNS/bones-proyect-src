#pragma once
#include <iostream>
#include <qlistwidget.h>
#include <QString>
#include <QMap>
#include <qlist.h>
class VolumeInteractions;

class ListInteractions: public QListWidget
{
	Q_OBJECT
public:
	ListInteractions();
	ListInteractions(QWidget* parent);
	~ListInteractions();

	void addNewItem(int port, QString name);
	void deleteItemFromPort(int port);

	QList<int>* GetSelectedPorts();
	QString GetItemName(int port);

public slots:
	void slotAddVolume(QListWidgetItem*);
	void slotChecked(QListWidgetItem*);

private:
	QMap<QListWidgetItem*, int> ItemtoPort;
	bool firstL = true;

	QList<int> Selected;

	int itemCount = 0;
};

