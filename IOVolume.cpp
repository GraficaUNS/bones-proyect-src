#include "IOVolume.h"
#include "Consola.h"
#include "CallbacksClass.h"
#include "MasterHandler.h"
#include "VolumeInteractions.h"

#include <vtkSmartPointer.h>
#include <vtkPolyData.h>
#include <vtkImageData.h>
#include <vtkVector.h>
#include <vtkCallbackCommand.h>

#include <vtkSTLWriter.h>
#include <vtkPLYWriter.h>
#include <vtkMath.h>
#include <vtkOBJReader.h>
#include <vtkSTLReader.h>
#include <vtkPLYReader.h>
#include <vtkTypedArray.h>
#include <vtkIdTypeArray.h>
#include <vtkIdList.h>

#include <iostream>
#include <fstream>
#include <myVolume.h>
#include <QJsonArray.h>
#include <QJsonObject.h>
#include <QJsonDocument.h>
#include <QJsonValue.h>
#include <QFile.h>
#include "vtkStdString.h"

#include <vtkPlane.h>
#include <vtkPlaneCollection.h>

using namespace std;

IOVolume::IOVolume()
{
}


IOVolume::~IOVolume()
{
}

void IOVolume::importTransferenceFunction(QString path, vtkSmartPointer<myVolume> volumen) {
	{
		QString ftfile = path + "/FT.json";
		QString val;
		QFile loadFile(ftfile);
		if (loadFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
			val = loadFile.readAll();
			loadFile.close();

			bool* colorActive = volumen->getActiveArrayColor();
			for (int i = 1; i < CANT_COLOR-1; i++) {
				if (colorActive[i]) volumen->removeColorPoint(i);
			}

			QJsonDocument jsonResponse = QJsonDocument::fromJson(val.toUtf8());

			QJsonObject jsonObject = jsonResponse.object();
			QJsonArray jsonArray = jsonObject["ColorFunction"].toArray();
			if (!jsonArray.isEmpty()) {
				for (int i = 0; i * 4 < jsonArray.count(); i++) { //el count anda bien, lee 374 componentes como es esperado

					int index = jsonArray.at(i * 4).toInt();
					int R = jsonArray.at(i * 4 + 1).toInt();
					int G = jsonArray.at(i * 4 + 2).toInt();
					int B = jsonArray.at(i * 4 + 3).toInt();

					volumen->addColorPoint(index, R, G, B);
				}
			}
			jsonArray = jsonObject["GradientFunction_Y"].toArray();
			if (!jsonArray.isEmpty()) {
				for (int i = 0; i < jsonArray.count(); ++i) {
					double num = jsonArray.at(i).toDouble();
					volumen->addGradientPoint(i, num);

				}
			}

			jsonArray = jsonObject["OpacityFunction_Y"].toArray();
			if (!jsonArray.isEmpty()) {
				for (int i = 0; i < jsonArray.count(); ++i) {
					double num = jsonArray.at(i).toDouble();
					volumen->addOpacityPoint(i, num);

				}
			}

			jsonArray = jsonObject["Pixmaps"].toArray();
			if (!jsonArray.isEmpty()) {
				for (int i = 0; i < jsonArray.count(); ++i) {
					QPixmap * pixmap;
					pixmap->loadFromData(QByteArray::fromBase64(jsonArray.at(i).toString().toLatin1()), "PNG");
					volumen->addImage(pixmap,i);

				}
			}

		}
	}
	
	{
		QString planesfile = path + "/Planes.json";
		QString val;
		QFile loadFile(planesfile);
		if (loadFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
			val = loadFile.readAll();
			loadFile.close();

			QJsonDocument jsonResponse = QJsonDocument::fromJson(val.toUtf8());

			QJsonObject jsonObject = jsonResponse.object();



			QJsonArray jsonBounds = jsonObject["Bounds"].toArray();

			double bounds[6];
			int indx = 0;
			foreach(const QJsonValue & value, jsonBounds) {
				double d = value.toDouble();
				bounds[indx] = d;
				indx++;
			}

			MasterHandler::GetVolumeInteractor()->setBounds(bounds, volumen);

			QJsonArray jsonPlanesArray = jsonObject["Clipping_planes"].toArray();


			
			foreach(const QJsonValue & value, jsonPlanesArray) {
				QJsonObject obj = value.toObject();
				QJsonObject plane = obj["Plane"].toObject();

				QJsonArray Normales = plane["Normal"].toArray();
				


				vtkSmartPointer<vtkPlane> vtkplano = vtkSmartPointer<vtkPlane>::New();

				double* Normal = new double[Normales.count()];

				if (!Normales.isEmpty()) {
					for (int i = 0; i < Normales.count(); ++i) {
						double num = Normales.at(i).toDouble();
						//vtkSmartPointer<vtkPlaneCollection* colectionPlanes = volumen->GetMapper()->SetClippingPlanes()
						Normal[i] = num;
					}
				}
				vtkplano->SetNormal(Normal[0], Normal[1], Normal[2]);
				delete[] Normal;
				QJsonArray Origenes = plane["Origin"].toArray();

				double* Origin = new double[Origenes.count()];

				if (!Origenes.isEmpty()) {
					for (int i = 0; i < Origenes.count(); ++i) {
						double num = Origenes.at(i).toDouble();
						Origin[i] = num;
					}
				}
				vtkplano->SetOrigin(Origin[0], Origin[1], Origin[2]);
				delete[] Origin;

				//planeCollection->AddItem(vtkSmartPointer<vtkplano);
				volumen->addClippingPlane(vtkplano);
			}

			//volumen->GetMapper()->SetClippingPlanes(planeCollection);


		}
	}

}

void IOVolume::exportTransferenceFunction(QString path, vtkSmartPointer<myVolume> volumen) {
	
	QString ftfile = path + "/FT.json"; 
	
	QJsonArray array1,array2,array3,array4;
	QJsonObject object;
	QVector<qreal> * arreglo1 = volumen->GetOpacityFunction_Y();
	QVector<qreal> * arreglo2 = volumen->GetGradientFunction_Y();
	QVector<qreal> * arreglo3 = volumen->GetColorFunction();
	
	

	foreach(double point, * arreglo1) {
		array1.append(point);
	}
	foreach(double point, *arreglo2) {
		array2.append(point);
	}
	bool* colorActive = volumen->getActiveArrayColor();

	int cant_color_active = 0;

	for (int i = 0; i < CANT_COLOR; i++) {
		if (colorActive[i]) cant_color_active++;
	}

	QVector<int> colorArray(cant_color_active*4);
	int j = 0;
	for (int i = 0; i < CANT_COLOR; i++) {
		if (colorActive[i]){
			colorArray[j * 4 + 0] = i;
			colorArray[j * 4 + 1] = (int)((*arreglo3)[i * 3] * 255);
			colorArray[j * 4 + 2] = (int)((*arreglo3)[i * 3 + 1] * 255);
			colorArray[j * 4 + 3] = (int)((*arreglo3)[i * 3 + 2] * 255);
			j++;
		}
	}

	foreach(int point, colorArray) {
		array3.append(point);
	}

	object["OpacityFunction_Y"] = array1;
	object["GradientFunction_Y"] = array2; 
	object["ColorFunction"] = array3;
	

	//Exporto las imagenes para las funciones de transferencia
	for (int i = 0; i < NUM_TRANSFER_FUNCTION; i++) {
		QPixmap * pix = ((volumen->getImage(i)));
		QBuffer buffer;
		buffer.open(QIODevice::WriteOnly);
		pix->save(&buffer, "PNG");
		QJsonValue encoded = QLatin1String(buffer.data().toBase64());
		array4.append(encoded);
	}

	object["Pixmaps"] = array4;


	QFile saveFile(ftfile);
	if (saveFile.open(QIODevice::WriteOnly)) {
		QJsonDocument saveDoc(object);
		saveFile.write(saveDoc.toJson());
	}


	QJsonObject objectP;
	vtkSmartPointer<vtkPlaneCollection> colectionPlanes = volumen->getClippingPlanes();
	if (colectionPlanes != nullptr) {
		int cant = colectionPlanes->GetNumberOfItems();
		if (cant > 0) {
			//guardo la cantidad de planos de corte
			QJsonArray arreglo;
			for (int i = 0; i < cant; i++) {
				QJsonObject objectPlane;
				QJsonObject objectComp;
				QJsonArray arr1, arr2, arr3;
				//obtengo el plano
				vtkSmartPointer<vtkPlane> plano = colectionPlanes->GetItem(i);
				double* origin = plano->GetOrigin();
				int len = 3;
				for (int k = 0; k < len; k++) {
					arr1.append(origin[k]);
				}
				objectComp["Origin"] = arr1;

				double* normal = plano->GetNormal();
				len = 3;
				for (int k = 0; k < len; k++) {
					arr2.append(normal[k]);
				}
				objectComp["Normal"] = arr2;

				objectPlane["Plane"] = objectComp;
				arreglo.append(objectPlane);

			}
			
			objectP["Clipping_planes"] = arreglo;

		}
	}
	QJsonArray bounds;

	double* bnds = volumen->getCustomBounds();

	for (int i = 0; i < 6; i++) {
		bounds.append(bnds[i]);
	}

	objectP["Bounds"] = bounds;

	//Consola::append((volumen->getPath()).toStdString());
	QString planesfile = path + "/Planes.json";

	QFile saveFile2(planesfile);
	if (saveFile2.open(QIODevice::WriteOnly)) {
		QJsonDocument saveDoc(objectP);
		saveFile2.write(saveDoc.toJson());
	}

	
	


}

