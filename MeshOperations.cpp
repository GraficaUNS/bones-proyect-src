/****************************************************************************
** Copyright (c) 2017 Adrian Schneider, AOT AG
**
** Permission is hereby granted, free of charge, to any person obtaining a
** copy of this software and associated documentation files (the "Software"),
** to deal in the Software without restriction, including without limitation
** the rights to use, copy, modify, merge, publish, distribute, sublicense,
** and/or sell copies of the Software, and to permit persons to whom the
** Software is furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in
** all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
** FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
** DEALINGS IN THE SOFTWARE.
**
*****************************************************************************/

#include "MeshOperations.h"

#include <vtkCenterOfMass.h>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkQuadricDecimation.h>
#include <vtkPolyDataConnectivityFilter.h>
#include <vtkSmoothPolyDataFilter.h>
#include <vtkSTLWriter.h>
#include <vtkPLYWriter.h>
#include <vtkMath.h>
#include <vtkOBJReader.h>
#include <vtkSTLReader.h>
#include <vtkPLYReader.h>
#include <vtkTypedArray.h>
#include <vtkIdTypeArray.h>
#include <vtkIdList.h>
#include <vtkActor.h>
#include <vtkPolyDataMapper.h>

#include <iostream>
#include <fstream>
#include "Consola.h"

#include "CallbacksClass.h"
#include "MasterHandler.h"
#include "SimpleView.h"
#include "SurfaceInteractions.h"
#include "ThreadHandler.h"
using namespace std;



MeshOperations::MeshOperations()
{
    m_progressCallback = vtkSmartPointer<vtkCallbackCommand>(NULL);
}

MeshOperations::~MeshOperations()
{
}

void MeshOperations::SetProgressCallback( vtkSmartPointer<vtkCallbackCommand> progressCallback )
{
    m_progressCallback = progressCallback;
}



void MeshOperations::moveMeshToCOSCenter( vtkSmartPointer<vtkPolyData> mesh )
{
    vtkSmartPointer<vtkCenterOfMass> computeCenter = vtkSmartPointer<vtkCenterOfMass>::New();
    computeCenter->SetInputData( mesh );
    computeCenter->SetUseScalarsAsWeights(false);
    computeCenter->Update();

    double* objectCenter = new double[3];
    computeCenter->GetCenter(objectCenter);

    vtkSmartPointer<vtkTransform> translation = vtkSmartPointer<vtkTransform>::New();
    translation->Translate(-objectCenter[0], -objectCenter[1], -objectCenter[2]);

    vtkSmartPointer<vtkTransformPolyDataFilter> transformFilter = vtkSmartPointer<vtkTransformPolyDataFilter>::New();
    transformFilter->SetInputData( mesh );
    transformFilter->SetTransform( translation );
    transformFilter->Update();

    mesh->DeepCopy( transformFilter->GetOutput() );

    // Free memory
    delete[] objectCenter;

}

void MeshOperations::meshReduction(vtkSmartPointer<vtkActor> actor,double reduction )
{
	
	
	vtkSmartPointer<vtkPolyData> mesh = (vtkPolyData*)actor->GetMapper()->GetInput();

	Consola::appendln("Reduccion de caras");
    // Note1: vtkSmartPointer<vtkQuadricDecimation seems to be better than vtkSmartPointer<vtkDecimatePro
    // Note2: vtkSmartPointer<vtkQuadricDecimation might have problem with face normals
    vtkSmartPointer<vtkQuadricDecimation> decimator = vtkSmartPointer<vtkQuadricDecimation>::New();
    decimator->SetInputData( mesh );
    decimator->SetTargetReduction( reduction );

	vtkSmartPointer<vtkCallbackCommand> m_progressCallback = CallbacksClass::getProgressCallBack();

   // if( m_progressCallback.Get() != NULL )
    {
		CallbacksClass::setEncabezado("Reduccion de caras: ");
        decimator->AddObserver(vtkCommand::ProgressEvent, m_progressCallback);
    }
    decimator->Update();

	vtkSmartPointer<vtkPolyDataMapper> polyMapper = (vtkPolyDataMapper*)actor->GetMapper();

	MasterHandler::GetThread()->lock_Actor();
	emit MasterHandler::GetSimpleView()->signal_RemoveActor(actor);
	MasterHandler::GetThread()->lock_Actor();

    mesh->DeepCopy( decimator->GetOutput() );

	polyMapper->SetInputData(mesh);
	polyMapper->ScalarVisibilityOff();
	MasterHandler::GetSurfaceInteractor()->AddActor(actor);

	MasterHandler::GetThread()->unlock_Actor();
	Consola::appendln("Reduccion de caras: Finalizado");
}

void MeshOperations::removeSmallObjects(vtkSmartPointer<vtkActor> actor, double ratio )
{
	vtkSmartPointer<vtkPolyData> mesh = (vtkPolyData*)actor->GetMapper()->GetInput();
	Consola::appendln("Removiendo objetos de dimension acotada");

    vtkSmartPointer<vtkPolyDataConnectivityFilter> connectivityFilter = vtkSmartPointer<vtkPolyDataConnectivityFilter>::New();
    connectivityFilter->SetInputData( mesh );
    connectivityFilter->SetExtractionModeToAllRegions();
	vtkSmartPointer<vtkCallbackCommand> m_progressCallback = CallbacksClass::getProgressCallBack();

	CallbacksClass::setEncabezado("Analizando objeto: ");
	connectivityFilter->AddObserver(vtkCommand::ProgressEvent, m_progressCallback);
	Consola::appendln("Etapa 1 de 2: Analizando objeto");
    connectivityFilter->Update();

    // remove objects consisting of less than ratio vertexes of the biggest object
    vtkIdTypeArray* regionSizes = connectivityFilter->GetRegionSizes();

    // find object with most vertices
    long maxSize = 0;
    for( int regions = 0; regions < connectivityFilter->GetNumberOfExtractedRegions(); regions++ )
        if( regionSizes->GetValue(regions) > maxSize )
            maxSize = regionSizes->GetValue(regions);


    // append regions of sizes over the threshold
    connectivityFilter->SetExtractionModeToSpecifiedRegions();
    for( int regions = 0; regions < connectivityFilter->GetNumberOfExtractedRegions(); regions++ )
        if( regionSizes->GetValue(regions) > maxSize * ratio )
            connectivityFilter->AddSpecifiedRegion(regions);

	Consola::appendln("Etapa 2 de 2: Removiendo objetos");

	CallbacksClass::setEncabezado("Removiendo objetos: ");
    connectivityFilter->Update();

	vtkSmartPointer<vtkPolyDataMapper> polyMapper = (vtkPolyDataMapper*)actor->GetMapper();

	MasterHandler::GetThread()->lock_Actor();
	emit MasterHandler::GetSimpleView()->signal_RemoveActor(actor);

	MasterHandler::GetThread()->lock_Actor();
    mesh->DeepCopy( connectivityFilter->GetOutput() );

	polyMapper->SetInputData(mesh);
	polyMapper->ScalarVisibilityOff();
	actor->SetMapper(polyMapper);

	MasterHandler::GetSurfaceInteractor()->AddActor(actor);

	MasterHandler::GetThread()->unlock_Actor();
	Consola::appendln("Removiendo objetos de dimension acotada: Finalizado");
}

//Todo: Understand FeatureAngle and RelaxationFactor. Then add it as argument.
void MeshOperations::smoothMesh( vtkSmartPointer<vtkActor> actor, unsigned int iterations )
{

	vtkSmartPointer<vtkPolyData> mesh = (vtkPolyData*)actor->GetMapper()->GetInput();
    //cout << "Mesh smoothing with " << nbrOfSmoothingIterations << " iterations." << endl;
	Consola::appendln("Suavizando objeto");

    vtkSmartPointer<vtkSmoothPolyDataFilter> smoother = vtkSmartPointer<vtkSmoothPolyDataFilter>::New();
    smoother->SetInputData( mesh );
    smoother->SetNumberOfIterations( int(iterations) );
    smoother->SetFeatureAngle(45);
    smoother->SetRelaxationFactor(0.05);

	vtkSmartPointer<vtkCallbackCommand> m_progressCallback = CallbacksClass::getProgressCallBack();

    {
		CallbacksClass::setEncabezado("Suavizando objeto: ");
        smoother->AddObserver(vtkCommand::ProgressEvent, m_progressCallback);
    }
    smoother->Update();
	
	vtkSmartPointer<vtkPolyDataMapper> polyMapper = (vtkPolyDataMapper*)actor->GetMapper();

	//lock mutex
	MasterHandler::GetThread()->lock_Actor();
	emit MasterHandler::GetSimpleView()->signal_RemoveActor(actor);

	//lock mutex
	MasterHandler::GetThread()->lock_Actor();
    mesh->DeepCopy( smoother->GetOutput() );

	polyMapper->SetInputData(mesh);

	polyMapper->ScalarVisibilityOff();

	actor->SetMapper(polyMapper);

	MasterHandler::GetSurfaceInteractor()->AddActor(actor);

	//unlock mutex
	MasterHandler::GetThread()->unlock_Actor();
	Consola::appendln("Suavizando objeto: Finalizado");
}

