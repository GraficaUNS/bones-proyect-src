#include "Paciente.h"
#include <QtWidgets/QTextBrowser>
#include <QTextDocument>
#include <QScrollBar>
#include "Consola.h"

Paciente::Paciente() {

}


void Paciente::setPatient(vtkDICOMValue StudyUID, vtkDICOMValue name, vtkDICOMValue age, vtkDICOMValue id, vtkDICOMValue birthdate, vtkDICOMValue birthname,
	vtkDICOMValue birthtime, vtkDICOMValue address, vtkDICOMValue sexo, vtkDICOMValue comments,
	vtkDICOMValue size, vtkDICOMValue weight, vtkDICOMValue studyID, vtkDICOMValue description, vtkDICOMValue studydate, vtkDICOMValue studytime,
	vtkDICOMValue accessionnumber, vtkDICOMValue referringphysician, vtkDICOMValue performingphysicianname, vtkDICOMValue modality,
	vtkDICOMValue additionalpatienthistory, vtkDICOMValue allergies, vtkDICOMValue currentpatientlocation,
	vtkDICOMValue modalitiesinstudy, vtkDICOMValue institutionname, vtkDICOMValue personname,
	vtkDICOMValue persontelephonenumbers) {

	StudyInstanceUID = StudyUID.AsString();
	PatientName = name.AsString();
	PatientAge = age.AsString();
	PatientID = id.AsString();
	PatientBirthDate = birthdate.AsString();
	PatientBirthName = birthname.AsString();
	PatientBirthTime = birthtime.AsString();
	PatientAddress = address.AsString();
	PatientSex = sexo.AsString();
	PatientComments = comments.AsString();
	PatientSize = size.AsString();
	PatientWeight = weight.AsString();
	StudyID = studyID.AsString();
	StudyDescription = description.AsString();
	StudyDate = studydate.AsString();
	StudyTime = studytime.AsString();
	AccessionNumber = accessionnumber.AsString();
	ReferringPhysician = referringphysician.AsString();
	PerformingPhysicianName = performingphysicianname.AsString();
	Modality = modality.AsString();
	AdditionalPatientHistory = additionalpatienthistory.AsString();
	Allergies = allergies.AsString();
	CurrentPatientLocation = currentpatientlocation.AsString();
	ModalitiesInStudy = modalitiesinstudy.AsString();
	InstitutionName = institutionname.AsString();
	PersonName = personname.AsString();
	PersonTelephoneNumbers = persontelephonenumbers.AsString();


}

void Paciente::setStudyInstanceUID() {}
void Paciente::setPatientName() {}
void Paciente::setPatientAge() {}
void Paciente::setPatientID() {}
void Paciente::setPatientBirthDate() {}
void Paciente::setPatientBirthName() {}
void Paciente::setPatientBirthTime() {}
void Paciente::setPatientAddress() {}
void Paciente::setPatientSex() {}
void Paciente::setPatientComments() {}
void Paciente::setPatientSize() {}
void Paciente::setPatientWeight() {}
void Paciente::setStudyID() {}
void Paciente::setStudyDescription() {}
void Paciente::setStudyDate() {}
void Paciente::setStudyTime() {}
void Paciente::setAccessionNumber() {}
void Paciente::setReferringPhysician() {}
void Paciente::setPerformingPhysicianName() {}
void Paciente::setModality() {}
void Paciente::setAdditionalPatientHistory() {}
void Paciente::setAllergies() {}
void Paciente::setCurrentPatientLocation() {}
void Paciente::setModalitiesInStudy() {}
void Paciente::setInstitutionName() {}
void Paciente::setPersonName() {}
void Paciente::setPersonTelephoneNumbers() {}

std::string Paciente::getStudyInstanceUID() { return StudyInstanceUID; }
std::string Paciente::getPatientName() { return PatientName; }
std::string Paciente::getPatientAge() { return PatientAge; }
std::string Paciente::getPatientID() { return PatientID; }
std::string Paciente::getPatientBirthDate() { return PatientBirthDate; }
std::string Paciente::getPatientBirthName() { return PatientBirthName; }
std::string Paciente::getPatientBirthTime() { return PatientBirthTime; }
std::string Paciente::getPatientAddress() { return PatientAddress; }
std::string Paciente::getPatientSex() { return PatientSex; }
std::string Paciente::getPatientComments() { return PatientComments; }
std::string Paciente::getPatientSize() { return PatientSize; }
std::string Paciente::getPatientWeight() { return PatientWeight; }
std::string Paciente::getStudyID() { return StudyID; }
std::string Paciente::getStudyDescription() { return StudyDescription; }
std::string Paciente::getStudyDate() { return StudyDate; }
std::string Paciente::getStudyTime() { return StudyTime; }
std::string Paciente::getAccessionNumber() { return AccessionNumber; }
std::string Paciente::getReferringPhysician() { return ReferringPhysician; }
std::string Paciente::getPerformingPhysicianName() { return PerformingPhysicianName; }
std::string Paciente::getModality() { return Modality; }
std::string Paciente::getAdditionalPatientHistory() { return AdditionalPatientHistory; }
std::string Paciente::getAllergies() { return Allergies; }
std::string Paciente::getCurrentPatientLocation() { return CurrentPatientLocation; }
std::string Paciente::getModalitiesInStudy() { return ModalitiesInStudy; }
std::string Paciente::getInstitutionName() { return InstitutionName; }
std::string Paciente::getPersonName() { return PersonName; }
std::string Paciente::getPersonTelephoneNumbers() { return PersonTelephoneNumbers; }



