#pragma once

#define Cons
#include <QtWidgets/QTextBrowser>
#include <QLabel>
#include <QString>
class Consola {
private:

public:
		
//implemento m�todos aca directamente, ni me caliento por el cpp

	static void setConsola(QTextBrowser * cons);
	static void setProgLabel(QLabel * label);

	static void append(double d);

	static void append(int i);

	static void append(char * string);

	static void append(std::string string);

	static void appendln(double d);

	static void appendln(int i);

	static void appendln(char * string);

	static void appendln(std::string string);

	static void appendln(QString string);

	static void clear();

	static void writeLabel(QString string);


};