#pragma once

#include <QDialog>
#include <QMutex>
#include <QString>
namespace Ui {
	class QualityBarWidget;
}



class QualityBarWidget : public QDialog
{
    Q_OBJECT
		
public:

	typedef QDialog Superclass;

    explicit QualityBarWidget(QWidget *parent = 0);
    ~QualityBarWidget();
	void setText(QString texto, QString high_text, QString mid_text, QString low_text, int h_value, int l_value, int default_value);

	void lock();
	void unlock();

	int getValue();

	bool isAccepted();

	void mostrar();

private slots:
	void accept() override;
	void reject() override;
	void handleSlider(int);
	void handleSpinBox(int);
	
private:
    Ui::QualityBarWidget *ui;
	
	QMutex qualityMutex;
	
	int lastValue;

	bool accepted;

	void checkWarning();

	int highValue = 60;
	int lowValue = 30;

	QString lowQuality;
	QString midQuality;
	QString highQuality;
};