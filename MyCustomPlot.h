#pragma once
#include "libs/qcustomplot/qcustomplot.h"
#include "QWidget.h"
#include <vtkVolume.h>
#include <vtkVolumeMapper.h>
#include "myVolume.h"
#include "SelectTransferFunctionDialog.h"

#define FT_BLOQUEADA "El trabajo actual esta utilizando la Funcion Transferencia.\nEspere a que se termine el trabajo."

class MyCustomPlot: public QCustomPlot
{
	Q_OBJECT
public:

	typedef QCustomPlot Superclass;

	MyCustomPlot(); 
	MyCustomPlot(QWidget* parent);
	~MyCustomPlot();

	void initOpacityFunction();
	
	void mousePressEvent(QMouseEvent *event) override;
	void mouseMoveEvent(QMouseEvent *event) override;
	void mouseReleaseEvent(QMouseEvent *event) override;

	void addCkeckBox_opacity(QCheckBox* check);
	void addCkeckBox_gradient(QCheckBox* check);
	void addCkeckBox_color(QCheckBox * check);
	void addActionRestore(QPushButton* boton);
	void addActionSelectTransferFunction(QPushButton* boton);
	void addComboBoxBlendModes(QComboBox* boton);

	void setVolume(vtkSmartPointer<myVolume> newVolume);

	void setIlluminationSpins(QDoubleSpinBox* AmbientSpin, QDoubleSpinBox* DiffuseSpin, QDoubleSpinBox* SpecularSpin, QDoubleSpinBox* SpecularPowerSpin);

public slots:

	virtual void slot_SetAmbient(double d);
	virtual void slot_SetDiffuse(double d);
	virtual void slot_SetSpecular(double d);
	virtual void slot_SetSpecularPower(double d);
	virtual void slot_ShowSelectTransferFunction();
	virtual void slotRestoreDefault();
	virtual void slot_SetBlendMode(int index);

private:
	vtkSmartPointer<myVolume> vol;

	QCPGraph *opacity_graph;
	QCPGraph *gradient_graph;
	QCPGraph *colorActiveTriangles[CANT_COLOR];

	QCPBars* colorBars[CANT_COLOR];

	QCheckBox* grad_check;
	QCheckBox* opac_check;
	QCheckBox* color_check;

	QDoubleSpinBox * AmbientSpin;
	QDoubleSpinBox * DiffuseSpin;
	QDoubleSpinBox * SpecularSpin;
	QDoubleSpinBox * SpecularPowerSpin;

	int index_op = -1;
	int oldIndex_op = -1;
	int index_gra = -1;
	int oldIndex_gra = -1;
	int indexColor = -1;
	//int oldIndexColor = -1;
	int oldColor[3];

	int copyColor[3];

	bool copyColorFlag = false;

	bool changeVolume;

	SelectTransferFunctionDialog* ventanaTransferFunction;

	QComboBox* BlendComboBox;

	int BlendModes[6] = {
		vtkVolumeMapper::COMPOSITE_BLEND,
		vtkVolumeMapper::MAXIMUM_INTENSITY_BLEND,
		vtkVolumeMapper::MINIMUM_INTENSITY_BLEND,
		vtkVolumeMapper::AVERAGE_INTENSITY_BLEND,
		vtkVolumeMapper::ADDITIVE_BLEND,
		vtkVolumeMapper::ISOSURFACE_BLEND
	};
};

