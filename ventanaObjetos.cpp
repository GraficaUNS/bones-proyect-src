#include "ventanaObjetos.h"
#include "ui_ventanaObjetos.h"
#include "QStringListModel.h"
#include "MasterHandler.h"
#include "SimpleView.h"

ventanaObjetos::ventanaObjetos(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ventanaObjetos)
{
    ui->setupUi(this);
	QStringListModel * model = new QStringListModel(this);
	QStringList list;
	list << "plano" << "a�adir nuevo objeto";
	model->setStringList(list);
	ui->listView->setModel(model);
}

ventanaObjetos::~ventanaObjetos()
{
    delete ui;
}

void ventanaObjetos::on_botonCerrar_clicked()
{
	this->close();
}

void ventanaObjetos::on_botonAgregar_clicked()
{
	QModelIndex index = ui->listView->currentIndex();
	QString itemText = index.data(Qt::DisplayRole).toString();
	std::string texto = itemText.toStdString();
	if (texto._Equal("plano")) {
		MasterHandler::mostrarPlano();
	}
	if (texto._Equal("a�adir nuevo objeto")) {
		//MasterHandler::getSimpleView().slotOpenFile();
		//SimpleView::slotOpenFile();
	}
	
}