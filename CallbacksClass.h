#pragma once

#include <vtkObject.h>
#include <vtkCallbackCommand.h>
#include <vtkSmartPointer.h>
#include <QString>
class CallbacksClass
{
public:
	CallbacksClass();
	~CallbacksClass();

	static void setProgressCallBack(void(*f)(vtkObject* caller, long unsigned int, void*, void*));
	static vtkSmartPointer<vtkCallbackCommand> getProgressCallBack();

	static void setEncabezado(QString s);
	static QString getEncabezado();

	static void lockProgressBar();
	static void unlockProgressBar();

};

