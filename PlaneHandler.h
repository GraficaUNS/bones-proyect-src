#pragma once

#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRegularPolygonSource.h>
#include <vtkSmartPointer.h>


class PlaneHandler
{
public:
	PlaneHandler();
	~PlaneHandler();
	void mostrarPlano();
	vtkSmartPointer<vtkRegularPolygonSource> getPlane();
	vtkSmartPointer<vtkRenderWindow> GetRenderWindow();
	void SetRenderWindow(vtkSmartPointer<vtkRenderWindow> ren);
	vtkSmartPointer<vtkActor> GetActor();
	void quitarPlano();

private:
	void inicializarPlano();
	vtkSmartPointer<vtkRenderWindow> renderWindow;
	
};

