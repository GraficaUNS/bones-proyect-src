#include "NoteInteractions.h"

#include <vtkActor.h>
#include <vtkNew.h>
#include <vtkCollection.h>
#include <vtkSmartPointer.h>
#include <vtkMatrix4x4.h>
#include <QKeyEvent>
#include <vtkRenderWindow.h>
#include <vtkPropPicker.h>
#include <vtkRendererCollection.h>
#include <vtkRenderer.h>
#include <vtkProperty.h>
#include <vtkMapper.h>
#include <vtkPolyDataMapper.h>
#include <vtkAppendPolyData.h>
#include <vtkTransformFilter.h>
#include <vtkTransform.h>

#include <vtkSphereWidget.h>
#include <vtkWidgetEvent.h>

#include <QToolTip>
#include <math.h>
#include "CameraHandler.h"
#include "Consola.h"
#include "CallbacksClass.h"
#include "Dialog.h"
#include "MasterHandler.h"
#include <vtkSphereSource.h>
#include "ThreadHandler.h"
#include "InputManager.h"
#include <QString>

#include <vtkTextSource.h>

#include <vtkFollower.h>

#define OFFSET 2

textoNota * popup;
//selected actor and color
vtkSmartPointer<vtkActor > lastPickedNota = nullptr;

vtkNew<vtkCollection> listaNotas;
vtkSmartPointer<vtkSphereWidget> nota;

//Matrices
//vtkSmartPointer<vtkMatrix4x4> notaMeshMatrix_n;
//vtkSmartPointer<vtkMatrix4x4> WordMatrix_n;
//vtkSmartPointer<vtkMatrix4x4> WordNormalMatrix_n;
//vtkSmartPointer<vtkMatrix4x4> WordNormalMatrix2_n;


vtkSmartPointer<vtkRenderWindow> renderWindow_n;

//flags

bool flagCrearNota = 0;

QMap<vtkSmartPointer<vtkSphereWidget>, QString> NoteToText;
QMap<vtkSmartPointer<vtkSphereWidget>, vtkSmartPointer<vtkTextSource>> NoteToTitle;
QMap<vtkSmartPointer<vtkSphereWidget>, vtkSmartPointer<vtkFollower>> NoteToFollower;

vtkSmartPointer<vtkActor> planoXY;
vtkSmartPointer<vtkActor> planoYZ;
vtkSmartPointer<vtkActor> planoZX;

vtkSmartPointer<vtkSphereWidget> selectedNote = nullptr;
bool thereIsSelected = false;


vtkSmartPointer<vtkCallbackCommand> noteSelectCallback;



bool scaling = false;

ThreadHandler* noteScallingThread;


double t = 3.1415926535* 3.0 / 4.0;

void functionSelectedNoteCallBack(vtkObject* caller, long unsigned int eventId, void* /*clientData*/, void* /*callData*/)
{
	vtkSmartPointer<vtkSphereWidget> note = (vtkSphereWidget*)caller;

	vtkSmartPointer<vtkTextSource> title = NoteToTitle.value(note);

	note->GetCenter();

	vtkSmartPointer<vtkFollower> follow = NoteToFollower.value(note);

	double* bnds = follow->GetBounds();

	follow->SetPosition(note->GetCenter()[0] - (bnds[1] - bnds[0])/2, note->GetCenter()[1] + OFFSET, note->GetCenter()[2]);
	
	emit MasterHandler::GetSimpleView()->signal_renderizar();
};

NoteInteractions::NoteInteractions()
{

	noteScallingThread = new ThreadHandler();
	noteScallingThread->set_Scale_Thread();
	noteScallingThread->init();


	popup = new textoNota();
	connect(popup, SIGNAL(enviarDatosPopUp(QStringList)), this, SLOT(recibirDatosPopUp(QStringList)));

	{
		vtkSmartPointer<vtkRegularPolygonSource> plane1 = vtkSmartPointer<vtkRegularPolygonSource>::New();
		plane1->SetNumberOfSides(4);
		plane1->SetRadius(5000);
		plane1->SetCenter(0, 0, 0);
		plane1->SetNormal(1.0, 0.0, 0.0);
		plane1->Update();
		vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
		mapper->SetInputConnection(plane1->GetOutputPort());
		planoYZ = vtkSmartPointer<vtkActor>::New();
		planoYZ->SetMapper(mapper);
		planoYZ->GetProperty()->SetColor(0, 0, 0);
		planoYZ->GetProperty()->SetOpacity(0.01);
	}
	{
		vtkSmartPointer<vtkRegularPolygonSource> plane1 = vtkSmartPointer<vtkRegularPolygonSource>::New();
		plane1->SetNumberOfSides(4);
		plane1->SetRadius(5000);
		plane1->SetCenter(0, 0, 0);
		plane1->SetNormal(0.0, 1.0, 0.0);
		plane1->Update();
		vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
		mapper->SetInputConnection(plane1->GetOutputPort());
		planoZX = vtkSmartPointer<vtkActor>::New();
		planoZX->SetMapper(mapper);

		planoZX->GetProperty()->SetColor(0, 0, 0);
		planoZX->GetProperty()->SetOpacity(0.01);
	}
	{
		vtkSmartPointer<vtkRegularPolygonSource> plane1 = vtkSmartPointer<vtkRegularPolygonSource>::New();
		plane1->SetNumberOfSides(4);
		plane1->SetRadius(5000);
		plane1->SetCenter(0, 0, 0);
		plane1->SetNormal(0.0, 0.0, 1.0);
		plane1->Update();
		vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
		mapper->SetInputConnection(plane1->GetOutputPort());
		planoXY = vtkSmartPointer<vtkActor>::New();
		planoXY->SetMapper(mapper);

		planoXY->GetProperty()->SetColor(0, 0, 0);
		planoXY->GetProperty()->SetOpacity(0.01);
	}

	noteSelectCallback = vtkSmartPointer<vtkCallbackCommand>::New();
	noteSelectCallback->SetCallback(functionSelectedNoteCallBack);

}


NoteInteractions::~NoteInteractions()
{
}

void NoteInteractions::keyPress() {



	if (InputManager::IsKeyPressed( Qt::Key_Delete)) {
		//simpleview->slotEliminar();
			//GetRenderWindow()->GetRenderers()->GetFirstRenderer()->RemoveActor(lastPickedNota);
			if (selectedNote != nullptr) {
				vtkSmartPointer<vtkFollower> title = NoteToFollower.value(selectedNote);
				selectedNote->Off();
				selectedNote = nullptr;
				GetRenderWindow()->GetRenderers()->GetFirstRenderer()->RemoveActor(title);
				GetRenderWindow()->Render();
			}

	}

}




vtkSmartPointer<vtkRenderWindow> NoteInteractions::GetRenderWindow() {
	return renderWindow_n;
}


void  NoteInteractions::SetRenderWindow(vtkSmartPointer<vtkRenderWindow> ren) {
	renderWindow_n = ren;
}

void NoteInteractions::setSelectedNoteToNull()
{
	lastPickedNota = nullptr;
}

vtkSmartPointer<vtkActor> NoteInteractions::getSelectedNote()
{
	return lastPickedNota;
}

double distanceBetweenPoints(double* point1, double* point2) {
	return sqrt((point1[0] - point2[0])*(point1[0] - point2[0]) + (point1[1] - point2[1])*(point1[1] - point2[1]) + (point1[2] - point2[2])*(point1[2] - point2[2]));
}


void NoteInteractions::checkPickedNote(CameraHandler* camera, QMouseEvent *event, double height, double width) {

	int* point = camera->GetRenderWindowInteractor()->GetEventPosition();	
	
	if (flagCrearNota == 1) {

		nota->EnabledOn();

		flagCrearNota = 0;

		GetRenderWindow()->SetCurrentCursor(Qt::ArrowCursor);

		nota->SetCurrentRenderer(GetRenderWindow()->GetRenderers()->GetFirstRenderer());

		GetRenderWindow()->GetInteractor()->GetPicker()->Pick(point[0], point[1], 0, GetRenderWindow()->GetRenderers()->GetFirstRenderer());

		double picked[3];
		GetRenderWindow()->GetInteractor()->GetPicker()->GetPickPosition(picked);

		nota->SetCenter(picked);
		selectedNote = nota;
		GetRenderWindow()->GetRenderers()->GetFirstRenderer()->RemoveActor(planoXY);
		GetRenderWindow()->GetRenderers()->GetFirstRenderer()->RemoveActor(planoYZ);
		GetRenderWindow()->GetRenderers()->GetFirstRenderer()->RemoveActor(planoZX);


		vtkSmartPointer<vtkTextSource> title = vtkSmartPointer<vtkTextSource>::New();

		title->SetText("");

		vtkSmartPointer<vtkPolyDataMapper> mapper =
			vtkSmartPointer<vtkPolyDataMapper>::New();
		mapper->SetInputConnection(title->GetOutputPort());

		// Create a subclass of vtkSmartPointer<vtkActor: a vtkSmartPointer<vtkFollower that remains facing the camera
		vtkSmartPointer<vtkFollower> follower = vtkSmartPointer<vtkFollower>::New();
		follower->SetMapper(mapper);

		follower->SetCamera(GetRenderWindow()->GetRenderers()->GetFirstRenderer()->GetActiveCamera());

		follower->SetScale(0.3);

		follower->SetPosition(picked[0], picked[1] + OFFSET, picked[2]);

		

		GetRenderWindow()->GetRenderers()->GetFirstRenderer()->AddActor(follower);

		NoteToTitle.insert(nota, title);

		NoteToFollower.insert(nota, follower);

		GetRenderWindow()->Render();

		selectedNote = nota;

		listaNotas->AddItem(nota);

		popup->appendText("");
		popup->show();


	}
	else {
		
		GetRenderWindow()->GetInteractor()->GetPicker()->Pick(point[0], point[1], 0,
			GetRenderWindow()->GetRenderers()->GetFirstRenderer()
		);
		double picked[3];
		GetRenderWindow()->GetInteractor()->GetPicker()->GetPickPosition(picked);

		bool encontre = false;
		for (int i = 0; i < listaNotas->GetNumberOfItems(); i++) 
		{
			vtkSmartPointer<vtkSphereWidget> note = (vtkSphereWidget*)listaNotas->GetItemAsObject(i);

			if (distanceBetweenPoints(note->GetCenter(), picked) < 30) 
			{
				if (selectedNote != nullptr) {
					selectedNote->SetRadius(MIN_RADIUS);
				}
				if (selectedNote != note) {
					t = 3.1415926535* 3.0 / 4.0;
				}
				selectedNote = note;
				
				encontre = true;
				break;
			}
		}

		if (encontre) 
		{
			//selectedNote->SetRadius(25);
			if (event->button() == Qt::LeftButton) 
			{
				
				if (NoteToText.value(selectedNote) != nullptr)
				{
					//Consola::appendln("");
					QToolTip::showText(event->globalPos(), NoteToText.value(selectedNote),
						MasterHandler::GetSimpleView()->OpenGLWidget);

					//Consola::appendln(mapeoNotas.find(selectedNote)->second);
				}
				
			}
			else if (event->button() == Qt::RightButton) {
				popup->appendText(NoteToText.value(selectedNote));
				popup->show();
			}
			scaling = true;
		}
		else 
		{
			if (selectedNote != nullptr) {
					selectedNote->SetRadius(MIN_RADIUS);
					selectedNote = nullptr;
					scaling = false;
				}
		}
		
	}
}

void NoteInteractions::crearNota() {
	
	GetRenderWindow()->SetCurrentCursor(Qt::PointingHandCursor);
	
	//renderWindow_n->SetCurrentCursor(Qt::PointingHandCursor);
	nota = vtkSmartPointer<vtkSphereWidget>::New();
	nota->SetInteractor(GetRenderWindow()->GetInteractor());

	nota->SetScale(true);
	nota->SetRadius(MIN_RADIUS);
	nota->TranslationOn();
	//nota->SetRepresentationToSurface();
	nota->SetThetaResolution(25);
	nota->SetThetaResolution(50);
	nota->AddObserver(vtkCommand::StartInteractionEvent, noteSelectCallback);
	nota->AddObserver(vtkCommand::InteractionEvent, noteSelectCallback);
	nota->AddObserver(vtkCommand::RightButtonPressEvent, noteSelectCallback);
	nota->AddObserver(vtkWidgetEvent::Move, noteSelectCallback);
	nota->AddObserver(vtkWidgetEvent::EndSelect, noteSelectCallback);
	nota->AddObserver(vtkCommand::EndInteractionEvent, noteSelectCallback);
	nota->AddObserver(vtkCommand::MouseMoveEvent, noteSelectCallback);
	

	GetRenderWindow()->Render();

	GetRenderWindow()->GetRenderers()->GetFirstRenderer()->AddActor(planoXY);
	GetRenderWindow()->GetRenderers()->GetFirstRenderer()->AddActor(planoYZ);
	GetRenderWindow()->GetRenderers()->GetFirstRenderer()->AddActor(planoZX);

	flagCrearNota = 1;
}

void NoteInteractions::AddNote(double * pos, QString text)
{
	crearNota();
	flagCrearNota = 0;

	vtkSmartPointer<vtkTextSource> title = vtkSmartPointer<vtkTextSource>::New();

	title->SetText("");

	vtkSmartPointer<vtkPolyDataMapper> mapper =
		vtkSmartPointer<vtkPolyDataMapper>::New();
	mapper->SetInputConnection(title->GetOutputPort());

	// Create a subclass of vtkSmartPointer<vtkActor: a vtkSmartPointer<vtkFollower that remains facing the camera
	vtkSmartPointer<vtkFollower> follower = vtkSmartPointer<vtkFollower>::New();
	follower->SetMapper(mapper);

	follower->SetCamera(GetRenderWindow()->GetRenderers()->GetFirstRenderer()->GetActiveCamera());

	follower->SetScale(0.3);

	GetRenderWindow()->GetRenderers()->GetFirstRenderer()->AddActor(follower);

	NoteToTitle.insert(nota, title);

	NoteToFollower.insert(nota, follower);

	GetRenderWindow()->Render();

	selectedNote = nota;

	listaNotas->AddItem(nota);

	nota->SetCenter(pos);

	nota->EnabledOn();

	QStringList lista(text);

	recibirDatosPopUp(lista);
}

QMap<vtkSmartPointer<vtkSphereWidget>, QString>* NoteInteractions::GetNotes()
{
	return &NoteToText;
}

void NoteInteractions::recibirDatosPopUp(QStringList texto) {
	QString text = texto[0];

	NoteToText.insert(selectedNote, text);
	
	vtkSmartPointer<vtkTextSource> title = NoteToTitle.value(selectedNote);

	QStringList aux1 = text.split(" ");

	QStringList aux2 = aux1[0].split("\n");

	QString aux = aux2[0];
	if (aux2.size() == 1) {
		if (aux1.size() > 1)
			aux += QString(" ") + aux1[1].split("\n")[0];
		if (aux1.size() > 2)
			aux += QString(" ") + aux1[2].split("\n")[0];
	}

	title->SetText(aux.toStdString().data());

	vtkSmartPointer<vtkFollower> follow = NoteToFollower.value(selectedNote);

	double* bnds = follow->GetBounds();

	follow->SetPosition(selectedNote->GetCenter()[0] - (bnds[1] - bnds[0]) / 2, selectedNote->GetCenter()[1] + OFFSET, selectedNote->GetCenter()[2]);

	GetRenderWindow()->Render();
}

void NoteInteractions::removeSelectedNote() {
	
}

void NoteInteractions::mouseMove(CameraHandler * camera, QMouseEvent * event)
{
	
	
}

void NoteInteractions::rotate()
{
	
	if (selectedNote != nullptr) {

		selectedNote->SetRadius(MIN_RADIUS + 5*(sin(t)+1));
		t += 0.7;
		emit MasterHandler::GetSimpleView()->signal_renderizar();
	}
	else {
		t = 3.1415926535* 3.0 / 4.0;
	}
}


