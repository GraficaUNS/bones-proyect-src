#include "IOSurface.h"
#include "Consola.h"
#include "CallbacksClass.h"

#include <vtkSmartPointer.h>
#include <vtkPolyData.h>
#include <vtkImageData.h>
#include <vtkVector.h>
#include <vtkCallbackCommand.h>

#include <vtkSTLWriter.h>
#include <vtkPLYWriter.h>
#include <vtkMath.h>
#include <vtkOBJReader.h>
#include <vtkSTLReader.h>
#include <vtkPLYReader.h>
#include <vtkTypedArray.h>
#include <vtkIdTypeArray.h>
#include <vtkIdList.h>
#include <vtkMapper.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>

#include <iostream>
#include <fstream>
#include "MasterHandler.h"
#include "SimpleView.h"



using namespace std;

IOSurface::IOSurface() 
{
	
}


IOSurface::~IOSurface()
{
}


vtkSmartPointer<vtkPolyData> IOSurface::loadInputData(bool& successful, std::string pathToInputData)
{
	vtkSmartPointer<vtkPolyData> mesh;
	bool loadObj = false; bool loadStl = false; bool loadPly = false;

	// check if input is a mesh file
	std::string::size_type idx = pathToInputData.rfind('.');
	if (idx != std::string::npos)
	{
		std::string extension = pathToInputData.substr(idx + 1);
		loadObj = extension == "obj";
		loadStl = extension == "stl";
		loadPly = extension == "ply";
	}

	//std::shared_ptr<vtkSmartPointer<vtkMeshRoutines> vmr = std::shared_ptr<vtkSmartPointer<vtkMeshRoutines>(new vtkSmartPointer<vtkMeshRoutines());
	//vmr->SetProgressCallback(m_vtkSmartPointer<vtkCallback);

	if (loadObj)
	{
		mesh = importObjFile(pathToInputData);
		successful = true;
	}
	else if (loadStl)
	{
		mesh = importStlFile(pathToInputData);
		successful = true;
	}
	else if (loadPly)
	{
		mesh = importPlyFile(pathToInputData);
		successful = true;
	}
	else
	{
		
		successful = false;
		
	}


	return mesh;
}

void IOSurface::exportObject(std::string direccion, vtkSmartPointer<vtkActor> actor) {
	if (actor != nullptr) {
		vtkSmartPointer<vtkPolyData> meshToSave = vtkSmartPointer<vtkPolyData>::New();
		meshToSave->DeepCopy(actor->GetMapper()->GetInput());

		std::string::size_type idx = direccion.rfind('.');
		if (idx != std::string::npos)
		{
			std::string extension = direccion.substr(idx + 1);

			if (extension == "obj")
				exportAsObjFile(meshToSave, direccion);
			else if (extension == "stl")
				exportAsStlFile(meshToSave, direccion);
			else if (extension == "ply")
				exportAsPlyFile(meshToSave, direccion);

		}
		else
		{
			//cerr << "No Filename." << endl;
		}
	}
}

void IOSurface::computeVertexNormalsTrivial(vtkSmartPointer<vtkPolyData> mesh, std::vector<vtkVector3d> &normals)
{
	vtkSmartPointer<vtkPoints> vertices = mesh->GetPoints();
	vtkSmartPointer<vtkDataArray> verticesArray = vertices->GetData();

	vtkIdType numberOfVertices = vertices->GetNumberOfPoints();
	vtkIdType numberOfFaces = mesh->GetNumberOfCells();

	for (vtkIdType i = 0; i < numberOfVertices; i++) {
		normals.push_back(vtkVector3d(1, 0, 0));
	}

	long long recorridas = numberOfVertices;
	long long numCmp = numberOfVertices * 2 + numberOfFaces * 2;

	// Go through all faces, compute face normal fn and set fn to all participating vertices.
	// The last computed fn of a vertex overwrites those before... that's why it is 'trivial' :)
	for (vtkIdType i = 0; i < numberOfFaces; i++)
	{
		vtkSmartPointer<vtkIdList> face = vtkSmartPointer<vtkIdList>::New();
		mesh->GetCellPoints(i, face);
		vtkIdType v0Idx = face->GetId(0);
		vtkIdType v1Idx = face->GetId(1);
		vtkIdType v2Idx = face->GetId(2);

		vtkVector3d v0(verticesArray->GetComponent(v0Idx, 0), verticesArray->GetComponent(v0Idx, 1), verticesArray->GetComponent(v0Idx, 2));
		vtkVector3d v1(verticesArray->GetComponent(v1Idx, 0), verticesArray->GetComponent(v1Idx, 1), verticesArray->GetComponent(v1Idx, 2));
		vtkVector3d v2(verticesArray->GetComponent(v2Idx, 0), verticesArray->GetComponent(v2Idx, 1), verticesArray->GetComponent(v2Idx, 2));

		// compute normal
		vtkVector3d v0v1; vtkMath::Subtract(v0.GetData(), v1.GetData(), v0v1.GetData());
		vtkVector3d v0v2; vtkMath::Subtract(v0.GetData(), v2.GetData(), v0v2.GetData());

		vtkVector3d fn = v0v1.Cross(v0v2);
		fn.Normalize();

		normals.at(size_t(v0Idx)) = fn;
		normals.at(size_t(v1Idx)) = fn;
		normals.at(size_t(v2Idx)) = fn;

		recorridas++;
		emit MasterHandler::GetSimpleView()->progressChanged(recorridas*100.0f / (double)numCmp);
		emit MasterHandler::GetSimpleView()->signal_WriteLabel(QString(
			QString("Creando normales: ")
			+ QString::number((int)(recorridas*100.0f / (double)numCmp))
			+ QString("%")));
	}
}

void IOSurface::exportAsObjFile(vtkSmartPointer<vtkPolyData> mesh, std::string path)
{
	
	Consola::appendln("Exportando archivo: ");
	Consola::appendln(path);

	int buffer_size = 256;
	string objContent("");
	char* buffer = new char[buffer_size];

	objContent.append("#obj exporter \n");

	vtkSmartPointer<vtkPoints> vertices = mesh->GetPoints();
	vtkSmartPointer<vtkDataArray> verticesArray = vertices->GetData();

	vtkIdType numberOfVertices = vertices->GetNumberOfPoints();
	vtkIdType  numberOfFaces = mesh->GetNumberOfCells();

	long long numCmp = numberOfFaces*2 + numberOfVertices * 2;
	long long recorridas = 0;

	objContent.append("g default \n");

	emit MasterHandler::GetSimpleView()->visibleProgressBar(true);

	// wrote vertices
	for (vtkIdType i = 0; i < numberOfVertices; i++)
	{
		sprintf_s(buffer, buffer_size, "v %f %f %f \n",
			verticesArray->GetComponent(i, 0),
			verticesArray->GetComponent(i, 1),
			verticesArray->GetComponent(i, 2));
		objContent.append(buffer);

		recorridas++;
		emit MasterHandler::GetSimpleView()->progressChanged(recorridas*100.0f / (double)numCmp);
		emit MasterHandler::GetSimpleView()->signal_WriteLabel(QString(
			QString("Procesando vertices: ")
			+ QString::number((int)(recorridas*100.0f / (double)numCmp))
			+ QString("%")));
	}

	// compute normals and write
	vector<vtkVector3d> normals;
	IOSurface::computeVertexNormalsTrivial(mesh, normals);

	recorridas += numberOfFaces;

	for (vtkIdType i = 0; i < numberOfVertices; i++)
	{
		vtkVector3d n = normals.at(size_t(i));
		sprintf_s(buffer, buffer_size, "vn %f %f %f \n", n.GetX(), n.GetY(), n.GetZ());
		objContent.append(buffer);

		recorridas++;
		emit MasterHandler::GetSimpleView()->progressChanged(recorridas*100.0f / (double)numCmp);
		emit MasterHandler::GetSimpleView()->signal_WriteLabel(QString(
			QString("Procesando normales: ")
			+ QString::number((int)(recorridas*100.0f / (double)numCmp))
			+ QString("%")));
	}

	objContent.append("\n");

	//faces
	objContent.append("g polyDefault \n");
	objContent.append("s off \n");

	for (long long i = 0; i < numberOfFaces; i++)
	{


		vtkSmartPointer<vtkIdList> face = vtkSmartPointer<vtkIdList>::New();
		mesh->GetCellPoints(i, face);
		long long v0Idx = face->GetId(0); long long v1Idx = face->GetId(1); long long v2Idx = face->GetId(2);

		sprintf_s(buffer, buffer_size, "f %lld//%lld %lld//%lld %lld//%lld \n",
			v0Idx + 1, v0Idx + 1,
			v1Idx + 1, v1Idx + 1,
			v2Idx + 1, v2Idx + 1);

		objContent.append(buffer);

		recorridas++;
		emit MasterHandler::GetSimpleView()->progressChanged(recorridas*100.0f / (double)numCmp);
		emit MasterHandler::GetSimpleView()->signal_WriteLabel(QString(
			QString("Procesando caras: ") 
			+ QString::number((int)(recorridas*100.0f / (double)numCmp))
			+ QString("%")));

	}

	objContent.append("\n\n#end of obj file\n");

	// write the whole buffer to the file
	ofstream objFile;
	objFile.open(path, ios::out);

	emit MasterHandler::GetSimpleView()->signal_WriteLabel("Exportando");
	objFile.write(objContent.c_str(), long(objContent.length()));
	objFile.flush();


	emit MasterHandler::GetSimpleView()->visibleProgressBar(false);

	delete[] buffer;

	Consola::append("Exportando archivo: Finalizado");
	emit MasterHandler::GetSimpleView()->signal_WriteLabel("Finalizado");
}

void IOSurface::exportAsStlFile(vtkSmartPointer<vtkPolyData> mesh, string path)
{

	Consola::appendln("Exportando archivo: ");
	Consola::appendln(path);

	vtkSmartPointer<vtkSTLWriter> writer = vtkSmartPointer<vtkSTLWriter>::New();
	writer->SetFileName(path.c_str());
	writer->SetInputData(mesh);
	writer->SetFileTypeToASCII();

	vtkSmartPointer<vtkCallbackCommand> call = CallbacksClass::getProgressCallBack();
	//if (m_progressCallback.Get() != NULL)
	{
		CallbacksClass::setEncabezado("Exportando archivo: ");
		writer->AddObserver(vtkCommand::ProgressEvent, call);
	}
	writer->Write();

	Consola::appendln("Exportando archivo: Finalizado");
}

void IOSurface::exportAsPlyFile(vtkSmartPointer<vtkPolyData> mesh, string path)
{

	Consola::appendln("Exportando archivo: ");
	Consola::appendln(path);

	vtkSmartPointer<vtkPLYWriter> writer = vtkSmartPointer<vtkPLYWriter>::New();
	writer->SetFileName(path.c_str());
	writer->SetInputData(mesh);
	writer->SetFileTypeToASCII();

	vtkSmartPointer<vtkCallbackCommand> call = CallbacksClass::getProgressCallBack();

	//if (m_progressCallback.Get() != NULL)
	{
		CallbacksClass::setEncabezado("Exportando archivo: ");
		writer->AddObserver(vtkCommand::ProgressEvent, call);
	}
	writer->Write();

	Consola::appendln("Exportando archivo: Finalizado");
}

vtkSmartPointer<vtkActor> IOSurface::importObject(bool & load, std::string path) {
	//vtkSmartPointer<vtkNew<vtkSmartPointer<vtkClipClosedSurface> clipper;
	vtkSmartPointer<vtkActor> actorMesh = vtkSmartPointer<vtkActor>::New();
	vtkSmartPointer<vtkPolyDataMapper> mapperMesh = vtkSmartPointer<vtkPolyDataMapper>::New();

	vtkSmartPointer<vtkPolyData> mesh;


	mesh = loadInputData(load, path);

	//clipper->SetInputData(mesh);

	mapperMesh->SetInputData(mesh);

	mapperMesh->ScalarVisibilityOff();
	actorMesh->SetMapper(mapperMesh);
	actorMesh->SetScale(1);
	actorMesh->GetProperty()->SetColor(1, 1, 1);

	return actorMesh;
}

vtkSmartPointer<vtkPolyData> IOSurface::importObjFile(std::string pathToObjFile)
{
	Consola::appendln("Cargando archivo: ");
	Consola::appendln(pathToObjFile);
	

	vtkSmartPointer<vtkOBJReader> reader = vtkSmartPointer<vtkOBJReader>::New();
	reader->SetFileName(pathToObjFile.c_str());

	vtkSmartPointer<vtkCallbackCommand> call = CallbacksClass::getProgressCallBack();

	//if (call.Get() != NULL)
	{
		CallbacksClass::setEncabezado("Cargando archivo: ");
		reader->AddObserver(vtkCommand::ProgressEvent, call);
	}
	reader->Update();

	vtkSmartPointer<vtkPolyData> mesh = vtkSmartPointer<vtkPolyData>::New();
	mesh->DeepCopy(reader->GetOutput());

	//reader->Delete();
	Consola::appendln("Cargando archivo: Finalizado");
	return mesh;
}

vtkSmartPointer<vtkPolyData> IOSurface::importStlFile(std::string pathToStlFile)
{
	
	Consola::appendln("Cargando archivo:");
	Consola::appendln(pathToStlFile);
	

	vtkSmartPointer<vtkSTLReader> reader = vtkSmartPointer<vtkSTLReader>::New();
	reader->SetFileName(pathToStlFile.c_str());

	vtkSmartPointer<vtkCallbackCommand> call = CallbacksClass::getProgressCallBack();

	//if (m_progressCallback.Get() != NULL)
	{
		CallbacksClass::setEncabezado("Cargando archivo: ");
		reader->AddObserver(vtkCommand::ProgressEvent, call);
	}
	reader->Update();

	vtkSmartPointer<vtkPolyData> mesh = vtkSmartPointer<vtkPolyData>::New();
	mesh->DeepCopy(reader->GetOutput());

	Consola::appendln("Cargando archivo: Finalizado");
	return mesh;
}

vtkSmartPointer<vtkPolyData> IOSurface::importPlyFile(std::string pathToPlyFile)
{

	Consola::appendln("Cargando archivo:");
	Consola::appendln(pathToPlyFile);

	vtkSmartPointer<vtkPLYReader> reader = vtkSmartPointer<vtkPLYReader>::New();
	reader->SetFileName(pathToPlyFile.c_str());

	vtkSmartPointer<vtkCallbackCommand> call = CallbacksClass::getProgressCallBack();

	//if (m_progressCallback.Get() != NULL)
	{
		CallbacksClass::setEncabezado("Cargando archivo: ");
		reader->AddObserver(vtkCommand::ProgressEvent, call);
	}
	reader->Update();

	vtkSmartPointer<vtkPolyData> mesh = vtkSmartPointer<vtkPolyData>::New();
	mesh->DeepCopy(reader->GetOutput());

	Consola::appendln("Cargando archivo: Finalizado");
	return mesh;
}

