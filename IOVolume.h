#pragma once

#include <vtkSmartPointer.h>
#include <vtkPolyData.h>
#include <vtkImageData.h>
#include <vtkVector.h>
#include <vtkPolyData.h>
#include <vtkCallbackCommand.h>
#include <string>
#include <myVolume.h>


class IOVolume
{

public:
	IOVolume();
	~IOVolume();


	/**
	* Export the mesh in STL format.
	* @param mesh Mesh to export.
	* @param path Path to the exported stl file.
	*/
	void exportTransferenceFunction(QString path, vtkSmartPointer<myVolume> volumen);
	void importTransferenceFunction(QString path, vtkSmartPointer<myVolume> volumen);

};

