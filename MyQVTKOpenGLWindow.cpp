#include <QVTKOpenGLWindow.h>
#include "MyQVTKOpenGLWindow.h"

#include <iostream>
#include <QtWidgets/QSplitter>
#include <QKeyEvent>

#define NULL 0

#include <QCoreApplication.h>

#include "Consola.h"
#include "CameraHandler.h"
#include "MasterHandler.h"
#include "InputManager.h"


MyQVTKOpenGLWindow::~MyQVTKOpenGLWindow()
{

}

MyQVTKOpenGLWindow::MyQVTKOpenGLWindow()
	: MyQVTKOpenGLWindow(nullptr, QOpenGLContext::currentContext(), NoPartialUpdate, Q_NULLPTR)
{

	this->camera = new CameraHandler();
	this->camera->setOpenGLWindow(this);

}

//-----------------------------------------------------------------------------
MyQVTKOpenGLWindow::MyQVTKOpenGLWindow(QOpenGLContext *shareContext,
	UpdateBehavior updateBehavior, QWindow *parent)
	: QVTKOpenGLWindow(nullptr, shareContext, updateBehavior, parent)
{
	this->camera = new CameraHandler();
	this->camera->setOpenGLWindow(this);

}

//-----------------------------------------------------------------------------
MyQVTKOpenGLWindow::MyQVTKOpenGLWindow(vtkGenericOpenGLRenderWindow* w,
	QOpenGLContext *shareContext, UpdateBehavior updateBehavior, QWindow *parent)
	: QVTKOpenGLWindow(w, shareContext, updateBehavior, parent)

{
	this->camera = new CameraHandler();
	this->camera->setOpenGLWindow(this);
}

//-----------------------------------------------------------------------------


void MyQVTKOpenGLWindow::SetRenderWindow(vtkGenericOpenGLRenderWindow * ren)
{

	Superclass::SetRenderWindow(ren);
	this->camera->SetRenderWindow(ren);
}


//Oyentes del Mouse
void MyQVTKOpenGLWindow::mousePressEvent(QMouseEvent *event)
{

	{
		if (event->button() == Qt::RightButton) {

			double height = MyQVTKOpenGLWindow::height();
			double width = MyQVTKOpenGLWindow::width();
			MasterHandler::mousePressEvent(event, height, width);

			//MasterHandler::checkPickedActor();

			QMouseEvent* mEvt = new QMouseEvent(QEvent::MouseButtonPress, event->pos(), Qt::MidButton, Qt::MidButton, nullptr);

			Superclass::mousePressEvent(mEvt);

		}
		else if (camera->canMouseMove() && event->button() == Qt::LeftButton) {

			double height = MyQVTKOpenGLWindow::height();
			double width = MyQVTKOpenGLWindow::width();
			MasterHandler::mousePressEvent(event, height, width);

			//MasterHandler::checkPickedActor();

			Superclass::mousePressEvent(event);
		}
	}

	/**/
}

void MyQVTKOpenGLWindow::mouseMoveEvent(QMouseEvent *event)
{

	camera->mouseMoveEvent(event);

	{
		double height = MyQVTKOpenGLWindow::height();
		double width = MyQVTKOpenGLWindow::width();

		MasterHandler::mouseMoveEvent(event, height, width);

		{

			Superclass::mouseMoveEvent(event);
		}

	}

	camera->clearPlaneMove();

}

void MyQVTKOpenGLWindow::mouseReleaseEvent(QMouseEvent *event)
{

	double height = MyQVTKOpenGLWindow::height();
	double width = MyQVTKOpenGLWindow::width();

	MasterHandler::mouseReleaseEvent(event, height, width);

	if (event->button() == Qt::RightButton) {

		QMouseEvent* mEvt = new QMouseEvent(QEvent::MouseButtonRelease, event->pos(), Qt::MidButton, Qt::MidButton, nullptr);

		Superclass::mouseReleaseEvent(mEvt);

	}
	else {
		Superclass::mouseReleaseEvent(event);
	}

}

void MyQVTKOpenGLWindow::wheelEvent(QWheelEvent* e)
{
	if (InputManager::IsKeyPressed(Qt::Key::Key_Control))
	{
		this->camera->wheelEvent(e);
	}
	else
	{
		Superclass::wheelEvent(e);
	}

}


//Oyentes del teclado
void MyQVTKOpenGLWindow::keyPress(QKeyEvent *event)
{
	
	bool control = InputManager::IsKeyPressed(Qt::Key::Key_Control);
	//ZOOM a la CAMARA
	//Control + ... acercar
	if ((event->key() == Qt::Key_Plus || event->key() == Qt::Key_BracketRight) && control) {
		this->camera->zoomIn();
	}

	//Control - .... alejar
	if (event->key() == Qt::Key_Minus && control) {
		//creo un evento fantasma de scroll hacia abajo
		this->camera->zoomOut();
	}

}

CameraHandler* MyQVTKOpenGLWindow::GetCameraHandler() {
	return this->camera;
}