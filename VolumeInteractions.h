#pragma once
#include <QSplitter>
#include "myVolume.h"
#include "MyCustomPlot.h"
#include <vtkRenderWindow.h>
#include <vtkSmartPointer.h>
#include <vtkRenderer.h>
#include "MasterHandler.h"
#include "PlaneHandler.h"
#include "IOVolume.h"


#define NUMERODEPUNTOS 61

class CameraHandler;
class Ui_transferFunction;

class VolumeInteractions
{
public:
	VolumeInteractions();
	~VolumeInteractions();
	vtkSmartPointer<vtkRenderWindow> GetRenderWindow();
	vtkSmartPointer<vtkRenderWindow> GetRenderWindow2();
	void SetRenderWindow(vtkSmartPointer<vtkRenderWindow> ren);

	void initGUI(QSplitter* splitter);

	MyCustomPlot* getCustomPlot();
	
	void mousePressEvent(CameraHandler* camera, QMouseEvent *event);
	void mouseMoveEvent(CameraHandler* camera, QMouseEvent *event);
	void mouseReleaseEvent(CameraHandler* camera, QMouseEvent *event);

	void clearTransforms();

	void setVolume(int port);
	
	vtkSmartPointer<myVolume> getPickedVolume();

	void RemoveSelectedVolume();

	void registrarVolumen(vtkSmartPointer<myVolume> vol, QString name);

	void separarVolumen(vtkSmartPointer<myVolume> vol, double ratio);
	void unirVolumenesSeleccionados();

	void removeSelectedCells();

	void setCorteTridimensional(CameraHandler* camera, vtkSmartPointer<myVolume> vol);

	void setCortePreciso();

	vtkSmartPointer<vtkVolumeCollection> getCollection();

	Ui_transferFunction *ui;

	void setBounds(double bnds[6]);
	void setBounds(double bnds[6], vtkSmartPointer<myVolume> vol);


	vtkSmartPointer<myVolume> GetActualVolume();

	void cortarPorPlano(PlaneHandler* handler, vtkSmartPointer<myVolume> vol);

	void exportarVolumen(QString direction, vtkSmartPointer<myVolume> vol);

	void importarVolumen(QString direction);

private:
	bool black_white = false;

	MyCustomPlot * customPlot_Opacity;
	vtkSmartPointer<vtkRenderWindow> renwin;
	vtkSmartPointer<vtkRenderWindow> renWinSecondPlane;

	vtkSmartPointer<vtkMatrix4x4> volumeMatrix = vtkSmartPointer<vtkMatrix4x4>::New();
	vtkSmartPointer<vtkMatrix4x4> WordMatrix = vtkSmartPointer<vtkMatrix4x4>::New();
	vtkSmartPointer<vtkMatrix4x4> WordNormalMatrix = vtkSmartPointer<vtkMatrix4x4>::New();
	vtkSmartPointer<vtkMatrix4x4> WordNormalMatrix2 = vtkSmartPointer<vtkMatrix4x4>::New();

	vtkSmartPointer<myVolume> lastPickedVolume = nullptr;

	vtkSmartPointer<myVolume> lastVolume = nullptr;

	bool volumeInserted = false;

	bool cortePreciso = false;

	int portReg = 1;

	vtkSmartPointer<vtkVolumeCollection> coleccion;


	int separarObjeto_rec(vtkSmartPointer<vtkImageData> input, int* contorno, int cant_contorno, int min, int max, vtkSmartPointer<vtkImageData> output, vtkSmartPointer<vtkImageData> acumulado, int* contornoExterno, int cant_contornoExterno);

	void limpiar(vtkSmartPointer<vtkImageData> input, int* contorno, int cant_contorno, double umbral);

};

