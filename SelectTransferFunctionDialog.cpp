#include "SelectTransferFunctionDialog.h"
#include "SelectTransferFunctionWidget.h"
#include "SelectTransferFunctionWindow.h"
#include "SelectTransferFunctionLabel.h"
#include "ui_SelectTransferFunctionWindow.h"
#include <vtkGenericOpenGLRenderWindow.h>
#include <vtkRenderer.h>

#include "Consola.h"
#include "MasterHandler.h"
#include "VolumeInteractions.h"
#include "SimpleView.h"
#include "ThreadHandler.h"


#include <vtkPNGWriter.h>
#include "TransferFunctionsList.h"

/*
* Ventana que mostrar� la ventana con los 8 volumenes
*/


SelectTransferFunctionDialog::SelectTransferFunctionDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui_SelectTransferFunctionWindow)
{
    ui->setupUi(this);

	
}

SelectTransferFunctionDialog::~SelectTransferFunctionDialog()
{
    delete ui;
}



void SelectTransferFunctionDialog::mostrar()
{
	

	this->show();
}

void SelectTransferFunctionDialog::Init()
{
	if (first) {
		first = false;

		
		/*
		vtkSmartPointer<vtkGenericOpenGLRenderWindow> renwin1 = vtkSmartPointer<vtkGenericOpenGLRenderWindow>::New();
		vtkSmartPointer<vtkGenericOpenGLRenderWindow> renwin2 = vtkSmartPointer<vtkGenericOpenGLRenderWindow>::New();
		vtkSmartPointer<vtkGenericOpenGLRenderWindow> renwin3 = vtkSmartPointer<vtkGenericOpenGLRenderWindow>::New();
		vtkSmartPointer<vtkGenericOpenGLRenderWindow> renwin4 = vtkSmartPointer<vtkGenericOpenGLRenderWindow>::New();
		vtkSmartPointer<vtkGenericOpenGLRenderWindow> renwin5 = vtkSmartPointer<vtkGenericOpenGLRenderWindow>::New();
		vtkSmartPointer<vtkGenericOpenGLRenderWindow> renwin6 = vtkSmartPointer<vtkGenericOpenGLRenderWindow>::New();
		vtkSmartPointer<vtkGenericOpenGLRenderWindow> renwin7 = vtkSmartPointer<vtkGenericOpenGLRenderWindow>::New();
		vtkSmartPointer<vtkGenericOpenGLRenderWindow> renwin8 = vtkSmartPointer<vtkGenericOpenGLRenderWindow>::New();

		ui->openGLWidget_1->SetRenderWindow(renwin1);
		
		ui->openGLWidget_2->SetRenderWindow(renwin2);
		ui->openGLWidget_3->SetRenderWindow(renwin3);
		ui->openGLWidget_4->SetRenderWindow(renwin4);
		ui->openGLWidget_5->SetRenderWindow(renwin5);
		ui->openGLWidget_6->SetRenderWindow(renwin6);
		ui->openGLWidget_7->SetRenderWindow(renwin7);
		ui->openGLWidget_8->SetRenderWindow(renwin8);
		*/

		listaDeWidgets[0] = ui->groupBox_1;
		listaDeWidgets[1] = ui->groupBox_2;
		listaDeWidgets[2] = ui->groupBox_3;
		listaDeWidgets[3] = ui->groupBox_4;
		listaDeWidgets[4] = ui->groupBox_5;
		listaDeWidgets[5] = ui->groupBox_6;
		listaDeWidgets[6] = ui->groupBox_7;
		listaDeWidgets[7] = ui->groupBox_8;

		listaDeLabels[0] = ui->label;
		listaDeLabels[1] = ui->label_2;
		listaDeLabels[2] = ui->label_3;
		listaDeLabels[3] = ui->label_4;
		listaDeLabels[4] = ui->label_5;
		listaDeLabels[5] = ui->label_6;
		listaDeLabels[6] = ui->label_7;
		listaDeLabels[7] = ui->label_8;
		

		for (int i = 0; i < NUM_TRANSFER_FUNCTION; i++) {

			if (i == indexSeleccionado)
				listaDeWidgets[i]->setStyleSheet("border: 3px solid red");
			else
				listaDeWidgets[i]->setStyleSheet("border: 3px solid black");
			
			listaDeLabels[i]->SetParentDialog(this);

			listaDeLabels[i]->SetIndex(i);

		}

		//Lista de ventanas es un arreglo que contiene cada una de las ventanitas particulares que
		//tienen los volumenes, y aca les estoy seteando la funci�n de transferencia a los volumenes,
		//aca deber�a setear la imagen en vez de esto
		/*
		for (int i = 0; i < 8; i++) {
			listaDeVentanas[i]->setTransferFunction(TransferFunctionsList::GetTransferFunctionArray()[i], NUMERODEPUNTOS_SCALAR);
		}
		*/
		//MasterHandler::GetVolumeInteractor()->SetRenderWindows2(renwin1);

		//adentro de este listaDeVentanas hay un volumen, tengo que sacar esa mierda de ah�
		//deber�a hacer el preprocesamiento antes, y obtener una imagen en vez de volumenes ah� dentro
		//creo el volumen con el ImageData en el constructor new myVolume(ImageData)
	}
}

//void progressChanged(int p);
//void visibleProgressBar(bool b);

//hace un pre procesamiento, con esto voy a agregar el volumen
//la voy a necesitar
void SelectTransferFunctionDialog::setVolume(vtkSmartPointer<myVolume> vol)
{
	if (!first && vol != volumen) {

		Consola::appendln("Cargando funciones transferencia genericas");
		MasterHandler::GetSimpleView()->progressChanged(1);
		
		
		volumen = vol;

		if (!vol->getIsSplitted()) {
			MasterHandler::GetSimpleView()->visibleProgressBar(true);
			for (int i = 0; i < NUM_TRANSFER_FUNCTION; i++) {

				listaDeWidgets[i]->setStyleSheet("border: 3px solid black");
				indexSeleccionado = -1;


				if (vol == nullptr) {

				}
				else {
					//aca estoy agregando el volumen en general
					listaDeLabels[i]->setPixmap(*(vol->getImage(i)));

				}

				MasterHandler::GetSimpleView()->progressChanged((double)(i + 1.0f) * (double)(100.0f / NUM_TRANSFER_FUNCTION));
			}


			MasterHandler::GetSimpleView()->visibleProgressBar(false);
		}
		
	}

	
}

void SelectTransferFunctionDialog::select(int index)
{
	for (int i = 0; i < NUM_TRANSFER_FUNCTION; i++) {

		if (i == index) {
			listaDeWidgets[index]->setStyleSheet("border: 3px solid rgb(0,128,128)");
		}
		else if (i == indexSeleccionado) {
			listaDeWidgets[i]->setStyleSheet("border: 3px solid red");
		}
		else {
			listaDeWidgets[i]->setStyleSheet("border: 3px solid black");
		}
	}


}

//setea el volumen en el render windows original
void SelectTransferFunctionDialog::accept(int index) {
	///QStringList texto;
	//texto.append(ui->textEdit->toPlainText());
	//emit enviarDatosPopUp(texto);

	if (!MasterHandler::GetThread()->tryLock_TransferFunction()) {
		//Consola::appendln("Espere a que termine el trabajo");
		emit MasterHandler::GetSimpleView()->signal_Warning(HILO_TRABAJANDO);
		this->close();
		return;
	}

	indexSeleccionado = index;
	for (int i = 0; i < NUM_TRANSFER_FUNCTION; i++) {
		if (i==indexSeleccionado) {
			listaDeWidgets[index]->setStyleSheet("border: 3px solid red");
		}
		else
		{
			listaDeWidgets[i]->setStyleSheet("border: 3px solid black");
		}
	}

	if (volumen != nullptr) {
		
		volumen->restoreValues();
		//recupero los valores aqu� de la imagen
		double* vec = (TransferFunctionsList::GetTransferFunctionArray()[index]);
		//QVector<double> vec = (*listaDeVentanas[index]->getTransferFunction());

	
		for (int j = 0; j < NUMERODEPUNTOS_SCALAR; j++) {

			this->volumen->addOpacityPoint(j, vec[j]);
		}

		MasterHandler::GetVolumeInteractor()->setVolume(volumen->GetCustomPort());
	}

	MasterHandler::GetThread()->unlock_TransferFunction();
	this->close();
}

