#include "CameraHandler.h"
#include <stdio.h>
#include <QWheelEvent>
#include <QAction>
#include <vtkSmartPointer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRendererCollection.h>
#include <vtkInteractorStyleTrackballActor.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkInteractorStyleImage.h>
#include <vtkRenderWindow.h>

#include <vtkVolumeCollection.h>
#include <vtkImageData.h>
#include <vtkImageActor.h> 
#include <vtkCamera.h> 
#include <vtkImageSliceMapper.h>

#include <QCoreApplication.h>
#include <QObject>

#include "MasterHandler.h"
#include "VolumeInteractions.h"
#include "Consola.h"
#include <QSize>
#include <QWheelEvent>
#include <vtkDistanceWidget.h>
#include "RectangleHandler.h"

#define minWheelDelta 120

#include <vtkAxesActor.h>
#include <vtkCaptionActor2D.h>
#include <vtkTextActor.h>
#include <vtkTextProperty.h>
#include <vtkLeaderActor2D.h>
#include <vtkProperty2D.h>
#include <vtkPlaneSource.h>
#include "sliceVolume.h"

#include <vtkPoints.h>
#include <vtkCellArray.h>
#include <vtkPolyLine.h>
#include <vtkCoordinate.h>
#include <vtkPolyDataMapper2D.h>
#include <vtkActor2D.h>
#include "MyQVTKOpenGLWindow.h"



RectangleHandler* rectHand1;

//https://lorensen.github.io/vtkExamples/site/Cxx/Utilities/ViewportBorders/
void ViewportBorder(vtkSmartPointer<vtkRenderer >renderer, double *color, bool last)
{
	// points start at upper right and proceed anti-clockwise
	vtkSmartPointer<vtkPoints> points =
		vtkSmartPointer<vtkPoints>::New();
	points->SetNumberOfPoints(4);
	points->InsertPoint(0, 1, 1, 0);
	points->InsertPoint(1, 0, 1, 0);
	points->InsertPoint(2, 0, 0, 0);
	points->InsertPoint(3, 1, 0, 0);

	// create cells, and lines
	vtkSmartPointer<vtkCellArray> cells =
		vtkSmartPointer<vtkCellArray>::New();
	cells->Initialize();

	vtkSmartPointer<vtkPolyLine> lines =
		vtkSmartPointer<vtkPolyLine>::New();

	// only draw last line if this is the last viewport
	// this prevents double vertical lines at right border
	// if different colors are used for each border, then do
	// not specify last
	if (last)
	{
		lines->GetPointIds()->SetNumberOfIds(5);
	}
	else
	{
		lines->GetPointIds()->SetNumberOfIds(4);
	}
	for (unsigned int i = 0; i < 4; ++i)
	{
		lines->GetPointIds()->SetId(i, i);
	}
	if (last)
	{
		lines->GetPointIds()->SetId(4, 0);
	}
	cells->InsertNextCell(lines);

	// now make tge polydata and display it
	vtkSmartPointer<vtkPolyData> poly =
		vtkSmartPointer<vtkPolyData>::New();
	poly->Initialize();
	poly->SetPoints(points);
	poly->SetLines(cells);

	// use normalized viewport coordinates since
	// they are independent of window size
	vtkSmartPointer<vtkCoordinate> coordinate =
		vtkSmartPointer<vtkCoordinate>::New();
	coordinate->SetCoordinateSystemToNormalizedViewport();

	vtkSmartPointer<vtkPolyDataMapper2D> mapper =
		vtkSmartPointer<vtkPolyDataMapper2D>::New();
	mapper->SetInputData(poly);
	mapper->SetTransformCoordinate(coordinate);

	vtkSmartPointer<vtkActor2D> actor =
		vtkSmartPointer<vtkActor2D>::New();
	actor->SetMapper(mapper);
	actor->GetProperty()->SetColor(color);
	// line width should be at least 2 to be visible at extremes

	actor->GetProperty()->SetLineWidth(4.0); // Line Width

	renderer->AddViewProp(actor);
}


CameraHandler::CameraHandler()
{
	styleCamera = vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New();
	styleActor = vtkSmartPointer<vtkInteractorStyleTrackballActor>::New();
	styleImage = vtkSmartPointer<vtkInteractorStyleImage>::New();

	renderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();

	ren1 = vtkSmartPointer<vtkRenderer>::New();
	ren2 = vtkSmartPointer<vtkRenderer>::New();
	ren3 = vtkSmartPointer<vtkRenderer>::New();

	ren1->LightFollowCameraOn();
	ren2->LightFollowCameraOn();
	ren3->LightFollowCameraOn();
	
	//ren1->SetBackground(.1, .1, .1);
	//ren2->SetBackground(.1, .1, .1);
	//ren3->SetBackground(.1, .1, .1);
	double color[3] = { 0.005, 0.98, 0.98 };

	ViewportBorder(ren1, color, true);
	ViewportBorder(ren2, color, true);
	ViewportBorder(ren3, color, true);

	compare_ren = vtkSmartPointer<vtkRenderer>::New();

	actor1 = actor2 = actor3 = nullptr;

	rectHand1 = new RectangleHandler(ren1, ren2, ren3);

	//ren1->AddWid(rectHand->getPoint1());
}

CameraHandler::~CameraHandler()
{
}

/*
* Getter for renderWindow
*/
vtkSmartPointer<vtkRenderWindow> CameraHandler::GetRenderWindow() {
	return cameraRenderWindow;
}

/*
* Setter for RenderWindow
*/
void CameraHandler::SetRenderWindow(vtkSmartPointer<vtkRenderWindow> ren) {
	
	cameraRenderWindow = ren;

	renderWindowInteractor->SetInteractorStyle(styleCamera);

	renderWindowInteractor->SetRenderWindow(cameraRenderWindow);

	cameraRenderWindow->SetInteractor(renderWindowInteractor);

	actualStyle = styleCamera;
	lastStyle = actualStyle;
	cameraFlag = true;

	ren0 = ren->GetRenderers()->GetFirstRenderer();

	compare_ren->SetActiveCamera(ren0->GetActiveCamera());
	compare_ren->SetViewport(xmins_comp[0], ymins_comp[0], xmaxs_comp[0], ymaxs_comp[0]);

	ren0->GetActiveCamera()->SetPosition(defaultCameraPos[0], defaultCameraPos[1], defaultCameraPos[2]);

	double color[3] = { 0.005, 0.98, 0.98 };

	ViewportBorder(ren0, color, true);
	ViewportBorder(compare_ren, color, true);

	initAxes();

	cameraRenderWindow->Render();
}

vtkSmartPointer<vtkRenderWindowInteractor> CameraHandler::GetRenderWindowInteractor() {
	return renderWindowInteractor;
}

void CameraHandler::setOpenGLWindow(MyQVTKOpenGLWindow* win) {
	OpenGLWindow = win;
}

void CameraHandler::changeInteractor() {
	
	if (cameraFlag) {
		//cambio el a modo objeto
			

		renderWindowInteractor->SetInteractorStyle(styleActor);

		actualStyle = styleActor;
		lastStyle = actualStyle;
		cameraFlag = false;
	}
	else {
		//cambio el interactor

		renderWindowInteractor->SetInteractorStyle(styleCamera);

		actualStyle = styleCamera;
		lastStyle = actualStyle;
		cameraFlag = true;

	}
}

void CameraHandler::zoomIn() {
	//creo un evento fantasma de scroll hacia arriba
	QWheelEvent* qwevt = new  QWheelEvent(*(new QPoint(0, 0)), 300, nullptr, nullptr, Qt::Vertical);

	qApp->sendEvent((QObject*)OpenGLWindow, qwevt);

	delete qwevt;
}

void CameraHandler::zoomOut() {
	//creo un evento fantasma de scroll hacia arriba
	QWheelEvent* qwevt = new  QWheelEvent(*(new QPoint(0, 0)), -300, nullptr, nullptr, Qt::Vertical);

	qApp->sendEvent((QObject*)OpenGLWindow, qwevt);

	delete qwevt;
}

void CameraHandler::setFourCameras()
{
	
	if (twoCameras)
		setCompareMode();

	if (!fourCameras) {
		GetRenderWindow()->AddRenderer(ren1);
		GetRenderWindow()->AddRenderer(ren2);
		GetRenderWindow()->AddRenderer(ren3);

		ren0->SetViewport(xmins_four[0], ymins_four[0], xmaxs_four[0], ymaxs_four[0]);
		ren1->SetViewport(xmins_four[1], ymins_four[1], xmaxs_four[1], ymaxs_four[1]);
		ren2->SetViewport(xmins_four[2], ymins_four[2], xmaxs_four[2], ymaxs_four[2]);
		ren3->SetViewport(xmins_four[3], ymins_four[3], xmaxs_four[3], ymaxs_four[3]);


		fourCameras = true;

		fourCamerasAction->setChecked(true);
		compareCamerasAction->setChecked(false);
	}
	else {
		GetRenderWindow()->RemoveRenderer(ren1);
		GetRenderWindow()->RemoveRenderer(ren2);
		GetRenderWindow()->RemoveRenderer(ren3);

		ren0->SetViewport(0, 0, 1, 1);

		fourCameras = false;

		fourCamerasAction->setChecked(false);

		renderWindowInteractor->SetInteractorStyle(lastStyle);
		actualStyle = lastStyle;
	}

	GetRenderWindow()->Render();
}

void CameraHandler::setImageActors(vtkSmartPointer<vtkImageActor> imageActor1, vtkSmartPointer<vtkImageActor> imageActor2, vtkSmartPointer<vtkImageActor> imageActor3, double* bounds)
{
	//imagen 1
	
	if (imageActor1 != nullptr) {
		actor1 = imageActor1;
		ren1->AddActor(imageActor1);
		//ren1->ResetCamera();
	}
	//imagen 2
	
	if (imageActor2 != nullptr) {
		actor2 = imageActor2;
		ren2->AddActor(imageActor2);
		//ren2->ResetCamera();
	}
	
	//imagen 2
	
	if (imageActor3 != nullptr) {
		actor3 = imageActor3;
		ren3->AddActor(imageActor3);
		//ren3->ResetCamera();
	}
	
	rectHand1->setActors(imageActor1, imageActor2, imageActor3, bounds);

}

void CameraHandler::setSliceVolumes(vtkSmartPointer<sliceVolume> vol1, vtkSmartPointer<sliceVolume> vol2, vtkSmartPointer<sliceVolume> vol3, double* bounds)
{
	//imagen 1

	if (vol1 != nullptr) {
		volumen1 = vol1;
		ren1->AddVolume(volumen1);
		//ren1->ResetCamera();
	}
	//imagen 2

	if (vol2 != nullptr) {
		volumen2 = vol2;
		ren2->AddVolume(volumen2);
		//ren2->ResetCamera();
	}

	//imagen 2

	if (vol3 != nullptr) {
		volumen3 = vol3;
		ren3->AddVolume(volumen3);
		//ren3->ResetCamera();
	}
	//rectHand1->setRectanglesOff();
	rectHand1->setActors(vol1, vol2, vol3, bounds);
	
}

void CameraHandler::removeImageActors()
{
	if (actor1 != nullptr)
		ren1->RemoveActor2D(actor1);
	if (actor2 != nullptr)
		ren2->RemoveActor2D(actor2);
	if (actor3 != nullptr)
		ren3->RemoveActor2D(actor3);

	actor1 = nullptr;
	actor2 = nullptr;
	actor3 = nullptr;

	rectHand1->setRectanglesOff();

}

void CameraHandler::removeSliceVolumes()
{
	if (volumen1 != nullptr)
		ren1->RemoveVolume(volumen1);
	if (volumen2 != nullptr)
		ren2->RemoveVolume(volumen2);
	if (volumen3 != nullptr)
		ren3->RemoveVolume(volumen3);

	volumen1 = nullptr;
	volumen2 = nullptr;
	volumen3 = nullptr;
	if (drawPolygon)
		setStylePolygon();
}

bool CameraHandler::isFourCamera()
{
	return fourCameras;
}

bool CameraHandler::isCompareCamera()
{
	return twoCameras;
}

void CameraHandler::wheelEvent(QWheelEvent * e)
{

	if (fourCameras) {
		
		QSize size = OpenGLWindow->size();

		if(actor1 != nullptr && actor2 != nullptr && actor3 != nullptr)
			procesarWheelEvent(e->delta(), size.width(), size.height(), e->pos());

		if (volumen1 != nullptr && volumen2 != nullptr && volumen3 != nullptr)
			procesarWheelEventVol(e->delta(), size.width(), size.height(), e->pos());
	}
}

void CameraHandler::mouseMoveEvent(QMouseEvent * event)
{
	if (fourCameras) {
		QPoint pos = event->pos();
		QSize size = OpenGLWindow->size();
		if (pos.x() > 0 && pos.x() < size.width() && pos.y() > 0 && pos.y() < size.height()) {
			
			if ((pos.y() >= size.height() / 2 || pos.x() >= size.width() / 2) && actualStyle != styleImage)
			{
				renderWindowInteractor->SetInteractorStyle(styleImage);
				lastStyle = actualStyle;
				actualStyle = styleImage;
				
				
				//if(ren1->GetActors()->GetNumberOfItems() > 0)
				rectHand1->setInteractor(GetRenderWindow()->GetInteractor());
				
			}
			else if (actualStyle == styleImage && pos.x() <= size.width() / 2 && pos.y() <= size.height() / 2)
			{
				renderWindowInteractor->SetInteractorStyle(lastStyle);
				actualStyle = lastStyle;
			}
			
			
		}
		else {
			if (actualStyle != styleImage) {
				renderWindowInteractor->SetInteractorStyle(styleImage);
				lastStyle = actualStyle;
				actualStyle = styleImage;
				//rectHand1->setInteractor(GetRenderWindow()->GetInteractor());
			}
		}
	}

	rectHand1->clearMove();
}

void CameraHandler::setStylePolygon()
{
	
	if (drawPolygon) {
		if (actor1 != nullptr && actor2 != nullptr && actor3 != nullptr || 
			volumen1 != nullptr && volumen2 != nullptr && volumen3 != nullptr) {
			drawPolygon = false;
			rectHand1->setRectanglesOff();
			corteAction->setChecked(false);

		}
		else {
			corteAction->setChecked(false);
		}
		
	}
	else {
		if (actor1 != nullptr && actor2 != nullptr && actor3 != nullptr ||
			volumen1 != nullptr && volumen2 != nullptr && volumen3 != nullptr) {
			drawPolygon = true;
			rectHand1->setInteractor(GetRenderWindow()->GetInteractor());
			double pos = volumen1->getImageData()->GetSpacing()[2];

			if (volumen1->getImageData()->GetSpacing()[0] > pos)
				pos = volumen1->getImageData()->GetSpacing()[0];
			if (volumen1->getImageData()->GetSpacing()[1] > pos)
				pos = volumen1->getImageData()->GetSpacing()[1];

			rectHand1->setRectanglesOn(pos + 1);
			corteAction->setChecked(true);
		}
		else {
			corteAction->setChecked(false);
		}

	}

	GetRenderWindow()->Render();
}

void CameraHandler::clearPlaneMove()
{
	rectHand1->clearMove();
}

void CameraHandler::setAxesMode()
{
	if (axesActive) {
		axesActive = false;
		ren0->RemoveActor(axes);
		compare_ren->RemoveActor(axes);
		ren1->RemoveActor2D(actorXY_x);
		ren1->RemoveActor2D(actorXY_y);
		ren2->RemoveActor2D(actorYZ_x);
		ren2->RemoveActor2D(actorYZ_y);
		ren3->RemoveActor2D(actorZX_x);
		ren3->RemoveActor2D(actorZX_y);
	}
	else {
		axesActive = true;
		ren0->AddActor(axes);
		compare_ren->AddActor(axes);
		ren1->AddActor2D(actorXY_x);
		ren1->AddActor2D(actorXY_y);
		ren2->AddActor2D(actorYZ_x);
		ren2->AddActor2D(actorYZ_y);
		ren3->AddActor2D(actorZX_x);
		ren3->AddActor2D(actorZX_y);
	}
}

void CameraHandler::setButtonActionFourCameras(QAction * btn)
{
	fourCamerasAction = btn;
}

void CameraHandler::setButtonActionCompareMode(QAction * btn)
{
	compareCamerasAction = btn;
}

void CameraHandler::setButtonCorte(QAction * btn)
{
	corteAction = btn;
}

void CameraHandler::setCompareMode()
{
	if (fourCameras)
		setFourCameras();

	vtkSmartPointer<myVolume> vol = MasterHandler::GetVolumeInteractor()->GetActualVolume();
	
	if (!twoCameras) {
		GetRenderWindow()->AddRenderer(compare_ren);

		ren0->SetViewport(xmins_comp[1], ymins_comp[1], xmaxs_comp[1], ymaxs_comp[1]);
		

		twoCameras = true;

		if (vol != nullptr) {
			compare_ren->AddVolume(vol);
			ren0->RemoveVolume(vol);
		}

		compareCamerasAction->setChecked(true);
		fourCamerasAction->setChecked(false);
			
	}
	else {

		if (vol != nullptr) {
			ren0->AddVolume(vol);
			compare_ren->RemoveVolume(vol);
		}
		GetRenderWindow()->RemoveRenderer(compare_ren);

		ren0->SetViewport(0, 0, 1, 1);

		twoCameras = false;

		compareCamerasAction->setChecked(false);


	}

	GetRenderWindow()->Render();
	
}

void CameraHandler::ResetCamera()
{
	//ren0->GetActiveCamera()->SetEyePosition(defaultCameraView);
	//ren0->GetActiveCamera()->SetViewUp(defaultCameraUp);
	ren0->ResetCamera();
	ren0->GetActiveCamera()->SetPosition(defaultCameraPos);
	ren0->GetActiveCamera()->SetViewUp(defaultCameraUp);
	GetRenderWindow()->Render();
}

vtkSmartPointer<vtkRenderer > CameraHandler::GetImageRenderer()
{
	return ren1;
}

bool CameraHandler::canMouseMove()
{
	return styleImage != actualStyle || drawPolygon;
}

void CameraHandler::resetCameras()
{
	ren1->ResetCamera();
	ren2->ResetCamera();
	ren3->ResetCamera();

}

void setAxisProperty(vtkSmartPointer<vtkLeaderActor2D> axes, double R, double G, double B, char* text, double posX_1, double posY_1, double posX_2, double posY_2, double aspect = 1) 
{
	axes->SetLabel(text);
	axes->GetLabelTextProperty()->SetFontFamilyAsString("Courier");
	axes->GetLabelTextProperty()->SetColor(R, G, B);
	axes->GetLabelTextProperty()->SetFontSize(30);
	axes->GetLabelTextProperty()->SetShadow(false);

	axes->GetProperty()->SetColor(R, G, B);
	axes->SetVisibility(true);
	axes->SetArrowPlacement(vtkLeaderActor2D::VTK_ARROW_POINT2);

	axes->SetPosition(posX_1, posY_1 * aspect);
	axes->SetPosition2(posX_2, posY_2 * aspect);

	axes->SetArrowWidth(0.01f);
}

void CameraHandler::initAxes()
{

	{
		//plano XY eje X
		actorXY_x = vtkSmartPointer<vtkLeaderActor2D>::New();
		setAxisProperty(actorXY_x, 1, 0, 0, "X",  0.05f, 0.05, 0.5f , 0.05f, 1);

		//plano XY eje Y
		actorXY_y = vtkSmartPointer<vtkLeaderActor2D>::New();
		setAxisProperty(actorXY_y, 0, 1, 0, "Y", 0.05f, 0.05, 0.05f, 0.5f, 1);
	}
	{
		//plano XY eje X
		actorYZ_x = vtkSmartPointer<vtkLeaderActor2D>::New();
		setAxisProperty(actorYZ_x, 0, 1, 0, "Y", 0.05f, 0.95, 0.5f, 0.95f, 1);

		//plano XY eje Y
		actorYZ_y = vtkSmartPointer<vtkLeaderActor2D>::New();
		setAxisProperty(actorYZ_y, 0, 0, 1, "Z", 0.05f, 0.95, 0.05f, 0.5f, 1);
	}
	{
		//plano XY eje X
		actorZX_x = vtkSmartPointer<vtkLeaderActor2D>::New();
		setAxisProperty(actorZX_x, 1, 0, 0, "X", 0.05f, 0.95, 0.5f, 0.95f, 1);

		//plano XY eje Y
		actorZX_y = vtkSmartPointer<vtkLeaderActor2D>::New();
		setAxisProperty(actorZX_y, 0, 0, 1, "Z", 0.05f, 0.95, 0.05f, 0.5f, 1);
	}
	{
		//axes for 3D view

		axes = vtkSmartPointer<vtkAxesActor>::New();

		//AXES
		axes->SetTotalLength(300, 300, 300);
		axes->SetConeRadius(0.10);
		//eje X
		axes->GetXAxisCaptionActor2D()->GetTextActor()->SetTextScaleModeToNone();
		axes->SetXAxisLabelText("X");
		axes->GetXAxisCaptionActor2D()->GetTextActor()->GetTextProperty()->SetFontFamilyAsString("Courier");
		axes->GetXAxisCaptionActor2D()->GetTextActor()->GetTextProperty()->SetColor(1, 0, 0);
		axes->GetXAxisCaptionActor2D()->GetTextActor()->GetTextProperty()->SetFontSize(30);
		axes->GetXAxisCaptionActor2D()->GetTextActor()->GetTextProperty()->SetShadow(false);
		//axes->GetXAxisCaptionActor2D()->GetTextActor()->GetTextProperty()->SetItalic(true);
		//axes->GetXAxisCaptionActor2D()->GetTextActor()->GetTextProperty()->UseTightBoundingBoxOn();
		//eje Y
		axes->GetYAxisCaptionActor2D()->GetTextActor()->SetTextScaleModeToNone();
		axes->SetYAxisLabelText("Y");
		axes->GetYAxisCaptionActor2D()->GetTextActor()->GetTextProperty()->SetFontFamilyAsString("Courier");
		axes->GetYAxisCaptionActor2D()->GetTextActor()->GetTextProperty()->SetColor(0, 1, 0);
		axes->GetYAxisCaptionActor2D()->GetTextActor()->GetTextProperty()->SetFontSize(30);
		axes->GetYAxisCaptionActor2D()->GetTextActor()->GetTextProperty()->SetShadow(false);
		//axes->GetYAxisCaptionActor2D()->GetTextActor()->GetTextProperty()->SetItalic(true);
		//axes->GetYAxisCaptionActor2D()->GetTextActor()->GetTextProperty()->UseTightBoundingBoxOn();
		//eje Z
		axes->GetZAxisCaptionActor2D()->GetTextActor()->SetTextScaleModeToNone();
		axes->SetZAxisLabelText("Z");
		axes->GetZAxisCaptionActor2D()->GetTextActor()->GetTextProperty()->SetFontFamilyAsString("Courier");
		axes->GetZAxisCaptionActor2D()->GetTextActor()->GetTextProperty()->SetColor(0, 0, 1);
		axes->GetZAxisCaptionActor2D()->GetTextActor()->GetTextProperty()->SetFontSize(30);
		axes->GetZAxisCaptionActor2D()->GetTextActor()->GetTextProperty()->SetShadow(false);
		//axes->GetZAxisCaptionActor2D()->GetTextActor()->GetTextProperty()->SetItalic(true);
		//axes->GetZAxisCaptionActor2D()->GetTextActor()->GetTextProperty()->UseTightBoundingBoxOn();

	}

	if (axesActive) {
		ren0->AddActor(axes);
		compare_ren->AddActor(axes);
		ren1->AddActor2D(actorXY_x);
		ren1->AddActor2D(actorXY_y);
		ren2->AddActor2D(actorYZ_x);
		ren2->AddActor2D(actorYZ_y);
		ren3->AddActor2D(actorZX_x);
		ren3->AddActor2D(actorZX_y);
	}
}

void CameraHandler::procesarWheelEvent(int delta, int width, int height, QPoint pos)
{
	
	if (pos.x() > 0 && pos.x() < width && pos.y() > 0 && pos.y() < height) {
		if (pos.x() > width / 2 && pos.y() <= height / 2 && actor1 != nullptr) {

			//estamos en el ren1

			//vtkSmartPointer<vtkImageSliceMapper* imageMapper = (vtkSmartPointer<vtkImageSliceMapper*)actor1->GetMapper();

			int actualSlice = actor1->GetZSlice();

			int maxZ = actor1->GetWholeZMax();
			int minZ = actor1->GetWholeZMin();

			int cant = delta / minWheelDelta;
			double posZ = - actor1->GetMapper()->GetInput()->GetSpacing()[2] * (actualSlice + cant);
			double *position = actor1->GetPosition();
			

			if (delta < 0 && actualSlice > minZ) {
				actor1->SetZSlice(actualSlice + cant);
				//vtkSmartPointer<vtkImageSliceMapper* imageMapper = (vtkSmartPointer<vtkImageSliceMapper*)actor1->GetMapper();
				//imageMapper->SetSliceNumber(actualSlice + cant);
				actor1->SetPosition(position[0],position[1],posZ);
			}
			if (delta > 0 && actualSlice < maxZ) {
				actor1->SetZSlice(actualSlice + cant);
				actor1->SetPosition(position[0], position[1], posZ);
			}
			GetRenderWindow()->Render();
		}

		if (pos.x() <= width / 2 && pos.y() > height / 2 && actor2 != nullptr) {

			//estamos en el ren2

			//vtkSmartPointer<vtkImageSliceMapper* imageMapper = (vtkSmartPointer<vtkImageSliceMapper*)actor2->GetMapper();

			int actualSlice = actor2->GetZSlice();

			int maxZ = actor2->GetWholeZMax();
			int minZ = actor2->GetWholeZMin();

			int cant = delta / minWheelDelta;

			double posZ = -actor2->GetMapper()->GetInput()->GetSpacing()[2] * (actualSlice + cant);
			double *position = actor2->GetPosition();

			if (delta < 0 && actualSlice > minZ) {
				actor2->SetZSlice(actualSlice + cant);
				actor2->SetPosition(position[0], position[1], posZ);
			}
			if (delta > 0 && actualSlice < maxZ) {
				actor2->SetZSlice(actualSlice + cant);
				actor2->SetPosition(position[0], position[1], posZ);
			}
			GetRenderWindow()->Render();
		}

		if (pos.x() > width / 2 && pos.y() > height / 2 && actor3 != nullptr) {

			//estamos en el ren3

			//vtkSmartPointer<vtkImageSliceMapper* imageMapper = (vtkSmartPointer<vtkImageSliceMapper*)actor3->GetMapper();

			int actualSlice = actor3->GetZSlice();

			int maxZ = actor3->GetWholeZMax();
			int minZ = actor3->GetWholeZMin();

			int cant = delta / minWheelDelta;

			double posZ = -actor3->GetMapper()->GetInput()->GetSpacing()[2] * (actualSlice + cant);
			double *position = actor3->GetPosition();

			if (delta < 0 && actualSlice > minZ) {
				actor3->SetZSlice(actualSlice + cant);
				actor3->SetPosition(position[0], position[1], posZ);
			}
			if (delta > 0 && actualSlice < maxZ) {
				actor3->SetZSlice(actualSlice + cant);
				actor3->SetPosition(position[0], position[1], posZ);
			}
			GetRenderWindow()->Render();
		}
	}
}

void CameraHandler::procesarWheelEventVol(int delta, int width, int height, QPoint pos)
{
	if (pos.x() > 0 && pos.x() < width && pos.y() > 0 && pos.y() < height) {
		if (pos.x() > width / 2 && pos.y() <= height / 2 && volumen1 != nullptr) {

			//estamos en el ren1

			//vtkSmartPointer<vtkImageSliceMapper* imageMapper = (vtkSmartPointer<vtkImageSliceMapper*)actor1->GetMapper();

			int actualSlice = volumen1->GetSlice();

			int maxZ = volumen1->GetMax();
			int minZ = volumen1->GetMin();

			int cant = delta / minWheelDelta;
			double posZ = -volumen1->getImageData()->GetSpacing()[2]  * (actualSlice + cant - maxZ/2);
			double *position = volumen1->GetPosition();


			if (delta < 0 && actualSlice > minZ) {
				volumen1->SetSlice(actualSlice + cant);
				volumen1->SetPosition(position[0], position[1], posZ);
			}
			if (delta > 0 && actualSlice < maxZ) {
				volumen1->SetSlice(actualSlice + cant);
				volumen1->SetPosition(position[0], position[1], posZ);
			}
			GetRenderWindow()->Render();
		}
		
		if (pos.x() <= width / 2 && pos.y() > height / 2 && volumen2 != nullptr) {
			//ren 2 YZ
			int actualSlice = volumen2->GetSlice();

			int maxZ = volumen2->GetMax();
			int minZ = volumen2->GetMin();

			int cant = delta / minWheelDelta;
			double posX = -volumen2->getImageData()->GetSpacing()[0] * (actualSlice + cant - maxZ / 2);
			double *position = volumen2->GetPosition();


			if (delta < 0 && actualSlice > minZ) {
				volumen2->SetSlice(actualSlice + cant);
				volumen2->SetPosition(posX, position[1], position[2]);
			}
			if (delta > 0 && actualSlice < maxZ) {
				volumen2->SetSlice(actualSlice + cant);
				volumen2->SetPosition(posX, position[1], position[2]);
			}
			GetRenderWindow()->Render();
		}
		
		if (pos.x() > width / 2 && pos.y() > height / 2 && volumen3 != nullptr) {

			//estamos en el ren3 

			int actualSlice = volumen3->GetSlice();

			int maxZ = volumen3->GetMax();
			int minZ = volumen3->GetMin();

			int cant = delta / minWheelDelta;
			double posY = -volumen3->getImageData()->GetSpacing()[1] * (actualSlice + cant - maxZ / 2);
			double *position = volumen3->GetPosition();


			if (delta < 0 && actualSlice > minZ) {
				volumen3->SetSlice(actualSlice + cant);
				volumen3->SetPosition(position[0], posY, position[2]);
			}
			if (delta > 0 && actualSlice < maxZ) {
				volumen3->SetSlice(actualSlice + cant);
				volumen3->SetPosition(position[0], posY, position[2]);
			}
			GetRenderWindow()->Render();
		}
		/**/
	}
}

