#pragma once

#include <vtkRenderWindow.h>
#include <vtkDistanceWidget.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkProp3D.h>
#include <QKeyEvent>
#include <vtkSmartPointer.h>


class RectangleHandler
{
public:
	RectangleHandler(vtkSmartPointer<vtkRenderer> ren1, vtkSmartPointer<vtkRenderer> ren2, vtkSmartPointer<vtkRenderer> ren3);
	~RectangleHandler();
	
	void setInteractor(vtkSmartPointer<vtkRenderWindowInteractor> interactor);


	void clearMove();

	void setActors(vtkSmartPointer<vtkProp3D> actor1, vtkSmartPointer<vtkProp3D> actor2, vtkSmartPointer<vtkProp3D> actor3, double* bounds);

	void setRectanglesOn(double pos);
	void setRectanglesOff();

	bool isOn();

private:
	bool firstFourCamera = true;
	bool enabled = false;
};

