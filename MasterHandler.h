#pragma once

#include <vtkRenderWindow.h>
#include <vtkActor.h>
#include <QKeyEvent>
#include "CameraHandler.h"
#include "tablaPaciente.h"
#include "ventanaObjetos.h"
#include <qprogressbar.h>
#include "PlaneHandler.h"
#include "tablaPaciente.h"
#include "SimpleView.h"
#include "myVolume.h"
#include "IOSurface.h"


#include <qlistwidget.h>

#include <vtkWindowToImageFilter.h>
#include <vtkPNGWriter.h>

class VolumeInteractions;
class SurfaceInteractions;

class ThreadHandler;
class CameraHandler;
class SimpleView;
class NoteInteractions;
class ListInteractions;

class MasterHandler
{

public:

	

	MasterHandler();
	~MasterHandler();



	static void initHandlers(vtkSmartPointer<vtkRenderWindow > ren, CameraHandler* cam);
	static void InsertarMedida();
	static vtkSmartPointer<vtkActor> GetActorPlane();
	static void cortarPorPlano(vtkSmartPointer<vtkActor> actor);
	static void cortarVolumenPorPlano(vtkSmartPointer<myVolume> vol);
	static void mostrarPlano();

	static void mouseMoveEvent(QMouseEvent *event, double height, double width);
	static void mousePressEvent(QMouseEvent *event, double height, double width);
	static void mouseReleaseEvent(QMouseEvent *event, double height, double width);

	//static void checkPickedNote(CameraHandler* camera);

	static void masterKeyPress(QKeyEvent *event);
	static void masterKeyRelease(QKeyEvent *event);
	static void MasterHandler::Renderizar();

	static SurfaceInteractions* MasterHandler::GetSurfaceInteractor();
	static VolumeInteractions* MasterHandler::GetVolumeInteractor();

	static ThreadHandler* MasterHandler::GetThread();
	static PlaneHandler* getPlaneHandler();


	static NoteInteractions* MasterHandler::GetNoteInteractor();

	static void setTablaPaciente(tablaPaciente* tabla);
	static tablaPaciente* getTablaPaciente();

	static void setVentanaObjetos(ventanaObjetos* ventana);
	static ventanaObjetos* getVentanaObjetos();


	static void setProgressBar(QProgressBar* prog_bar);
	static QProgressBar* getProgressBar();

	static void SetSimpleView(SimpleView* s);
	static SimpleView* GetSimpleView();

	static void run_openVolume(std::string dir_path);

	static vtkSmartPointer<myVolume> get_last_volume();



	static void setListWidget(ListInteractions* t);
	static ListInteractions* getListWidget();


	static CameraHandler* GetCameraHandler();

	static IOSurface* GetIOSurface();




};

