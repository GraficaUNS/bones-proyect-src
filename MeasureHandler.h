#pragma once

#include <vtkRenderWindow.h>
#include <vtkDistanceWidget.h>
#include <vtkCallbackCommand.h>
#include <vtkSmartPointer.h>

#include <QKeyEvent>


class MeasureHandler
{
public:
	MeasureHandler();
	~MeasureHandler();
	
	/*
	 * Seter de RenderWindow
	 */
	void SetRenderWindow(vtkSmartPointer<vtkRenderWindow> ren);

	

	/*
	 * Retorna la medida seleccionada actual
	 */
	vtkSmartPointer<vtkDistanceWidget> GetMedidaSeleccionada();

	/*
	 * Se utiliza para setear una medida desde el exterior, 
	 * por lo general para poner en null la variable interna 
	 * y as� no interactuar con la ultima medida seleccionada
	 */
	void SetMedidaSeleccionada(vtkSmartPointer<vtkDistanceWidget> widget);

	/*
	* Crea una nueva medida y la inserta al interactor actual del renderWindow
	*/
	void insertarMedida();

	/*
	* Alinea la medida con el eje global correspondiente
	* el evento puede ser de la tecla X, Y o Z
	*/
	void alinearMedida();

	/*
	 * Restaura el color al color por defecto una vez soltada la tecla del eje que le da color
	 */
	void restaurarColor();


	void keyPress();


private:

	
	/*
	 * Setea una callback que se utiliza para retornar la ultima medida seleccionada 
	 */
	void setCallback();

	/*
	* Retorna la RenderWindow
	*/
	vtkSmartPointer<vtkRenderWindow> GetRenderWindow();

	



};

