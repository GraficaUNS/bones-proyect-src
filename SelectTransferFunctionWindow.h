#pragma once

#include <QVTKOpenGLWindow.h>
#include <QtWidgets/QSplitter>
#include <QMouseEvent>
#include <vtkRenderer.h>
#include <QOpenGLWindow>
#include <QWindow>
#include <QKeyEvent>

#include <QtGui/QOpenGLContext.h>

#include <vtkGenericOpenGLRenderWindow.h>
#include "SelectTransferFunctionDialog.h"
#include "myVolume.h"

class CameraHandler;
class SimpleView;

//aca esta extendiendo a la clase qvtkopen, asi se extiende
class SelectTransferFunctionWindow : public QVTKOpenGLWindow
{

	Q_OBJECT

public:

	SelectTransferFunctionWindow();

	SelectTransferFunctionWindow(vtkGenericOpenGLRenderWindow* w,
		QOpenGLContext *shareContext = QOpenGLContext::currentContext(),
		UpdateBehavior updateBehavior = NoPartialUpdate,
		QWindow *parent = Q_NULLPTR);

	SelectTransferFunctionWindow(QOpenGLContext *shareContext,
		UpdateBehavior updateBehavior = NoPartialUpdate,
		QWindow *parent = Q_NULLPTR);

	~SelectTransferFunctionWindow();

	void SetRenderWindow(vtkGenericOpenGLRenderWindow* renwin) override;

	void SetParentDialog(SelectTransferFunctionDialog* d);

	void SetIndex(int i);

	void mousePressEvent(QMouseEvent *event) override;
	void mouseMoveEvent(QMouseEvent *event) override;
	void mouseReleaseEvent(QMouseEvent *event) override;

	void wheelEvent(QWheelEvent* e) override;

	void setData(vtkImageData* selectedVolume);

	void removeVolumeFromRender();

	void addVolumeToRender();

	void setTransferFunction(double* vector, int cant);

	QVector<double>* getTransferFunction();

	double* obtenerViewport();

private:
	typedef QVTKOpenGLWindow Superclass;

	SelectTransferFunctionDialog* parentDialog;
	int index;
	vtkRenderer * renderer1;

	vtkSmartPointer<myVolume> vol;

	double CameraPos[3] = {
		393.697
		,
		361.101
		,
		-379.154
	};
	double CameraUp[3] = {
		-0.369397
		,
		-0.404254
		,
		-0.836735
	};

};

