#pragma once

#define ThreadHand
#include <QtWidgets/QTextBrowser>
#include <qthread.h>
#include <vtkActor.h>
#include <myVolume.h>
#include <iostream>
#include <QMutex>
#include <QTimer>

#include "QualityBarWidget.h"


#define HILO_TRABAJANDO "Se esta realizando un trabajo.\nPor favor espere a que este finalize."

class ThreadHandler : public QThread {

	Q_OBJECT


private:

public:
	ThreadHandler();
	~ThreadHandler();
	
	void set_openVolume(std::string p);
	void set_exportVolume(std::string p, vtkSmartPointer<myVolume> v);
	void set_importVolume(std::string p);

	void set_separarVolumen(vtkSmartPointer<myVolume> v, double r);
	void set_separarSuperficie(vtkSmartPointer<vtkActor> a, double r);
	
	void set_unirSuperficie();
	void set_unirVolumen();

	void set_cortarSuperficie(vtkSmartPointer<vtkActor> a);
	void set_cortarVolumen(vtkSmartPointer<myVolume> v);

	void set_meshReduction(vtkSmartPointer<vtkActor> actor, double r);
	void set_smoothMesh(vtkSmartPointer<vtkActor> actor, int iterations);
	void set_removeSmallObjects(vtkSmartPointer<vtkActor> actor, double r);

	void set_corteTridimensional(vtkSmartPointer<myVolume> v);
	void set_convertVolume(vtkSmartPointer<myVolume> v, double r);

	void set_exportObject(vtkSmartPointer<vtkActor> actor, std::string p);
	void set_importObject(std::string p);

	void set_convertToGCode(vtkSmartPointer<myVolume> v, double presition);

	void set_Scale_Thread();

	void thread_stop();
	
	void init();

	bool isWorking();

	void lock_TransferFunction();
	void unlock_TransferFunction();
	bool tryLock_TransferFunction();

	void lock_Actor();
	void unlock_Actor();
	bool tryLock_Actor();

	void lock_Render();
	void unlock_Render();
	bool tryLock_Render();

protected:
	void run() override;

protected slots:
	void slot_updateTimer();
	
private:
	vtkSmartPointer<myVolume> vol;
	vtkSmartPointer<vtkActor> act;
	std::string path;

	QualityBarWidget* qualityWidget;

	QMutex* mutex_TransferFunction;
	QMutex*	mutex_Actor;
	QMutex*	mutex_Render;

	bool control_thread_stop = false;
	bool working = false;

	bool control_openVolume = false;
	bool control_importVolume = false;
	bool control_exportVolume = false;

	bool control_separarVolume = false;
	bool control_separarSuperficie = false;

	bool control_cortarVolumen = false;
	bool control_cortarSuperficie = false;

	bool control_unirVolumen = false;
	bool control_unirSuperficie = false;

	bool control_meshReduction = false;
	bool control_smoothMesh = false;
	bool control_removeSmallObjects = false;

	bool control_corteTridimensional = false;
	bool control_convertVolumen = false;

	bool control_exportObject = false;
	bool control_importObject = false;

	bool control_convertToGCode = false;

	bool is_scale_thread = false;

	int i;
	double ratio;

	QTimer *timer;
	int tiempo = 0;
};