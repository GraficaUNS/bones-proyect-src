
#pragma once

#include <vtkVolume.h>
#include <qvector.h>
#include "Paciente.h"
#include <vtkColorTransferFunction.h>
#include <vtkVolumeProperty.h>
#include <vtkDICOMImageReader.h>
#include <vtkImageData.h>
#include <vtkImageActor.h>
#include <vtkSmartVolumeMapper.h>
#include <vtkPlane.h> 
/*
#define SLICE_CANT_COLOR 125
#define SLICE_STEP_COLOR 20
#define SLICE_CANT_COMPONENTS_COLOR 3
#define SLICE_INIT_VALUE_COLOR 1000
#define SLICE_CANT_COLOR_INDEX_ACTIVE_DEFAULT 9
/**/
#define SLICE_XY_PLANE 2
#define SLICE_YZ_PLANE 0
#define SLICE_ZX_PLANE 1

//#define Default_Ambient 0.1
//#define Default_Diffuse 0.9
//#define Default_Specular 0.2
//#define Default_SpecularPower 10.0

class sliceVolume: public vtkVolume
{
public:
	
	typedef vtkVolume Superclass;
	
	sliceVolume();
	sliceVolume(vtkSmartPointer<vtkImageData> img, int cut, vtkSmartPointer<vtkColorTransferFunction> colorTransferFunction);
	
	~sliceVolume();
	
	vtkSmartPointer<vtkImageData> getImageData(); 

	void centrar();

	void SetSlice(int i);

	int GetSlice();

	int GetMin();
	int GetMax();


private:
	int maxDim;

	int slice;

	vtkSmartPointer<vtkPlane > planos[2];

	double spacing;

	vtkSmartPointer<vtkVolumeProperty> initVolumeProperty();
	
	vtkSmartPointer<vtkColorTransferFunction> color;
	
	vtkSmartPointer<vtkImageData> imageData = nullptr;

	vtkSmartPointer<vtkSmartVolumeMapper> volumeMapper;


};