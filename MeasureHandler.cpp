#include "MeasureHandler.h"
#include <vtkDistanceWidget.h>
//#include <vtkSmartPointer.h>
#include <vtkDistanceRepresentation3D.h>
#include <vtkPointHandleRepresentation3D.h>
#include <vtkCommand.h>
#include <vtkWidgetEvent.h>
#include <vtkProperty.h>
#include <vtkCallbackCommand.h>
#include <vtkCollection.h>
#include "Consola.h"
#include "InputManager.h"

//color por default (esto hay que unificarlo en una superclase)
double defaultMeasureColor[3] = { 0.005, 0.98, 0.98 };

double coordenadaAnteriorP1[3] = { 0.0,0.0,0.0 };
//el callback command para setear a las medidas creadas
vtkSmartPointer<vtkCallbackCommand> measureCallback;

//Render Window 
vtkSmartPointer<vtkRenderWindow> renderWindow_measure;

//esta coleccion mantiene las medidas insertadas, podria utilizarse para implementar una vista mas detallada
//vtkSmartPointer<vtkCollection* medidasInsertadas;

//ultima medida seleccionada
vtkSmartPointer<vtkDistanceWidget> distanceWidgetSeleccionado = nullptr;


MeasureHandler::MeasureHandler()
{
	//medidasInsertadas = vtkSmartPointer<vtkCollection>::New();
	setCallback();
}


MeasureHandler::~MeasureHandler()
{
}

/*
 * Callback para conocer la ultima medida seleccionada
 */
void functionMeasureCallBack(vtkObject* caller, long unsigned int eventId, void* /*clientData*/, void* /*callData*/)
{
	//aca se seleccionan las medidas

	vtkDistanceWidget* widget = (vtkDistanceWidget*)caller;
	if (distanceWidgetSeleccionado != widget) {
		distanceWidgetSeleccionado = widget;
		//lastPickedActor = nullptr;
	}

	if (InputManager::IsKeyPressed(Qt::Key::Key_Control)) {

		vtkSmartPointer<vtkDistanceRepresentation3D> dRep2 = (vtkDistanceRepresentation3D*)distanceWidgetSeleccionado->GetDistanceRepresentation();

		if (eventId == vtkCommand::StartInteractionEvent) {
			double * aux1 = dRep2->GetPoint1WorldPosition();
			coordenadaAnteriorP1[0] = aux1[0];
			coordenadaAnteriorP1[1] = aux1[1];
			coordenadaAnteriorP1[2] = aux1[2];
		}
		else {
			if (eventId == vtkCommand::InteractionEvent || eventId == vtkWidgetEvent::Move) {
				double * coordenadaActualP1 = dRep2->GetPoint1WorldPosition();
				double * coordenadaActualP2 = dRep2->GetPoint2WorldPosition();
				double aux[3] = { (coordenadaActualP1[0] - coordenadaAnteriorP1[0]), (coordenadaActualP1[1] - coordenadaAnteriorP1[1]), (coordenadaActualP1[2] - coordenadaAnteriorP1[2]) };
				double coordenadaNuevaP2[3] = { coordenadaActualP2[0] + aux[0], coordenadaActualP2[1] + aux[1],coordenadaActualP2[2] + aux[2] };
				dRep2->SetPoint2WorldPosition(coordenadaNuevaP2);
				coordenadaAnteriorP1[0] = coordenadaActualP1[0];
				coordenadaAnteriorP1[1] = coordenadaActualP1[1];
				coordenadaAnteriorP1[2] = coordenadaActualP1[2];
			}
			else {
				if (eventId == vtkCommand::EndInteractionEvent) {
					double * coordenadaActualP1 = dRep2->GetPoint1WorldPosition();
					double * coordenadaActualP2 = dRep2->GetPoint2WorldPosition();
					double aux[3] = { (coordenadaActualP1[0] - coordenadaAnteriorP1[0]), (coordenadaActualP1[1] - coordenadaAnteriorP1[1]), (coordenadaActualP1[2] - coordenadaAnteriorP1[2]) };
					double coordenadaNuevaP2[3] = { coordenadaActualP2[0] + aux[0], coordenadaActualP2[1] + aux[1],coordenadaActualP2[2] + aux[2] };
					dRep2->SetPoint2WorldPosition(coordenadaNuevaP2);
				}

				//renderWindow_measure->Render();
			}
		}
	}
};


/*
 * Getter for renderWindow
 */
vtkSmartPointer<vtkRenderWindow> MeasureHandler::GetRenderWindow() {
	return renderWindow_measure;
}

/*
* Setter for RenderWindow
*/
void MeasureHandler::SetRenderWindow(vtkSmartPointer<vtkRenderWindow> ren) {
	renderWindow_measure = ren;
}

/*
* Retorna la medida seleccionada actual
*/
vtkSmartPointer<vtkDistanceWidget> MeasureHandler::GetMedidaSeleccionada() {

	return distanceWidgetSeleccionado;
}

/*
* Se utiliza para setear una medida desde el exterior,
* por lo general para poner en null la variable interna
* y as� no interactuar con la ultima medida seleccionada
*/
void MeasureHandler::SetMedidaSeleccionada(vtkSmartPointer<vtkDistanceWidget> widget) {
	distanceWidgetSeleccionado = widget;
}

/*
* Crea una nueva medida y la inserta al interactor actual del renderWindow
*/
void MeasureHandler::insertarMedida() {
	//se ejecuta solo una vez
	// Create the widget and its representation
	vtkSmartPointer<vtkDistanceWidget> distanceWidget = vtkSmartPointer<vtkDistanceWidget>::New();
	vtkSmartPointer<vtkDistanceRepresentation3D> dRep2 = vtkSmartPointer<vtkDistanceRepresentation3D>::New();


	vtkSmartPointer<vtkPointHandleRepresentation3D> handle2 = vtkSmartPointer<vtkPointHandleRepresentation3D>::New();
	handle2->GetProperty()->SetColor(1, 0, 0);

	dRep2->SetHandleRepresentation(handle2);
	dRep2->InstantiateHandleRepresentation();
	dRep2->RulerModeOn();
	dRep2->SetRulerDistance(0.25);
	dRep2->GetLineProperty()->SetColor(defaultMeasureColor);
	dRep2->GetLabelProperty()->SetColor(defaultMeasureColor);
	distanceWidget->SetInteractor(GetRenderWindow()->GetInteractor());
	distanceWidget->SetRepresentation(dRep2);
	vtkSmartPointer<vtkDistanceRepresentation > aux = (vtkDistanceRepresentation *)(distanceWidget->GetRepresentation());
	aux->SetLabelFormat("%-#6.5g mm");
	if (measureCallback != nullptr)
	{
		//dRep2->AddObserver(vtkSmartPointer<vtkCommand::PickEvent, measureCallback);
		distanceWidget->AddObserver(vtkCommand::StartInteractionEvent, measureCallback);
		distanceWidget->AddObserver(vtkCommand::InteractionEvent, measureCallback);
		distanceWidget->AddObserver(vtkWidgetEvent::Move, measureCallback);
		distanceWidget->AddObserver(vtkWidgetEvent::EndSelect, measureCallback);
		distanceWidget->AddObserver(vtkCommand::EndInteractionEvent, measureCallback);
	}
	GetRenderWindow()->Render();
	distanceWidget->EnabledOn();
	//medidasInsertadas->AddItem(distanceWidget);
	GetRenderWindow()->Render();

}

/*
* Alinea la medida con el eje global correspondiente
* el evento puede ser de la tecla X, Y o Z
*/
void MeasureHandler::alinearMedida() {
	if (distanceWidgetSeleccionado != nullptr) {
		vtkSmartPointer<vtkDistanceRepresentation3D> dRep2 = (vtkDistanceRepresentation3D*)distanceWidgetSeleccionado->GetDistanceRepresentation();
		if (InputManager::IsKeyPressed(Qt::Key_X)) {

			double* coordPoint1 = dRep2->GetPoint1WorldPosition(); //el punto 1 queda fijo, tengo que pedir el Y y el Z
			double* coordPoint2 = dRep2->GetPoint2WorldPosition();
			double aux[3] = { coordPoint2[0], coordPoint1[1] , coordPoint1[2] };
			dRep2->SetPoint2WorldPosition(aux);
			dRep2->GetLineProperty()->SetColor(1.0, 0.0, 0.0);
			GetRenderWindow()->Render();
		}
		else if (InputManager::IsKeyPressed(Qt::Key_Y)) {
			//ACA TENGO QUE MANEJAR EL WIDGET de distancia asi no toco nada en simple view solo mover el objeto
			double* coordPoint1 = dRep2->GetPoint1WorldPosition(); //el punto 1 queda fijo, tengo que pedir el Y y el Z
			double* coordPoint2 = dRep2->GetPoint2WorldPosition();
			double aux[3] = { coordPoint1[0], coordPoint2[1] , coordPoint1[2] };
			dRep2->SetPoint2WorldPosition(aux);
			dRep2->GetLineProperty()->SetColor(0.0, 1.0, 0.0);
			GetRenderWindow()->Render();
		}
		else if (InputManager::IsKeyPressed(Qt::Key_Z)) {
			double* coordPoint1 = dRep2->GetPoint1WorldPosition(); //el punto 1 queda fijo, tengo que pedir el Y y el Z
			double* coordPoint2 = dRep2->GetPoint2WorldPosition();
			double aux[3] = { coordPoint1[0], coordPoint1[1] , coordPoint2[2] };
			dRep2->SetPoint2WorldPosition(aux);
			dRep2->GetLineProperty()->SetColor(0.0, 0.0, 1.0);
			GetRenderWindow()->Render(); //cuando modifico algo debo llamar a esta funcion, es como el redibujar
										 //solo lo llamo aca porque solo con estas teclas redibujo
		}
	}
}

void MeasureHandler::restaurarColor() {
	if (GetMedidaSeleccionada() != nullptr) {
		vtkSmartPointer<vtkDistanceRepresentation3D> dRep2 = (vtkDistanceRepresentation3D*)GetMedidaSeleccionada()->GetDistanceRepresentation();
		dRep2->GetLineProperty()->SetColor(defaultMeasureColor);
		GetRenderWindow()->Render();
	}
}

void MeasureHandler::keyPress()
{
	if (GetMedidaSeleccionada() != nullptr) {
		if (!InputManager::IsKeyPressed(Qt::Key::Key_Control))
			alinearMedida();
		if (InputManager::IsKeyPressed(Qt::Key_Delete)) {
			
			GetMedidaSeleccionada()->Off();
			//medidasInsertadas->RemoveItem(GetMedidaSeleccionada());
			vtkSmartPointer<vtkDistanceWidget> distanceWidget = GetMedidaSeleccionada();
			vtkSmartPointer<vtkDistanceRepresentation> dRep2 = (vtkDistanceRepresentation*)distanceWidget->GetRepresentation();
			
			dRep2->Delete();
			distanceWidget->Delete();
			
			distanceWidgetSeleccionado = nullptr;
		}
	}
}

/*
* Setea una callback que se utiliza para retornar la ultima medida seleccionada
*/
void MeasureHandler::setCallback() {
	measureCallback = vtkSmartPointer<vtkCallbackCommand>::New();
	measureCallback->SetCallback(functionMeasureCallBack);
	
}
