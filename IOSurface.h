#pragma once

#include <vtkPolyData.h>
#include <vtkImageData.h>
#include <vtkVector.h>
#include <vtkPolyData.h>
#include <vtkCallbackCommand.h>
#include <vtkActor.h>
#include <string>


class IOSurface
{

public:
	IOSurface();
	~IOSurface();

	vtkSmartPointer<vtkPolyData> loadInputData(bool& successful, std::string pathToInputData);

	vtkSmartPointer<vtkActor> importObject(bool & load, std::string path);

	void exportObject(std::string direccion, vtkSmartPointer<vtkActor> actor);
	/**
	* Export the mesh in STL format.
	* @param mesh Mesh to export.
	* @param path Path to the exported stl file.
	*/
	void exportAsStlFile(vtkSmartPointer<vtkPolyData> mesh, std::string path);

	/**
	* Export the mesh in OBJ format.
	* @param mesh Mesh to export.
	* @param path Path to the exported obj file.
	*/
	void exportAsObjFile(vtkSmartPointer<vtkPolyData> mesh, std::string path);

	/**
	* Opens a obj file and returns a vtkSmartPointer<vtkPolyData mesh.
	* @param pathToObjFile Path to the obj file.
	* @return Resulting 3D mesh.
	*/
	vtkSmartPointer<vtkPolyData> importObjFile(std::string pathToObjFile);

	/**
	* Opens a stl file and returns a vtkSmartPointer<vtkPolyData mesh.
	* @param pathToStlFile Path to the stl file.
	* @return Resulting 3D mesh.
	*/
	vtkSmartPointer<vtkPolyData> importStlFile(std::string pathToStlFile);

	/**
	* Opens a ply file and returns a vtkSmartPointer<vtkPolyData mesh.
	* @param pathToPlyFile Path to the ply file.
	* @return Resulting 3D mesh.
	*/
	vtkSmartPointer<vtkPolyData> importPlyFile(std::string pathToPlyFile);

	/**
	* Export the mesh in PLY format.
	* @param mesh Mesh to export.
	* @param path Path to the exported ply file.
	*/
	void exportAsPlyFile(vtkSmartPointer<vtkPolyData> mesh,std::string path);

	/**
	* Compute the vertex normals of a mesh.
	* @param mesh The mesh, of which the vertex nomrals are computed.
	* @param normals Contains normals at return.
	*/
	static void computeVertexNormalsTrivial(vtkSmartPointer<vtkPolyData> mesh, std::vector<vtkVector3d>& normals);


};

