#pragma once

#define MyQt
#include <QVTKOpenGLWindow.h>
#include <QtWidgets/QSplitter>
#include <QMouseEvent>
#include <vtkActor.h>
#include <QOpenGLWindow>
#include <QWindow>
#include <QKeyEvent>
#include "CameraHandler.h"

#include <QtGui/QOpenGLContext.h>

#include <vtkGenericOpenGLRenderWindow.h>


#include "PlaneHandler.h"
#include "CameraHandler.h"

#include "CallbacksClass.h"

class CameraHandler;

//aca esta extendiendo a la clase qvtkopen, asi se extiende
class MyQVTKOpenGLWindow : public QVTKOpenGLWindow
{

	//Q_OBJECT



public:

	MyQVTKOpenGLWindow();

	MyQVTKOpenGLWindow(vtkGenericOpenGLRenderWindow* w,
		QOpenGLContext *shareContext = QOpenGLContext::currentContext(),
		UpdateBehavior updateBehavior = NoPartialUpdate,
		QWindow *parent = Q_NULLPTR);

	MyQVTKOpenGLWindow(QOpenGLContext *shareContext,
		UpdateBehavior updateBehavior = NoPartialUpdate,
		QWindow *parent = Q_NULLPTR);

	~MyQVTKOpenGLWindow();

	typedef QVTKOpenGLWindow Superclass;

	void MyQVTKOpenGLWindow::mousePressEvent(QMouseEvent *event) override;
	void MyQVTKOpenGLWindow::mouseMoveEvent(QMouseEvent *event) override;
	void MyQVTKOpenGLWindow::mouseReleaseEvent(QMouseEvent *event) override;

	void MyQVTKOpenGLWindow::keyPress(QKeyEvent *event);
	
	void MyQVTKOpenGLWindow::wheelEvent(QWheelEvent* e) override;

	CameraHandler* GetCameraHandler();

	void SetRenderWindow(vtkGenericOpenGLRenderWindow* ren) override;

private:

	CameraHandler * camera;
};

