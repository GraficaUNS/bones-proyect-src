#include "ListInteractions.h"
#include <qlistwidget.h>
#include <qstring.h>
#include <QListWidgetItem>
#include "Consola.h"
#include "MasterHandler.h"
#include "VolumeInteractions.h"
#include <qlist.h>
ListInteractions::ListInteractions(QWidget* parent) : QListWidget(parent)
{
	
}

ListInteractions::ListInteractions() : QListWidget()
{
	
}


ListInteractions::~ListInteractions()
{
}

void ListInteractions::addNewItem(int port, QString name)
{
	if (firstL) {
		firstL = false;
		connect(this, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(slotAddVolume(QListWidgetItem*)));
		connect(this, SIGNAL(itemChanged(QListWidgetItem*)), this, SLOT(slotChecked(QListWidgetItem*)));
		QListWidgetItem *item = new QListWidgetItem("(Vacio)", this);

		this->addItem(item);

		ItemtoPort.insert(item, 0);
	}
	QListWidgetItem *item = new QListWidgetItem(name, this);
	item->setFlags(item->flags() | Qt::ItemIsUserCheckable | Qt::ItemIsEditable);
	item->setCheckState(Qt::Unchecked);
	this->addItem(item);

	//itemCount++;

	ItemtoPort.insert(item, port);
}


void ListInteractions::deleteItemFromPort(int port)
{
	
	QListWidgetItem* item = ItemtoPort.key(port);
	
	Selected.removeOne(port);

	if (item != nullptr) {
		this->removeItemWidget(item);

		delete item;
	}
	
	
}

QList<int>* ListInteractions::GetSelectedPorts()
{
	int cant = Selected.size();
	return &Selected;
}

QString ListInteractions::GetItemName(int port)
{
	return ItemtoPort.key(port)->text();
}

void ListInteractions::slotChecked(QListWidgetItem * item)
{
	if (item->checkState() == Qt::Checked)
	{
		int port = ItemtoPort.value(item);
		Selected.push_front(port);
	}
	else 
	{
		int port = ItemtoPort.value(item);
		Selected.removeOne(port);
	}
}

void ListInteractions::slotAddVolume(QListWidgetItem* it) 
{
	int port = ItemtoPort.value(it);
	//Consola::appendln("slot");
	MasterHandler::GetSimpleView()->visibleProgressBar(true);
	MasterHandler::GetSimpleView()->progressChanged(10);

	MasterHandler::GetVolumeInteractor()->setVolume(port);
}

