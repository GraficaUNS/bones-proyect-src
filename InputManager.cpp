#include "InputManager.h"
#include "MasterHandler.h"


bool KeyControl = false;
bool KeyDelete = false;
bool Key_X = false;
bool Key_Y = false;
bool Key_Z = false;

InputManager::InputManager()
{
}

InputManager::~InputManager()
{
}

void InputManager::keyReleaseEvent(QKeyEvent * event)
{
	int key = event->key();

	bool b = false;

	switch (key) 
	{
	case Qt::Key::Key_Control:
		KeyControl = b;
		break;
	case Qt::Key::Key_Delete:
		KeyDelete = b;
		break;
	case Qt::Key_X:
		Key_X = b;
		break;
	case Qt::Key_Y:
		Key_Y = b;
		break;
	case Qt::Key_Z:
		Key_Z = b;
		break;
	}
	MasterHandler::masterKeyRelease(event);
}

void InputManager::keyPressEvent(QKeyEvent * event)
{
	int key = event->key();

	bool b = true;

	switch (key)
	{
	case Qt::Key::Key_Control:
		KeyControl = b;
		break;
	case Qt::Key::Key_Delete:
		KeyDelete = b;
		break;
	case Qt::Key_X:
		Key_X = b;
		break;
	case Qt::Key_Y:
		Key_Y = b;
		break;
	case Qt::Key_Z:
		Key_Z = b;
		break;
	}

	MasterHandler::masterKeyPress(event);
}

bool InputManager::IsKeyPressed(int keycode)
{

	int key = keycode;

	switch (key)
	{
	case Qt::Key::Key_Control:
		return KeyControl;
	case Qt::Key::Key_Delete:
		return KeyDelete;
	case Qt::Key_X:
		return Key_X;
	case Qt::Key_Y:
		return Key_Y;
	case Qt::Key_Z:
		return Key_Z;
	}

	return false;
}
