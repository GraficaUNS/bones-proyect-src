#include "PlaneHandler.h"
#include <vtkNew.h>
#include <vtkArrowSource.h>
#include <vtkAppendPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkTransformFilter.h>
#include <vtkTransform.h>
#include <vtkSmartPointer.h>
#include <vtkRegularPolygonSource.h>
#include <vtkActor.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkRendererCollection.h>
#include <vtkClipClosedSurface.h>
#include <vtkPlane.h>
#include <vtkPlaneCollection.h>
#include <vtkCommand.h>
#include <vtkMatrix4x4.h>

#include "Consola.h"
#include "CallbacksClass.h"
#include "SimpleView.h"
#include "MasterHandler.h"
#include "ThreadHandler.h"




//vars para el plano
//plano
vtkSmartPointer<vtkRegularPolygonSource> plane = vtkSmartPointer<vtkRegularPolygonSource>::New();
vtkSmartPointer<vtkActor> actorPlane = vtkSmartPointer<vtkActor>::New();

bool planeOn_h;



//matrices
vtkSmartPointer<vtkMatrix4x4> actorMeshMatrix_h = vtkSmartPointer<vtkMatrix4x4>::New();
vtkSmartPointer<vtkMatrix4x4> WordMatrix_h = vtkSmartPointer<vtkMatrix4x4>::New();
vtkSmartPointer<vtkMatrix4x4> WordNormalMatrix_h = vtkSmartPointer<vtkMatrix4x4>::New();
vtkSmartPointer<vtkMatrix4x4> WordNormalMatrix2_h = vtkSmartPointer<vtkMatrix4x4>::New();



PlaneHandler::PlaneHandler()
{
	inicializarPlano();

}


PlaneHandler::~PlaneHandler()
{
}


/*
* Getter for renderWindow
*/
vtkSmartPointer<vtkRenderWindow> PlaneHandler::GetRenderWindow() {
	return renderWindow;
}

/*
* Setter for RenderWindow
*/
void PlaneHandler::SetRenderWindow(vtkSmartPointer<vtkRenderWindow> ren) {
	renderWindow = ren;
}

vtkSmartPointer<vtkActor> PlaneHandler::GetActor() {
	if(planeOn_h)
		return actorPlane;
	else
		return nullptr;
}

void PlaneHandler::quitarPlano()
{
	planeOn_h = false;
	MasterHandler::GetThread()->lock_Actor();
	emit MasterHandler::GetSimpleView()->signal_RemoveActor(actorPlane);
	MasterHandler::GetThread()->lock_Actor();
	MasterHandler::GetThread()->unlock_Actor();
}

void PlaneHandler::inicializarPlano() {
	//SE PUEDE INICIALIZAR UNA VEZ
	vtkNew<vtkArrowSource> arrow;

	vtkNew<vtkPolyDataMapper> mapperPlane;
	vtkNew<vtkAppendPolyData> appendPlaneArrow;
	vtkSmartPointer<vtkTransformFilter> transformFilter = vtkSmartPointer<vtkTransformFilter>::New();

	vtkSmartPointer<vtkTransform> transform =
		vtkSmartPointer<vtkTransform>::New();

	plane->SetNumberOfSides(4);
	plane->SetRadius(350);
	plane->SetCenter(0, 0, 0);
	plane->SetNormal(1.0, 0.0, 0.0);
	plane->Update();

	transform->Scale(50, 50, 50);
	transform->RotateZ(180);
	transform->Update();

	arrow->SetShaftResolution(20);
	arrow->SetTipResolution(30);

	transformFilter->SetInputConnection(arrow->GetOutputPort());
	transformFilter->SetTransform(transform);


	appendPlaneArrow->AddInputData(plane->GetOutput());
	appendPlaneArrow->AddInputConnection(transformFilter->GetOutputPort());
	appendPlaneArrow->Update();

	mapperPlane->SetInputConnection(appendPlaneArrow->GetOutputPort());
	//MESH ACTOR

	actorPlane->SetMapper(mapperPlane);

	actorPlane->GetProperty()->SetColor(0.9, 0.37, 0.0);
	//actorPlane->GetProperty()->SetOpacity(0.7);
	//SE PUEDE INICIALIZAR UNA VEZ
}


void PlaneHandler::mostrarPlano() {
	planeOn_h = true;

	GetRenderWindow()->GetRenderers()->GetFirstRenderer()->AddActor(actorPlane);

	GetRenderWindow()->Render();
}

vtkSmartPointer<vtkRegularPolygonSource> PlaneHandler::getPlane()
{
	return plane;
}

