#include "Consola.h"

#include <QtWidgets/QTextBrowser>
#include <QTextDocument>
#include <QScrollBar>
#include <QLabel>
#include <QMutex>
#include <ctime>
QTextBrowser * cons;
QLabel* progLabel;
QMutex* labelMutex = new QMutex();

long tiempoActual = 0;
long offset = 300;


void Consola::setConsola(QTextBrowser * consola) {
	cons = consola;
}

void Consola::setProgLabel(QLabel * label)
{
	progLabel = label;
}

void Consola::append(double d) {
	auto textCursor = cons->textCursor();
	textCursor.movePosition(QTextCursor::End);
	cons->setTextCursor(textCursor);

	cons->insertPlainText(QString::number(d));

	cons->verticalScrollBar()->setValue(
		cons->verticalScrollBar()->maximum());
}

void Consola::append(int i) {
	auto textCursor = cons->textCursor();
	textCursor.movePosition(QTextCursor::End);
	cons->setTextCursor(textCursor);

	cons->insertPlainText(QString::number(i));

	cons->verticalScrollBar()->setValue(
		cons->verticalScrollBar()->maximum());
}


void Consola::append(char * string) {
	auto textCursor = cons->textCursor();
	textCursor.movePosition(QTextCursor::End);
	cons->setTextCursor(textCursor);
	cons->insertPlainText(string);

	cons->verticalScrollBar()->setValue(
		cons->verticalScrollBar()->maximum());
}

void Consola::append(std::string string) {
	auto textCursor = cons->textCursor();
	textCursor.movePosition(QTextCursor::End);
	cons->setTextCursor(textCursor);

	cons->insertPlainText(QString::fromStdString(string));

	cons->verticalScrollBar()->setValue(
		cons->verticalScrollBar()->maximum());
}



void Consola::appendln(double d) {
	auto textCursor = cons->textCursor();
	textCursor.movePosition(QTextCursor::End);
	cons->setTextCursor(textCursor);

	cons->insertPlainText(QString::number(d));
	cons->insertPlainText("\n");

	cons->verticalScrollBar()->setValue(
		cons->verticalScrollBar()->maximum());
}

void Consola::appendln(int i) {
	auto textCursor = cons->textCursor();
	textCursor.movePosition(QTextCursor::End);
	cons->setTextCursor(textCursor);

	cons->insertPlainText(QString::number(i));
	cons->insertPlainText("\n");

	cons->verticalScrollBar()->setValue(
		cons->verticalScrollBar()->maximum());
}


void Consola::appendln(char * string) {
	auto textCursor = cons->textCursor();
	textCursor.movePosition(QTextCursor::End);
	cons->setTextCursor(textCursor);
	cons->insertPlainText(string);
	cons->insertPlainText("\n");

	cons->verticalScrollBar()->setValue(
		cons->verticalScrollBar()->maximum());
}

void Consola::appendln(std::string string) {
	auto textCursor = cons->textCursor();
	textCursor.movePosition(QTextCursor::End);
	cons->setTextCursor(textCursor);

	cons->insertPlainText(QString::fromStdString(string));
	cons->insertPlainText("\n");

	cons->verticalScrollBar()->setValue(
		cons->verticalScrollBar()->maximum());
}

void Consola::appendln(QString string)
{
	auto textCursor = cons->textCursor();
	textCursor.movePosition(QTextCursor::End);
	cons->setTextCursor(textCursor);

	cons->insertPlainText(string);
	cons->insertPlainText("\n");

	cons->verticalScrollBar()->setValue(
		cons->verticalScrollBar()->maximum());
}

void Consola::clear() {
	cons->clear();
}


void Consola::writeLabel(QString string)
{
	labelMutex->lock();
	if (time(0) * 1000 - tiempoActual > offset) {
		progLabel->setText(string);
		tiempoActual = time(0) * 1000;
	}
	labelMutex->unlock();
}