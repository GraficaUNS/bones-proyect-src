#pragma once
#define dialog
#include <QWidget>
#include <QCoreApplication.h>
#include <QWidget.h>
#include <QMessageBox>
#include <QString>

class Dialog /*: public QWidget/**/ {
	//Q_OBJECT
public:

	//Dialog(QWidget* ap);
	static void setErrorMessage(QMessageBox* e);
	static void mensajeError(QString string);
	static void mensajeWarning(QString string);
	static void mensajeInfo(QString string);


};